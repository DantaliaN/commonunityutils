#if UNITY_EDITOR

using System.IO;
using ONiGames.Utilities.Editor;
using UnityEditor;

namespace ONiGames.CodeGenerators.Editor
{
    public static class TypeCreatorHelper
    {
        [MenuItem("ONiGames/Code Generators/TypeCreatorHelper/CreateType", priority = int.MinValue)]
        static void CreateType()
        {
            InputDialog.Show("Type Creator", "Enter type name", "Create", "Cancel", typeName =>
            {
                InputDialog.Show("Type Creator", $"Enter namespace name to {typeName}", "Create", "Cancel", namespaceName =>
                {
                    CreateType(namespaceName, typeName);
                }, () => { });
            }, () => { });
        }
        
        static void CreateType(string namespaceName, string typeName)
        {
            var namespacePath = namespaceName.Replace(".", "/");

            var typeFile = GetTypeTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{typeName}}", typeName)
                .Replace("{{namespacePath}}", namespacePath);
            
            var typeIdSettingsFile = GetTypeIdSettingsTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{typeName}}", typeName)
                .Replace("{{namespacePath}}", namespacePath);
            
            var typeIdFieldDrawerFile = GetTypeIdFieldDrawerTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{typeName}}", typeName)
                .Replace("{{namespacePath}}", namespacePath);
            
            var typeItemFieldDrawerFile = GetTypeItemFieldDrawerTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{typeName}}", typeName)
                .Replace("{{namespacePath}}", namespacePath);
            
            var directoryPath = Path.Combine("Assets", namespacePath.Replace(".", $"{Path.DirectorySeparatorChar}"), $"{typeName}Types");
            var directoryEditorPath = Path.Combine(directoryPath, "Editor");
            
            var typeFilePath = Path.Combine($"{typeName}Type.cs");
            var typeIdSettingsFilePath = Path.Combine($"{typeName}TypeIdSettings.cs");
            var typeIdFieldDrawerFilePath = Path.Combine($"{typeName}TypeIdFieldDrawer.cs");
            var typeItemFieldDrawerFilePath = Path.Combine($"{typeName}TypeItemFieldDrawer.cs");
            
            Directory.CreateDirectory(directoryPath);
            Directory.CreateDirectory(directoryEditorPath);
            
            File.WriteAllText(Path.Combine(directoryPath, typeFilePath), typeFile);
            File.WriteAllText(Path.Combine(directoryPath, typeIdSettingsFilePath), typeIdSettingsFile);
            File.WriteAllText(Path.Combine(directoryEditorPath, typeIdFieldDrawerFilePath), typeIdFieldDrawerFile);
            File.WriteAllText(Path.Combine(directoryEditorPath, typeItemFieldDrawerFilePath), typeItemFieldDrawerFile);
        }
        
        static string GetTypeTemplate()
        {
            return @"
using System;
using System.Linq;
using ONiGames.Adjectives.TypedUtils;
using Sirenix.OdinInspector;
using UnityEngine;

namespace {{namespaceName}}.{{typeName}}Types
{
    public class {{typeName}}Type : MonoBehaviour
    {
        [Serializable]
        public class Id : TypedId
        {
            public override string Name => {{typeName}}TypeIdSettings.Instance.Items.FirstOrDefault(o => o.uid == uid)?.name;
        }

        [Serializable]
        public class Item : TypedItem<Id>
        {
            protected override ValueDropdownList<Id> GetItems()
            {
                var values = new ValueDropdownList<Id>();

                foreach (var item in {{typeName}}TypeIdSettings.Instance.Items)
                    values.Add(item.name, new Id { uid = item.uid });

                return values;
            }
        }
    }
}
";
        }
        
        static string GetTypeIdSettingsTemplate()
        {
            return @"
using ONiGames.CustomProjectSettings.ExtendedUtilities;

namespace {{namespaceName}}.{{typeName}}Types
{
    public class {{typeName}}TypeIdSettings : IdSettings<{{typeName}}TypeIdSettings>
    {
#if UNITY_EDITOR
        [UnityEditor.MenuItem(""{{namespacePath}}/{{typeName}}Types/{{typeName}}TypeIdSettings"")]
        static void ED_Select()
        {
            Select();
        }
#endif
    }
}
";
        }
        
        static string GetTypeIdFieldDrawerTemplate()
        {
            return @"
#if UNITY_EDITOR

using System.Collections.Generic;
using ONiGames.Adjectives.TypedUtils.Editor;
using ONiGames.CustomProjectSettings.ExtendedUtilities;
using UnityEditor;

namespace {{namespaceName}}.{{typeName}}Types.Editor
{
    [CustomPropertyDrawer(typeof({{typeName}}Type.Id))]
    public class {{typeName}}TypeIdFieldDrawer : TypedIdFieldDrawer 
    {
        protected override IReadOnlyList<IdSettings.Id> Items => {{typeName}}TypeIdSettings.Instance.Items;
    }
}

#endif
";
        }
        
        static string GetTypeItemFieldDrawerTemplate()
        {
            return @"
#if UNITY_EDITOR

using JetBrains.Annotations;
using ONiGames.Adjectives.TypedUtils.Editor;

namespace {{namespaceName}}.{{typeName}}Types.Editor
{
    [UsedImplicitly]
    public class {{typeName}}TypeItemFieldDrawer : TypedItemFieldDrawer<{{typeName}}Type.Item> { }
}

#endif
";
        }
    }
}

#endif