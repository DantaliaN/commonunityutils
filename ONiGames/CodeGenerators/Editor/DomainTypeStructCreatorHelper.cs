#if UNITY_EDITOR

using System.IO;
using ONiGames.Utilities;
using ONiGames.Utilities.Editor;
using UnityEditor;

namespace ONiGames.CodeGenerators.Editor
{
    public static class DomainTypeStructCreatorHelper
    {
        [MenuItem("ONiGames/Code Generators/DomainTypeStructCreatorHelper/CreateDomainTypeStruct", priority = int.MinValue)]
        static void CreateDomainTypeStruct()
        {
            InputDialog.Show("Domain Type Struct Creator", "Enter Domain Type name", "Create", "Cancel", typeName =>
            {
                InputDialog.Show("Domain Type Struct Creator", $"Enter namespace to Domain Type", "Create", "Cancel", namespaceName =>
                {
                    CreateDomainTypeStruct(namespaceName, typeName);
                }, () => { });
            }, () => { });
        }
        
        static void CreateDomainTypeStruct(string namespaceName, string typeName)
        {
            var typeSmallName = typeName.ToLowerFirstChar();
            var namespacePath = namespaceName.Replace(".", "/");
            
            var iXableFile = GetIXableTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{namespacePath}}", namespacePath)
                .Replace("{{typeName}}", typeName)
                .Replace("{{typeSmallName}}", typeSmallName);
            
            var xFile = GetXTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{namespacePath}}", namespacePath)
                .Replace("{{typeName}}", typeName)
                .Replace("{{typeSmallName}}", typeSmallName);
            
            var xModelsDataFile = GetXModelsDataTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{namespacePath}}", namespacePath)
                .Replace("{{typeName}}", typeName)
                .Replace("{{typeSmallName}}", typeSmallName);
            
            var xDataFile = GetXDataTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{namespacePath}}", namespacePath)
                .Replace("{{typeName}}", typeName)
                .Replace("{{typeSmallName}}", typeSmallName);
            
            var xsDataFile = GetXsDataTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{namespacePath}}", namespacePath)
                .Replace("{{typeName}}", typeName)
                .Replace("{{typeSmallName}}", typeSmallName);
            
            var directoryPath = Path.Combine("Assets", namespaceName.Replace(".", $"{Path.DirectorySeparatorChar}"), "Domains", $"{typeName}s");
            var directoryDynamicsPath = Path.Combine(directoryPath, "Dynamics");
            var directoryDynamicsDataPath = Path.Combine(directoryDynamicsPath, "Data");
            var directoryDynamicsPrefabsPath = Path.Combine(directoryDynamicsPath, "Prefabs");
            var directoryStaticsPath = Path.Combine(directoryPath, "Statics");
            var directoryStaticsDataPath = Path.Combine(directoryStaticsPath, "Data");
            
            var iXableFilePath = Path.Combine($"I{typeName}able.cs");
            var xFilePath = Path.Combine($"{typeName}.cs");
            var xModelsDataFilePath = Path.Combine($"{typeName}ModelsData.cs");
            var xDataFilePath = Path.Combine($"{typeName}Data.cs");
            var xsDataFilePath = Path.Combine($"{typeName}sData.cs");
            
            Directory.CreateDirectory(directoryPath);
            Directory.CreateDirectory(directoryDynamicsPath);
            Directory.CreateDirectory(directoryDynamicsDataPath);
            Directory.CreateDirectory(directoryDynamicsPrefabsPath);
            Directory.CreateDirectory(directoryStaticsPath);
            Directory.CreateDirectory(directoryStaticsDataPath);
            
            File.WriteAllText(Path.Combine(directoryDynamicsPath, iXableFilePath), iXableFile);
            File.WriteAllText(Path.Combine(directoryDynamicsPath, xFilePath), xFile);
            File.WriteAllText(Path.Combine(directoryDynamicsPath, xModelsDataFilePath), xModelsDataFile);
            File.WriteAllText(Path.Combine(directoryStaticsPath, xDataFilePath), xDataFile);
            File.WriteAllText(Path.Combine(directoryStaticsPath, xsDataFilePath), xsDataFile);
        }
        
        static string GetIXableTemplate()
        {
            return @"
using {{namespaceName}}.Domains.{{typeName}}s.Statics;
using JetBrains.Annotations;

namespace {{namespaceName}}.Domains.{{typeName}}s.Dynamics
{
    public interface I{{typeName}}able
    {
        [CanBeNull]
        {{typeName}}sData {{typeName}}sData { get; }
        
        [CanBeNull]
        IPlayerable Player { get; }
    }
}
";
        }
        
        static string GetXTemplate()
        {
            return @"
using {{namespaceName}}.Domains.{{typeName}}s.Statics;
using {{namespaceName}}.Saves;
using {{namespaceName}}.Types.{{typeName}}Types;
using JetBrains.Annotations;
using ONiGames.Asserts;
using Sirenix.OdinInspector;
using UnityEngine;

namespace {{namespaceName}}.Domains.{{typeName}}s.Dynamics
{
    public class {{typeName}} : MonoBehaviour
    {
        [CanBeNull]
        [ShowInInspector, ReadOnly]
        public I{{typeName}}able Owner { private get; set; }
        
        [NotNull]
        [ShowInInspector, ReadOnly]
        public {{typeName}}Type.Item {{typeName}}TypeItem { get; set; } = new();

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        public bool TryGetData(out {{typeName}}Data {{typeSmallName}}Data)
        {
            if (Owner == null || Owner.{{typeName}}sData == null)
            {
                {{typeSmallName}}Data = null;
                return false;
            }
            
            return Owner.{{typeName}}sData.TryGetData({{typeName}}TypeItem.id, out {{typeSmallName}}Data);
        }

        bool TryGetSave(out SavePlayer.Save{{typeName}} save{{typeName}})
        {
            if (Owner == null || Owner.Player == null)
            {
                save{{typeName}} = null;
                return false;
            }

            save{{typeName}} = Owner.Player.Save.Player.{{typeName}}s[{{typeName}}TypeItem];
            return true;
        }
    }
}
";
        }
        
        static string GetXModelsDataTemplate()
        {
            return @"
using System;
using System.Linq;
using {{namespaceName}}.Domains.{{typeName}}s.Statics;
using {{namespaceName}}.Types.{{typeName}}Types;
using JetBrains.Annotations;
using ONiGames.Asserts.Attributes;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace {{namespaceName}}.Domains.{{typeName}}s.Dynamics
{
    [CreateAssetMenu(menuName = ""{{namespaceName}}/Domains/{{typeName}}s/Dynamics/{{typeName}}ModelsData"")]
    public class {{typeName}}ModelsData : ScriptableObject
    {   
        [Serializable]
        public class Item
        {
            [SerializeField]
            {{typeName}}Type.Id id = new();

            [InlineEditor(InlineEditorObjectFieldModes.Foldout, Expanded = false)]
            [CanBeNull]
            [Prefab]
            [PrefabField]
            [SerializeField]
            {{typeName}} prefab;

            public {{typeName}}Type.Id Id => id;

            [CanBeNull]
            public {{typeName}} Prefab => prefab;

            internal {{typeName}}Type.Id _id { set => id = value; }

#if UNITY_EDITOR
            [InlineEditor(InlineEditorObjectFieldModes.Foldout, Expanded = false)]
            [CanBeNull]
            [ShowInInspector, ReadOnly]
            {{typeName}}Data Data
            {
                get
                {
                    var {{typeSmallName}}sData = ONiGames.Utilities.Editor.UtilsEditor.FindAssetInInspector<{{typeName}}sData>();
                    if ({{typeSmallName}}sData == null)
                        return null;

                    if (!{{typeSmallName}}sData.TryGetData(id, out var {{typeSmallName}}Data))
                        return null;

                    return {{typeSmallName}}Data;
                }
            }
#endif
        }

        [ListDrawerSettings]
        [SerializeField]
        // [Searchable]
        Item[] items = new Item[0];

        void OnValidate()
        {
            var types = {{typeName}}TypeIdSettings.Instance.Items;
            items = items.DistinctBy(o => o.Id.uid).Where(o => types.Any(oo => o.Id.uid == oo.uid)).Concat(types.Where(o => items.All(oo => o.uid != oo.Id.uid)).Select(o => new Item { _id = new {{typeName}}Type.Id { uid = o.uid } })).ToArray();
        }

        public bool TryGetPrefab({{typeName}}Type.Id id, out {{typeName}} prefab)
        {
            prefab = items.FirstOrDefault(o => o.Id == id)?.Prefab;

            Insist.IsNull(prefab, () => $""{name}: Can't find prefab for {id}"", LogType.Warning);

            return prefab != null;
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem(""{{namespaceName}}/Domains/{{typeName}}s/Dynamic/{{typeName}}ModelsData"")]
        static void ED_SelectConfig()
        {
            UnityEditor.Selection.activeObject = ONiGames.Utilities.Editor.UtilsEditor.FindAssetInInspector<{{typeName}}ModelsData>();
        }

        [PropertySpace]
        [Button]
        void SelectData()
        {
            UnityEditor.Selection.activeObject = ONiGames.Utilities.Editor.UtilsEditor.FindAssetInInspector<{{typeName}}sData>();
        }

        [PropertySpace]
        [Button]
        void SelectIdSettings()
        {
            UnityEditor.Selection.activeObject = {{typeName}}TypeIdSettings.Instance;
        }
#endif
    }
}
";
        }
        
        static string GetXDataTemplate()
        {
            return @"
using ONiGames.Adjectives.Localization.Attributes;
using UnityEngine;

namespace {{namespaceName}}.Domains.{{typeName}}s.Statics
{
    [CreateAssetMenu(menuName = ""{{namespaceName}}/Domains/{{typeName}}s/Statics/{{typeName}}Data"")]
    public class {{typeName}}Data : ScriptableObject
    {
        [LocalizationListPopupField]
        [SerializeField]
        string nameKey = string.Empty;
        
        public string NameKey => nameKey;
    }
}
";
        }
        
        static string GetXsDataTemplate()
        {
            return @"
using System;
using System.Linq;
using {{namespaceName}}.Types.{{typeName}}Types;
using JetBrains.Annotations;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;

namespace {{namespaceName}}.Domains.{{typeName}}s.Statics
{
    [CreateAssetMenu(menuName = ""{{namespaceName}}/Domains/{{typeName}}s/Statics/{{typeName}}sData"")]
    public class {{typeName}}sData : ScriptableObject
    {   
        [Serializable]
        public class Item
        {
            [SerializeField]
            {{typeName}}Type.Id id = new();

            [InlineEditor(InlineEditorObjectFieldModes.Foldout, Expanded = false)]
            [CanBeNull]
            [SerializeField]
            {{typeName}}Data data;

            public {{typeName}}Type.Id Id => id;

            [CanBeNull]
            public {{typeName}}Data Data => data;

            internal {{typeName}}Type.Id _id { set => id = value; }
        }

        [ListDrawerSettings]
        [SerializeField]
        // [Searchable]
        Item[] items = new Item[0];

        void OnValidate()
        {
            var types = {{typeName}}TypeIdSettings.Instance.Items;
            items = items.DistinctBy(o => o.Id.uid).Where(o => types.Any(oo => o.Id.uid == oo.uid)).Concat(types.Where(o => items.All(oo => o.uid != oo.Id.uid)).Select(o => new Item { _id = new {{typeName}}Type.Id { uid = o.uid } })).ToArray();
        }

        public bool TryGetData({{typeName}}Type.Id id, out {{typeName}}Data data)
        {
            data = items.FirstOrDefault(o => o.Id == id)?.Data;

            Insist.IsNull(data, () => $""{name}: Can't find data for {id}"", LogType.Warning);

            return data != null;
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem(""{{namespaceName}}/Domains/{{typeName}}s/Statics/{{typeName}}sData"")]
        static void ED_SelectConfig()
        {
            UnityEditor.Selection.activeObject = ONiGames.Utilities.Editor.UtilsEditor.FindAssetInInspector<{{typeName}}sData>();
        }

        [PropertySpace]
        [Button]
        void SelectIdSettings()
        {
            UnityEditor.Selection.activeObject = {{typeName}}TypeIdSettings.Instance;
        }
#endif
    }
}
";
        }
    }
}

#endif