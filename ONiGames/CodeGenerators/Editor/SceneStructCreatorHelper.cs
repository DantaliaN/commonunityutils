#if UNITY_EDITOR

using System.IO;
using ONiGames.Utilities;
using ONiGames.Utilities.Editor;
using UnityEditor;

namespace ONiGames.CodeGenerators.Editor
{
    public static class SceneStructCreatorHelper
    {
        [MenuItem("ONiGames/Code Generators/SceneStructCreatorHelper/CreateSceneStruct", priority = int.MinValue)]
        static void CreateSceneStruct()
        {
            InputDialog.Show("Scene Struct Creator", "Enter scene name", "Create", "Cancel", sceneName =>
            {
                InputDialog.Show("Scene Struct Creator", $"Enter namespace to Scene", "Create", "Cancel", namespaceName =>
                {
                    CreateSceneStruct(namespaceName, sceneName);
                }, () => { });
            }, () => { });
        }
        
        static void CreateSceneStruct(string namespaceName, string sceneName)
        {
            var sceneSmallName = sceneName.ToLowerFirstChar();
            
            var controllerFile = GetControllerTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{sceneName}}", sceneName)
                .Replace("{{sceneSmallName}}", sceneSmallName);
            
            var playerControllerFile = GetPlayerControllerTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{sceneName}}", sceneName)
                .Replace("{{sceneSmallName}}", sceneSmallName);
            
            var hubFile = GetHubTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{sceneName}}", sceneName)
                .Replace("{{sceneSmallName}}", sceneSmallName);
            
            var iControllableFile = GetIControllableTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{sceneName}}", sceneName)
                .Replace("{{sceneSmallName}}", sceneSmallName);
            
            var uiFile = GetUITemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{sceneName}}", sceneName)
                .Replace("{{sceneSmallName}}", sceneSmallName);
            
            var iModelableFile = GetIModelableTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{sceneName}}", sceneName)
                .Replace("{{sceneSmallName}}", sceneSmallName);
            
            var uiWindowFile = GetUIWindowTemplate()
                .Replace("{{namespaceName}}", namespaceName)
                .Replace("{{sceneName}}", sceneName)
                .Replace("{{sceneSmallName}}", sceneSmallName);
            
            var directoryPath = Path.Combine("Assets", namespaceName.Replace(".", $"{Path.DirectorySeparatorChar}"), $"{sceneName}");
            var directoryModelsPath = Path.Combine(directoryPath, "Models");
            var directoryUIPath = Path.Combine(directoryPath, "UI");
            var directoryUIWindowPath = Path.Combine(directoryUIPath, $"{sceneName}Window");
            var directoryUIWindowAtlasesPath = Path.Combine(directoryUIWindowPath, "Atlases");
            var directoryUIWindowPrefabsPath = Path.Combine(directoryUIWindowPath, "Prefabs");
            var directoryUIWindowSpritesPath = Path.Combine(directoryUIWindowPath, "Sprites");
            
            var controllerFilePath = Path.Combine($"{sceneName}Controller.cs");
            var playerControllerFilePath = Path.Combine("PlayerController.cs");
            var hubFilePath = Path.Combine($"{sceneName}Hub.cs");
            var iControllableFilePath = Path.Combine($"I{sceneName}Controllable.cs");
            var uiFilePath = Path.Combine($"UI{sceneName}.cs");
            var iModelableFilePath = Path.Combine($"I{sceneName}Modelable.cs");
            var uiWindowFilePath = Path.Combine($"UI{sceneName}Window.cs");
            
            Directory.CreateDirectory(directoryPath);
            Directory.CreateDirectory(directoryModelsPath);
            Directory.CreateDirectory(directoryUIPath);
            Directory.CreateDirectory(directoryUIWindowPath);
            Directory.CreateDirectory(directoryUIWindowAtlasesPath);
            Directory.CreateDirectory(directoryUIWindowPrefabsPath);
            Directory.CreateDirectory(directoryUIWindowSpritesPath);
            
            File.WriteAllText(Path.Combine(directoryPath, controllerFilePath), controllerFile);
            File.WriteAllText(Path.Combine(directoryPath, playerControllerFilePath), playerControllerFile);
            File.WriteAllText(Path.Combine(directoryModelsPath, hubFilePath), hubFile);
            File.WriteAllText(Path.Combine(directoryModelsPath, iControllableFilePath), iControllableFile);
            File.WriteAllText(Path.Combine(directoryUIPath, uiFilePath), uiFile);
            File.WriteAllText(Path.Combine(directoryUIPath, iModelableFilePath), iModelableFile);
            File.WriteAllText(Path.Combine(directoryUIWindowPath, uiWindowFilePath), uiWindowFile);
        }
        
        static string GetHubTemplate()
        {
            return @"
using JetBrains.Annotations;
using ONiGames.Asserts;
using Sirenix.OdinInspector;
using UnityEngine;

namespace {{namespaceName}}.{{sceneName}}.Models
{
    public class {{sceneName}}Hub : MonoBehaviour
    {
        [CanBeNull]
        [ShowInInspector, ReadOnly]
        public I{{sceneName}}Controllable Controller { private get; set; }
        
        void OnValidate()
        {
            if (name.Contains(""#""))
                return;
            
            name = $""[{GetType().Name}]"";
        }
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
    }
}
";
        }
        
        static string GetIControllableTemplate()
        {
            return @"
namespace {{namespaceName}}.{{sceneName}}.Models
{
    public interface I{{sceneName}}Controllable
    {
        
    }
}
";
        }
        
        static string GetUIWindowTemplate()
        {
            return @"
using {{namespaceName}}.{{sceneName}}.Models;
using ONiGames.Adjectives.Utilities.UI.Windows;
using ONiGames.Asserts;

namespace {{namespaceName}}.{{sceneName}}.UI.{{sceneName}}Window
{
    public class UI{{sceneName}}Window : ASimpleWindow<{{sceneName}}Hub>
    {
        void Awake()
        {
            Assert.SerializedFields(this);
        }
    }
}
";
        }
        
        static string GetIModelableTemplate()
        {
            return @"
using {{namespaceName}}.{{sceneName}}.Models;

namespace {{namespaceName}}.{{sceneName}}.UI
{
    public interface I{{sceneName}}Modelable
    {
        {{sceneName}}Hub {{sceneName}}Hub { get; }
    }
}
";
        }
        
        static string GetUITemplate()
        {
            return @"
using {{namespaceName}}.{{sceneName}}.UI.{{sceneName}}Window;
using JetBrains.Annotations;
using ONiGames.Asserts;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace {{namespaceName}}.{{sceneName}}.UI
{
    public class UI{{sceneName}} : MonoBehaviour
    {
        [AutoInjectField]
        [SerializeField]
        UI{{sceneName}}Window {{sceneSmallName}}Window;
        
        [CanBeNull]
        [ShowInInspector, ReadOnly]
        public I{{sceneName}}Modelable Controller { private get; set; }
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
        
        public void Show()
        {
            if (Insist.IsNull(Controller, $""{gameObject.GetPath()}: {nameof(Controller)} not set"", LogType.Error))
                return;
            
            if (Insist.IsNull(Controller.{{sceneName}}Hub, $""{gameObject.GetPath()}: {nameof(Controller.{{sceneName}}Hub)} not set"", LogType.Error))
                return;
            
            {{sceneSmallName}}Window.Show(Controller.{{sceneName}}Hub);
        }
    }
}
";
        }
        
        static string GetControllerTemplate()
        {
            return @"
using System.Collections;
using {{namespaceName}}.{{sceneName}}.Models;
using {{namespaceName}}.{{sceneName}}.UI;
using ONiGames.Asserts;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace {{namespaceName}}.{{sceneName}}
{
    public class {{sceneName}}Controller : MonoBehaviour, I{{sceneName}}Controllable, I{{sceneName}}Modelable
    {
        [AutoInjectField]
        [SerializeField]
        PlayerController player;
        
        [AutoInjectField]
        [SerializeField]
        {{sceneName}}Hub {{sceneSmallName}}Hub;
        
        [AutoInjectField]
        [SerializeField]
        UI{{sceneName}} {{sceneSmallName}};
        
        public {{sceneName}}Hub {{sceneName}}Hub => {{sceneSmallName}}Hub;
        
        void OnValidate()
        {
            if (name.Contains(""#""))
                return;
            
            name = $""[{GetType().Name}]"";
        }
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
        
        IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();
            
            {{sceneSmallName}}Hub.Controller = this;
            
            {{sceneSmallName}}.Controller = this;
            {{sceneSmallName}}.Show();
        }
    }
}
";
        }
        
        static string GetPlayerControllerTemplate()
        {
            return @"
using {{namespaceName}}.Managers;
using {{namespaceName}}.Saves;
using ONiGames.Asserts;
using ONiGames.Utilities.CoreTypes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace {{namespaceName}}.{{sceneName}}
{
    public class PlayerController : MonoBehaviour
    {
        [ShowInInspector, ReadOnly]
        public SerializedVersion SaveVersion => PlayerPreferences.SaveVersion.Get();
        
        [ShowInInspector, ReadOnly]
        public SerializedDateTime SaveTimestamp => PlayerPreferences.SaveTimestamp.Get();
        
        [ShowInInspector, ReadOnly]
        public Save Save => PlayerPreferences.Save.Get();
        
        void OnValidate()
        {
            if (name.Contains(""#""))
                return;
            
            name = $""[{GetType().Name}]"";
        }
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
        
        void OnDestroy()
        {
            PlayerPreferences.Apply();
        }
        
        void OnApplicationQuit()
        {
            PlayerPreferences.Apply();
        }
        
        void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
                return;
            
            PlayerPreferences.Apply();
        }
        
        void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
                return;
            
            PlayerPreferences.Apply();
        }
    }
}
";
        }
    }
}

#endif