#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ONiGames.Utilities;
using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace ONiGames.Tools.Editor
{
    public static class UtilsEditor
    {
        [MenuItem("ONiGames/Tools/Capture Screenshot", priority = int.MinValue)]
        static void CaptureScreenshotMenu()
        {
            var fileName = $"Screenshot_{DateTime.UtcNow:yyyy-MM-dd-hh-mm-ss}.png";
            var filePath = Path.Combine("..", fileName);

            ScreenCapture.CaptureScreenshot(filePath);

            var path = Path.Combine(Application.dataPath, "..");
            EditorUtility.RevealInFinder(path);
        }

        [MenuItem("ONiGames/Tools/Resave Prefabs From Path")]
        static void ResavePrefabsFromPathMenu()
        {
            InputDialog.Show("Resave prefabs from path", "Path from assets:", "Resave", "Cancel", s =>
            {
                if (s.IsNullOrEmpty())
                    return;

                try
                {
                    var guids = AssetDatabase.FindAssets("t:prefab");

                    var prefabs = guids.Select(AssetDatabase.GUIDToAssetPath).Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToList();

                    var pathedPrefabs = prefabs.Where(o => AssetDatabase.GetAssetPath(o.GetInstanceID()).StartsWith(s));

                    foreach (var prefab in pathedPrefabs)
                    {
                        var path = AssetDatabase.GetAssetPath(prefab.GetInstanceID());
                        var go = PrefabUtility.LoadPrefabContents(path);

                        PrefabUtility.SaveAsPrefabAsset(go, path);

                        PrefabUtility.UnloadPrefabContents(go);
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex);
                }
            }, () => { });
        }

        [MenuItem("ONiGames/Tools/Remove Item In All Scenes")]
        static void RemoveItemInAllScenes()
        {
            InputDialog.Show("Remove item from all scenes", "Item name", "Ok", "Cancel", s =>
            {
                if (string.IsNullOrEmpty(s))
                    return;

                if (EditorUtility.DisplayDialog("Are you sure?", $"Remove all items with name '{s}'", "Ok", "Cancel"))
                {
                    var currentScenePaths = new List<string>();

                    for (var i = 0; i < SceneManager.sceneCount; i++)
                        currentScenePaths.Add(SceneManager.GetSceneAt(i).path);

                    for (var i = 0; i < SceneManager.sceneCount; i++)
                        EditorSceneManager.CloseScene(SceneManager.GetSceneAt(i), true);

                    var scenesGUIDs = AssetDatabase.FindAssets("t:Scene");
                    var scenesPaths = scenesGUIDs.Select(AssetDatabase.GUIDToAssetPath).Where(o => o.StartsWith("Assets/"));

                    foreach (var path in scenesPaths)
                    {
                        var scene = EditorSceneManager.OpenScene(path);

                        var found = false;
                        
                        foreach (var go in scene.FindGameObjects(true))
                        {
                            if (go.name != s)
                                continue;
                            
                            found = true;
                            Object.DestroyImmediate(go);
                        }

                        if (found)
                            EditorSceneManager.SaveScene(scene);
                        
                        EditorSceneManager.CloseScene(scene, true);
                    }
                    
                    foreach (var currentScenePath in currentScenePaths)
                        EditorSceneManager.OpenScene(currentScenePath);
                }
            }, () => { });
        }
        
        [MenuItem("ONiGames/Tools/Select GameObjects With Missing Scripts")]
        static void SelectGameObjectsWithMissingScripts()
        {
            var objectsWithDeadLinks = new List<Object>();
            
            for (var i = 0; i < SceneManager.sceneCount; i++)
            {
                var currentScene = SceneManager.GetSceneAt(i);
                var rootObjects = currentScene.GetRootGameObjects();

                foreach (var g in rootObjects)
                {
                    var components = g.GetComponentsInChildren<Component>();
                    foreach (var currentComponent in components)
                    {
                        if (currentComponent != null)
                            continue;

                        objectsWithDeadLinks.Add(g);
                        Selection.activeGameObject = g;
                        break;
                    }
                }
            }

            if (objectsWithDeadLinks.Count > 0)
                Selection.objects = objectsWithDeadLinks.ToArray();
            else
                Debug.Log("No GameObjects With Missing Scripts");
        }
        
        [MenuItem("ONiGames/Tools/Select Prefabs With Missing Scripts")]
        static void SelectPrefabsWithMissingScripts()
        {
            var objectsWithDeadLinks = new List<Object>();
            
            var prefabs = Utilities.Editor.UtilsEditor.LoadAllPrefabs();
            
            foreach (var g in prefabs)
            {
                var components = g.GetComponentsInChildren<Component>();
                foreach (var currentComponent in components)
                {
                    if (currentComponent != null)
                        continue;

                    objectsWithDeadLinks.Add(g);
                    Selection.activeGameObject = g;
                    break;
                }
            }

            if (objectsWithDeadLinks.Count > 0)
                Selection.objects = objectsWithDeadLinks.ToArray();
            else
                Debug.Log("No GameObjects With Missing Scripts");
        }
    }
}

#endif