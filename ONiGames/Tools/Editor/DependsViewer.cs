#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Tools.Editor
{
    public class DependsViewer : EditorWindow
    {
        class Item
        {
            [NotNull]
            public string path = string.Empty;

            public List<string> parents = new();
        }

        enum SortingDirection
        {
            Ascending,
            Descending
        };

        static bool IsBuildInProgress => mBuildingEnumerator != null;
        static int BuildListProgress { get; set; } = 0;
        static int BuildListTotal { get; set; } = 0;

        static readonly Dictionary<string, List<string>> DependsOn = new();

        static IEnumerator mBuildingEnumerator;

        [CanBeNull]
        Object SelectedObject { get; set; } = null;

        SortingDirection mSortingDirection = SortingDirection.Ascending;

        Vector2 mDependsScrollPosition = Vector2.zero;

        bool mWasBuilding = false;

        [MenuItem("ONiGames/Tools/Depends Viewer/Open", priority = 500)]
        static void InitFromMainMenu()
        {
            BeginBuildReferencesMap();

            var window = GetWindow<DependsViewer>();
            window.titleContent = new GUIContent("Depends Viewer", "Used to display dependencies between assets");
            window.SelectedObject = Selection.activeObject;
            window.Show();
        }

        void Update()
        {
            if (mBuildingEnumerator != null)
            {
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                while (sw.Elapsed.TotalMilliseconds < 1000.0f / 50.0f)
                {
                    var moved = mBuildingEnumerator.MoveNext();
                    if (!moved)
                    {
                        mBuildingEnumerator = null;
                        // force repaint to ensure correct counters
                        EditorApplication.RepaintProjectWindow();
                        break;
                    }
                }

                sw.Stop();
            }

            if (IsBuildInProgress)
            {
                EditorUtility.DisplayProgressBar("Depends Viewer", $"Building Lookup Tables... ({BuildListProgress}/{BuildListTotal})", (float) BuildListProgress / BuildListTotal);

                mWasBuilding = true;
            }
            else if (mWasBuilding)
            {
                EditorUtility.ClearProgressBar();
                mWasBuilding = false;
            }
        }

        void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            {
                SelectedObject = EditorGUILayout.ObjectField("Selected Object:", SelectedObject, typeof(Object), false);

                EditorGUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button($"Sorting by name: {(mSortingDirection == SortingDirection.Ascending ? "a..z" : "z..a")}"))
                    {
                        mSortingDirection = mSortingDirection == SortingDirection.Ascending ? SortingDirection.Descending : SortingDirection.Ascending;
                    }
                }
                EditorGUILayout.EndHorizontal();

                if (SelectedObject != null)
                {
                    GUILayout.Space(10);

                    if (!IsBuildInProgress)
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.BeginVertical();
                            {
                                GUILayout.Label("All Depends on Assets:");
                                GUILayout.Space(10);

                                mDependsScrollPosition = EditorGUILayout.BeginScrollView(mDependsScrollPosition);
                                {
                                    var dependsItems = new List<Item>();

                                    var assetPath = AssetDatabase.GetAssetPath(SelectedObject);

                                    GetAllDepends(new Item { path = assetPath, parents = new List<string>() }, dependsItems);

                                    if (mSortingDirection == SortingDirection.Ascending)
                                    {
                                        foreach (var item in dependsItems.OrderBy(o => o.path.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar).LastOrDefault()))
                                            DrawReferenceBlock(item);
                                    }
                                    else
                                    {
                                        foreach (var item in dependsItems.OrderByDescending(o => o.path.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar).LastOrDefault()))
                                            DrawReferenceBlock(item);
                                    }

                                    void GetAllDepends(Item path, ICollection<Item> items)
                                    {
                                        if (!DependsOn.TryGetValue(path.path, out var array))
                                            return;

                                        foreach (var item in array.Where(o => items.All(oo => oo.path != o)))
                                        {
                                            var i = new Item { path = item, parents = path.parents.ToList() };
                                            i.parents.Insert(0, item);

                                            items.Add(i);

                                            GetAllDepends(i, items);
                                        }
                                    }

                                    void DrawReferenceBlock(Item item)
                                    {
                                        EditorGUILayout.BeginHorizontal();
                                        {
                                            foreach (var parentPath in item.parents)
                                            {
                                                var parentAsset = AssetDatabase.LoadAssetAtPath<Object>(parentPath);
                                                EditorGUILayout.ObjectField(parentAsset, typeof(Object), false, GUILayout.Width(250));
                                            }
                                        }
                                        EditorGUILayout.EndHorizontal();
                                    }
                                }
                                EditorGUILayout.EndScrollView();
                            }
                            EditorGUILayout.EndVertical();
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
            EditorGUILayout.EndVertical();
        }

        static void BeginBuildReferencesMap()
        {
            if (!IsBuildInProgress)
                mBuildingEnumerator = BuildingFunc(AssetDatabase.GetAllAssetPaths());
        }

        static IEnumerator BuildingFunc(IReadOnlyList<string> allAssets)
        {
            DependsOn.Clear();

            BuildListTotal = allAssets.Count;

            for (var i = 0; i < allAssets.Count; ++i)
            {
                var deps = AssetDatabase.GetDependencies(allAssets[i], false);

                DependsOn[allAssets[i]] = deps.ToList();

                BuildListProgress = i;

                yield return null;
            }
        }
    }
}

#endif