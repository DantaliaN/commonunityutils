﻿#if UNITY_EDITOR

using System;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Tools.Editor
{
    public class RemoveEmptyFolders
    {
        [UsedImplicitly]
        [MenuItem("ONiGames/Tools/Remove empty folders/Check")]
        static void Check()
        {
            Remove(true);
        }

        [UsedImplicitly]
        [MenuItem("ONiGames/Tools/Remove empty folders/Delete")]
        static void Delete()
        {
            Remove(false);
        }

        public static void Remove(bool dryRun)
        {
            Remove(Application.dataPath, dryRun);
        }

        public static void Remove(string path, bool dryRun = false)
        {
            if (!Directory.Exists(path))
            {
                Debug.LogError($"Directory {path} not exist");
                return;
            }

            var index = path.IndexOf("/Assets", StringComparison.Ordinal);
            var projectSubfolders = Directory.GetDirectories(path, "*", SearchOption.AllDirectories).ToList();

            projectSubfolders.Add(path);

            // Create a list of all the empty subfolders under Assets.
            var emptyFolders = projectSubfolders.Where(IsEmptyRecursive).ToArray();

            foreach (var folder in emptyFolders)
            {
                // Verify that the folder exists (may have been already removed).
                if (Directory.Exists(folder))
                {
                    Debug.Log($"Will be deleted: {folder.Substring(index + 1)}");

                    if (!dryRun)
                    {
                        // Remove dir (recursively)
                        Directory.Delete(folder, true);

                        var metaPath = $"{folder}.meta";
                        if (File.Exists(metaPath))
                            File.Delete(metaPath);

                        // Sync AssetDatabase with the delete operation.
                        AssetDatabase.DeleteAsset(folder.Substring(index + 1));
                    }
                }
            }

            // Refresh the asset database once we're done.
            AssetDatabase.Refresh();
        }

        /// <summary>
        /// A helper method for determining if a folder is empty or not.
        /// </summary>
        public static bool IsEmptyRecursive(string path)
        {
            // A folder is empty if it (and all its subdirs) have no files (ignore .meta files)
            return !Directory.GetFiles(path).Select(file => !file.EndsWith(".meta")).Any() && Directory.GetDirectories(path, string.Empty, SearchOption.AllDirectories).All(IsEmptyRecursive);
        }
    }
}

#endif