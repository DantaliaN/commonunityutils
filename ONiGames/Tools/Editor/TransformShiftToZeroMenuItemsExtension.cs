﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Tools.Editor
{
    public class MenuItemsExtension
    {
        [MenuItem("CONTEXT/Transform/Shift to Zero")]
        public static void TransformShiftToZero(MenuCommand menuCommand)
        {
            var transform = menuCommand.context as Transform;
            if (transform == null)
                return;

            var records = new List<Object> { transform };
            foreach (Transform child in transform)
            {
                records.Add(child);
            }

            Undo.RecordObjects(records.ToArray(), "Shift to Zero");

            var localPosition = transform.localPosition;
            var localRotation = transform.localRotation;
            var localScale = transform.localScale;

            foreach (Transform child in transform)
            {
                child.localPosition = Vector3.Scale(localRotation * child.localPosition, localScale) + localPosition;
                child.localRotation = localRotation * child.localRotation;
                child.localScale = Vector3.Scale(child.localScale, localScale);
            }

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }
    }
}

#endif