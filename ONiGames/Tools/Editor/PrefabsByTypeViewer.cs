#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Utilities;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ONiGames.Tools.Editor
{
    public class PrefabsByTypeViewer : EditorWindow
    {
        readonly List<GameObject> prefabs = new();

        string searchingType = string.Empty;
        Vector2 scrollPos = Vector2.zero;
        
        [UsedImplicitly]
        [MenuItem("ONiGames/Tools/Prefabs By Type Viewer")]
        static void CreateReplaceWithPrefab()
        {
            GetWindow<PrefabsByTypeViewer>();
        }

        void OnGUI()
        {
            searchingType = EditorGUILayout.TextField("Type:", searchingType);

            GUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Find"))
            {
                if (searchingType.IsNullOrEmpty())
                    return;
                
                prefabs.Clear();
                
                var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(o => o.GetTypes()).Where(o => string.Equals(o.Name, searchingType, StringComparison.CurrentCultureIgnoreCase));
                foreach (var type in types)
                    prefabs.AddRange(Utilities.Editor.UtilsEditor.PrefabsByType(type).Select(o => o.gameObject));
            }

            if (GUILayout.Button("Select"))
            {
                Selection.objects = prefabs.Select(o => (Object) o).ToArray();
            }
            
            GUILayout.EndHorizontal();

            EditorGUILayout.LabelField($"Founded: {prefabs.Count}:");

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            
            foreach (var go in prefabs)
                EditorGUILayout.ObjectField(go, typeof(GameObject), false);

            EditorGUILayout.EndScrollView();
        }
    }
}

#endif