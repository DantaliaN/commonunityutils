#if UNITY_EDITOR && UNITY_ANDROID

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Tools.Editor
{
    public static class AndroidSymbolsShrinker
    {
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Local")]
        class ProcessResult
        {
            internal int ExitCode { get; }
            internal string StdOut { get; }
            internal string StdErr { get; }

            internal bool Failure => ExitCode != 0;

            internal ProcessResult(int exitCode, string stdOut, string stdErr)
            {
                ExitCode = exitCode;
                StdOut = stdOut;
                StdErr = stdErr;
            }

            public override string ToString()
            {
                return $"Exit Code: {ExitCode}\nStdOut:\n{StdOut}\nStdErr:\n{StdErr}";
            }
        }

        [SuppressMessage("ReSharper", "UnusedMember.Local")]
        enum Compression7Zip
        {
            NoCompression = 0,
            Fastest = 1,
            Fast = 3,
            Normal = 5,
            Maximum = 7,
            Ultra = 9
        }

        public static void ShrinkSymbols(string location)
        {
            if (string.IsNullOrEmpty(location))
            {
                LogError($"{nameof(AndroidSymbolsShrinker)}: Selected location is null");
                return;
            }

            var unityProcess = Process.GetCurrentProcess();
            var unityExePath = unityProcess.MainModule?.FileName ?? string.Empty;
            var unityPath = Path.GetDirectoryName(unityExePath) ?? string.Empty;

            var app7ZFullPath = Path.Combine(unityPath, "Data", "Tools", "7z");
            if (Application.platform == RuntimePlatform.WindowsEditor)
                app7ZFullPath += ".exe";

            if (!File.Exists(app7ZFullPath))
            {
                LogError($"Failed to locate {app7ZFullPath}");
                return;
            }

            EditorUtility.DisplayProgressBar("Shrinking symbols", "Unzip symbol files", 0f);

            var targetDirectory = Path.GetDirectoryName(location) ?? string.Empty;
            var intermediatePath = Path.Combine(targetDirectory, "TempShrink");

            Cleanup(intermediatePath);

            var unzipResult = RunProcess(targetDirectory, app7ZFullPath, $"x -o\"{intermediatePath}\" \"{location}\"");
            if (unzipResult.Failure)
            {
                LogError(unzipResult.ToString());

                EditorUtility.ClearProgressBar();
                return;
            }

            EditorUtility.DisplayProgressBar("Shrinking symbols", "Renaming symbol files", 0.3f);

            const string symSo = ".sym.so";

            var files = Directory.GetFiles(intermediatePath, "*.*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                if (file.EndsWith(".dbg.so"))
                    Cleanup(file);

                if (file.EndsWith(symSo))
                {
                    var fileSo = file.Substring(0, file.Length - symSo.Length) + ".so";

                    Log($"Rename {file} --> {fileSo}");

                    try
                    {
                        File.Move(file, fileSo);
                    }
                    catch (Exception ex)
                    {
                        LogError(ex.ToString());

                        EditorUtility.ClearProgressBar();
                        return;
                    }
                }
            }

            EditorUtility.DisplayProgressBar("Shrinking symbols", "Zip symbol files", 0.5f);

            var newZipFilePath = Path.Combine(targetDirectory, Path.GetFileNameWithoutExtension(location) + ".shrank.zip");

            Cleanup(newZipFilePath);

            const Compression7Zip compressionType = Compression7Zip.Ultra;
            const int passNumber = 1; // [1; 15] - bigger is more compression, but slower. for other arguments check Tools/7z/MANUAL/start.htm
            var compressionArgs = $"a -tzip -mm=Deflate -mx={(int) compressionType} -mfb=257 -mpass={passNumber} -mmt -r \"{newZipFilePath}\"";

            var zipResult = RunProcess(intermediatePath, app7ZFullPath, compressionArgs);

            if (zipResult.Failure)
            {
                LogError(zipResult.ToString());

                EditorUtility.ClearProgressBar();
                return;
            }

            Cleanup(intermediatePath);

            EditorUtility.ClearProgressBar();

            var resultZipFileInfo = new FileInfo(newZipFilePath);
            var resultZipFileMb = (double) resultZipFileInfo.Length / 1024 / 1024;

            if (resultZipFileMb > 299.9999d)
                LogWarning("Android debug symbols too big to be uploaded to GooglePlay (must be up to 300 MB). Try remove unused code or increase compression ratio (will increase build time)");
            else if (resultZipFileMb > 290d)
                LogWarning($"Android debug symbols were shrank to {resultZipFileMb} MB. It is too close to limit of 300 MB for GooglePlay");

            Log($"The new shrank symbols: {newZipFilePath} ({resultZipFileMb} MB)");
        }

        static ProcessResult RunProcess(string workingDirectory, string fileName, string args)
        {
            Log($"Executing {fileName} {args} (Working Directory: {workingDirectory}");

            var process = new Process();
            process.StartInfo.FileName = fileName;
            process.StartInfo.Arguments = args;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.WorkingDirectory = workingDirectory;
            process.StartInfo.CreateNoWindow = true;

            var output = new StringBuilder();

            process.OutputDataReceived += (_, e) =>
            {
                if (!string.IsNullOrEmpty(e.Data))
                    output.AppendLine(e.Data);
            };

            var error = new StringBuilder();

            process.ErrorDataReceived += (_, e) =>
            {
                if (!string.IsNullOrEmpty(e.Data))
                    error.AppendLine(e.Data);
            };

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();

            Log($"{fileName} exited with {process.ExitCode}");

            return new ProcessResult(process.ExitCode, output.ToString(), error.ToString());
        }

        static void Cleanup(string path)
        {
            if (Directory.Exists(path))
            {
                Log($"Delete {path}");
                Directory.Delete(path, true);
            }

            if (File.Exists(path))
            {
                Log($"Delete {path}");
                File.Delete(path);
            }
        }

        static void Log(string message)
        {
            UnityEngine.Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, null, message);
        }

        static void LogWarning(string message)
        {
            UnityEngine.Debug.LogFormat(LogType.Warning, LogOption.NoStacktrace, null, message);
        }

        static void LogError(string message)
        {
            UnityEngine.Debug.LogFormat(LogType.Error, LogOption.NoStacktrace, null, message);
        }

        [MenuItem("ONiGames/Tools/Android Symbols/Shrink")]
        static void ED_ShrinkSymbols()
        {
            const string lastSymbolToShrinkLocationSaveKey = nameof(lastSymbolToShrinkLocationSaveKey);

            var location = EditorPrefs.GetString(lastSymbolToShrinkLocationSaveKey, Path.Combine(Application.dataPath, ".."));

            EditorPrefs.SetString(lastSymbolToShrinkLocationSaveKey, location);

            location = EditorUtility.OpenFilePanel("Open Android Symbol Package to shrink", location, "*.zip");

            ShrinkSymbols(location);
        }
    }
}

#endif