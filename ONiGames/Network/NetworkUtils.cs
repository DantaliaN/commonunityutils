﻿using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace ONiGames.Network
{
    static class SocketExtensions
    {
        public static bool IsAlive(this Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the keep-alive interval for the socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="time">Time between two keep alive "pings".</param>
        /// <param name="interval">Time between two keep alive "pings" when first one fails.</param>
        /// <returns>If the keep alive infos were succefully modified.</returns>
        public static bool SetKeepAlive(this Socket socket, ulong time, ulong interval)
        {
            try
            {
                //the native structure
                //struct tcp_keepalive {
                //  ULONG onoff;
                //  ULONG keepalivetime;
                //  ULONG keepaliveinterval;
                //};

                var size = Marshal.SizeOf(new ulong());
                var inOptionValues = new byte[size * 3];

                var onOff = time == 0 || interval == 0 ? 0ul : 1ul;

                BitConverter.GetBytes(onOff).CopyTo(inOptionValues, size * 0);
                BitConverter.GetBytes(time).CopyTo(inOptionValues, size * 1);
                BitConverter.GetBytes(interval).CopyTo(inOptionValues, size * 2);

                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);

                // Write SIO_VALS to Socket IOControl.
                socket.IOControl(IOControlCode.KeepAliveValues, inOptionValues, null);
            }
            catch (SocketException)
            {
                return false;
            }

            return true;
        }
    }
}