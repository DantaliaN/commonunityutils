﻿using System;
using System.Net;
using ONiGames.Network.Protocol;
using UnityEngine;

namespace ONiGames.Network
{
    public delegate void ClientEventHandler();

    public delegate void ClientEventHandler<T>(ClientEventArgs<T> e) where T : ServerMessage;

    public class ClientEventArgs<T> : EventArgs where T : ServerMessage
    {
        public T Message { get; }

        public ClientEventArgs(T message)
        {
            Message = message;
        }

        public override string ToString()
        {
            return $"{nameof(GetType)}: {Message}";
        }
    }

    public abstract class ClientController : MonoBehaviour
    {
        public event ClientEventHandler OnConnect = delegate { };
        public event ClientEventHandler OnDisconnect = delegate { };

        public bool autoReconnect = true;

        public float reconnectDelay = 5f;

        public bool IsConnected => client.IsConnected;

        public int BufferSize
        {
            get => client.bufferSize;
            set => client.bufferSize = value;
        }

        readonly TcpClientAdapter client = new();

        IPEndPoint endPoint = null;

        float currentTime = 0f;

        void OnEnable()
        {
            client.OnError += OnError;
            client.OnConnect += OnServerConnect;
            client.OnDisconnect += OnServerDisconnect;
            client.OnResponse += OnServerResponse;
        }

        void OnDisable()
        {
            client.OnError -= OnError;
            client.OnConnect -= OnServerConnect;
            client.OnDisconnect -= OnServerDisconnect;
            client.OnResponse -= OnServerResponse;
        }

        void Update()
        {
            if (endPoint != null && !IsConnected)
            {
                currentTime += Time.deltaTime;

                if (autoReconnect && currentTime > reconnectDelay)
                {
                    currentTime = 0f;
                    client.Connect(endPoint);
                }
            }
        }

        void FixedUpdate()
        {
            client.Update(Time.fixedDeltaTime);
        }

        void OnDestroy()
        {
            Disconnect();
        }

        public void Connect(string ip, int port)
        {
            endPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            client.Connect(endPoint);
        }

        public void Disconnect()
        {
            client.Disconnect();
        }

        protected abstract void Response(ServerMessage message);

        protected void Send(ClientMessage clientMessage)
        {
            var message = new NetworkMessage(clientMessage);
            client.Send(message.Serialize());
        }

        void OnError(TcpClientErrorEventArgs e)
        {
            //Debug.LogError($"{name} OnErrorReceived {e.Exception}");
        }

        void OnServerConnect()
        {
            Debug.Log($"{name} OnConnect");
            OnConnect.Invoke();
        }

        void OnServerDisconnect()
        {
            //     Debug.Log($"{name} OnDisconnect");
            OnDisconnect.Invoke();
        }

        void OnServerResponse(TcpClientResponseEventArgs e)
        {
            //Debug.Log($"{name} OnDataArrived");

            try
            {
                var message = NetworkMessage.Deserialize(e.Data);

                Response((ServerMessage) message.Message);
            }
            catch (Exception ex)
            {
                Debug.LogError($"{name} Can't serialize {ex} {e.Data}");
            }
        }
    }
}