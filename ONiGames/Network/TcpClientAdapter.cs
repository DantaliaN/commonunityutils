﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using ONiGames.Utilities;

namespace ONiGames.Network
{
    public delegate void TcpClientConnectEventHandler();

    public delegate void TcpClientDisconnectEventHandler();

    public delegate void TcpClientResponseEventHandler(TcpClientResponseEventArgs e);

    public delegate void TcpClientErrorEventHandler(TcpClientErrorEventArgs e);

    public class TcpClientResponseEventArgs : EventArgs
    {
        public byte[] Data { get; }

        public TcpClientResponseEventArgs(byte[] data)
        {
            Data = data;
        }
    }

    public class TcpClientErrorEventArgs : EventArgs
    {
        public Exception Exception { get; }

        public TcpClientErrorEventArgs(Exception exception)
        {
            Exception = exception;
        }

        public TcpClientErrorEventArgs(string message) : this(new Exception(message)) { }

        public override string ToString()
        {
            return $"{nameof(GetType)}: {Exception}";
        }
    }

    public class TcpClientAdapter
    {
        public event TcpClientConnectEventHandler OnConnect = delegate { };
        public event TcpClientDisconnectEventHandler OnDisconnect = delegate { };
        public event TcpClientResponseEventHandler OnResponse = delegate { };
        public event TcpClientErrorEventHandler OnError = delegate { };

        public int bufferSize = 1024 * 32;

        public float checkConnectionTime = 1f;

        const ulong KEEP_ALIVE_TIME = 2000;
        const ulong KEEP_ALIVE_INTERVAL = 1000;

        //обходим потоки
        readonly object locker = new();
        readonly Queue<Action> threadSafeCommand = new();

        byte[] receiveBuffer = new byte[0];

        Socket socket;

        float checkConnectionTimer = 0f;

        public void Connect(EndPoint endPoint)
        {
            socket?.Close();

            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.NoDelay = true;

            socket.SetKeepAlive(KEEP_ALIVE_TIME, KEEP_ALIVE_INTERVAL);

            //socket.ReceiveBufferSize = BufferSize;
            //socket.SendBufferSize = BufferSize;

            try
            {
                var socketEventArg = new SocketAsyncEventArgs();
                socketEventArg.Completed += OnSocketEventCompleted;

                socketEventArg.RemoteEndPoint = endPoint;

                var willRaiseEvent = socket.ConnectAsync(socketEventArg);
                if (!willRaiseEvent)
                    ProcessConnect(socketEventArg);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        public bool IsConnected => socket != null && socket.Connected && socket.IsAlive();

        public void Update(float deltaTime)
        {
            checkConnectionTimer += deltaTime;
            if (checkConnectionTimer > checkConnectionTime)
            {
                checkConnectionTimer = 0f;

                if (socket != null)
                {
                    if (!socket.IsAlive())
                    {
                        socket.Close();
                        socket = null;

                        OnDisconnect.Invoke();
                    }
                }
            }

            lock (locker)
            {
                if (threadSafeCommand.Count > 0)
                {
                    while (threadSafeCommand.Count > 0)
                        threadSafeCommand.Dequeue()();
                }
            }
        }

        public void Disconnect()
        {
            if (!IsConnected)
                return;

            try
            {
                var socketEventArg = new SocketAsyncEventArgs();
                socketEventArg.Completed += OnSocketEventCompleted;

                var willRaiseEvent = socket.DisconnectAsync(socketEventArg);
                if (!willRaiseEvent)
                    ProcessDisconnect(socketEventArg);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        public void Send(byte[] data)
        {
            if (!IsConnected)
                return;

            try
            {
                var socketEventArg = new SocketAsyncEventArgs();
                socketEventArg.Completed += OnSocketEventCompleted;

                socketEventArg.SetBuffer(data, 0, data.Length);

                var willRaiseEvent = socket.SendAsync(socketEventArg);
                if (!willRaiseEvent)
                    ProcessSend(socketEventArg);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        void Error(Exception ex)
        {
            OnError.Invoke(new TcpClientErrorEventArgs(ex));
        }

        #region Thread

        void ThreadAction(Action a)
        {
            lock (locker)
            {
                threadSafeCommand.Enqueue(a);
            }
        }

        void ThreadError(Exception ex)
        {
            lock (locker)
            {
                threadSafeCommand.Enqueue(() => Error(ex));
            }
        }

        void Receive()
        {
            try
            {
                var receive = new byte[bufferSize];

                var socketEventArg = new SocketAsyncEventArgs();
                socketEventArg.Completed += OnSocketEventCompleted;

                socketEventArg.SetBuffer(receive, 0, receive.Length);

                var willRaiseEvent = socket.ReceiveAsync(socketEventArg);
                if (!willRaiseEvent)
                    ProcessReceive(socketEventArg);
            }
            catch (Exception ex)
            {
                ThreadError(ex);
            }
        }

        /// <summary>
        /// A single callback is used for all socket operations. This method forwards execution on to the correct handler 
        /// based on the type of completed operation
        /// </summary>
        void OnSocketEventCompleted(object sender, SocketAsyncEventArgs e)
        {
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Connect:
                    ProcessConnect(e);
                    break;
                case SocketAsyncOperation.Receive:
                    ProcessReceive(e);
                    break;
                case SocketAsyncOperation.Send:
                    ProcessSend(e);
                    break;
                case SocketAsyncOperation.Disconnect:
                    ProcessDisconnect(e);
                    break;
                default:
                    ThreadError(new Exception($"Invalid operation completed {e.LastOperation}"));
                    return;
            }
        }

        /// <summary>
        /// Called when a ConnectAsync operation completes
        /// </summary>
        void ProcessConnect(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                ThreadAction(() => OnConnect.Invoke());

                Receive();
            }
            else
            {
                ThreadError(new SocketException((int) e.SocketError));
            }

            e.Dispose();
        }

        /// <summary>
        /// Called when a ReceiveAsync operation completes
        /// </summary>
        void ProcessReceive(SocketAsyncEventArgs e)
        {
            if (!IsConnected)
                return;

            Receive();

            if (e.SocketError == SocketError.Success)
            {
                //wait other data
                receiveBuffer = Utils.Combine(receiveBuffer, Utils.Crop(e.Buffer, e.BytesTransferred));

                if (socket.Available == 0)
                {
                    var received = Utils.Combine(receiveBuffer);
                    receiveBuffer = new byte[0];

                    ThreadAction(() => OnResponse.Invoke(new TcpClientResponseEventArgs(received)));
                }
            }
            else
            {
                ThreadError(new SocketException((int) e.SocketError));
            }

            e.Dispose();
        }

        /// <summary>
        /// Called when a SendAsync operation completes
        /// </summary>
        void ProcessSend(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                ThreadAction(() => { });
            }
            else
            {
                ThreadError(new SocketException((int) e.SocketError));
            }

            e.Dispose();
        }

        void ProcessDisconnect(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                var s = e.UserToken as Socket ?? socket;
                s.Close();

                ThreadAction(() => OnDisconnect.Invoke());
            }
            else
            {
                ThreadError(new SocketException((int) e.SocketError));
            }

            e.Dispose();
        }

        #endregion
    }
}