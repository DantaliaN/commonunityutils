﻿using System;

namespace ONiGames.Network.Protocol
{
    [Serializable]
    public class NetworkMessage
    {
        public AMessage Message { get; }

        public NetworkMessage(AMessage message)
        {
            Message = message;
        }

        public static NetworkMessage Deserialize(byte[] data)
        {
            return Utilities.Serialization.Utils.Deserialize<NetworkMessage>(data);
        }

        public byte[] Serialize()
        {
            return Utilities.Serialization.Utils.SerializeToBytes(this);
        }
    }

    [Serializable]
    public abstract class AMessage
    {
        public abstract string GetName();
    }

    [Serializable]
    public abstract class ServerMessage : AMessage { }

    [Serializable]
    public abstract class ClientMessage : AMessage { }
}