#if UNITY_EDITOR

using System;
using System.Linq;
using System.Text.RegularExpressions;
using ONiGames.Utilities;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace ONiGames.Simplifications.Editor
{
    public class ONiGamesSimplificationBuildProcessor : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        public int callbackOrder { get; } = int.MaxValue;

        void IPreprocessBuildWithReport.OnPreprocessBuild(BuildReport report)
        {
            try
            {
                PlayerSettings.SplashScreen.showUnityLogo = false;
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{typeof(ONiGamesSimplificationBuildProcessor)}: {ex}");
            }

#if UNITY_IOS
            RemoveFmodIl2CppArgs();
#endif

            var buildSettings = new UtilsEditor.BuildSettings();

#if UNITY_ANDROID
            buildSettings.bundleNumber = $"{PlayerSettings.Android.bundleVersionCode}";
#elif UNITY_IOS
            buildSettings.bundleNumber = PlayerSettings.iOS.buildNumber;
#endif
            
            Utils.SaveResource(buildSettings);
            
            var addressableSettings = new UtilsEditor.AddressableSettings();
            
            var settings = AddressableAssetSettingsDefaultObject.Settings;
            if (settings != null)
            {
                foreach (var entry in settings.groups.SelectMany(o => o.entries))
                    addressableSettings.AssetReferences[entry.guid] = entry.address;
            }
                
            Utils.SaveResource(addressableSettings);
        }

        void IPostprocessBuildWithReport.OnPostprocessBuild(BuildReport report)
        {
            RemoveFmodIl2CppArgs();
            
            Utils.RemoveResource<UtilsEditor.BuildSettings>();
            Utils.RemoveResource<UtilsEditor.AddressableSettings>();
            
/*
#if UNITY_IOS
            try
            {
                var projPath = UnityEditor.iOS.Xcode.PBXProject.GetPBXProjectPath(report.summary.outputPath);

                var project = new UnityEditor.iOS.Xcode.PBXProject();
                project.ReadFromFile(projPath);

                var mainTargetGuid = project.GetUnityMainTargetGuid();

                project.SetBuildProperty(mainTargetGuid, "DEFINES_MODULE", "YES");
                project.SetBuildProperty(mainTargetGuid, "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "NO");
                project.SetBuildProperty(project.GetUnityFrameworkTargetGuid(), "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "NO");

                const string entitlementsContent = @"
<?xml version=""1.0"" encoding=""UTF-8""?>
<!DOCTYPE plist PUBLIC ""-//Apple//DTD PLIST 1.0//EN"" ""https://www.apple.com/DTDs/PropertyList-1.0.dtd"">
  <plist version = ""1.0"">
   <dict>
       <key>aps-environment</key>
       <string>development</string>
   </dict>
</plist>";
 
                var entitlementsPath = Path.Combine(report.summary.outputPath, $"Unity-iPhone/{Application.productName}.entitlements");
                
                File.WriteAllText(entitlementsPath, entitlementsContent);
                
                project.AddCapability(mainTargetGuid, UnityEditor.iOS.Xcode.PBXCapabilityType.PushNotifications, entitlementsPath);
                
                project.WriteToFile(projPath);
            }
            catch (Exception ex)
            {
                Debug.LogError($"{typeof(FlappyLandBuildProcessor)}: {ex}");
            }

            var workspaceSettingsPath = Path.Combine(report.summary.outputPath, "Unity-iPhone.xcodeproj/project.xcworkspace/xcshareddata/WorkspaceSettings.xcsettings");
            if (File.Exists(workspaceSettingsPath))
            {
                var workspaceSettings = new UnityEditor.iOS.Xcode.PlistDocument();
                workspaceSettings.ReadFromFile(workspaceSettingsPath);

                var root = workspaceSettings.root;
                    
                if (root.values.ContainsKey("BuildSystemType"))
                    root.values.Remove("BuildSystemType");
                    
                workspaceSettings.WriteToFile(workspaceSettingsPath);
            }
            
            var fbSDKMessengerIconPath = Path.Combine(report.summary.outputPath, "Pods/FBSDKShareKit/FBSDKShareKit/FBSDKShareKit/Internal/FBSDKMessengerIcon.h");
            if (File.Exists(fbSDKMessengerIconPath))
            {
                var fbSDKMessengerIcon = File.ReadAllText(fbSDKMessengerIconPath);

                fbSDKMessengerIcon = fbSDKMessengerIcon.Replace(@"#import ""FBSDKCoreKitImport.h""", "#import <FBSDKCoreKit/FBSDKCoreKit+Internal.h>");
                
                File.Delete(fbSDKMessengerIconPath); //b/c readonly

                File.WriteAllText(fbSDKMessengerIconPath, fbSDKMessengerIcon);
            }
#endif
*/
        }

        static void RemoveFmodIl2CppArgs()
        {
            var additionalIl2CppArgs = PlayerSettings.GetAdditionalIl2CppArgs();

            additionalIl2CppArgs = Regex.Replace(additionalIl2CppArgs, "--additional-cpp=\"([^\"]*fmod_register_static_plugins.cpp)\"", string.Empty);
            additionalIl2CppArgs = Regex.Replace(additionalIl2CppArgs, "--additional-cpp=\"([^\"]*fmod_static_plugin_support.h)\"", string.Empty);

            PlayerSettings.SetAdditionalIl2CppArgs(additionalIl2CppArgs.Trim());
        }
    }
}

#endif