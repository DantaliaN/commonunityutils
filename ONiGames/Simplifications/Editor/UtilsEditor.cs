using System;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Utilities.CoreTypes;
using UnityEngine;

namespace ONiGames.Simplifications.Editor
{
    public static class UtilsEditor
    {
        [Serializable]
        public class BuildSettings
        {
            [NotNull]
            public string bundleNumber = string.Empty;
        }

        [Serializable]
        public class AddressableSettings
        {
            [SerializeField]
            SerializedDictionary<string, string> assetReferences = new();

            public SerializedDictionary<string, string> AssetReferences
            {
                get
                {
#if UNITY_EDITOR
                    var settings = UnityEditor.AddressableAssets.AddressableAssetSettingsDefaultObject.Settings;
                    if (settings != null)
                    {
                        foreach (var entry in settings.groups.SelectMany(o => o.entries))
                            assetReferences[entry.guid] = entry.address;
                    }
#endif

                    return assetReferences;
                }
            }
        }
    }
}