using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Simplifications.Migrations
{
    public abstract class AMigration : MonoBehaviour
    {
        [ShowInInspector]
        string MigrationVersion => Version.ToString();
        
        public abstract Version Version { get; }

        public abstract string Migrate(string save);
    }
}