﻿using JetBrains.Annotations;
using ONiGames.Utilities;
using RuntimeInspectorNamespace;
using UnityEngine;

namespace ONiGames.Simplifications.Managers
{
    public class RuntimeInspectorManager : ASingletonBehaviour<RuntimeInspectorManager>
    {
        [CanBeNull]
        [SerializeField]
        RuntimeHierarchy runtimeHierarchy;
        
        [CanBeNull]
        [SerializeField]
        RuntimeInspector runtimeInspector;

        [SerializeField]
        bool isActive = false;

        void Update()
        {
            if (runtimeHierarchy != null)
                runtimeHierarchy.gameObject.SetActive(isActive);
            
            if (runtimeInspector != null)
                runtimeInspector.gameObject.SetActive(isActive);
        }

        public void Switch()
        {
            isActive = !isActive;
        }
    }
}
