﻿using System;
using Balaso;
using JetBrains.Annotations;
using ONiGames.Utilities;
using UnityEngine;

namespace ONiGames.Simplifications.Managers
{
    public class AppTrackingTransparencyController : MonoBehaviour
    {
        public enum AuthorizationStatus
        {
            NotDetermined,
            Restricted,
            Denied,
            Authorized
        };

        [SerializeField]
        int conversionCount = 3;

        public AuthorizationStatus Status => GetStatus(AppTrackingTransparency.TrackingAuthorizationStatus);
        
        [CanBeNull]
        Action<AuthorizationStatus> complete = null;
        
        void OnValidate()
        {
            if (name.Contains("#"))
                return;
            
            name = $"[{GetType().Name}]";
        }
        
        void Start()
        {
            AppTrackingTransparency.RegisterAppForAdNetworkAttribution();
            AppTrackingTransparency.UpdateConversionValue(conversionCount);
        }

        public void Show(Action<AuthorizationStatus> onComplete)
        {
            if (PlayAs.IsIOS)
            {
                var currentStatus = AppTrackingTransparency.TrackingAuthorizationStatus;
                if (currentStatus == AppTrackingTransparency.AuthorizationStatus.AUTHORIZED)
                {
                    onComplete(GetStatus(currentStatus));
                    return;
                }
                
                complete = onComplete;
                
                Debug.Log($"{gameObject.GetPath()}: Requesting authorization...");
                
                AppTrackingTransparency.RequestTrackingAuthorization();
            }
            else
            {
                complete = onComplete;
                
                complete?.Invoke(GetStatus(AppTrackingTransparency.AuthorizationStatus.NOT_DETERMINED));
            }
        } 

        void OnEnable()
        {
            AppTrackingTransparency.OnAuthorizationRequestDone += OnAuthorizationRequestDone;
        }

        void OnDisable()
        {
            AppTrackingTransparency.OnAuthorizationRequestDone -= OnAuthorizationRequestDone;
        }
        
        void OnAuthorizationRequestDone(AppTrackingTransparency.AuthorizationStatus status)
        {
            Debug.Log($"{gameObject.GetPath()}: IDFA: {AppTrackingTransparency.IdentifierForAdvertising()}");

            complete?.Invoke(GetStatus(status));
        }
        
        static AuthorizationStatus GetStatus(AppTrackingTransparency.AuthorizationStatus attStatus)
        {
            return attStatus switch
            {
                AppTrackingTransparency.AuthorizationStatus.NOT_DETERMINED => AuthorizationStatus.NotDetermined,
                AppTrackingTransparency.AuthorizationStatus.RESTRICTED => AuthorizationStatus.Restricted,
                AppTrackingTransparency.AuthorizationStatus.DENIED => AuthorizationStatus.Denied,
                AppTrackingTransparency.AuthorizationStatus.AUTHORIZED => AuthorizationStatus.Authorized,
                _ => AuthorizationStatus.NotDetermined
            };
        } 
    }
}