﻿using System;
using System.Collections;
using System.Collections.Generic;
using ONiGames.Utilities.CoreTypes;
using UnityEngine;

namespace ONiGames.Utilities.Tools
{
    [Serializable]
    public class SaveItems<TKey, TData> : IEnumerable<KeyValuePair<TKey, TData>> where TData : new()
    {
        [Serializable]
        public class Items : SerializedDictionary<TKey, TData>
        {
            public Items() { }
            public Items(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        [SerializeField]
        Items items = new();

        public TData this[TKey key]
        {
            get => Get(key);
            set => Set(key, value);
        }

        public TData Get(TKey item)
        {
            if (items.TryGetValue(item, out var value))
                return value;

            value = new TData();
            items.Add(item, value);

            return value;
        }

        public void Set(TKey item, TData data)
        {
            if (items.ContainsKey(item))
                items[item] = data;
            else
                items.Add(item, data);
        }

        public void Remove(TKey item)
        {
            if (items.ContainsKey(item))
                items.Remove(item);
        }

        public bool TryFindKey(Func<TData, bool> predicate, out TKey key)
        {
            return items.TryFindKey(predicate, out key);
        }

        public void Clear() => items.Clear();

        public IEnumerator<KeyValuePair<TKey, TData>> GetEnumerator() => items.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
    
    [Serializable]
    public class JsonableSaveItems<TKey, TData> : IEnumerable<KeyValuePair<TKey, TData>>, ISerializationCallbackReceiver where TData : new()
    {
        [Serializable]
        public class Items : SerializedDictionary<TKey, TData>
        {
            public Items() { }
            public Items(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        [NonSerialized]
        Items items = new();

        [SerializeField, HideInInspector]
        string itemsData = string.Empty;
        
        public TData this[TKey key]
        {
            get => Get(key);
            set => Set(key, value);
        }

        public TData Get(TKey item)
        {
            if (items.TryGetValue(item, out var value))
                return value;

            value = new TData();
            items.Add(item, value);

            return value;
        }

        public void Set(TKey item, TData data)
        {
            if (items.ContainsKey(item))
                items[item] = data;
            else
                items.Add(item, data);
        }

        public bool TryFindKey(Func<TData, bool> predicate, out TKey key)
        {
            return items.TryFindKey(predicate, out key);
        }

        public void Clear() => items.Clear();

        public IEnumerator<KeyValuePair<TKey, TData>> GetEnumerator() => items.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            itemsData = Serialization.Utils.SerializeToJson(items);
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            items = Serialization.Utils.DeserializeFromJson(itemsData, new Items());
        }
    }
}