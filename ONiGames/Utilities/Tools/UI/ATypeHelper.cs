using UnityEngine;

namespace ONiGames.Utilities.Tools.UI
{
    public abstract class ATypeHelper<T1> : MonoBehaviour where T1 : new()
    {
        [SerializeField]
        T1 item = new();

        public T1 Item => item;
    }

    public abstract class ATypeHelper<T1, T2> : MonoBehaviour where T1 : new() where T2 : new()
    {
        [SerializeField]
        T1 item = new();

        [SerializeField]
        T2 item2 = new();

        public T1 Item => item;

        public T2 Item2 => item2;
    }

    public abstract class ATypeHelper<T1, T2, T3> : MonoBehaviour where T1 : new() where T2 : new() where T3 : new()
    {
        [SerializeField]
        T1 item = new();

        [SerializeField]
        T2 item2 = new();

        [SerializeField]
        T3 item3 = new();

        public T1 Item => item;

        public T2 Item2 => item2;

        public T3 Item3 => item3;
    }
}