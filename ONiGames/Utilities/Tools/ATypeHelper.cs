using System;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Utilities.Tools
{
    public abstract class ATypeHelper<T1> : MonoBehaviour
    {
        [NonSerialized]
        [DynamicField]
        public T1 item = default;
    }

    public abstract class ATypeHelper<T1, T2> : MonoBehaviour
    {
        [NonSerialized]
        [DynamicField]
        public T1 item = default;
        
        [NonSerialized]
        [DynamicField]
        public T2 item2 = default;
    }

    public abstract class ATypeHelper<T1, T2, T3> : MonoBehaviour
    {
        [NonSerialized]
        [DynamicField]
        public T1 item = default;
        
        [NonSerialized]
        [DynamicField]
        public T2 item2 = default;
        
        [NonSerialized]
        [DynamicField]
        public T3 item3 = default;
    }
}