using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace ONiGames.Utilities
{
    public class Updater
    {
        interface IUpdatable
        {
            void Update(float deltaTime);
        }
        
        class Item<T> : IUpdatable
        {
            class RefValue<TValue>
            {
                public readonly TValue value;
                
                public RefValue(TValue value)
                {
                    this.value = value;
                }
            }
            
            readonly Func<T> valueGetter;
            readonly Action<T> action;
            readonly float updateTime;
            
            [CanBeNull]
            RefValue<T> cache = null;
            float updateTimer = 0f;
            
            public Item(Func<T> valueGetter, Action<T> action, float updateTime)
            {
                this.valueGetter = valueGetter;
                this.action = action;
                this.updateTime = updateTime;
            }
            
            public void Update(float deltaTime)
            {
                updateTimer -= deltaTime;
                if (updateTimer > 0f)
                    return;
                
                updateTimer = updateTime;
                
                var value = valueGetter();
                
                if (cache != null && Equals(cache.value, value))
                    return;
                
                cache = new RefValue<T>(value);
                
                action(cache.value);
            }
            
            bool Equals([CanBeNull] T value1, [CanBeNull] T value2)
            {
                return EqualityComparer<T>.Default.Equals(value1, value2);
            }
        }
        
        readonly List<IUpdatable> items = new();
        
        public void Update(float deltaTime)
        {
            foreach (var item in items)
                item.Update(deltaTime);
        }
        
        public void Add(Action action, float updateTime = 0f)
        {
            Add(() => 0, _ => action(), updateTime);
        }
        
        public void Add<T>(Func<T> valueGetter, Action<T> action, float updateTime = 0f)
        {
            var item = new Item<T>(valueGetter, action, updateTime);
            item.Update(0f);
            
            items.Add(item);
        }
        
        public void Clear()
        {
            items.Clear();
        }
    }
}
