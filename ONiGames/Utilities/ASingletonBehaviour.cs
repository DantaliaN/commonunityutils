﻿using UnityEngine;

namespace ONiGames.Utilities
{
    public abstract class ASingletonBehaviour<T> : MonoBehaviour where T : ASingletonBehaviour<T>
    {
        static T __instance;

        public static T Instance
        {
            get
            {
                if (!Application.isPlaying)
                {
                    var obj = FindObjectOfType<T>();
                    if (obj == null)
                    {
                        var go = new GameObject($"[{typeof(T).Name}]");
                        obj = go.AddComponent<T>();
                    }

                    return obj;
                }

                if (__instance == null)
                {
                    __instance = FindObjectOfType<T>();
                    if (__instance == null)
                    {
                        var go = new GameObject($"[{typeof(T).Name}]");
                        __instance = go.AddComponent<T>();
                    }
                }

                return __instance;
            }
        }

        protected virtual bool IsSinglet => true;
        
        protected virtual bool UseDontDestroyOnLoad => true;

        protected virtual void OnValidate()
        {
            name = $"[{typeof(T).Name}]";
        }

        protected virtual void Awake()
        {
            if (!IsSinglet)
                return;
            
            //make singlet any
            transform.SetParent(null);

            var cacheName = name;
            if (!Utils.MakeSinglet(this, UseDontDestroyOnLoad))
                Debug.Log($"{cacheName}: Found another Singleton {typeof(T)} on the scene");
        }
    }
}