using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities
{
    public static class Insist
    {
        public delegate string LazyString();
        
        [ContractAnnotation("obj:null=>true")]
        public static bool IsNull(Object obj, LazyString lazyMessage, LogType logType = LogType.Log)
        {
            if (obj == null)
            {
                var message = lazyMessage();
                if (!message.IsNullOrEmpty())
                    Debug.unityLogger.Log(logType, message);
                return true;
            }
            
            return false;
        }
        
        [ContractAnnotation("obj:null=>true")]
        public static bool IsNull([CanBeNull] Object obj, string message = "", LogType logType = LogType.Log)
        {
            return IsNull(obj, () => message, logType);
        }
        
        [ContractAnnotation("obj:null=>false")]
        public static bool IsNotNull([CanBeNull] Object obj, LazyString lazyMessage, LogType logType = LogType.Log)
        {
            if (obj != null)
            {
                var message = lazyMessage();
                if (!message.IsNullOrEmpty())
                    Debug.unityLogger.Log(logType, message);
                return true;
            }
            
            return false;
        }
        
        [ContractAnnotation("obj:null=>false")]
        public static bool IsNotNull([CanBeNull] Object obj, string message = "", LogType logType = LogType.Log)
        {
            return IsNotNull(obj, () => message, logType);
        }
        
        [ContractAnnotation("obj:null=>true")]
        public static bool IsNull([CanBeNull] object obj, LazyString lazyMessage, LogType logType = LogType.Log)
        {
            if (obj == null)
            {
                var message = lazyMessage();
                if (!message.IsNullOrEmpty())
                    Debug.unityLogger.Log(logType, message);
                return true;
            }
            
            return false;
        }
        
        [ContractAnnotation("obj:null=>true")]
        public static bool IsNull([CanBeNull] object obj, string message = "", LogType logType = LogType.Log)
        {
            return IsNull(obj, () => message, logType);
        }
        
        [ContractAnnotation("obj:null=>false")]
        public static bool IsNotNull([CanBeNull] object obj, LazyString lazyMessage, LogType logType = LogType.Log)
        {
            if (obj != null)
            {
                var message = lazyMessage();
                if (!message.IsNullOrEmpty())
                    Debug.unityLogger.Log(logType, message);
                return true;
            }
            
            return false;
        }
        
        [ContractAnnotation("obj:null=>false")]
        public static bool IsNotNull([CanBeNull] object obj, string message = "", LogType logType = LogType.Log)
        {
            return IsNotNull(obj, () => message, logType);
        }
        
        public static bool IsTrue(bool expression, LazyString lazyMessage, LogType logType = LogType.Log)
        {
            if (expression)
            {
                var message = lazyMessage();
                if (!message.IsNullOrEmpty())
                    Debug.unityLogger.Log(logType, message);
                return true;
            }
            
            return false;
        }
        
        public static bool IsTrue(bool expression, string message = "", LogType logType = LogType.Log)
        {
            return IsTrue(expression, () => message, logType);
        }
        
        public static bool IsFalse(bool expression, LazyString lazyMessage, LogType logType = LogType.Log)
        {
            if (!expression)
            {
                var message = lazyMessage();
                if (!message.IsNullOrEmpty())
                    Debug.unityLogger.Log(logType, message);
                return true;
            }
            
            return false;
        }
        
        public static bool IsFalse(bool expression, string message = "", LogType logType = LogType.Log)
        {
            return IsFalse(expression, () => message, logType);
        }
    }
}