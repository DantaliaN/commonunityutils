using System.Collections.Generic;
using JetBrains.Annotations;

namespace ONiGames.Utilities
{
    public class DelayedActionQueue
    {
        readonly Queue<DelayedAction> actions = new();

        [CanBeNull]
        DelayedAction currentAction;

        public void Enqueue(DelayedAction action)
        {
            actions.Enqueue(action);
        }

        public void Update(float deltaTime)
        {
            if (currentAction is { IsFinished: false })
                currentAction.Update(deltaTime);

            if (actions.Count == 0 || currentAction is { IsFinished: false })
                return;

            currentAction = actions.Dequeue();
        }
    }
}