﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class DelayedAction
    {
        public bool IsStarted { get; private set; }
        public bool IsFinished { get; private set; }
        public float Progress { get; private set; }

        readonly float time;

        [CanBeNull]
        readonly Action started;
        [CanBeNull]
        readonly Action<float> update;
        [CanBeNull]
        readonly Action finished;

        float timer = 0f;

        public DelayedAction(float time, [CanBeNull] Action finished = null) : this(time, null, null, finished) { }

        public DelayedAction(float time, [CanBeNull] Action started = null, [CanBeNull] Action<float> update = null, [CanBeNull] Action finished = null)
        {
            this.time = time;

            this.started = started;
            this.update = update;
            this.finished = finished;
        }

        public void Update(float deltaTime)
        {
            if (IsFinished)
                return;

            timer += deltaTime;

            if (!IsStarted)
            {
                IsStarted = true;
                started?.Invoke();
            }

            Progress = Mathf.Clamp01(time > 0f ? timer / time : 1f);

            update?.Invoke(Progress);

            if (timer > time)
                Stop();
        }

        public void Stop(bool callFinish = true)
        {
            IsFinished = true;

            if (callFinish)
                finished?.Invoke();
        }
    }
}