﻿using System.IO;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities
{
    public abstract class ASingletonScriptableObject<T, TSettings> : ScriptableObject where T : ASingletonScriptableObject<T, TSettings> where TSettings : ASingletonScriptableObject<T, TSettings>.ASettings, new()
    {
        public abstract class ASettings
        {
            public virtual string ResourcesPath => "Resources";

            public abstract string Path { get; }
        }

        const string AssetsFolder = "Assets";
        const string AssetExtension = ".asset";

        [CanBeNull]
        static T __instance;

        public static T Instance
        {
            get
            {
                if (__instance == null)
                {
                    var settings = new TSettings();

                    __instance = Resources.Load(settings.Path) as T;

                    if (__instance == null)
                    {
                        Debug.LogWarning($"{nameof(T)}: Cannot find settings, creating default");

                        var assetPath = Path.Combine(AssetsFolder, settings.ResourcesPath, settings.Path + AssetExtension);

                        //in OnValidate we need to wait a bit before Resources.Load initialize
                        if (File.Exists(assetPath))
                            return CreateInstance<T>();

                        __instance = CreateInstance<T>();
#if UNITY_EDITOR
                        var directoryPath = Path.GetDirectoryName(assetPath) ?? string.Empty;
                        if (!Directory.Exists(directoryPath))
                            Directory.CreateDirectory(directoryPath);

                        UnityEditor.AssetDatabase.CreateAsset(__instance, assetPath);
#endif
                    }
                }

                return __instance;
            }
        }
    }
}