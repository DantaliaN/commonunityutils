using System;
using System.Collections.Generic;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class MainThreadDispatcher : MonoBehaviour
    {
        readonly Queue<Action> exequtions = new();

        void Update()
        {
            lock (exequtions)
            {
                while (exequtions.Count > 0)
                    exequtions.Dequeue().Invoke();
            }
        }

        void OnDestroy()
        {
            lock (exequtions)
            {
                exequtions.Clear();
            }
        }

        public void Process(Action action)
        {
            lock (exequtions)
            {
                exequtions.Enqueue(action);
            }
        }
    }
}