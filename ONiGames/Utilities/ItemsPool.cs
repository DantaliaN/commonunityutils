using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ONiGames.Utilities
{
    public class ItemsPool
    {
        readonly GameObject poolPrefab;
        
        [CanBeNull]
        readonly Transform poolTransform;

        readonly List<GameObject> instances = new();

        public IReadOnlyList<GameObject> Instances => instances;

        public ItemsPool(GameObject prefab, [CanBeNull] Transform transform = null) : this(prefab, 0, transform) { }

        public ItemsPool(GameObject prefab, int count, [CanBeNull] Transform transform = null)
        {
            poolPrefab = prefab;
            poolTransform = transform;

            ChangeCount(count);
        }

        public void ChangeCount(int count)
        {
            if (instances.Count == count)
                return;

            if (instances.Count > count)
            {
                for (var i = instances.Count - 1; i >= count && i >= instances.Count; i--)
                {
                    Object.Destroy(instances[i].gameObject);
                    instances.RemoveAt(i);
                }
            }

            for (var i = instances.Count; i < count; i++)
            {
                var instance = poolTransform != null ? Object.Instantiate(poolPrefab, poolTransform) : Object.Instantiate(poolPrefab);
                instances.Add(instance);
            }
        }

        public IEnumerable<(int, TItem, GameObject)> Change<TItem>(IReadOnlyList<TItem> items)
        {
            ChangeCount(items.Count);
            
            for (var i = 0; i < items.Count && Instances.InBounds(i); i++)
                yield return (i, items[i], Instances[i]);
        }

        public IEnumerable<(int, GameObject)> Change(int count)
        {
            ChangeCount(count);
            
            for (var i = 0; i < count && Instances.InBounds(i); i++)
                yield return (i, Instances[i]);
        }

        public void Clear()
        {
            foreach (var item in instances)
                Object.Destroy(item.gameObject);
            instances.Clear();
        }
    }

    public class ItemsPool<T> where T : Component
    {
        readonly T poolPrefab;

        [CanBeNull]
        readonly Transform poolTransform;

        readonly List<T> instances = new();

        public IReadOnlyList<T> Instances => instances;

        public ItemsPool(T prefab, [CanBeNull] Transform transform = null) : this(prefab, 0, transform) { }
        
        public ItemsPool(T prefab, int count, [CanBeNull] Transform transform = null)
        {
            poolPrefab = prefab;
            poolTransform = transform;

            ChangeCount(count);
        }

        public void ChangeCount(int count)
        {
            if (instances.Count == count)
                return;

            if (instances.Count > count)
            {
                for (var i = instances.Count - 1; i >= count && i >= 0; i--)
                {
                    Object.Destroy(instances[i].gameObject);
                    instances.RemoveAt(i);
                }
            }

            for (var i = instances.Count; i < count; i++)
            {
                var instance = poolTransform != null ? Object.Instantiate(poolPrefab, poolTransform) : Object.Instantiate(poolPrefab);
                instances.Add(instance);
            }
        }

        public void Change<TItem>(IReadOnlyList<TItem> items, Action<int, TItem, T> predicate)
        {
            foreach (var (index, item, instance) in Change(items))
                predicate(index, item, instance);
        }

        public void Change<TItem>(IReadOnlyList<TItem> items, Action<IEnumerable<(int, TItem, T)>> onResult)
        {
            onResult(Change(items));
        }
        
        public IEnumerable<(int, TItem, T)> Change<TItem>(IReadOnlyList<TItem> items)
        {
            ChangeCount(items.Count);
            
            for (var i = 0; i < items.Count && Instances.InBounds(i); i++)
                yield return (i, items[i], Instances[i]);
        }

        public IEnumerable<(int, (TOwner, TItem), T)> Change<TOwner, TItem>(IReadOnlyList<KeyValuePair<TOwner, IReadOnlyList<TItem>>> items)
        {
            ChangeCount(items.Sum(o => o.Value.Count));

            var index = 0;
            
            foreach (var item in items)
            {
                foreach (var value in item.Value)
                {
                    if (!Instances.InBounds(index))
                        continue;
                    
                    yield return (index, (item.Key, value), Instances[index]);
                    
                    index++;
                }
            }
        }

        public IEnumerable<(int, T)> Change(int count)
        {
            ChangeCount(count);
            
            for (var i = 0; i < count && Instances.InBounds(i); i++)
                yield return (i, Instances[i]);
        }

        public void Clear()
        {
            foreach (var item in instances)
                Object.Destroy(item.gameObject);
            instances.Clear();
        }
    }
}