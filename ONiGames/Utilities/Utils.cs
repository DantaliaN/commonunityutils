﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace ONiGames.Utilities
{
    // ReSharper disable UnusedMember.Global
    public static class Utils
    {
        #region Prefs
        
        public const string DateFormat = "yyyy-MM-dd HH:mm:ss.fffffff";
        
        public static class PlayerPrefsHelper
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            static readonly Dictionary<string, object> Prefs = new Dictionary<string, object>();
#endif
            
            public static int GetInt(string name, int def = 0)
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                try
                {
                    return Prefs[name] is int ? (int) Prefs[name] : def;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);

                    return def;
                }
#else
                return PlayerPrefs.GetInt(name, def);
#endif
            }
            
            public static float GetFloat(string name, float def = 0f)
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                try
                {
                    return Prefs[name] is float ? (float) Prefs[name] : def;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);

                    return def;
                }
#else
                return PlayerPrefs.GetFloat(name, def);
#endif
            }
            
            public static string GetString(string name, string def = "")
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                try
                {
                    return Prefs[name] is string ? (string) Prefs[name] : def;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);

                    return def;
                }
#else
                return PlayerPrefs.GetString(name, def);
#endif
            }
            
            public static void SetInt(string name, int value)
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                Prefs[name] = value;
#else
                PlayerPrefs.SetInt(name, value);
#endif
            }
            
            public static void SetFloat(string name, float value)
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                Prefs[name] = value;
#else
                PlayerPrefs.SetFloat(name, value);
#endif
            }
            
            public static void SetString(string name, string value)
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                Prefs[name] = value;
#else
                PlayerPrefs.SetString(name, value);
#endif
            }
            
            public static void Save()
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                //TODO need save for webgl
#else
                PlayerPrefs.Save();
#endif
            }
            
            public static void DeleteAll()
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                Prefs.Clear();
#else
                PlayerPrefs.DeleteAll();
#endif
            }
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static TValue GetPlayerPrefs<TValue>(string prefName, string cryptKey, TValue def = default) where TValue : IConvertible
        {
            if (!TryDecryptAes<TValue>(GetPlayerPrefs(Md5(prefName), string.Empty), cryptKey, out var value))
            {
                Debug.LogWarning($"Can't decrypt prefs: {prefName}");
                return def;
            }
            
            return value;
        }
        
        public static void SetPlayerPrefs<TValue>(string prefName, TValue value, string cryptKey) where TValue : IConvertible
        {
            SetPlayerPrefs(Md5(prefName), EncryptAes(value, cryptKey), false);
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T GetPlayerPrefs<T>(string name, T def = default) where T : IConvertible
        {
            try
            {
                var @switch = new Dictionary<Type, Func<T>>
                {
                    { typeof(int), () => ConvertValue<int, T>(PlayerPrefsHelper.GetInt(name, ConvertValue<T, int>(def))) },
                    { typeof(float), () => ConvertValue<float, T>(PlayerPrefsHelper.GetFloat(name, ConvertValue<T, float>(def))) },
                    { typeof(string), () => ConvertValue<string, T>(PlayerPrefsHelper.GetString(name, ConvertValue<T, string>(def))) },
                    { typeof(DateTime), () => ConvertValue<DateTime, T>(DateTime.TryParse(PlayerPrefsHelper.GetString(name, ConvertValue<T, DateTime>(def).ToString(DateFormat)), out var dateTime) ? dateTime : DateTime.MinValue) },
                    { typeof(bool), () => ConvertValue<bool, T>(PlayerPrefsHelper.GetInt(name, ConvertValue<T, bool>(def) ? 1 : 0) == 1) },
                    { typeof(Enum), () => GetEnumMember<T>(PlayerPrefsHelper.GetString(name, GetEnumValue((Enum) (object) def))) },
                    { typeof(double), () => ConvertValue<double, T>(Convert.ToDouble(PlayerPrefsHelper.GetString(name, Convert.ToString(ConvertValue<T, double>(def), CultureInfo.InvariantCulture)), CultureInfo.InvariantCulture)) },
                    { typeof(decimal), () => ConvertValue<decimal, T>(Convert.ToDecimal(PlayerPrefsHelper.GetString(name, Convert.ToString(ConvertValue<T, decimal>(def), CultureInfo.InvariantCulture)), CultureInfo.InvariantCulture)) },
                };
                var type = typeof(T).IsEnum ? typeof(Enum) : typeof(T);
                return @switch[type].Invoke();
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                return def;
            }
        }
        
        public static void SetPlayerPrefs<T>(string name, T val, bool needSave = true) where T : IConvertible
        {
            try
            {
                var @switch = new Dictionary<Type, Action>
                {
                    { typeof(int), () => { PlayerPrefsHelper.SetInt(name, ConvertValue<T, int>(val)); } },
                    { typeof(float), () => { PlayerPrefsHelper.SetFloat(name, ConvertValue<T, float>(val)); } },
                    { typeof(string), () => { PlayerPrefsHelper.SetString(name, ConvertValue<T, string>(val)); } },
                    { typeof(DateTime), () => { PlayerPrefsHelper.SetString(name, ConvertValue<T, DateTime>(val).ToString(DateFormat)); } },
                    { typeof(bool), () => { PlayerPrefsHelper.SetInt(name, ConvertValue<T, bool>(val) ? 1 : 0); } },
                    { typeof(Enum), () => { PlayerPrefsHelper.SetString(name, GetEnumValue((Enum) (object) val)); } },
                    { typeof(double), () => { PlayerPrefsHelper.SetString(name, Convert.ToString(ConvertValue<T, double>(val), CultureInfo.InvariantCulture)); } },
                    { typeof(decimal), () => { PlayerPrefsHelper.SetString(name, Convert.ToString(ConvertValue<T, decimal>(val), CultureInfo.InvariantCulture)); } },
                };
                var type = typeof(T).IsEnum ? typeof(Enum) : typeof(T);
                @switch[type].Invoke();
                
                if (needSave)
                    PlayerPrefsHelper.Save();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }
        
        public static T ConvertValue<TU, T>(TU value) where T : IConvertible where TU : IConvertible
        {
            return (T) Convert.ChangeType(value, typeof(T));
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T GetEnumMember<T>(string value, T def = default) where T : IConvertible
        {
            foreach (T o in Enum.GetValues(typeof(T)))
            {
                if (StringValue.GetStringValue(o as Enum) == value)
                {
                    return o;
                }
            }
            
            try
            {
                return (T) Enum.Parse(typeof(T), value);
            }
            catch
            {
                Debug.LogWarning($"{value} not found in {typeof(T)}");
                
                return def;
            }
        }
        
        public static string GetEnumValue(Enum e)
        {
            var value = StringValue.GetStringValue(e);
            if (!string.IsNullOrEmpty(value))
                return value;
            
            value = Enum.GetName(e.GetType(), e);
            if (!string.IsNullOrEmpty(value))
                return value;
            
            return e.ToString();
        }
        
        #endregion
        
        #region Object Value
        
        [CanBeNull]
        public static object GetObjectValue(object source, string name)
        {
            if (source == null)
                return null;
            
            var type = source.GetType();
            
            while (type != null)
            {
                var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                if (f != null)
                    return f.GetValue(source);
                
                var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if (p != null)
                    return p.GetValue(source, null);
                
                type = type.BaseType;
            }
            
            return null;
        }
        
        [CanBeNull]
        public static object GetObjectValue(object source, string name, int index)
        {
            if (!(GetObjectValue(source, name) is IEnumerable enumerable))
                return null;
            
            var enm = enumerable.GetEnumerator();
            
            for (var i = 0; i <= index; i++)
            {
                if (!enm.MoveNext())
                    return null;
            }
            
            return enm.Current;
        }
        
        public static void SetObjectValue(object source, string name, object value)
        {
            var type = source.GetType();
            
            var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (f != null)
            {
                try
                {
                    f.SetValue(source, value);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }
                
                return;
            }
            
            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p != null)
            {
                try
                {
                    p.SetValue(source, value);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }
            }
        }
        
        public static void SetObjectValue(object source, string name, int index, object value)
        {
            if (GetObjectValue(source, name) is IList arr)
            {
                try
                {
                    arr[index] = value;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }
            }
        }
        
        #endregion
        
        #region Crypto
        
        public static string Encrypt<T>(T value, string key) where T : IConvertible
        {
            return Encrypt(value, key, Encoding.Default);
        }
        
        public static string Encrypt<T>(T value, string key, Encoding encoding) where T : IConvertible
        {
            using (var provider = new RSACryptoServiceProvider(new CspParameters { KeyContainerName = key }))
            {
                var str = ConvertValue<T, string>(value);
                var encryptedBytes = provider.Encrypt(encoding.GetBytes(str), true);
                return Convert.ToBase64String(encryptedBytes);
            }
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T Decrypt<T>(string str, string key, T def = default) where T : IConvertible
        {
            return Decrypt(str, key, Encoding.Default, def);
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T Decrypt<T>(string str, string key, Encoding encoding, T def = default) where T : IConvertible
        {
            using (var provider = new RSACryptoServiceProvider(new CspParameters { KeyContainerName = key }))
            {
                try
                {
                    var bytes = provider.Decrypt(Convert.FromBase64String(str), true);
                    var decrypted = encoding.GetString(bytes);
                    return ConvertValue<string, T>(decrypted);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                    return def;
                }
            }
        }
        
        public static string EncryptAes<T>(T value, string key) where T : IConvertible
        {
            const int aesKeySize = 32;
            
            var k = Encoding.UTF8.GetBytes(key);
            if (k.Length > aesKeySize)
                Array.Resize(ref k, aesKeySize);
            
            var str = ConvertValue<T, string>(value);
            var data = Encoding.UTF8.GetBytes(str);
            
            using (var aes = new AesCryptoServiceProvider())
            {
                aes.Key = k;
                aes.GenerateIV();
                
                using (var encryptor = aes.CreateEncryptor())
                {
                    var bytes = encryptor.TransformFinalBlock(data, 0, data.Length);
                    var allData = aes.IV.Concat(bytes);
                    return Convert.ToBase64String(allData);
                }
            }
        }
        
        public static bool TryDecryptAes<T>(string str, string key, out T value) where T : IConvertible
        {
            const int aesKeySize = 32;
            const int aesIvSize = 16;
            
            var k = Encoding.UTF8.GetBytes(key);
            if (k.Length > aesKeySize)
                Array.Resize(ref k, aesKeySize);
            
            byte[] allData;
            try
            {
                allData = Convert.FromBase64String(str);
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                
                value = default;
                return false;
            }
            
            if (allData.Length < aesIvSize)
            {
                value = default;
                return false;
            }
            
            var iv = new byte[aesIvSize];
            Array.Copy(allData, 0, iv, 0, iv.Length);
            
            var data = new byte[allData.Length - iv.Length];
            Array.Copy(allData, iv.Length, data, 0, data.Length);
            
            using (var aes = new AesCryptoServiceProvider())
            {
                aes.Key = k;
                aes.IV = iv;
                
                using (var decrypt = aes.CreateDecryptor())
                {
                    try
                    {
                        var bytes = decrypt.TransformFinalBlock(data, 0, data.Length);
                        var enc = Encoding.UTF8.GetString(bytes);
                        value = ConvertValue<string, T>(enc);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Debug.LogWarning(ex);
                        
                        value = default;
                        return false;
                    }
                }
            }
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T DecryptAes<T>(string str, string key, T def = default) where T : IConvertible
        {
            return TryDecryptAes<T>(str, key, out var value) ? value : def;
        }
        
        public static string Md5(string s)
        {
            return Md5(s, Encoding.Default);
        }
        
        public static string Md5(string s, Encoding encoding)
        {
            var bytes = encoding.GetBytes(s);
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var hash = md5.ComputeHash(bytes);
                return hash.Aggregate(string.Empty, (current, b) => current + b.ToString("x2"));
            }
        }
        
        #endregion
        
        #region Resource Load/Save/Remove
        
        public static void ResaveResource(string path)
        {
            LoadResource(path, file =>
            {
                SaveFile(MakePersistentFilePath(path), file);
            });
        }
        
        public static void LoadResource(string path, Action<XmlDocument> onComplete)
        {
            LoadResource(path, file =>
            {
                var xml = new XmlDocument();
                xml.LoadXml(file);
                onComplete.Invoke(xml);
            });
        }
        
        public static void LoadResource(string path, Action<string> onComplete)
        {
            var data = Resources.Load<TextAsset>(path);
            if (data == null)
                throw new Exception($"File {path} not found");
            
            var str = data.text;
            
            onComplete.Invoke(str);
        }
        
        public static T LoadResource<T>() where T : new()
        {
            var fileNameWithoutExtension = $"{typeof(T)}";
            
#if UNITY_EDITOR
            var targetFolder = Path.Combine(Application.dataPath, "Resources");
            
            var fileName = $"{fileNameWithoutExtension}.json";
            
            var path = Path.Combine(targetFolder, fileName);
            
            try
            {
                if (File.Exists(path))
                    return Serialization.Utils.DeserializeFromJson(File.ReadAllText(path), new T());
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }
            
            return new T();
#else
            try
            {
                var txtAsset = Resources.Load<TextAsset>(fileNameWithoutExtension);
            
                return Serialization.Utils.DeserializeFromJson(txtAsset.text, new T());
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return new T();
#endif
        }
        
        public static void SaveResource<T>(T data)
        {
            var targetFolder = Path.Combine(Application.dataPath, "Resources");
            
            var fileNameWithoutExtension = $"{typeof(T)}";
            var fileName = $"{fileNameWithoutExtension}.json";
            
            if (!Directory.Exists(targetFolder))
                Directory.CreateDirectory(targetFolder);
            
            File.WriteAllText(Path.Combine(targetFolder, fileName), Serialization.Utils.SerializeToJson(data));
        }
        
        public static void RemoveResource<T>()
        {
            var targetFolder = Path.Combine(Application.dataPath, "Resources");
            
            var fileNameWithoutExtension = $"{typeof(T)}";
            var fileName = $"{fileNameWithoutExtension}.json";
            var metaFileName = $"{fileName}.meta";
            
            if (File.Exists(Path.Combine(targetFolder, fileName)))
                File.Delete(Path.Combine(targetFolder, fileName));
            
            if (File.Exists(Path.Combine(targetFolder, metaFileName)))
                File.Delete(Path.Combine(targetFolder, metaFileName));
        }
        
        public static string MakePersistentFilePath(string path)
        {
            const string tmpFileExt = ".dat";
            return Path.Combine(Application.persistentDataPath, path + tmpFileExt);
        }
        
        #endregion
        
        #region StreamingAsset
        
#if !UNITY_WEBGL && !UNITY_ANDROID && !UNITY_IOS
        [Obsolete]
        public static IEnumerator LoadStreamingAssetWWW(string path, Action<MovieTexture> onComplete)
        {
            //NOTE в юнити 5.3 не работает MovieTexture.. выдает нулл при загрузке.. какая-то ошибка у юнити

            var url = Path.Combine("file:///" + Application.streamingAssetsPath, path);

            MovieTexture movie = null;

            var dh = new DownloadHandlerMovieTexture();
            yield return LoadWWW(url, dh, () =>
            {
                movie = dh.movieTexture;
            });

            while (!movie.isReadyToPlay)
                yield return null;

            onComplete.Invoke(movie);
        }
#endif
        
        public static IEnumerator LoadStreamingAssetWWW(string path, Action<XmlDocument> onComplete)
        {
            var url = Path.Combine("file:///" + Application.streamingAssetsPath, path);
            
            yield return LoadWWW(url, str =>
            {
                var xml = new XmlDocument();
                xml.LoadXml(str);
                
                onComplete.Invoke(xml);
            });
        }
        
        public static void LoadStreamingAsset(string path, Action<XmlDocument> onComplete)
        {
            LoadStreamingAsset(path, str =>
            {
                var xml = new XmlDocument();
                xml.LoadXml(str);
                
                onComplete.Invoke(xml);
            });
        }
        
        public static void LoadStreamingAsset(string path, Action<string> onComplete)
        {
            LoadFile(Path.Combine(Application.streamingAssetsPath, path), onComplete.Invoke);
        }
        
        #endregion
        
        #region Http
        
        public static string HttpGet(string url)
        {
            return HttpGet(url, new Dictionary<HttpRequestHeader, string>());
        }
        
        public static string HttpGet(string url, string contentType)
        {
            var headers = new Dictionary<HttpRequestHeader, string> { { HttpRequestHeader.ContentType, contentType } };
            return HttpGet(url, headers);
        }
        
        public static string HttpGet(string url, Dictionary<HttpRequestHeader, string> headers)
        {
            try
            {
                var request = (HttpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Get;
                
                foreach (var pair in headers.ToList())
                {
                    switch (pair.Key)
                    {
                        case HttpRequestHeader.ContentType:
                        case HttpRequestHeader.Accept:
                        case HttpRequestHeader.Referer:
                        case HttpRequestHeader.UserAgent:
                            request.Accept = pair.Value;
                            headers.Remove(pair.Key);
                            break;
                    }
                }
                
                foreach (var pair in headers)
                    request.Headers.Add(pair.Key, pair.Value);
                
                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            if (stream == null)
                                return string.Empty;
                            
                            using (var read = new StreamReader(stream))
                                return read.ReadToEnd();
                        }
                    }
                    
                    Debug.Log($"{url}: {response.StatusCode}, {response.StatusDescription}");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
            }
            
            return string.Empty;
        }
        
        public static string HttpPost(string url, string data)
        {
            return HttpPost(url, new Dictionary<HttpRequestHeader, string>(), new UTF8Encoding().GetBytes(data));
        }
        
        public static string HttpPost(string url, string data, string contentType)
        {
            var headers = new Dictionary<HttpRequestHeader, string> { { HttpRequestHeader.ContentType, contentType } };
            return HttpPost(url, headers, new UTF8Encoding().GetBytes(data));
        }
        
        public static string HttpPost(string url, Dictionary<HttpRequestHeader, string> headers, string data)
        {
            return HttpPost(url, headers, new UTF8Encoding().GetBytes(data));
        }
        
        public static string HttpPost(string url, Dictionary<HttpRequestHeader, string> headers, byte[] data)
        {
            try
            {
                var request = (HttpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Post;
                
                foreach (var pair in headers.ToList())
                {
                    switch (pair.Key)
                    {
                        case HttpRequestHeader.ContentType:
                        case HttpRequestHeader.Accept:
                        case HttpRequestHeader.Referer:
                        case HttpRequestHeader.UserAgent:
                            request.Accept = pair.Value;
                            headers.Remove(pair.Key);
                            break;
                    }
                }
                
                foreach (var pair in headers)
                    request.Headers.Add(pair.Key, pair.Value);
                
                request.ContentLength = data.LongLength;
                
                using (var stream = request.GetRequestStream())
                    stream.Write(data, 0, data.Length);
                
                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            if (stream == null)
                                return string.Empty;
                            
                            using (var read = new StreamReader(stream))
                                return read.ReadToEnd();
                        }
                    }
                    
                    Debug.Log($"{url}: {response.StatusCode}, {response.StatusDescription}");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
            }
            
            return string.Empty;
        }
        
        public static async Task<string> HttpGetAsync(string url)
        {
            return await HttpGetAsync(url, new Dictionary<HttpRequestHeader, string>());
        }
        
        public static async Task<string> HttpGetAsync(string url, string contentType)
        {
            var headers = new Dictionary<HttpRequestHeader, string> { { HttpRequestHeader.ContentType, contentType } };
            return await HttpGetAsync(url, headers);
        }
        
        public static async Task<string> HttpGetAsync(string url, Dictionary<HttpRequestHeader, string> headers)
        {
            try
            {
                var request = (HttpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Get;
                
                foreach (var pair in headers.ToList())
                {
                    switch (pair.Key)
                    {
                        case HttpRequestHeader.ContentType:
                        case HttpRequestHeader.Accept:
                        case HttpRequestHeader.Referer:
                        case HttpRequestHeader.UserAgent:
                            request.Accept = pair.Value;
                            headers.Remove(pair.Key);
                            break;
                    }
                }
                
                foreach (var pair in headers)
                    request.Headers.Add(pair.Key, pair.Value);
                
                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            if (stream == null)
                                return string.Empty;
                            
                            using (var read = new StreamReader(stream))
                                return await read.ReadToEndAsync();
                        }
                    }
                    
                    Debug.Log($"{url}: {response.StatusCode}, {response.StatusDescription}");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
            }
            
            return string.Empty;
        }
        
        public static async Task<string> HttpPostAsync(string url, string data)
        {
            return await HttpPostAsync(url, new UTF8Encoding().GetBytes(data), new Dictionary<HttpRequestHeader, string>());
        }
        
        public static async Task<string> HttpPostAsync(string url, string data, string contentType)
        {
            var headers = new Dictionary<HttpRequestHeader, string> { { HttpRequestHeader.ContentType, contentType } };
            return await HttpPostAsync(url, new UTF8Encoding().GetBytes(data), headers);
        }
        
        public static async Task<string> HttpPostAsync(string url, byte[] data, Dictionary<HttpRequestHeader, string> headers)
        {
            try
            {
                var request = (HttpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Post;
                
                foreach (var pair in headers.ToList())
                {
                    switch (pair.Key)
                    {
                        case HttpRequestHeader.ContentType:
                        case HttpRequestHeader.Accept:
                        case HttpRequestHeader.Referer:
                        case HttpRequestHeader.UserAgent:
                            request.Accept = pair.Value;
                            headers.Remove(pair.Key);
                            break;
                    }
                }
                
                foreach (var pair in headers)
                    request.Headers.Add(pair.Key, pair.Value);
                
                request.ContentLength = data.LongLength;
                
                using (var stream = request.GetRequestStream())
                    await stream.WriteAsync(data, 0, data.Length);
                
                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            if (stream == null)
                                return string.Empty;
                            
                            using (var read = new StreamReader(stream))
                                return await read.ReadToEndAsync();
                        }
                    }
                    
                    Debug.Log($"{url}: {response.StatusCode}, {response.StatusDescription}");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
            }
            
            return string.Empty;
        }
        
        #endregion
        
        #region Unity Http
        
        public static async Task<HttpResponseMessage> WebRequest(HttpMethod pMethod, string pUrl, string pJsonContent, Dictionary<string, string> pHeaders)
        {
            var client = new HttpClient();
            
            var httpRequestMessage = new HttpRequestMessage { Method = pMethod, RequestUri = new Uri(pUrl) };
            foreach (var head in pHeaders)
            {
                httpRequestMessage.Headers.Add(head.Key, head.Value);
            }
            
            switch (pMethod.Method)
            {
                case "POST":
                    HttpContent httpContent = new StringContent(pJsonContent, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = httpContent;
                    break;
            }
            
            return await client.SendAsync(httpRequestMessage);
        }
        
        #endregion
        
        #region Ftp
        
        public static bool FtpMakeDirectory(string url, string userName, string password, bool enableSsl = true)
        {
            try
            {
                var request = (FtpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                
                request.Credentials = new NetworkCredential(userName, password);
                request.EnableSsl = enableSsl;
                
                request.Proxy = null;
                request.KeepAlive = true;
                request.UseBinary = true;
                request.UsePassive = true;
                
                using (var response = (FtpWebResponse) request.GetResponse())
                {
                    if (response.StatusCode == FtpStatusCode.PathnameCreated)
                        return true;
                    
                    Debug.Log($"{url}: {response.StatusCode}, {response.StatusDescription}");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
            }
            
            return false;
        }
        
        public static IReadOnlyList<string> FtpListDirectory(string url, string userName, string password, bool enableSsl = true)
        {
            try
            {
                var request = (FtpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                
                request.Credentials = new NetworkCredential(userName, password);
                request.EnableSsl = enableSsl;
                
                request.Proxy = null;
                request.KeepAlive = true;
                request.UseBinary = true;
                request.UsePassive = true;
                
                using (var response = (FtpWebResponse) request.GetResponse())
                {
                    if (response.StatusCode == FtpStatusCode.OpeningData)
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            if (stream == null)
                                return new List<string>();
                            
                            using (var reader = new StreamReader(stream))
                            {
                                var items = new List<string>();
                                while (!reader.EndOfStream)
                                {
                                    var s = reader.ReadLine();
                                    if (!string.IsNullOrEmpty(s))
                                        items.Add(s);
                                }
                                
                                return items;
                            }
                        }
                    }
                    
                    Debug.Log($"{url}: {response.StatusCode}, {response.StatusDescription}");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
            }
            
            return new List<string>();
        }
        
        public static string FtpDownloadFile(string url, string userName, string password)
        {
            try
            {
                var request = (FtpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.EnableSsl = false;
                request.Proxy = null;
                request.KeepAlive = true;
                request.UseBinary = true;
                request.UsePassive = true;
                
                request.Credentials = new NetworkCredential(userName, password);
                
                request.Proxy = null;
                
                using (var response = (FtpWebResponse) request.GetResponse())
                {
                    if (response.StatusCode != FtpStatusCode.OpeningData)
                    {
                        Debug.LogWarning($"Download from FTP:{url}: {response.StatusCode}, {response.StatusDescription}");
                        return string.Empty;
                    }
                    
                    var responseStream = response.GetResponseStream();
                    if (responseStream == null)
                    {
                        Debug.LogWarning($"Response stream is null on FTP:{url}");
                        return string.Empty;
                    }
                    
                    var reader = new StreamReader(responseStream);
                    
                    var output = reader.ReadToEnd();
                    
                    return output;
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"Download from FTP {url}: {ex}");
            }
            
            return string.Empty;
        }
        
        public static bool FtpUploadFile(string url, string userName, string password, byte[] data, bool enableSsl = true)
        {
            try
            {
                var request = (FtpWebRequest) System.Net.WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                
                request.Credentials = new NetworkCredential(userName, password);
                request.EnableSsl = enableSsl;
                
                request.Proxy = null;
                request.KeepAlive = true;
                request.UseBinary = true;
                request.UsePassive = true;
                
                request.ContentLength = data.Length;
                
                using (var stream = request.GetRequestStream())
                    stream.Write(data, 0, data.Length);
                
                using (var response = (FtpWebResponse) request.GetResponse())
                {
                    if (response.StatusCode != FtpStatusCode.ClosingControl)
                    {
                        Debug.LogWarning($"{url}: {response.StatusCode}, {response.StatusDescription}");
                        return false;
                    }
                    
                    return true;
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
            }
            
            return false;
        }
        
        public static bool FtpSendFile(string url, string userName, string password, string path, byte[] data, bool forceReplace = true, bool enableSsl = true)
        {
            try
            {
                var paths = path.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                var fileName = paths.LastOrDefault();
                var directories = paths.SkipLast(1);
                
                if (string.IsNullOrEmpty(fileName))
                {
                    Debug.LogWarning("File path not set");
                    return false;
                }
                
                var baseUri = new Uri(url);
                
                var uri = baseUri;
                
                foreach (var directory in directories)
                {
                    uri = uri.Append(directory);
                    
                    var currentItems = FtpListDirectory(uri.ToString(), userName, password, enableSsl);
                    if (currentItems.Any(o => o == directory))
                        continue;
                    
                    if (!FtpMakeDirectory(uri.ToString(), userName, password, enableSsl))
                        return false;
                }
                
                uri = baseUri.Append(path);
                
                if (!forceReplace)
                {
                    var items = FtpListDirectory(uri.ToString(), userName, password, enableSsl);
                    if (items.Any(o => o == fileName))
                    {
                        Debug.Log($"File {path} already exist. Skip");
                        return true;
                    }
                }
                
                return FtpUploadFile(uri.ToString(), userName, password, data, enableSsl);
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{url}: {ex}");
                return false;
            }
        }
        
        #endregion
        
        #region WWW
        
        public class WebData
        {
            public string Method { get; set; } = UnityWebRequest.kHttpVerbGET;
            
            [NotNull]
            public byte[] Data { get; set; } = new byte[0];
            
            [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
            [NotNull]
            public Dictionary<string, string> Headers { get; set; } = new();
        }
        
        public static IEnumerator LoadWWW(string url, Action<XmlDocument> onComplete)
        {
            return LoadWWW(url, new WebData(), onComplete);
        }
        
        public static IEnumerator LoadWWW(string url, WebData webData, Action<XmlDocument> onComplete)
        {
            yield return LoadWWW(url, webData, str =>
            {
                var xml = new XmlDocument();
                xml.LoadXml(str);
                
                onComplete.Invoke(xml);
            });
        }
        
        public static IEnumerator LoadWWW(string url, Action<string> onComplete)
        {
            return LoadWWW(url, new WebData(), onComplete);
        }
        
        public static IEnumerator LoadWWW(string url, WebData webData, Action<string> onComplete)
        {
            var dh = new DownloadHandlerBuffer();
            yield return LoadWWW(url, dh, webData, () =>
            {
                var str = dh.text;
                onComplete.Invoke(str);
            });
        }
        
        public static IEnumerator LoadWWW<T>(string url, T downloadHandler, Action onComplete) where T : DownloadHandler
        {
            return LoadWWW(url, downloadHandler, new WebData(), onComplete);
        }
        
        public static IEnumerator LoadWWW<T>(string url, T downloadHandler, WebData webData, Action onComplete) where T : DownloadHandler
        {
            using (var www = new UnityWebRequest(new Uri(url), webData.Method, downloadHandler, webData.Data.Length == 0 ? null : new UploadHandlerRaw(webData.Data)))
            {
                foreach (var header in webData.Headers)
                    www.SetRequestHeader(header.Key, header.Value);
                
                yield return www.SendWebRequest();
                
#if UNITY_2019 || UNITY_2018 || UNITY_2017
                if (www.isNetworkError || www.isHttpError)
                    throw new Exception($"Can't load {url} b/c {www.error}");
#else
                if (www.result != UnityWebRequest.Result.Success)
                    throw new Exception($"Can't load {url} b/c {www.error}");
#endif
            }
            
            onComplete.Invoke();
        }
        
        #endregion
        
        #region File
        
        public static void LoadFile(string filePath, Action<XmlDocument> onComplete)
        {
            LoadFile(filePath, file =>
            {
                var xml = new XmlDocument();
                xml.LoadXml(file);
                
                onComplete.Invoke(xml);
            });
        }
        
        public static void LoadFile(string filePath, Action<string> onComplete)
        {
            LoadFile(filePath, Encoding.Default, onComplete);
        }
        
        public static void LoadFile(string filePath, Encoding encoding, Action<string> onComplete)
        {
            LoadFile(filePath, data =>
            {
                var str = encoding.GetString(data);
                
                onComplete.Invoke(str);
            });
        }
        
        public static void LoadFile(string filePath, Action<byte[]> onComplete)
        {
            if (!File.Exists(filePath))
                throw new Exception($"File {filePath} not exist");
            
            byte[] data;
            using (var fs = File.OpenRead(filePath))
            {
                data = new byte[fs.Length];
                var _ = fs.Read(data, 0, data.Length);
            }
            
            onComplete.Invoke(data);
        }
        
        public static void SaveFile(string filePath, string data)
        {
            SaveFile(filePath, data, Encoding.Default);
        }
        
        public static void SaveFile(string filePath, string data, Encoding encoding)
        {
            var bytes = encoding.GetBytes(data);
            SaveFile(filePath, bytes);
        }
        
        public static void SaveFile(string filePath, byte[] data)
        {
            var directory = Path.GetDirectoryName(filePath);
            if (directory == null)
                throw new Exception($"Can't get directory from {filePath}");
            
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                Debug.LogWarning($"File {filePath} exist and override");
            }
            
            Directory.CreateDirectory(directory);
            
            using (var fs = File.OpenWrite(filePath))
            {
                fs.Write(data, 0, data.Length);
            }
        }
        
        #endregion
        
        #region Copy Files
        
        public static async void CopyFilesRecursivelyAsync(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (var dir in source.GetDirectories())
                CopyFilesRecursivelyAsync(dir, target.CreateSubdirectory(dir.Name));
            
            foreach (var file in source.GetFiles())
                await Task.Run(() => file.CopyTo(Path.Combine(target.FullName, file.Name), true));
        }
        
        public static void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (var dir in source.GetDirectories())
                CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
            foreach (var file in source.GetFiles())
                file.CopyTo(Path.Combine(target.FullName, file.Name), true);
        }
        
        #endregion
        
        #region Math
        
        public static bool IsEven(int value)
        {
            return (value & 1) == 0;
        }
        
        public static bool IsOdd(int value)
        {
            return (value & 1) == 1;
        }
        
        public static float FromPercent(float percent, float min, float max)
        {
            return min + (max - min) * percent;
        }
        
        public static float Normalize(float value, float min, float max)
        {
            return SafeDivide(value - min, max - min);
        }
        
        public static float ClampMin(float value, float min)
        {
            return value < min ? min : value;
        }
        
        public static float ClampMax(float value, float max)
        {
            return value > max ? max : value;
        }
        
        public static int ClampMin(int value, int min)
        {
            return value < min ? min : value;
        }
        
        public static int ClampMax(int value, int max)
        {
            return value > max ? max : value;
        }
        
        public static float SafeDivide(float dividend, float divisor, float byZero = 0f)
        {
            return Mathf.Abs(divisor) < Mathf.Epsilon ? byZero : dividend / divisor;
        }
        
        public static float SafeModulo(float dividend, float divisor, float byZero = 0f)
        {
            return Mathf.Abs(divisor) < Mathf.Epsilon ? byZero : dividend % divisor;
        }
        
        public static float NearestZero(float value)
        {
            return value > 0f ? Mathf.Floor(value) : Mathf.Ceil(value);
        }
        
        public static int NearestZeroToInt(float value)
        {
            return value > 0f ? Mathf.FloorToInt(value) : Mathf.CeilToInt(value);
        }
        
        public static bool InRange(int value, int min, int max)
        {
            return value >= min && value <= max;
        }
        
        public static bool InRange(float value, float min, float max)
        {
            return value >= min && value <= max;
        }
        
        public static bool InRangeExclusive(int value, int min, int max)
        {
            return value > min && value < max;
        }
        
        public static bool InRangeExclusive(float value, float min, float max)
        {
            return value > min && value < max;
        }
        
        public static float Angle360Between(Vector3 a, Vector3 b, Vector3 n)
        {
            var angle = Vector3.Angle(a, b);
            var sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));
            angle *= sign;
            if (angle < 0f)
                angle += 360f;
            return angle;
        }
        
        public static Vector3 QuadraticBezierPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
        {
            var u = 1f - t;
            var tt = t * t;
            var uu = u * u;
            
            return uu * p0 + 2f * u * t * p1 + tt * p2;
        }
        
        public static Vector3 CubicBezierPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            var u = 1f - t;
            var tt = t * t;
            var uu = u * u;
            var uuu = uu * u;
            var ttt = tt * t;
            
            return uuu * p0 + 3f * uu * t * p1 + 3f * u * tt * p2 + ttt * p3;
        }
        
        public static int[] TakePieces(int value, int piecesCount)
        {
            if (piecesCount <= 0)
                return new int[0];
            
            var arr = new int[piecesCount];
            
            var count = value / piecesCount;
            var remainingCount = value - (count * piecesCount);
            
            for (var i = 0; i < arr.Length; i++)
            {
                arr[i] += count;
                
                if (remainingCount > 0)
                {
                    arr[i] += 1;
                    remainingCount -= 1;
                }
            }
            
            return arr;
        }
        
        public static float[] TakePieces(float value, int piecesCount)
        {
            if (piecesCount <= 0)
                return new float[0];
            
            var arr = new float[piecesCount];
            
            var count = value / piecesCount;
            
            for (var i = 0; i < arr.Length; i++)
                arr[i] += count;
            
            return arr;
        }
        
        public static Vector3 TransformPoint(Vector3 vector, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            return rotation * Vector3.Scale(vector, scale) + position;
        }
        
        public static float Angle360Between(Vector2 a, Vector2 b)
        {
            return Angle360Between(a, b, Vector3.back);
        }
        
        public static int RandomRange(int minInclusive, int maxInclusive) => UnityEngine.Random.Range(minInclusive, maxInclusive + 1);
        
        public static float RandomRange(float minInclusive, float maxExclusive) => UnityEngine.Random.Range(minInclusive, maxExclusive - Mathf.Epsilon);
        
        public static Vector2 RandomRange(Vector2 minInclusive, Vector2 maxInclusive) => new(UnityEngine.Random.Range(minInclusive.x, maxInclusive.x), UnityEngine.Random.Range(minInclusive.y, maxInclusive.y));
        
        public static Vector3 RandomRange(Vector3 minInclusive, Vector3 maxInclusive) => new(UnityEngine.Random.Range(minInclusive.x, maxInclusive.x), UnityEngine.Random.Range(minInclusive.y, maxInclusive.y), UnityEngine.Random.Range(minInclusive.z, maxInclusive.z));
        
        #endregion
        
        #region Array
        
        public static byte[] Combine(params byte[][] arrays)
        {
            var rv = new byte[arrays.Sum(a => a.Length)];
            var offset = 0;
            foreach (var array in arrays)
            {
                Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }
            
            return rv;
        }
        
        public static byte[] Crop(byte[] array, int length)
        {
            var rv = new byte[length];
            Buffer.BlockCopy(array, 0, rv, 0, length);
            return rv;
        }
        
        public static void ArrayAdd<T>(ref T[] arr, T obj)
        {
            ChangeArraySize(ref arr, arr.Length + 1);
            arr[arr.Length] = obj;
        }
        
        public static void ChangeArraySize<T>(ref T[] arr, int size)
        {
            if (size < 0)
                size = 0;
            var newArr = new T[size];
            for (var i = 0; i < newArr.Length; i++)
            {
                if (arr.Length <= i)
                    newArr[i] = default;
                else
                    newArr[i] = arr[i];
            }
            
            arr = newArr;
        }
        
        #endregion
        
        #region Color
        
        public static string ColorToHex(Color32 color, string prefix = "")
        {
            return prefix + color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2") + color.a.ToString("X2");
        }
        
        public static Color32 HexToColor(string hex)
        {
            var color = uint.Parse(hex, NumberStyles.HexNumber);
            return UIntToColor(color);
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "ShiftExpressionRealShiftCountIsZero")]
        public static uint ColorToUInt(Color32 color)
        {
            return (uint) (color.r << 24 | color.g << 16 | color.b << 8 | color.a << 0);
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "ShiftExpressionRealShiftCountIsZero")]
        public static Color32 UIntToColor(uint hex)
        {
            var r = (byte) ((hex >> 24) & 0xFF);
            var g = (byte) ((hex >> 16) & 0xFF);
            var b = (byte) ((hex >> 8) & 0xFF);
            var a = (byte) ((hex >> 0) & 0xFF);
            return new Color32(r, g, b, a);
        }
        
        #endregion
        
        #region Instantiate
        
        public static T Instantiate<T>(T prefab, Action<T> beforeAwake) where T : MonoBehaviour
        {
            return Instantiate(prefab, Object.Instantiate, beforeAwake);
        }
        
        public static T Instantiate<T>(T prefab, Vector3 position, Quaternion rotation, Action<T> beforeAwake) where T : MonoBehaviour
        {
            return Instantiate(prefab, p => Object.Instantiate(p, position, rotation), beforeAwake);
        }
        
        public static T Instantiate<T>(T prefab, Transform parent, Action<T> beforeAwake) where T : MonoBehaviour
        {
            return Instantiate(prefab, p => Object.Instantiate(p, parent), beforeAwake);
        }
        
        public static T Instantiate<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent, Action<T> beforeAwake) where T : MonoBehaviour
        {
            return Instantiate(prefab, p => Object.Instantiate(p, position, rotation, parent), beforeAwake);
        }
        
        public static T Instantiate<T>(T prefab, Func<T, T> instantiate, Action<T> beforeAwake) where T : MonoBehaviour
        {
            var isActive = prefab.gameObject.activeSelf;
            prefab.gameObject.SetActive(false);
            
            var obj = instantiate.Invoke(prefab);
            beforeAwake.Invoke(obj);
            
            obj.gameObject.SetActive(isActive);
            prefab.gameObject.SetActive(isActive);
            
            return obj;
        }
        
        #endregion
        
        #region Transient
        
        public static void Transient<T>(T prefab, Action<T> onComplete) where T : Component
        {
            var instance = Object.Instantiate(prefab);
            
            try
            {
                onComplete(instance);
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }
            
            Object.Destroy(instance.gameObject);
        }
        
        public static void Transient(GameObject prefab, Action<GameObject> onComplete)
        {
            var instance = Object.Instantiate(prefab);
            
            try
            {
                onComplete(instance);
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }
            
            Object.Destroy(instance.gameObject);
        }
        
        #endregion
        
        #region Action queue
        
        public static IEnumerator AsCoroutine(Action<Action> action)
        {
            var isComplete = new RefValue<bool>();
            
            action(() =>
            {
                isComplete.value = true;
            });
            
            while (!isComplete.value)
                yield return new WaitForEndOfFrame();
        }
        
        public static IEnumerator RunParallel(IReadOnlyList<IEnumerator> routines)
        {
            bool moveNext;
            do
            {
                moveNext = false;
                
                foreach (var routine in routines)
                    moveNext |= routine.MoveNext();
                
                yield return new WaitForEndOfFrame();
            } while (moveNext);
        }
        
        public static IEnumerator RunSequentially(IEnumerable<IEnumerator> routines)
        {
            foreach (var routine in routines)
            {
                while (routine.MoveNext())
                    yield return new WaitForEndOfFrame();
            }
        }
        
        #endregion
        
        public static IEnumerable<T> GetEnumValues<T>()
        {
            try
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                
                return new List<T>();
            }
        }
        
        public static bool MakeSinglet<T>(T obj, bool useDontDestroyOnLoad = true) where T : MonoBehaviour
        {
            var oo = Object.FindObjectsOfType<T>();
            if (oo.Any(o => o != obj))
            {
                Object.DestroyImmediate(obj.gameObject);
                return false;
            }
            
            if (useDontDestroyOnLoad && Application.isPlaying)
                Object.DontDestroyOnLoad(obj.gameObject);
            
            return true;
        }
        
        public static void Resize(Renderer obj, Vector2 size)
        {
            var bounds = obj.bounds;
            var t = obj.transform;
            var localScale = t.localScale;
            var scale = new Vector3(localScale.x / bounds.size.x, localScale.y / bounds.size.y, localScale.z / bounds.size.z);
            localScale = new Vector3(size.x * scale.x, size.y * scale.y, localScale.z);
            t.localScale = localScale;
        }
        
        public static Texture2D MakeTexture(RenderTexture rt)
        {
            // Remember currently active render texture
            var currentActiveRt = RenderTexture.active;
            
            // Set the supplied RenderTexture as the active one
            RenderTexture.active = rt;
            
            // Create a new Texture2D and read the RenderTexture image into it
            var tex = new Texture2D(rt.width, rt.height);
            tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
            tex.Apply();
            
            // Restorie previously active render texture
            RenderTexture.active = currentActiveRt;
            
            return tex;
        }
        
        public static Sprite MakeSprite(Texture2D tex, Vector2 pivot)
        {
            return Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), pivot);
        }
        
        public static IEnumerable<T> Repeat<T>(Func<T> selector, int count)
        {
            var result = new List<T>();
            
            for (var i = 0; i < count; i++)
                result.Add(selector());
            
            return result;
        }
        
        public static T CreateInstance<T>(params object[] arguments)
        {
            return (T) CreateInstance(typeof(T), arguments);
        }
        
        public static object CreateInstance(Type type, params object[] arguments)
        {
            return Activator.CreateInstance(type, arguments);
        }
        
        public static bool HasType(string fullName)
        {
            return AppDomain.CurrentDomain.GetAssemblies().Any(assembly => assembly.GetTypes().Any(type => type.FullName == fullName));
        }
    }
    
    public class StringValue : Attribute
    {
        public string Value { get; }
        
        public StringValue(string value)
        {
            Value = value;
        }
        
        public static string GetStringValue(Enum value)
        {
            var output = string.Empty;
            var type = value.GetType();
            var fi = type.GetField(value.ToString());
            if (fi.GetCustomAttributes(typeof(StringValue), false) is StringValue[] attrs && attrs.Length > 0)
            {
                output = attrs[0].Value;
            }
            
            return output;
        }
    }
    
    public class Singleton<T> where T : Singleton<T>, new()
    {
        static T __instance;
        
        public static T Instance => __instance ??= Utils.CreateInstance<T>();
    }
    
    public class EventArgs<T1, T2> : EventArgs
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public T1 Arg1 { get; }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public T2 Arg2 { get; }
        
        public EventArgs(T1 arg1, T2 arg2)
        {
            Arg1 = arg1;
            Arg2 = arg2;
        }
    }
    
    [DebuggerNonUserCode]
    public sealed class WeakEventHandler<TEventArgs> where TEventArgs : EventArgs
    {
        readonly WeakReference targetReference;
        readonly MethodInfo method;
        
        public WeakEventHandler(EventHandler<TEventArgs> callback)
        {
            method = callback.Method;
            targetReference = new WeakReference(callback.Target, true);
        }
        
        public void Handler(TEventArgs e)
        {
            Handler(this, e);
        }
        
        public void Handler(object sender, TEventArgs e)
        {
            var target = targetReference.Target;
            if (target != null)
            {
                var callback = (Action<object, TEventArgs>) Delegate.CreateDelegate(typeof(Action<object, TEventArgs>), target, method, true);
                callback?.Invoke(sender, e);
            }
        }
    }
    
    #region Unity Extensions
    
    public static class UriExtensions
    {
        public static Uri Append(this Uri source, params string[] paths)
        {
            return new Uri(paths.Aggregate(source.AbsoluteUri, (current, path) => $"{current.TrimEnd('/')}/{path.TrimStart('/')}"));
        }
    }
    
    public static class ArrayExtensions
    {
        public static T[] Concat<T>(this T[] source, params T[][] list)
        {
            var result = new T[source.Length + list.Sum(a => a.Length)];
            
            source.CopyTo(result, 0);
            
            var offset = source.Length;
            foreach (var t in list)
            {
                t.CopyTo(result, offset);
                offset += t.Length;
            }
            
            return result;
        }
        
        public static void Refill<T>(this T[] source, IEnumerable<T> items)
        {
            Array.Clear(source, 0, source.Length);
            source.AddRange(items);
        }
        
        public static void Refill<T>(this T[] source, T def = default)
        {
            Array.Clear(source, 0, source.Length);
            for (var i = 0; i < source.Length; i++)
                source[i] = def;
        }
    }
    
    // ReSharper disable once InconsistentNaming
    public static class IListExtension
    {
        sealed class ReadOnlyListWrapper<T> : IReadOnlyList<T>
        {
            public int Count => source.Count;
            
            public T this[int index] => source[index];
            
            readonly IList<T> source;
            
            public ReadOnlyListWrapper(IList<T> source)
            {
                this.source = source;
            }
            
            public IEnumerator<T> GetEnumerator()
            {
                return source.GetEnumerator();
            }
            
            IEnumerator IEnumerable.GetEnumerator()
            {
                return source.GetEnumerator();
            }
        }
        
        public static IReadOnlyCollection<T> AsReadOnly<T>(this IList<T> source)
        {
            return new ReadOnlyListWrapper<T>(source);
        }
        
        public static void Switch<T>(this IList<T> source, int index1, int index2)
        {
            if (index1 < 0 || index1 >= source.Count || index2 < 0 || index2 >= source.Count)
                return;
            
            (source[index1], source[index2]) = (source[index2], source[index1]);
        }
        
        public static void MoveTop<T>(this IList<T> source, int index)
        {
            if (index < 0 || index >= source.Count)
                return;
            
            var item = source[index];
            
            for (var i = index; i > 0; i--)
                source[i] = source[i - 1];
            
            source[0] = item;
        }
        
        public static void MoveBottom<T>(this IList<T> source, int index)
        {
            if (index < 0 || index >= source.Count)
                return;
            
            var item = source[index];
            
            for (var i = index; i < source.Count - 1; i++)
                source[i] = source[i + 1];
            
            source[source.Count - 1] = item;
        }
        
        public static void MoveLeft<T>(this IList<T> source, int count = 1)
        {
            if (source.Count == 0)
                return;
            
            for (var i = 0; i < count; i++)
            {
                var first = source[0];
                for (var j = 0; j < source.Count - 1; j++)
                    source[j] = source[j + 1];
                source[source.Count - 1] = first;
            }
        }
        
        public static void MoveRight<T>(this IList<T> source, int count = 1)
        {
            if (source.Count == 0)
                return;
            
            for (var i = 0; i < count; i++)
            {
                var last = source[source.Count - 1];
                for (var j = source.Count - 1; j > 0; j--)
                    source[j] = source[j - 1];
                source[0] = last;
            }
        }
        
        public static IList<T> Shuffle<T>(this IList<T> source, bool isUnaltered = false)
        {
            var newList = isUnaltered ? source.ToList() : source;
            
            for (var i = newList.Count - 1; i > 0; i--)
            {
                var j = Utils.RandomRange(0, i);
                (newList[i], newList[j]) = (newList[j], newList[i]);
            }
            
            return newList;
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T Remove<T>(this IList<T> source, Func<T, bool> predicate, T def = default)
        {
            var index = source.FindIndex(predicate);
            if (index < 0)
                return def;
            
            var item = source[index];
            source.RemoveAt(index);
            return item;
        }
        
        public static void Refill<T>(this IList<T> source, IEnumerable<T> items)
        {
            source.Clear();
            source.AddRange(items);
        }
    }
    
    // ReSharper disable once InconsistentNaming
    public static class IReadOnlyListExtensions
    {
        public static int RandomIndex<T>(this IReadOnlyList<T> source)
        {
            return UnityEngine.Random.Range(0, source.Count);
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T RandomOrDefault<T>(this IReadOnlyList<T> source, T def = default)
        {
            return source.Count != 0 ? source[RandomIndex(source)] : def;
        }
        
        public static IEnumerable<T> Random<T>(this IReadOnlyList<T> source, int count, bool isUnique = true)
        {
            var items = new List<T>();
            
            if (source.Count == 0)
                return items;
            
            if (!isUnique)
            {
                for (var i = 0; i < count; i++)
                    items.Add(source.RandomOrDefault());
                return items;
            }
            
            var tmpItems = new List<T>();
            tmpItems.AddRange(source);
            
            while (tmpItems.Count > 0 && items.Count < count)
            {
                var index = RandomIndex(tmpItems);
                items.Add(tmpItems[index]);
                tmpItems.RemoveAt(index);
            }
            
            return items;
        }
        
        public static IEnumerable<int> RandomIndex<T>(this IReadOnlyList<T> source, int count, bool isUnique = true)
        {
            var items = new List<int>();
            
            if (source.Count == 0)
                return items;
            
            if (!isUnique)
            {
                for (var i = 0; i < count; i++)
                    items.Add(source.RandomIndex());
                return items;
            }
            
            var tmpItems = new List<int>();
            tmpItems.AddRange(Enumerable.Range(0, source.Count));
            
            while (tmpItems.Count > 0 && items.Count < count)
            {
                var index = RandomIndex(tmpItems);
                items.Add(tmpItems[index]);
                tmpItems.RemoveAt(index);
            }
            
            return items;
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T InBoundsOrLastOrDefault<T>(this IReadOnlyList<T> source, int index, T def = default)
        {
            return source.InBounds(index) ? source[index] : source.LastOrDefault() ?? def;
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T InBoundsOrFirstOrDefault<T>(this IReadOnlyList<T> source, int index, T def = default)
        {
            return source.InBounds(index) ? source[index] : source.FirstOrDefault() ?? def;
        }
    }
    
    // ReSharper disable once InconsistentNaming
    public static class IReadOnlyCollectionExtensions
    {
        public static float AverageOrDefault<T>(this IReadOnlyCollection<T> source, Func<T, float> selector, float def = default)
        {
            return source.Count == 0 ? def : source.Average(selector);
        }
        
        public static double AverageOrDefault<T>(this IReadOnlyCollection<T> source, Func<T, int> selector, double def = default)
        {
            return source.Count == 0 ? def : source.Average(selector);
        }
        
        public static double AverageOrDefault<T>(this IReadOnlyCollection<T> source, Func<T, double> selector, double def = default)
        {
            return source.Count == 0 ? def : source.Average(selector);
        }
        
        public static double AverageOrDefault<T>(this IReadOnlyCollection<T> source, Func<T, long> selector, double def = default)
        {
            return source.Count == 0 ? def : source.Average(selector);
        }
        
        public static decimal AverageOrDefault<T>(this IReadOnlyCollection<T> source, Func<T, decimal> selector, decimal def = default)
        {
            return source.Count == 0 ? def : source.Average(selector);
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T RandomByWeightOrDefault<T>(this IReadOnlyCollection<T> source, Func<T, int> weightSelector, T def = default)
        {
            var totalWeight = source.Sum(o => Mathf.Abs(weightSelector(o)));
            var itemWeight = Utils.RandomRange(0, totalWeight);
            
            var currentWeight = 0;
            
            foreach (var item in source.Select(o => new { Value = o, Weight = Mathf.Abs(weightSelector(o)) }).Shuffle())
            {
                currentWeight += item.Weight;
                
                if (currentWeight >= itemWeight)
                    return item.Value;
            }
            
            return def;
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T RandomByWeightOrDefault<T>(this IReadOnlyCollection<T> source, Func<T, float> weightSelector, T def = default)
        {
            var totalWeight = source.Sum(o => Mathf.Abs(weightSelector(o)));
            var itemWeight = Utils.RandomRange(0f, totalWeight);
            
            var currentWeight = 0f;
            
            foreach (var item in source.Select(o => new { Value = o, Weight = Mathf.Abs(weightSelector(o)) }).Shuffle())
            {
                currentWeight += item.Weight;
                
                if (currentWeight >= itemWeight)
                    return item.Value;
            }
            
            return def;
        }
        
        public static IEnumerable<T> Map<T>(this IReadOnlyCollection<T> source, Action<T> updater)
        {
            foreach (var item in source)
                updater(item);
            return source;
        }
    }
    
    // ReSharper disable once InconsistentNaming
    public static class ICollectionExtensions
    {
        sealed class ReadOnlyCollectionWrapper<T> : IReadOnlyCollection<T>
        {
            public int Count => source.Count;
            
            readonly ICollection<T> source;
            
            public ReadOnlyCollectionWrapper(ICollection<T> source)
            {
                this.source = source;
            }
            
            public IEnumerator<T> GetEnumerator()
            {
                return source.GetEnumerator();
            }
            
            IEnumerator IEnumerable.GetEnumerator()
            {
                return source.GetEnumerator();
            }
        }
        
        public static IReadOnlyCollection<T> AsReadOnly<T>(this ICollection<T> source)
        {
            return new ReadOnlyCollectionWrapper<T>(source);
        }
        
        public static void Fill<T>(this ICollection<T> source, int count, T value = default)
        {
            foreach (var item in Enumerable.Repeat(value, count))
                source.Add(item);
        }
        
        public static IEnumerable<T> RemoveAll<T>(this ICollection<T> source, Func<T, bool> predicate)
        {
            var items = source.Where(predicate).ToArray();
            foreach (var item in items)
                source.Remove(item);
            return items;
        }
        
        public static void RemoveAll<T>(this ICollection<T> source, IEnumerable<T> items)
        {
            foreach (var item in items)
                source.Remove(item);
        }
        
        public static void AddRange<T>(this ICollection<T> source, IEnumerable<T> collection)
        {
            if (source is List<T>)
            {
                ((List<T>) source).AddRange(collection);
            }
            else
            {
                foreach (var obj in collection)
                    source.Add(obj);
            }
        }
        
        public static T FirstOrAdd<T>(this ICollection<T> source, Func<T, bool> predicate, Func<T> newItem)
        {
            var item = source.FirstOrDefault(predicate);
            if (item != null)
                return item;
            
            item = newItem();
            source.Add(item);
            
            return item;
        }
        
        public static T LastOrAdd<T>(this ICollection<T> source, Func<T, bool> predicate, Func<T> newItem)
        {
            var item = source.LastOrDefault(predicate);
            if (item != null)
                return item;
            
            item = newItem();
            source.Add(item);
            
            return item;
        }
    }
    
    // ReSharper disable once InconsistentNaming
    public static class IEnumerableExtensions
    {
        sealed class ReadOnlyWrapper<T> : IReadOnlyList<T>
        {
            public int Count
            {
                get
                {
                    var num = 0;
                    using (var enumerator = source.GetEnumerator())
                    {
                        while (enumerator.MoveNext())
                        {
                            checked { ++num; }
                        }
                    }
                    
                    return num;
                }
            }
            
            public T this[int index]
            {
                get
                {
                    var num = 0;
                    using (var enumerator = source.GetEnumerator())
                    {
                        while (enumerator.MoveNext())
                        {
                            if (num == index)
                                return enumerator.Current;
                            
                            checked { ++num; }
                        }
                    }
                    
                    throw new IndexOutOfRangeException($"Wrong index: {index}");
                }
            }
            
            readonly IEnumerable<T> source;
            
            public ReadOnlyWrapper(IEnumerable<T> source)
            {
                this.source = source;
            }
            
            public IEnumerator<T> GetEnumerator()
            {
                return source.GetEnumerator();
            }
            
            IEnumerator IEnumerable.GetEnumerator()
            {
                return source.GetEnumerator();
            }
        }
        
        public static IReadOnlyList<T> AsReadOnly<T>(this IEnumerable<T> source)
        {
            return new ReadOnlyWrapper<T>(source);
        }
        
        public static Queue<T> ToQueue<T>(this IEnumerable<T> source) => new(source);
        
        /// <summary>
        /// Return enumerable with unique elements by key
        /// </summary>
        /// <param name="source">The enumerable to search</param>
        /// <param name="predicate">The expression to test the items against</param>
        /// <returns></returns>
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> source, Func<T, TKey> predicate)
        {
            return source.GroupBy(predicate).Select(grp => grp.First());
        }
        
        ///<summary>Finds the index of the first item matching an expression in an enumerable</summary>
        ///<param name="source">The enumerable to search</param>
        ///<param name="predicate">The expression to test the items against</param>
        ///<returns>The index of the first matching item, or -1 if no items match</returns>
        public static int FindIndex<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            var retVal = 0;
            foreach (var item in source)
            {
                if (predicate(item))
                    return retVal;
                retVal++;
            }
            
            return -1;
        }
        
        ///<summary>Finds the index of the first occurrence of an item in an enumerable</summary>
        ///<param name="source">The enumerable to search</param>
        ///<param name="item">The item to find</param>
        ///<returns>The index of the first matching item, or -1 if the item was not found</returns>
        public static int IndexOf<T>(this IEnumerable<T> source, T item)
        {
            return source.FindIndex(i => EqualityComparer<T>.Default.Equals(item, i));
        }
        
        /// <summary>
        /// Bypasses a specified number of contiguous elements from the end of the sequence and returns the remaining elements.
        /// </summary>
        /// <typeparam name="T">Source sequence element type.</typeparam>
        /// <param name="source">Source sequence.</param>
        /// <param name="count">
        /// The number of elements to skip from the end of the sequence before returning the remaining
        /// elements.
        /// </param>
        /// <returns>Sequence bypassing the specified number of elements counting from the end of the source sequence.</returns>
        public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> source, int count)
        {
            var q = new Queue<T>();
            
            foreach (var x in source)
            {
                q.Enqueue(x);
                
                if (q.Count > count)
                    yield return q.Dequeue();
            }
        }
        
        /// <summary>Perform an action on each item.</summary>
        /// <param name="source">The source.</param>
        /// <param name="action">The action to perform.</param>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var obj in source)
                action(obj);
        }
        
        /// <summary>Check index in array bounds</summary>
        /// <param name="source">Source</param>
        /// <param name="index">check index</param>
        /// <returns></returns>
        public static bool InBounds<T>(this IEnumerable<T> source, int index)
        {
            return index >= 0 && index < source.Count();
        }
        
        public static IEnumerable<T> Concat<T>(this IEnumerable<T> source, IEnumerable<IEnumerable<T>> list)
        {
            foreach (var item in source)
                yield return item;
            
            foreach (var element in list)
            foreach (var subElement in element)
                yield return subElement;
        }
        
        public static IList<T> Shuffle<T>(this IEnumerable<T> source)
        {
            var newList = source.ToList();
            
            for (var i = newList.Count - 1; i > 0; i--)
            {
                var j = Utils.RandomRange(0, i);
                (newList[i], newList[j]) = (newList[j], newList[i]);
            }
            
            return newList;
        }
        
        public static bool Any<T>(this IEnumerable<T> source, IEnumerable<T> list)
        {
            return source.Any(o => list.Any(oo => Equals(o, oo)));
        }
        
        public static bool Any<T>(this IEnumerable<T> source, IEnumerable<T> list, Func<T, T, bool> comparer)
        {
            return source.Any(o => list.Any(oo => comparer(o, oo)));
        }
        
        public static bool Any<T1, T2>(this IEnumerable<T1> source, IEnumerable<T2> list, Func<T1, T2, bool> comparer)
        {
            return source.Any(o => list.Any(oo => comparer(o, oo)));
        }
        
        public static bool All<T>(this IEnumerable<T> source, IEnumerable<T> list)
        {
            return source.All(o => list.All(oo => Equals(o, oo)));
        }
        
        public static bool All<T>(this IEnumerable<T> source, IEnumerable<T> list, Func<T, T, bool> comparer)
        {
            return source.All(o => list.All(oo => comparer(o, oo)));
        }
        
        public static bool All<T1, T2>(this IEnumerable<T1> source, IEnumerable<T2> list, Func<T1, T2, bool> comparer)
        {
            return source.All(o => list.All(oo => comparer(o, oo)));
        }
        
        public static bool AllAndAny<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            var list = source.ToList();
            return list.All(predicate) && list.Any(predicate);
        }
        
        public static bool AllOrAny<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            var list = source.ToList();
            return list.All(predicate) || list.Any(predicate);
        }
        
        public static IEnumerable<int> CountInRows<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            var countInRows = new List<int>();
            
            foreach (var item in source)
            {
                if (predicate(item))
                {
                    if (countInRows.Count == 0)
                        countInRows.Add(0);
                    
                    countInRows[countInRows.Count - 1]++;
                }
                else
                {
                    countInRows.Add(0);
                }
            }
            
            countInRows.RemoveAll(o => o == 0);
            
            return countInRows;
        }
        
        public static IEnumerable<int> CountInRows<T>(this IEnumerable<T> source, T item)
        {
            return source.CountInRows(o => o.Equals(item));
        }
        
        public static int MaxCountInRow<T>(this IEnumerable<T> source, T item)
        {
            try
            {
                return source.CountInRows(o => o.Equals(item)).Max();
            }
            catch (Exception)
            {
                return 0;
            }
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T MaxOrDefault<T>(this IEnumerable<T> source, T def = default)
        {
            try
            {
                return source.Max();
            }
            catch (Exception)
            {
                return def;
            }
        }
    }
    
    public static class EnumExtensions
    {
        public static bool HasFlag<T>(this Enum source, T values) where T : struct, IConvertible
        {
            var current = ((T) Convert.ChangeType(source, typeof(T))).ToInt64(null);
            var mask = values.ToInt64(null);
            
            return (current & mask) == mask;
        }
        
        public static bool InFlag<T>(this Enum source, T values) where T : struct, IConvertible
        {
            var current = ((T) Convert.ChangeType(source, typeof(T))).ToInt64(null);
            var mask = values.ToInt64(null);
            
            return (mask & current) == current;
        }
        
        public static T Next<T>(this Enum source, bool circled = true) where T : struct, IConvertible
        {
            var arr = (T[]) Enum.GetValues(source.GetType());
            var index = Array.IndexOf(arr, source);
            var nextIndex = index + 1 >= arr.Length ? !circled ? index : 0 : index + 1;
            
            return arr[nextIndex];
        }
        
        public static T Prev<T>(this Enum source, bool circled = true) where T : struct, IConvertible
        {
            var arr = (T[]) Enum.GetValues(source.GetType());
            var index = Array.IndexOf(arr, source);
            var prevIndex = index - 1 < 0 ? !circled ? index : arr.Length - 1 : index - 1;
            
            return arr[prevIndex];
        }
    }
    
    public static class HttpRequestHeaderExtensions
    {
        public static string ToHeaderString(this HttpRequestHeader source)
        {
            return source switch
            {
                HttpRequestHeader.CacheControl => "Cache-Control",
                HttpRequestHeader.KeepAlive => "Keep-Alive",
                HttpRequestHeader.TransferEncoding => "Transfer-Encoding",
                HttpRequestHeader.ContentLength => "Content-Length",
                HttpRequestHeader.ContentType => "Content-Type",
                HttpRequestHeader.ContentEncoding => "Content-Encoding",
                HttpRequestHeader.ContentLanguage => "Content-Language",
                HttpRequestHeader.ContentLocation => "Content-Location",
                HttpRequestHeader.ContentMd5 => "Content-MD5",
                HttpRequestHeader.ContentRange => "Content-Range",
                HttpRequestHeader.LastModified => "Last-Modified",
                HttpRequestHeader.AcceptCharset => "Accept-Charset",
                HttpRequestHeader.AcceptEncoding => "Accept-Encoding",
                HttpRequestHeader.AcceptLanguage => "Accept-Language",
                HttpRequestHeader.IfMatch => "If-Match",
                HttpRequestHeader.IfModifiedSince => "If-Modified-Since",
                HttpRequestHeader.IfNoneMatch => "If-None-Match",
                HttpRequestHeader.IfRange => "If-Range",
                HttpRequestHeader.IfUnmodifiedSince => "If-Unmodified-Since",
                HttpRequestHeader.MaxForwards => "Max-Forwards",
                HttpRequestHeader.ProxyAuthorization => "Proxy-Authorization",
                HttpRequestHeader.UserAgent => "User-Agent",
                HttpRequestHeader.Accept => "Accept",
                HttpRequestHeader.Allow => "Allow",
                HttpRequestHeader.Authorization => "Authorization",
                HttpRequestHeader.Connection => "Connection",
                HttpRequestHeader.Cookie => "Cookie",
                HttpRequestHeader.Date => "Date",
                HttpRequestHeader.Expect => "Expect",
                HttpRequestHeader.Expires => "Expires",
                HttpRequestHeader.From => "From",
                HttpRequestHeader.Host => "Host",
                HttpRequestHeader.Pragma => "Pragma",
                HttpRequestHeader.Range => "Range",
                HttpRequestHeader.Referer => "Referer",
                HttpRequestHeader.Te => "Te",
                HttpRequestHeader.Trailer => "Trailer",
                HttpRequestHeader.Translate => "Translate",
                HttpRequestHeader.Upgrade => "Upgrade",
                HttpRequestHeader.Via => "Via",
                HttpRequestHeader.Warning => "Warning",
                _ => source.ToString()
            };
        }
    }
    
    public static class GameObjectExtensions
    {
        public static string GetPath(this GameObject source)
        {
            return source.transform.GetPath();
        }
        
        public static bool IsInLayerMask(this GameObject source, LayerMask mask)
        {
            return mask == (mask | (1 << source.layer));
        }
        
        public static void SetLayerRecursive(this GameObject source, int layer)
        {
            SetLayerInternal(source.transform, layer);
        }
        
        public static IEnumerable<Collider> GetOwnDetectedColliders(this GameObject source)
        {
            var colliders = source.GetComponents<Collider>().ToList();
            
            foreach (Transform o in source.transform)
                GetOwnDetectedCollidersInternal(o, colliders);
            
            return colliders;
        }
        
        public static void Log(this GameObject source, object message)
        {
            Debug.Log($"{source.GetPath()}: {message}");
        }
        
        public static void LogWarning(this GameObject source, object message)
        {
            Debug.LogWarning($"{source.GetPath()}: {message}");
        }
        
        public static void LogError(this GameObject source, object message)
        {
            Debug.LogError($"{source.GetPath()}: {message}");
        }
        
        public static void LogException(this GameObject source, object message)
        {
            Debug.unityLogger.Log(LogType.Exception, $"{source.GetPath()}: {message}");
        }
        
        public static T GetOrAddComponent<T>(this GameObject source) where T : Component
        {
            var component = source.GetComponent<T>();
            if (component == null)
                component = source.AddComponent<T>();
            
            return component;
        }
        
        static void SetLayerInternal(Transform transform, int layer)
        {
            transform.gameObject.layer = layer;
            
            foreach (Transform o in transform)
                SetLayerInternal(o, layer);
        }
        
        static void GetOwnDetectedCollidersInternal(Component obj, List<Collider> colliders)
        {
            if (obj.GetComponent<Rigidbody>())
                return;
            
            colliders.AddRange(obj.GetComponents<Collider>());
            
            foreach (Transform o in obj.transform)
                GetOwnDetectedCollidersInternal(o, colliders);
        }
    }
    
    public static class TransformExtensions
    {
        public static string GetPath(this Transform source)
        {
            if (source.parent == null)
                return source.name;
            return source.parent.GetPath() + "/" + source.name;
        }
        
        public static void Map(this Transform o, Action<Transform> action)
        {
            action(o);
            
            var numChildren = o.transform.childCount;
            for (var i = 0; i < numChildren; ++i)
                o.transform.GetChild(i).Map(action);
        }
        
        public static IEnumerable<Transform> All(this Transform source)
        {
            var numChildren = source.transform.childCount;
            for (var i = 0; i < numChildren; ++i)
            {
                var child = source.transform.GetChild(i);
                
                yield return child;
                
                foreach (var t in child.All())
                    yield return t;
            }
        }
        
        public static Vector3 PositionWithX(this Transform source, float x = 0f)
        {
            var tPos = source.position;
            return new Vector3(x, tPos.y, tPos.z);
        }
        
        public static Vector3 PositionWithY(this Transform source, float y = 0f)
        {
            var tPos = source.position;
            return new Vector3(tPos.x, y, tPos.z);
        }
        
        public static Vector3 PositionWithZ(this Transform source, float z = 0f)
        {
            var tPos = source.position;
            return new Vector3(tPos.x, tPos.y, z);
        }
        
        public static Vector3 PositionWithXY(this Transform source, float x = 0f, float y = 0f)
        {
            var tPos = source.position;
            return new Vector3(x, y, tPos.z);
        }
        
        public static Vector3 PositionWithXZ(this Transform source, float x = 0f, float z = 0f)
        {
            var tPos = source.position;
            return new Vector3(x, tPos.y, 0f);
        }
        
        public static Vector3 PositionWithYZ(this Transform source, float y = 0f, float z = 0f)
        {
            var tPos = source.position;
            return new Vector3(tPos.x, y, z);
        }
        
        public static Vector3 LocalPositionWithX(this Transform source, float x = 0f)
        {
            var tPos = source.localPosition;
            return new Vector3(x, tPos.y, tPos.z);
        }
        
        public static Vector3 LocalPositionWithY(this Transform source, float y = 0f)
        {
            var tPos = source.localPosition;
            return new Vector3(tPos.x, y, tPos.z);
        }
        
        public static Vector3 LocalPositionWithZ(this Transform source, float z = 0f)
        {
            var tPos = source.localPosition;
            return new Vector3(tPos.x, tPos.y, z);
        }
        
        public static Vector3 LocalPositionWithXY(this Transform source, float x = 0f, float y = 0f)
        {
            var tPos = source.localPosition;
            return new Vector3(x, y, tPos.z);
        }
        
        public static Vector3 LocalPositionWithXZ(this Transform source, float x = 0f, float z = 0f)
        {
            var tPos = source.localPosition;
            return new Vector3(x, tPos.y, 0f);
        }
        
        public static Vector3 LocalPositionWithYZ(this Transform source, float y = 0f, float z = 0f)
        {
            var tPos = source.localPosition;
            return new Vector3(tPos.x, y, z);
        }
        
        public static Vector3 LocalScaleWithX(this Transform source, float x = 0f)
        {
            var tScale = source.localScale;
            return new Vector3(x, tScale.y, tScale.z);
        }
        
        public static Vector3 LocalScaleWithY(this Transform source, float y = 0f)
        {
            var tScale = source.localScale;
            return new Vector3(tScale.x, y, tScale.z);
        }
        
        public static Vector3 LocalScaleWithZ(this Transform source, float z = 0f)
        {
            var tScale = source.localScale;
            return new Vector3(tScale.x, tScale.y, z);
        }
        
        public static Vector3 LocalScaleWithXY(this Transform source, float x = 0f, float y = 0f)
        {
            var tScale = source.localScale;
            return new Vector3(x, y, tScale.z);
        }
        
        public static Vector3 LocalScaleWithXZ(this Transform source, float x = 0f, float z = 0f)
        {
            var tScale = source.localScale;
            return new Vector3(x, tScale.y, 0f);
        }
        
        public static Vector3 LocalScaleWithYZ(this Transform source, float y = 0f, float z = 0f)
        {
            var tScale = source.localScale;
            return new Vector3(tScale.x, y, z);
        }
        
        public static Vector3 LossyScaleWithX(this Transform source, float x = 0f)
        {
            var tLScale = source.lossyScale;
            return new Vector3(x, tLScale.y, tLScale.z);
        }
        
        public static Vector3 LossyScaleWithY(this Transform source, float y = 0f)
        {
            var tLScale = source.lossyScale;
            return new Vector3(tLScale.x, y, tLScale.z);
        }
        
        public static Vector3 LossyScaleWithZ(this Transform source, float z = 0f)
        {
            var tLScale = source.lossyScale;
            return new Vector3(tLScale.x, tLScale.y, z);
        }
        
        public static Vector3 LossyScaleWithXY(this Transform source, float x = 0f, float y = 0f)
        {
            var tLScale = source.lossyScale;
            return new Vector3(x, y, tLScale.z);
        }
        
        public static Vector3 LossyScaleWithXZ(this Transform source, float x = 0f, float z = 0f)
        {
            var tLScale = source.lossyScale;
            return new Vector3(x, tLScale.y, 0f);
        }
        
        public static Vector3 LossyScaleWithYZ(this Transform source, float y = 0f, float z = 0f)
        {
            var tLScale = source.lossyScale;
            return new Vector3(tLScale.x, y, z);
        }
    }
    
    public static class RectTransformExtensions
    {
        public static void SetPivot(this RectTransform source, Vector2 pivot)
        {
            var rect = source.rect;
            
            var offsetPivot = pivot - source.pivot;
            
            offsetPivot.Scale(rect.size);
            
            var position = source.position + source.TransformVector(offsetPivot);
            
            source.pivot = pivot;
            
            source.position = position;
        }
    }
    
    public static class SceneExtensions
    {
        [CanBeNull]
        public static T FindObjectOfType<T>(this Scene source, bool includeInactive = false)
        {
            return source.GetRootGameObjects().Select(o => o.GetComponentInChildren<T>(includeInactive)).FirstOrDefault(o => o != null);
        }
        
        public static T[] FindObjectsOfType<T>(this Scene source, bool includeInactive = false)
        {
            return source.GetRootGameObjects().SelectMany(o => o.GetComponentsInChildren<T>(includeInactive)).ToArray();
        }
        
        public static GameObject[] FindGameObjects(this Scene source, bool includeInactive = false)
        {
            return source.GetRootGameObjects().SelectMany(o => o.GetComponentsInChildren<Transform>(includeInactive)).Select(o => o.gameObject).ToArray();
        }
    }
    
    public static class Color32Extensions
    {
        public static string ToHex(this Color32 source, string prefix = "")
        {
            return prefix + source.r.ToString("X2") + source.g.ToString("X2") + source.b.ToString("X2") + source.a.ToString("X2");
        }
    }
    
    public static class ColorExtensions
    {
        public static string ToHex(this Color source, string prefix = "")
        {
            return ((Color32) source).ToHex(prefix);
        }
    }
    
    public static class IDictionaryExtension
    {
        public static bool TryGetKey<TKey, TValue>(this IDictionary<TKey, TValue> source, TValue value, out TKey key) where TValue : IEquatable<TValue>
        {
            foreach (var pair in source)
            {
                if (value.Equals(pair.Value))
                {
                    key = pair.Key;
                    return true;
                }
            }
            
            key = default;
            return false;
        }
        
        public static bool TryFindKey<TKey, TValue>(this IDictionary<TKey, TValue> source, Func<TValue, bool> predicate, out TKey key)
        {
            foreach (var pair in source)
            {
                if (predicate(pair.Value))
                {
                    key = pair.Key;
                    return true;
                }
            }
            
            key = default;
            return false;
        }
        
        public static bool TryFindValue<TKey, TValue>(this IDictionary<TKey, TValue> source, Func<TKey, bool> predicate, out TValue value)
        {
            foreach (var pair in source)
            {
                if (predicate(pair.Key))
                {
                    value = pair.Value;
                    return true;
                }
            }
            
            value = default;
            return false;
        }
        
        public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key, TValue value)
        {
            if (source.ContainsKey(key))
                return false;
            
            source.Add(key, value);
            return true;
        }
        
        public static void Refill<TKey, TValue>(this IDictionary<TKey, TValue> source, IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            source.Clear();
            foreach (var pair in items)
                source.TryAdd(pair.Key, pair.Value);
        }
    }
    
    public static class GenericExtensions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "UnusedParameter.Global")]
        public static Type GetDeclaredType<T>([CanBeNull] this T _)
        {
            return typeof(T);
        }
        
        public static string GetDeclaredTypeName<T>([CanBeNull] this T obj)
        {
            return obj.GetDeclaredType().FullName;
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T DeepCloneJson<T>(this T obj, T def = default)
        {
            return Serialization.Utils.TryDeepCloneJson(obj, out var value) ? value : def;
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T DeepClone<T>(this T obj, T def = default)
        {
            return Serialization.Utils.TryDeepClone(obj, out var value) ? value : def;
        }
    }
    
    public static class Vector2Extensions
    {
        public static Vector3 xy(this Vector2 source) => new(source.x, source.y, 0f);
        public static Vector3 xz(this Vector2 source) => new(source.x, 0f, source.y);
        public static Vector3 yz(this Vector2 source) => new(0f, source.x, source.y);
        public static Vector2 flip(this Vector2 source) => new(source.y, source.x);
    }
    
    public static class AnimatorExtensions
    {
        public static IEnumerator Play(this Animator source, int animNameHash, Action onComplete) => Play(source, animNameHash, 0, onComplete);
        
        public static IEnumerator Play(this Animator source, int animNameHash, int layer, Action onComplete)
        {
            source.Play(animNameHash, layer);
            
            while (source.GetCurrentAnimatorStateInfo(layer).shortNameHash != animNameHash)
                yield return new WaitForEndOfFrame();
            
            while (source.GetCurrentAnimatorStateInfo(layer).shortNameHash == animNameHash && source.GetCurrentAnimatorStateInfo(layer).normalizedTime < 1f)
                yield return new WaitForEndOfFrame();
            
            onComplete();
        }
    }
    
#if UNITY_EDITOR
    public static class AnimatorControllerExtensions
    {
        public static IEnumerable<string> GetLayerNames(this UnityEditor.Animations.AnimatorController ac)
        {
            return ac.layers.Select(o => o.name);
        }
        
        public static IEnumerable<string> GetStateNames(this UnityEditor.Animations.AnimatorController ac)
        {
            return ac.layers.SelectMany(o => o.stateMachine.states.Select(oo => oo.state.name));
        }
        
        public static IEnumerable<string> GetStateNames(this UnityEditor.Animations.AnimatorController ac, int layerIndex)
        {
            var values = new List<string>();
            
            if (!ac.layers.InBounds(layerIndex))
                return values;
            
            var layer = ac.layers[layerIndex];
            
            values.AddRange(layer.stateMachine.states.Select(o => o.state.name));
            
            return values;
        }
        
        public static IEnumerable<string> GetClipNames(this UnityEditor.Animations.AnimatorController ac)
        {
            return ac.animationClips.Select(o => o.name);
        }
        
        public static IEnumerable<string> GetParameterNames(this UnityEditor.Animations.AnimatorController ac)
        {
            return ac.parameters.Select(o => o.name);
        }
    }
#endif
    
    public static class TypeExtensions
    {
        public static bool IsInterfaceOf<T>(this Type type)
        {
            return type.GetInterfaces().Contains(typeof(T));
        }
    }
    
    public static class StringExtension
    {
        [ContractAnnotation("str:null=>true")]
        public static bool IsNullOrEmpty([CanBeNull] this string str)
        {
            return string.IsNullOrEmpty(str);
        }
        
        public static string Or([CanBeNull] this string str, string s)
        {
            return !string.IsNullOrEmpty(str) ? str : s;
        }
        
        public static void CopyToClipboard(this string str)
        {
            GUIUtility.systemCopyBuffer = str;
        }
        
        public static string Clamp(this string str, int startIndex, int stopIndex)
        {
            if (str.IsNullOrEmpty())
                return str;
            if (str.Length < startIndex)
                return string.Empty;
            if (str.Length < startIndex + stopIndex)
                return str.Substring(startIndex, str.Length - startIndex);
            return str.Substring(startIndex, stopIndex);
        }
        
        public static string Clamp(this string str, int length)
        {
            return str.IsNullOrEmpty() ? str : str.Substring(0, Mathf.Min(str.Length, length));
        }
        
        public static IEnumerable<string> Split(this string str, int count)
        {
            if (count <= 0)
                yield break;
            
            var chunkLength = Mathf.CeilToInt(Utils.SafeDivide(str.Length, count));
            
            for (var i = 0; i < count; i++)
            {
                var startIndex = Mathf.Max(Mathf.Min(i * chunkLength, str.Length), 0);
                var length = Mathf.Max(Mathf.Min(chunkLength, str.Length - (i * chunkLength)), 0);
                
                yield return str.Substring(startIndex, length);
            }
        }
        
        public static string EscapeURL(this string str)
        {
            return UnityWebRequest.EscapeURL(str).Replace("+", "%20");
        }
        
        public static int ParametersCount(this string str)
        {
            const string pattern = @"(?<!\{)(?>\{\{)*\{\d(.*?)";
            var mc = Regex.Matches(str, pattern);
            var matches = mc.OfType<Match>().ToList();
            var uniqueMatchCount = matches.Select(o => o.Value).Distinct().Count();
            var parameterMatchCount = uniqueMatchCount == 0 ? 0 : matches.Select(o => o.Value).Distinct().Select(o => int.Parse(o.Replace("{", string.Empty))).Max() + 1;
            
            return parameterMatchCount;
        }
        
        public static string ReplaceRegex(this string str, string pattern, string replacement)
        {
            return Regex.Replace(str, pattern, replacement);
        }
        
        public static string ToLowerFirstChar(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            
            return char.ToLower(str[0]) + str[1..];
        }
    }
    
    public static class CustomYieldInstructionExtension
    {
        public static Task<T> ToTask<T>(this T yield, bool isStarted = true) where T : CustomYieldInstruction
        {
            var task = new Task<T>(() =>
            {
                while (yield.keepWaiting)
                    System.Threading.Thread.Sleep(100);
                
                return yield;
            });
            
            if (isStarted)
                task.Start();
            
            return task;
        }
    }
    
    #endregion
}