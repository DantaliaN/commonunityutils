﻿namespace ONiGames.Utilities
{
    public class RefValue<T> where T : struct
    {
        public T value;

        public RefValue(T initialValue = default)
        {
            value = initialValue;
        }
    }
}