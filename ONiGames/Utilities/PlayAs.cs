namespace ONiGames.Utilities
{
    public static class PlayAs
    {
        public static bool IsEditor
        {
            get
            {
#if UNITY_EDITOR
                return true;
#else
                return false;
#endif
            }
        }
        
        public static bool IsDevelopmentBuild
        {
            get
            {
#if DEVELOPMENT_BUILD
                return true;
#else
                return false;
#endif
            }
        }
        
        public static bool IsAndroid
        {
            get
            {
#if UNITY_ANDROID
                return true;
#else
                return false;
#endif
            }
        }
        
        public static bool IsIOS
        {
            get
            {
#if UNITY_IOS
                return true;
#else
                return false;
#endif
            }
        }
    }
}