using System;
using System.Collections.Generic;

namespace ONiGames.Utilities
{
    public class DisposableActionList
    {
        readonly Dictionary<int, Action> disposableActions = new();

        public bool CanInvoke => disposableActions.Count > 0;

        public int Count => disposableActions.Count;

        public DisposableActionList() { }

        public DisposableActionList(IEnumerable<Action> actions)
        {
            Set(actions);
        }

        public DisposableActionList(IEnumerable<KeyValuePair<int, Action>> pairs)
        {
            Set(pairs);
        }

        public void Set(IEnumerable<Action> actions)
        {
            disposableActions.Clear();

            var index = 0;
            foreach (var action in actions)
                disposableActions.Add(index++, action);
        }

        public void Set(IEnumerable<KeyValuePair<int, Action>> pairs)
        {
            disposableActions.Refill(pairs);
        }

        public void Invoke(int index)
        {
            if (!disposableActions.ContainsKey(index))
                return;
            
            var action = disposableActions[index];
            disposableActions.Clear();
            action?.Invoke();
        }
    }
}
