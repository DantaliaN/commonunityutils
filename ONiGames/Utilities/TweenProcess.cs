﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class TweenProcess<T>
    {
        public bool IsStarted { get; private set; }
        public bool IsFinished { get; private set; }
        public float Progress { get; private set; }

        readonly T initialValue;
        readonly T targetValue;

        readonly float time;

        readonly Func<T, T, float, T> interpolator;
        readonly AnimationCurve animationCurve;

        [CanBeNull]
        readonly Action started;

        [CanBeNull]
        readonly Action<T> update;

        [CanBeNull]
        readonly Action finished;

        float timer = 0f;

        public TweenProcess(T initialValue, T targetValue, Func<T, T, float, T> interpolator, float time, [CanBeNull] AnimationCurve animationCurve = null, [CanBeNull] Action started = null, [CanBeNull] Action<T> update = null, [CanBeNull] Action finished = null)
        {
            this.initialValue = initialValue;
            this.targetValue = targetValue;
            this.interpolator = interpolator;

            this.time = time;

            this.animationCurve = animationCurve ?? AnimationCurve.Linear(0f, 0f, 1f, 1f);

            this.started = started;
            this.update = update;
            this.finished = finished;
        }

        public void Update(float deltaTime)
        {
            if (IsFinished)
                return;

            timer += deltaTime;

            if (!IsStarted)
            {
                started?.Invoke();
                IsStarted = true;
            }

            Progress = Mathf.Clamp01(time > 0f ? timer / time : 1f);
            var interpolationValue = animationCurve.Evaluate(Progress);

            var newValue = interpolator.Invoke(initialValue, targetValue, interpolationValue);
            update?.Invoke(newValue);

            if (timer > time)
                Stop();
        }

        public void Stop(bool callFinish = true)
        {
            IsFinished = true;

            if (callFinish)
                finished?.Invoke();
        }
    }
}