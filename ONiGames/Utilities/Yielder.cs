using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class Yielder : MonoBehaviour
    {
        void OnDestroy()
        {
            StopAllCoroutines();
        }

        public void Wait(IEnumerator instruction, Action onComplete)
        {
            StartCoroutine(YieldRunner(instruction, onComplete));
        }

        public void Wait<T>(IEnumerator<T> instruction, Action<T> onComplete)
        {
            StartCoroutine(YieldRunner(instruction, onComplete));
        }

        public void Wait(YieldInstruction instruction, Action onComplete)
        {
            StartCoroutine(YieldRunner(instruction, onComplete));
        }

        public void Wait(CustomYieldInstruction instruction, Action onComplete)
        {
            StartCoroutine(YieldRunner(instruction, onComplete));
        }

        public void Wait<T>(T instruction, Action<T> onComplete) where T : CustomYieldInstruction
        {
            StartCoroutine(YieldRunner(instruction, onComplete));
        }

        static IEnumerator YieldRunner(IEnumerator instruction, Action onComplete)
        {
            yield return instruction;
            onComplete();
        }

        static IEnumerator YieldRunner<T>(IEnumerator<T> instruction, Action<T> onComplete)
        {
            yield return instruction;
            onComplete(instruction.Current);
        }

        static IEnumerator YieldRunner(YieldInstruction instruction, Action onComplete)
        {
            yield return instruction;
            onComplete();
        }

        static IEnumerator YieldRunner(CustomYieldInstruction instruction, Action onComplete)
        {
            yield return instruction;
            onComplete();
        }

        static IEnumerator YieldRunner<T>(T instruction, Action<T> onComplete) where T : CustomYieldInstruction
        {
            yield return instruction;
            onComplete(instruction);
        }
    }
}