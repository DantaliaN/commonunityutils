using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class Cacher
    {
        static readonly Dictionary<int, Cacher> Cachers = new();

        readonly WeakReference<GameObject> owner;

        readonly Dictionary<Type, List<WeakReference<Component>>> caches = new();

        public Cacher(GameObject owner) => this.owner = new WeakReference<GameObject>(owner);

        public static Cacher Take(GameObject owner)
        {
            var key = owner.GetHashCode();

            if (!Cachers.TryGetValue(key, out var cacher))
            {
                cacher = new Cacher(owner);
                Cachers.Add(key, cacher);
            }

            return cacher;
        }

        public static void Clear(GameObject owner)
        {
            var key = owner.GetHashCode();

            if (Cachers.TryGetValue(key, out var cacher))
                cacher.Clear();
        }

        [CanBeNull]
        public T GetComponent<T>(bool canBeNull = false) where T : Component
        {
            if (!owner.TryGetTarget(out var gameObject))
                return null;

            var key = typeof(T);

            if (!caches.TryGetValue(key, out var components))
            {
                components = new List<WeakReference<Component>>();
                caches.Add(key, components);
            }

            var component = components.FirstOrDefault();
            if (component == null)
            {
                component = new WeakReference<Component>(gameObject.GetComponent<T>());
                components.Add(component);
            }

            if (!component.TryGetTarget(out var target) && !canBeNull)
            {
                target = gameObject.AddComponent<T>();
                component.SetTarget(target);
            }

            return target as T;
        }

        public IEnumerable<T> GetComponents<T>() where T : Component
        {
            if (!owner.TryGetTarget(out var gameObject))
                return null;

            var key = typeof(T);

            if (!caches.TryGetValue(key, out var components))
            {
                components = new List<WeakReference<Component>>(gameObject.GetComponents<T>().Select(o => new WeakReference<Component>(o)));
                caches.Add(key, components);
            }

            return components.Select(o => o.TryGetTarget(out var target) ? target as T : null).Where(o => o != null);
        }

        public void Clear()
        {
            caches.Clear();
        }

        public void Clear<T>()
        {
            var key = typeof(T);

            if (caches.ContainsKey(key))
                caches.Remove(key);
        }
    }
}