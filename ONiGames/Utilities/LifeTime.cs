using UnityEngine;

namespace ONiGames.Utilities
{
    public class LifeTime : MonoBehaviour
    {
        [SerializeField]
        float time = 1f;
        
        [SerializeField]
        bool unscaledTime = false;
        
        float timer = 0f;
        
        void Update()
        {
            timer += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            
            if (timer > time)
            {
                Destroy(gameObject);
            }
        }
    }
}
