using System;
using JetBrains.Annotations;

namespace ONiGames.Utilities
{
    public class DisposableAction
    {
        [CanBeNull]
        Action disposableAction = null;

        public bool CanInvoke => disposableAction != null;

        public DisposableAction() { }

        public DisposableAction(Action action)
        {
            Set(action);
        }

        public void Set(Action action)
        {
            disposableAction = action;
        }

        public void Invoke()
        {
            var action = disposableAction;
            disposableAction = null;
            action?.Invoke();
        }
    }

    public class DisposableAction<T>
    {
        [CanBeNull]
        Action<T> disposableAction = null;

        public bool CanInvoke => disposableAction != null;

        public DisposableAction() { }

        public DisposableAction(Action<T> action)
        {
            Set(action);
        }

        public void Set(Action<T> action)
        {
            disposableAction = action;
        }

        public void Invoke(T arg)
        {
            var action = disposableAction;
            disposableAction = null;
            action?.Invoke(arg);
        }
    }

    public class DisposableAction<T1, T2>
    {
        [CanBeNull]
        Action<T1, T2> disposableAction = null;

        public bool CanInvoke => disposableAction != null;

        public DisposableAction() { }

        public DisposableAction(Action<T1, T2> action)
        {
            Set(action);
        }

        public void Set(Action<T1, T2> action)
        {
            disposableAction = action;
        }

        public void Invoke(T1 arg1, T2 arg2)
        {
            var action = disposableAction;
            disposableAction = null;
            action?.Invoke(arg1, arg2);
        }
    }

    public class DisposableAction2
    {
        [CanBeNull]
        Action disposableAction1 = null;

        [CanBeNull]
        Action disposableAction2 = null;

        public bool CanInvoke => disposableAction1 != null && disposableAction2 != null;

        public DisposableAction2() { }

        public DisposableAction2(Action action1, Action action2)
        {
            Set(action1, action2);
        }

        public void Set(Action action1, Action action2)
        {
            disposableAction1 = action1;
            disposableAction2 = action2;
        }

        public void Invoke1()
        {
            var action = disposableAction1;
            disposableAction1 = null;
            disposableAction2 = null;
            action?.Invoke();
        }

        public void Invoke2()
        {
            var action = disposableAction2;
            disposableAction1 = null;
            disposableAction2 = null;
            action?.Invoke();
        }
    }

    public class DisposableAction2<T1>
    {
        [CanBeNull]
        Action<T1> disposableAction1 = null;

        [CanBeNull]
        Action disposableAction2 = null;

        public bool CanInvoke => disposableAction1 != null && disposableAction2 != null;

        public DisposableAction2() { }

        public DisposableAction2(Action<T1> action1, Action action2)
        {
            Set(action1, action2);
        }

        public void Set(Action<T1> action1, Action action2)
        {
            disposableAction1 = action1;
            disposableAction2 = action2;
        }

        public void Invoke1(T1 arg)
        {
            var action = disposableAction1;
            disposableAction1 = null;
            disposableAction2 = null;
            action?.Invoke(arg);
        }

        public void Invoke2()
        {
            var action = disposableAction2;
            disposableAction1 = null;
            disposableAction2 = null;
            action?.Invoke();
        }
    }

    public class DisposableAction2<T1, T2>
    {
        [CanBeNull]
        Action<T1> disposableAction1 = null;

        [CanBeNull]
        Action<T2> disposableAction2 = null;

        public bool CanInvoke => disposableAction1 != null && disposableAction2 != null;

        public DisposableAction2() { }

        public DisposableAction2(Action<T1> action1, Action<T2> action2)
        {
            Set(action1, action2);
        }

        public void Set(Action<T1> action1, Action<T2> action2)
        {
            disposableAction1 = action1;
            disposableAction2 = action2;
        }

        public void Invoke1(T1 arg)
        {
            var action = disposableAction1;
            disposableAction1 = null;
            disposableAction2 = null;
            action?.Invoke(arg);
        }

        public void Invoke2(T2 arg)
        {
            var action = disposableAction2;
            disposableAction1 = null;
            disposableAction2 = null;
            action?.Invoke(arg);
        }
    }
}