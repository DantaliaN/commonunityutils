using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities
{
    [RequireComponent(typeof(Collider2D))]
    public abstract class WholeObjectTrigger2D<T> : MonoBehaviour where T : Component
    {
        public event Action<T> Enter = delegate { };
        public event Action<T> Stay = delegate { };
        public event Action<T> Exit = delegate { };

        public T[] Objects => objects.Keys.Where(o => o != null).ToArray();

        readonly Dictionary<T, List<Collider2D>> objects = new();

        Collider2D _collider2D;
        Collider2D Collider2D => _collider2D ??= GetComponent<Collider2D>();

        void Start()
        {
            //force make trigger, b/c we don't listen OnCollision events
            Collider2D.isTrigger = true;
        }

        void Update()
        {
            foreach (var pair in objects)
            {
                for (var i = pair.Value.Count - 1; i >= 0; i--)
                {
                    if (pair.Value[i] == null)
                    {
                        pair.Value.RemoveAt(i);
                    }
                }
            }
        }

        void OnDisable()
        {
            Clear();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            var obj = GetObject(other);
            if (obj != null)
            {
                if (!objects.ContainsKey(obj))
                {
                    objects.Add(obj, new List<Collider2D>());
                    objects[obj].Add(other);

                    Enter.Invoke(obj);
                }
                else if (!objects[obj].Contains(other))
                {
                    objects[obj].Add(other);
                }
            }
        }

        void OnTriggerStay2D(Collider2D other)
        {
            var obj = GetObject(other);
            if (obj != null)
            {
                if (!objects.ContainsKey(obj))
                {
                    objects.Add(obj, new List<Collider2D>());
                    objects[obj].Add(other);

                    Enter.Invoke(obj);
                }
                else if (!objects[obj].Contains(other))
                {
                    objects[obj].Add(other);
                }
            }

            var firstObj = objects.FirstOrDefault(i => i.Value.Contains(other));
            if (firstObj.Key != null)
            {
                Stay.Invoke(firstObj.Key);
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            var obj = objects.FirstOrDefault(i => i.Value.Contains(other));
            if (obj.Key != null)
            {
                obj.Value.Remove(other);

                if (obj.Value.Count == 0)
                {
                    objects.Remove(obj.Key);

                    Exit.Invoke(obj.Key);
                }
            }
        }

        public void Clear()
        {
            objects.Clear();
        }

        [CanBeNull]
        protected abstract T GetObject(Collider2D other);
    }
}