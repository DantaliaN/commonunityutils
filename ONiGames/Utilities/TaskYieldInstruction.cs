using System;
using System.Threading.Tasks;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class TaskYieldInstruction : CustomYieldInstruction
    {
        public override bool keepWaiting
        {
            get
            {
                if (task.Exception != null)
                    throw task.Exception;

                return !task.IsCompleted;
            }
        }

        readonly Task task;

        public TaskYieldInstruction(Task task)
        {
            this.task = task;
        }
    }

    public class TaskYieldInstruction<T> : CustomYieldInstruction
    {
        public override bool keepWaiting
        {
            get
            {
                if (task.Exception != null)
                    throw task.Exception;

                return !task.IsCompleted;
            }
        }

        public T Result => task.Result;

        readonly Task<T> task;

        public TaskYieldInstruction(Task<T> task)
        {
            this.task = task;
        }
    }

    public static class TaskYieldInstructionExtension
    {
        public static TaskYieldInstruction AsCoroutine(this Task task)
        {
            if (task == null)
                throw new NullReferenceException(nameof(task));

            return new TaskYieldInstruction(task);
        }

        public static TaskYieldInstruction<T> AsCoroutine<T>(this Task<T> task)
        {
            if (task == null)
                throw new NullReferenceException(nameof(task));

            return new TaskYieldInstruction<T>(task);
        }
    }
}