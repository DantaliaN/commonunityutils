﻿using System;

namespace ONiGames.Utilities
{
    [Serializable]
    public class TimerKeeper
    {
        public float time = 0f;

        public bool IsComplete => currentTime >= time;

        float currentTime = 0f;

        public bool ComplexUpdate(float deltaTime, bool needReset = true)
        {
            Update(deltaTime);
            
            var isComplete = IsComplete;
            
            if (needReset && isComplete)
                Reset();
            
            return isComplete;
        }

        public void Update(float deltaTime)
        {
            currentTime += deltaTime;
        }

        public void Reset()
        {
            currentTime = 0.0f;
        }
    }
}