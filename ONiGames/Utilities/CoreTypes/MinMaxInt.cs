using System;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public struct MinMaxInt
    {
        [SerializeField]
        int min;

        [SerializeField]
        int max;

        public int Min
        {
            get => min;
            set => min = value > max ? max : value;
        }

        public int Max
        {
            get => max;
            set => max = value < min ? min : value;
        }

        public MinMaxInt(int min, int max)
        {
            this.min = min < max ? min : max;
            this.max = max > min ? max : min;
        }

        public static MinMaxInt operator *(MinMaxInt minMax, int value)
        {
            return new MinMaxInt(minMax.min * value, minMax.max * value);
        }

        public int Random() => Utils.RandomRange(min, max);

        public int Clamp(int value) => Mathf.Clamp(value, min, max);

        public bool InRange(int value) => Utils.InRange(value, min, max);

        public bool InRangeExclusive(int value) => Utils.InRangeExclusive(value, min, max);
    }
}