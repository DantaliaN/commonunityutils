using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class SerializedGuid : ISerializationCallbackReceiver, ISerializable, IComparable, IEquatable<SerializedGuid>, IComparable<SerializedGuid>
    {
        [SerializeField]
        [HideInInspector]
        string valueData = string.Empty;
        
        public Guid Value { get; set; } = Guid.NewGuid();

        public SerializedGuid()
        {
            Value = Guid.NewGuid();
        }

        public SerializedGuid(Guid newGuid)
        {
            Value = newGuid;
        }

        protected SerializedGuid(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                return;

            try
            {
                Value = Guid.Parse(info.GetString(nameof(Value)));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public static implicit operator Guid(SerializedGuid value)
        {
            return value.Value;
        }

        public static implicit operator SerializedGuid(Guid value)
        {
            return new SerializedGuid(value);
        }

        public void ResetGuid()
        {
            Value = Guid.NewGuid();
        }

        public int CompareTo(object obj)
        {
            return Value.CompareTo(obj);
        }

        public int CompareTo(SerializedGuid other)
        {
            return Value.CompareTo(other.Value);
        }

        #region Equals

        public static bool operator ==([CanBeNull] SerializedGuid obj1, [CanBeNull] SerializedGuid obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] SerializedGuid obj1, [CanBeNull] SerializedGuid obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as SerializedGuid));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public bool Equals(SerializedGuid other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return Value.Equals(other.Value);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return Value.ToString();
        }

        #endregion

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue(nameof(Value), Value);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            valueData = Value.ToString();
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Value = Guid.TryParse(valueData, out var guid) ? guid : Guid.NewGuid();
        }
    }
}