﻿using System;
using System.Globalization;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class SerializableDateTime : IEquatable<SerializableDateTime>, IComparable<SerializableDateTime>
    {
        [Min(1)]
        [SerializeField]
        int year = 1;

        [Min(1)]
        [SerializeField]
        int month = 1;

        [Min(1)]
        [SerializeField]
        int day = 1;

        [Min(0)]
        [SerializeField]
        int hour = 0;

        [Min(0)]
        [SerializeField]
        int minute = 0;

        [Min(0)]
        [SerializeField]
        int second = 0;

        [Min(0)]
        [SerializeField]
        int millisecond = 0;

        public DateTime Value
        {
            get
            {
                try
                {
                    return new DateTime(year, month, day, hour, minute, second, millisecond);
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex);

                    return new DateTime();
                }
            }
        }

        public static implicit operator DateTime(SerializableDateTime value)
        {
            return value.Value;
        }

        public int CompareTo(SerializableDateTime other)
        {
            return Value.CompareTo(other.Value);
        }

        #region Equals

        public static bool operator ==([CanBeNull] SerializableDateTime obj1, [CanBeNull] SerializableDateTime obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] SerializableDateTime obj1, [CanBeNull] SerializableDateTime obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as SerializableDateTime));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public bool Equals(SerializableDateTime other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return year == other.year && month == other.month && day == other.day && hour == other.hour && minute == other.minute && second == other.second && millisecond == other.millisecond;
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }

        #endregion
    }
}