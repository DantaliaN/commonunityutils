﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class SerializedDateTime : ISerializationCallbackReceiver, ISerializable, IComparable, IEquatable<SerializedDateTime>, IComparable<SerializedDateTime>
    {
        public static readonly string DateFormat = "yyyy-MM-dd HH:mm:ss.fffffff";

        [SerializeField, HideInInspector]
        string valueData = string.Empty;

        public DateTime Value { get; set; } = DateTime.MinValue;

        public SerializedDateTime() { }

        public SerializedDateTime(DateTime dateTime)
        {
            Value = dateTime;
        }

        protected SerializedDateTime(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                return;

            try
            {
                Value = info.GetDateTime(nameof(Value));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public static implicit operator DateTime(SerializedDateTime value)
        {
            return value.Value;
        }

        public static implicit operator SerializedDateTime(DateTime value)
        {
            return new SerializedDateTime(value);
        }
        
        public static bool operator >(SerializedDateTime value1, DateTime value2)
        {
            return value1.Value > value2;
        }
        
        public static bool operator <(SerializedDateTime value1, DateTime value2)
        {
            return value1.Value > value2;
        }
        
        public static bool operator >(DateTime value1, SerializedDateTime value2)
        {
            return value1 > value2.Value;
        }
        
        public static bool operator <(DateTime value1, SerializedDateTime value2)
        {
            return value1 < value2.Value;
        }
        
        public static bool operator >(SerializedDateTime value1, SerializedDateTime value2)
        {
            return value1.Value > value2.Value;
        }
        
        public static bool operator <(SerializedDateTime value1, SerializedDateTime value2)
        {
            return value1.Value < value2.Value;
        }
        
        public int CompareTo(object obj)
        {
            return Value.CompareTo(obj);
        }

        public int CompareTo(SerializedDateTime other)
        {
            return Value.CompareTo(other.Value);
        }

        #region Equals

        public static bool operator ==([CanBeNull] SerializedDateTime obj1, [CanBeNull] SerializedDateTime obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] SerializedDateTime obj1, [CanBeNull] SerializedDateTime obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as SerializedDateTime));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public bool Equals(SerializedDateTime other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return Value.Equals(other.Value);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return Value.ToString(DateFormat);
        }

        #endregion

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue(nameof(Value), Value);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            try
            {
                valueData = Value.ToString(DateFormat, DateTimeFormatInfo.InvariantInfo);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Value = DateTime.TryParseExact(valueData, DateFormat, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out var dateTime) ? dateTime : DateTime.MinValue;
        }
    }
}