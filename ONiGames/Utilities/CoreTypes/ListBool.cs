using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class ListBool : IEquatable<ListBool>, IEquatable<bool>, IEnumerable<bool>
    {
        [NotNull]
        [SerializeField, HideInInspector]
        List<bool> items = new();

        public IReadOnlyList<bool> Items => items;

        public void Clear()
        {
            items.Clear();
        }

        public int TrueCount()
        {
            return items.Count(o => o);
        }

        public int FalseCount()
        {
            return items.Count(o => !o);
        }

        public void Add(bool item)
        {
            items.Add(item);
        }

        public void AddFirst(bool item)
        {
            items.Insert(0, item);
        }

        public bool Remove(bool item)
        {
            return items.Remove(item);
        }

        public bool RemoveLast(bool item)
        {
            var index = items.LastIndexOf(item);
            if (index < 0)
                return false;
            items.RemoveAt(index);
            return true;
        }

        public static ListBool operator ++(ListBool obj1)
        {
            obj1.items.Add(true);
            return obj1;
        }

        public static ListBool operator --(ListBool obj1)
        {
            if (obj1.items.Count == 0)
                return obj1;

            obj1.items.RemoveAt(obj1.items.Count - 1);
            return obj1;
        }

        public static ListBool operator +(ListBool obj1, bool obj2)
        {
            obj1.items.Add(obj2);
            return obj1;
        }

        public static ListBool operator -(ListBool obj1, bool obj2)
        {
            obj1.items.Remove(obj2);
            return obj1;
        }

        public static bool operator ==([CanBeNull] ListBool obj1, bool obj2)
        {
            return !ReferenceEquals(obj1, null) && obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] ListBool obj1, bool obj2)
        {
            return !(obj1 == obj2);
        }

        public static bool operator ==(bool obj1, [CanBeNull] ListBool obj2)
        {
            return !ReferenceEquals(obj2, null) && obj2.Equals(obj1);
        }

        public static bool operator !=(bool obj1, [CanBeNull] ListBool obj2)
        {
            return !(obj2 == obj1);
        }

        public bool Equals(bool other)
        {
            return items.All(o => o == other);
        }

        public static bool operator ==([CanBeNull] ListBool obj1, [CanBeNull] ListBool obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] ListBool obj1, [CanBeNull] ListBool obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as ListBool));
        }

        public bool Equals(ListBool other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            if (items.Count != other.items.Count)
                return false;

            // ReSharper disable once LoopCanBeConvertedToQuery
            for (var i = 0; i < items.Count; i++)
                if (items[i] != other.items[i])
                    return false;

            return true;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return items.GetHashCode();
        }

        public IEnumerator<bool> GetEnumerator() => items.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}