using System;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Obfuscateds
{   
    [Serializable]
    public class Obfuscated<T> : IEquatable<Obfuscated<T>>, IEquatable<T>, IComparable, IComparable<Obfuscated<T>>, IComparable<T> where T : IConvertible, IComparable, IComparable<T>, new()
    {
        [SerializeField]
        [HideInInspector]
        string valueData = string.Empty;
        
        public T Value
        {
            get => Utils.DecryptAes(valueData, Key, new T());
            set => valueData = Utils.EncryptAes(value, Key);
        }
        
        string Key => AppDomain.CurrentDomain.DomainManager?.EntryAssembly?.GetName().Name ?? $"ONiGames.Utilities.CoreTypes.Obfuscated<{typeof(T).Name}>";
        
        public Obfuscated()
        {
            Value = new T();
        }
        
        public Obfuscated(T newValue)
        {
            Value = newValue;
        }
        
        public static implicit operator T(Obfuscated<T> value)
        {
            return value.Value;
        }
        
        public static implicit operator Obfuscated<T>(T value)
        {
            return new Obfuscated<T>(value);
        }
        
        public int CompareTo(object obj)
        {
            return Value.CompareTo(obj);
        }
        
        public int CompareTo(Obfuscated<T> other)
        {
            return Value.CompareTo(other.Value);
        }
        
        public int CompareTo(T other)
        {
            return Value.CompareTo(other);
        }
        
        #region Equals
        
        public static bool operator ==([CanBeNull] Obfuscated<T> obj1, [CanBeNull] Obfuscated<T> obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }
        
        public static bool operator !=([CanBeNull] Obfuscated<T> obj1, [CanBeNull] Obfuscated<T> obj2)
        {
            return !(obj1 == obj2);
        }
        
        public static bool operator ==([CanBeNull] Obfuscated<T> obj1, [CanBeNull] T obj2)
        {
            if (obj1 != null && ReferenceEquals(obj1.Value, obj2))
                return true;
            if (obj1 != null && (ReferenceEquals(obj1.Value, null) || ReferenceEquals(obj2, null)))
                return false;
            return obj1 != null && obj1.Value.Equals(obj2);
        }
        
        public static bool operator !=([CanBeNull] Obfuscated<T> obj1, [CanBeNull] T obj2)
        {
            return !(obj1 == obj2);
        }
        
        public static bool operator ==([CanBeNull] T obj1, [CanBeNull] Obfuscated<T> obj2)
        {
            if (obj2 != null && ReferenceEquals(obj1, obj2.Value))
                return true;
            if (obj2 != null && (ReferenceEquals(obj1, null) || ReferenceEquals(obj2.Value, null)))
                return false;
            return obj2 != null && obj1.Equals(obj2.Value);
        }
        
        public static bool operator !=([CanBeNull] T obj1, [CanBeNull] Obfuscated<T> obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as Obfuscated<T>));
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        
        public bool Equals(Obfuscated<T> other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return Value.Equals(other.Value);
        }
        
        public bool Equals(T other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(Value, other))
                return true;
            return Value.Equals(other);
        }
        
        #endregion
        
        #region ToString
        
        public override string ToString()
        {
            return $"{Value.ToString()} [{valueData}]";
        }
        
        #endregion
    }
}