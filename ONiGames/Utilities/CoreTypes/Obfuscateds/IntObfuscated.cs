using System;

namespace ONiGames.Utilities.CoreTypes.Obfuscateds
{
    [Serializable]
    public class IntObfuscated : Obfuscated<int>
    {
        public IntObfuscated() : base(default) { }
        
        public IntObfuscated(int value) : base(value) { }
        
        public static implicit operator int(IntObfuscated value)
        {
            return value.Value;
        }
        
        public static implicit operator IntObfuscated(int value)
        {
            return new IntObfuscated(value);
        }
    }
}