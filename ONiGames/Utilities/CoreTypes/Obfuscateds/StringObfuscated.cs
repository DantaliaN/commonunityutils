using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Obfuscateds
{
    [Serializable]
    public class StringObfuscated : IEquatable<StringObfuscated>, IEquatable<string>, IComparable, IComparable<StringObfuscated>, IComparable<string>
    {
        [SerializeField]
        [HideInInspector]
        string valueData = string.Empty;
        
        public string Value
        {
            get => Utils.DecryptAes(valueData, Key, string.Empty);
            set => valueData = Utils.EncryptAes(value, Key);
        }
        
        string Key => AppDomain.CurrentDomain.DomainManager?.EntryAssembly?.GetName().Name ?? $"ONiGames.Utilities.CoreTypes.StringObfuscated>";
        
        public StringObfuscated()
        {
            Value = string.Empty;
        }
        
        public StringObfuscated(string newValue)
        {
            Value = newValue;
        }
        
        public static implicit operator string(StringObfuscated value)
        {
            return value.Value;
        }
        
        public static implicit operator StringObfuscated(string value)
        {
            return new StringObfuscated(value);
        }
        
        public int CompareTo(object obj)
        {
            return Value.CompareTo(obj);
        }
        
        [SuppressMessage("ReSharper", "StringCompareToIsCultureSpecific")]
        public int CompareTo(StringObfuscated other)
        {
            return Value.CompareTo(other.Value);
        }
        
        [SuppressMessage("ReSharper", "StringCompareToIsCultureSpecific")]
        public int CompareTo(string other)
        {
            return Value.CompareTo(other);
        }
        
        #region Equals
        
        public static bool operator ==([CanBeNull] StringObfuscated obj1, [CanBeNull] StringObfuscated obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }
        
        public static bool operator !=([CanBeNull] StringObfuscated obj1, [CanBeNull] StringObfuscated obj2)
        {
            return !(obj1 == obj2);
        }
        
        public static bool operator ==([CanBeNull] StringObfuscated obj1, [CanBeNull] string obj2)
        {
            if (ReferenceEquals(obj1?.Value, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }
        
        public static bool operator !=([CanBeNull] StringObfuscated obj1, [CanBeNull] string obj2)
        {
            return !(obj1 == obj2);
        }
        
        public static bool operator ==([CanBeNull] string obj1, [CanBeNull] StringObfuscated obj2)
        {
            if (ReferenceEquals(obj1, obj2?.Value))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }
        
        public static bool operator !=([CanBeNull] string obj1, [CanBeNull] StringObfuscated obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as StringObfuscated));
        }
        
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        
        public bool Equals(StringObfuscated other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return Value.Equals(other.Value);
        }
        
        public bool Equals(string other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(Value, other))
                return true;
            return Value.Equals(other);
        }
        
        #endregion
        
        #region ToString
        
        public override string ToString()
        {
            return $"{Value} [{valueData}]";
        }
        
        #endregion
    }
}