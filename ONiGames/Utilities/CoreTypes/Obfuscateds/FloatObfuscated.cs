using System;

namespace ONiGames.Utilities.CoreTypes.Obfuscateds
{
    [Serializable]
    public class FloatObfuscated : Obfuscated<float>
    {
        public FloatObfuscated() : base(default) { }
        
        public FloatObfuscated(float value) : base(value) { }
        
        public static implicit operator float(FloatObfuscated value)
        {
            return value.Value;
        }
        
        public static implicit operator FloatObfuscated(float value)
        {
            return new FloatObfuscated(value);
        }
    }
}