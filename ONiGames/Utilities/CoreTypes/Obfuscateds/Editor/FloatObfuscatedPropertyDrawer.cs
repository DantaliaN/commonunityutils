#if UNITY_EDITOR

using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Obfuscateds.Editor
{
    [CustomPropertyDrawer(typeof(FloatObfuscated), true)]
    public class FloatObfuscatedPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(UtilsEditor.GetTargetObjectOfProperty(property, true) is FloatObfuscated floatObfuscated))
                return;
            
            var value = EditorGUI.FloatField(position, new GUIContent(label.ToString(), floatObfuscated.ToString()), floatObfuscated.Value);
            
            if (Mathf.Approximately(value, floatObfuscated.Value))
                return;
            
            floatObfuscated.Value = value;
            
            property.serializedObject.ApplyModifiedProperties();
            
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
    }
}

#endif