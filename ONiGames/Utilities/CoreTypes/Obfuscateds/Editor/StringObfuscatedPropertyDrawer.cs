#if UNITY_EDITOR

using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Obfuscateds.Editor
{
    [CustomPropertyDrawer(typeof(StringObfuscated), true)]
    public class StringObfuscatedPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(UtilsEditor.GetTargetObjectOfProperty(property, true) is StringObfuscated stringObfuscated))
                return;
            
            var value = EditorGUI.TextField(position, new GUIContent(label.ToString(), stringObfuscated.ToString()), stringObfuscated.Value);
            
            if (value == stringObfuscated.Value)
                return;
            
            stringObfuscated.Value = value;
            
            property.serializedObject.ApplyModifiedProperties();
            
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
    }
}

#endif