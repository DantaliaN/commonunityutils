#if UNITY_EDITOR

using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Obfuscateds.Editor
{
    [CustomPropertyDrawer(typeof(IntObfuscated), true)]
    public class IntObfuscatedPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(UtilsEditor.GetTargetObjectOfProperty(property, true) is IntObfuscated intObfuscated))
                return;
            
            var value = EditorGUI.IntField(position, new GUIContent(label.ToString(), intObfuscated.ToString()), intObfuscated.Value);
            
            if (value == intObfuscated.Value)
                return;
            
            intObfuscated.Value = value;
            
            property.serializedObject.ApplyModifiedProperties();
            
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
    }
}

#endif