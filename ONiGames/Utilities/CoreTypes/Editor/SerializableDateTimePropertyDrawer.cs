#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(SerializableDateTime))]
    public class SerializableDateTimePropertyDrawer : PropertyDrawer
    {
        const string YEAR_PROP_NAME = "year";
        const string MONTH_PROP_NAME = "month";
        const string DAY_PROP_NAME = "day";
        const string HOUR_PROP_NAME = "hour";
        const string MINUTE_PROP_NAME = "minute";
        const string SECOND_PROP_NAME = "second";
        const string MILLISECOND_PROP_NAME = "millisecond";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var yearProp = property.FindPropertyRelative(YEAR_PROP_NAME);
            if (yearProp == null || yearProp.propertyType != SerializedPropertyType.Integer)
                return;
            var monthProp = property.FindPropertyRelative(MONTH_PROP_NAME);
            if (monthProp == null || monthProp.propertyType != SerializedPropertyType.Integer)
                return;
            var dayProp = property.FindPropertyRelative(DAY_PROP_NAME);
            if (dayProp == null || dayProp.propertyType != SerializedPropertyType.Integer)
                return;
            var hourProp = property.FindPropertyRelative(HOUR_PROP_NAME);
            if (hourProp == null || hourProp.propertyType != SerializedPropertyType.Integer)
                return;
            var minuteProp = property.FindPropertyRelative(MINUTE_PROP_NAME);
            if (minuteProp == null || minuteProp.propertyType != SerializedPropertyType.Integer)
                return;
            var secondProp = property.FindPropertyRelative(SECOND_PROP_NAME);
            if (secondProp == null || secondProp.propertyType != SerializedPropertyType.Integer)
                return;
            var millisecondProp = property.FindPropertyRelative(MILLISECOND_PROP_NAME);
            if (millisecondProp == null || millisecondProp.propertyType != SerializedPropertyType.Integer)
                return;

            var year = yearProp.intValue;
            var month = monthProp.intValue;
            var day = dayProp.intValue;
            var hour = hourProp.intValue;
            var minute = minuteProp.intValue;
            var second = secondProp.intValue;
            var millisecond = millisecondProp.intValue;

            #region Draw

            const float labelWidth = 35f;
            const int count = 7;
            const float gapWidth = 5f;

            var fieldWidth = (position.width - EditorGUIUtility.labelWidth) / count - labelWidth - gapWidth + gapWidth / count;

            var xOffset = position.x;

            EditorGUI.LabelField(new Rect(xOffset, position.y, EditorGUIUtility.labelWidth, position.height), label?.text);

            xOffset += EditorGUIUtility.labelWidth;
            
            //--

            EditorGUI.LabelField(new Rect(xOffset, position.y, labelWidth, position.height), "Year:");

            xOffset += labelWidth;

            year = EditorGUI.IntField(new Rect(xOffset, position.y, fieldWidth, position.height), year);

            xOffset += fieldWidth + gapWidth;
            
            //--

            EditorGUI.LabelField(new Rect(xOffset, position.y, labelWidth, position.height), "Month:");

            xOffset += labelWidth;
                
            month = EditorGUI.IntField(new Rect(xOffset, position.y, fieldWidth, position.height), month);

            xOffset += fieldWidth + gapWidth;
            
            //--

            EditorGUI.LabelField(new Rect(xOffset, position.y, labelWidth, position.height), "Day:");

            xOffset += labelWidth;
                
            day = EditorGUI.IntField(new Rect(xOffset, position.y, fieldWidth, position.height), day);

            xOffset += fieldWidth + gapWidth;
            
            //--

            EditorGUI.LabelField(new Rect(xOffset, position.y, labelWidth, position.height), "Hour:");

            xOffset += labelWidth;
                
            hour = EditorGUI.IntField(new Rect(xOffset, position.y, fieldWidth, position.height), hour);

            xOffset += fieldWidth + gapWidth;
            
            //--

            EditorGUI.LabelField(new Rect(xOffset, position.y, labelWidth, position.height), "Min:");

            xOffset += labelWidth;
                
            minute = EditorGUI.IntField(new Rect(xOffset, position.y, fieldWidth, position.height), minute);

            xOffset += fieldWidth + gapWidth;
            
            //--

            EditorGUI.LabelField(new Rect(xOffset, position.y, labelWidth, position.height), "Sec:");

            xOffset += labelWidth;
                
            second = EditorGUI.IntField(new Rect(xOffset, position.y, fieldWidth, position.height), second);

            xOffset += fieldWidth + gapWidth;
            
            //--

            EditorGUI.LabelField(new Rect(xOffset, position.y, labelWidth, position.height), "Ms:");

            xOffset += labelWidth;
                
            millisecond = EditorGUI.IntField(new Rect(xOffset, position.y, fieldWidth, position.height), millisecond);

            #endregion
            
            yearProp.intValue = year;
            monthProp.intValue = month;
            dayProp.intValue = day;
            hourProp.intValue = hour;
            minuteProp.intValue = minute;
            secondProp.intValue = second;
            millisecondProp.intValue = millisecond;
        }
    }
}

#endif