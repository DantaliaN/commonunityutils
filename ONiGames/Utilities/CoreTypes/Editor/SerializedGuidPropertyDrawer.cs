#if UNITY_EDITOR

using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(SerializedGuid), true)]
    public class SerializedGuidPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(UtilsEditor.GetTargetObjectOfProperty(property, true) is SerializedGuid serializedGuid))
                return;

            GUI.enabled = false;
            EditorGUI.TextField(new Rect(position.x, position.y, position.width - 100f, position.height), label, serializedGuid.Value.ToString());
            GUI.enabled = true;

            if (GUI.Button(new Rect(position.x + position.width - 100f, position.y, 100f, position.height), "Reset GUID"))
            {
                serializedGuid.ResetGuid();

                property.serializedObject.ApplyModifiedProperties();

                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }
    }
}

#endif