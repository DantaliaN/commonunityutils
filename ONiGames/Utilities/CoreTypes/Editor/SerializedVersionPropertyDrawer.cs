#if UNITY_EDITOR

using System;
using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(SerializedVersion), true)]
    public class SerializedVersionPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(UtilsEditor.GetTargetObjectOfProperty(property, true) is SerializedVersion serializedVersion))
                return;

            var textField = EditorGUI.TextField(position, label, serializedVersion.Value.ToString());
            if (!Version.TryParse(textField, out var value))
                return;

            if (value == serializedVersion.Value)
                return;

            serializedVersion.Value = value;

            property.serializedObject.ApplyModifiedProperties();

            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
    }
}

#endif