﻿#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(MinMax01))]
    public class MinMax01PropertyDrawer : PropertyDrawer
    {
        const string MIN_PROP_NAME = "min";
        const string MAX_PROP_NAME = "max";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var minProp = property.FindPropertyRelative(MIN_PROP_NAME);
            if (minProp == null || minProp.propertyType != SerializedPropertyType.Float)
                return;

            var maxProp = property.FindPropertyRelative(MAX_PROP_NAME);
            if (maxProp == null || maxProp.propertyType != SerializedPropertyType.Float)
                return;

            var min = (float) Math.Round(minProp.floatValue, 2);
            var max = (float) Math.Round(maxProp.floatValue, 2);

            var countLabel = new GUIContent(label) { text = $"{label.text} [{min} : {max}]" };

            EditorGUI.MinMaxSlider(position, countLabel, ref min, ref max, 0f, 1f);

            minProp.floatValue = min;
            maxProp.floatValue = max;
        }
    }
}

#endif