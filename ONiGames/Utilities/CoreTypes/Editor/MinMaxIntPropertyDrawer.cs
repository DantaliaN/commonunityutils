#if UNITY_EDITOR

using System.Linq;
using ONiGames.Utilities.CoreTypes.Attributes;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(MinMaxInt))]
    public class MinMaxIntPropertyDrawer : PropertyDrawer
    {
        const string MIN_PROP_NAME = "min";
        const string MAX_PROP_NAME = "max";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var minProp = property.FindPropertyRelative(MIN_PROP_NAME);
            if (minProp == null || minProp.propertyType != SerializedPropertyType.Integer)
                return;

            var maxProp = property.FindPropertyRelative(MAX_PROP_NAME);
            if (maxProp == null || maxProp.propertyType != SerializedPropertyType.Integer)
                return;

            var min = minProp.intValue;
            var max = maxProp.intValue;

            #region Draw

            const float minLabelWidth = 30f;
            const float maxLabelWidth = 33f;
            const float minMaxGapWidth = 5f;

            var minMaxFieldWidth = (position.width - EditorGUIUtility.labelWidth - minLabelWidth - maxLabelWidth - minMaxGapWidth) / 2f;

            var xOffset = position.x;

            EditorGUI.LabelField(new Rect(xOffset, position.y, EditorGUIUtility.labelWidth, position.height), label?.text);

            xOffset += EditorGUIUtility.labelWidth;

            EditorGUI.LabelField(new Rect(xOffset, position.y, minLabelWidth, position.height), "Min:");

            xOffset += minLabelWidth;

            min = EditorGUI.IntField(new Rect(xOffset, position.y, minMaxFieldWidth, position.height), min);

            xOffset += minMaxFieldWidth;

            xOffset += minMaxGapWidth;

            EditorGUI.LabelField(new Rect(xOffset, position.y, maxLabelWidth, position.height), "Max:");

            xOffset += maxLabelWidth;

            max = EditorGUI.IntField(new Rect(xOffset, position.y, minMaxFieldWidth, position.height), max);

            #endregion

            if (fieldInfo != null && fieldInfo.GetCustomAttributes(typeof(MinMaxMinAttribute), false).FirstOrDefault() is MinMaxMinAttribute minAttribute)
            {
                min = min > (int) minAttribute.min ? min : (int) minAttribute.min;
                max = max > (int) minAttribute.min ? max : (int) minAttribute.min;
            }

            if (fieldInfo != null && fieldInfo.GetCustomAttributes(typeof(MinMaxRangeAttribute), false).FirstOrDefault() is MinMaxRangeAttribute rangeAttribute)
            {
                min = Mathf.Clamp(min, (int) rangeAttribute.min, (int) rangeAttribute.max);
                max = Mathf.Clamp(max, (int) rangeAttribute.min, (int) rangeAttribute.max);
            }

            minProp.intValue = min > max ? max : min;
            maxProp.intValue = max; //since it is updated simultaneously
        }
    }
}

#endif