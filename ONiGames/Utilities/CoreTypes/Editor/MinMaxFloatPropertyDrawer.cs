#if UNITY_EDITOR

using System.Linq;
using ONiGames.Utilities.CoreTypes.Attributes;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(MinMaxFloat))]
    public class MinMaxFloatPropertyDrawer : PropertyDrawer
    {
        const string MIN_PROP_NAME = "min";
        const string MAX_PROP_NAME = "max";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var minProp = property.FindPropertyRelative(MIN_PROP_NAME);
            if (minProp == null || minProp.propertyType != SerializedPropertyType.Float)
                return;

            var maxProp = property.FindPropertyRelative(MAX_PROP_NAME);
            if (maxProp == null || maxProp.propertyType != SerializedPropertyType.Float)
                return;

            var min = minProp.floatValue;
            var max = maxProp.floatValue;

            #region Draw

            const float minLabelWidth = 30f;
            const float maxLabelWidth = 33f;
            const float minMaxGapWidth = 5f;

            var minMaxFieldWidth = (position.width - EditorGUIUtility.labelWidth - minLabelWidth - maxLabelWidth - minMaxGapWidth) / 2f;

            var xOffset = position.x;

            EditorGUI.LabelField(new Rect(xOffset, position.y, EditorGUIUtility.labelWidth, position.height), label?.text);

            xOffset += EditorGUIUtility.labelWidth;

            EditorGUI.LabelField(new Rect(xOffset, position.y, minLabelWidth, position.height), "Min:");

            xOffset += minLabelWidth;

            min = EditorGUI.FloatField(new Rect(xOffset, position.y, minMaxFieldWidth, position.height), min);

            xOffset += minMaxFieldWidth;

            xOffset += minMaxGapWidth;

            EditorGUI.LabelField(new Rect(xOffset, position.y, maxLabelWidth, position.height), "Max:");

            xOffset += maxLabelWidth;

            max = EditorGUI.FloatField(new Rect(xOffset, position.y, minMaxFieldWidth, position.height), max);

            #endregion

            if (fieldInfo != null && fieldInfo.GetCustomAttributes(typeof(MinMaxMinAttribute), false).FirstOrDefault() is MinMaxMinAttribute minAttribute)
            {
                min = min > minAttribute.min ? min : minAttribute.min;
                max = max > minAttribute.min ? max : minAttribute.min;
            }

            if (fieldInfo != null && fieldInfo.GetCustomAttributes(typeof(MinMaxRangeAttribute), false).FirstOrDefault() is MinMaxRangeAttribute rangeAttribute)
            {
                min = Mathf.Clamp(min, rangeAttribute.min, rangeAttribute.max);
                max = Mathf.Clamp(max, rangeAttribute.min, rangeAttribute.max);
            }

            minProp.floatValue = min > max ? max : min;
            maxProp.floatValue = max; //since it is updated simultaneously
        }
    }
}

#endif