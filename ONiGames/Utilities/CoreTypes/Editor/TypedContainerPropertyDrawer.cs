﻿#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(ATypedContainer), true)]
    public class TypedContainerPropertyDrawer : PropertyDrawer
    {
        const string OBJ_FIELD_NAME = "obj";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var tc = fieldInfo.GetValue(property.serializedObject.targetObject) as ATypedContainer;
            tc?.Validate();

            var objProp = property.FindPropertyRelative(OBJ_FIELD_NAME);
            if (objProp == null)
                throw new InvalidCastException($"Can't find {OBJ_FIELD_NAME} field in {property.type}");

            EditorGUI.PropertyField(position, objProp, label);
        }
    }
}

#endif