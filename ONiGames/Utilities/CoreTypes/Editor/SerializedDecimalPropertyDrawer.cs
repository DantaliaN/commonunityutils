#if UNITY_EDITOR

using System.Globalization;
using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(SerializedDecimal), true)]
    public class SerializedDecimalPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(UtilsEditor.GetTargetObjectOfProperty(property, true) is SerializedDecimal serializedDecimal))
                return;

            var textField = EditorGUI.TextField(position, label, serializedDecimal.Value.ToString(CultureInfo.InvariantCulture));
            if (!decimal.TryParse(textField, NumberStyles.Any, CultureInfo.InvariantCulture, out var value))
                return;

            if (value == serializedDecimal.Value)
                return;

            serializedDecimal.Value = value;

            property.serializedObject.ApplyModifiedProperties();

            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
    }
}

#endif