#if UNITY_EDITOR

using System;
using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Editor
{
    [CustomPropertyDrawer(typeof(SerializedDateTime), true)]
    public class SerializedDateTimePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!(UtilsEditor.GetTargetObjectOfProperty(property, true) is SerializedDateTime serializedDateTime))
                return;

            var textField = EditorGUI.TextField(position, label, serializedDateTime.Value.ToString(SerializedDateTime.DateFormat));
            if (!DateTime.TryParse(textField, out var value))
                return;

            if (value == serializedDateTime.Value)
                return;

            serializedDateTime.Value = value;

            property.serializedObject.ApplyModifiedProperties();

            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }
    }
}

#endif