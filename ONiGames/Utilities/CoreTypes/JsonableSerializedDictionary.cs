﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class JsonableSerializedDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        [SerializeField, HideInInspector]
        List<string> keyData = new();

        [SerializeField, HideInInspector]
        List<string> valueData = new();

        public JsonableSerializedDictionary() { }
        public JsonableSerializedDictionary(SerializationInfo info, StreamingContext context) : base(info, context) { }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Clear();

            for (var i = 0; i < keyData.Count && i < valueData.Count; i++)
            {
                var key = Serialization.Utils.DeserializeFromJson<TKey>(keyData[i]);
                var value = Serialization.Utils.DeserializeFromJson<TValue>(valueData[i]);

                if (key == null)
                {
                    Debug.LogWarning($"Can't parse {keyData[i]} to {typeof(TKey)}");
                    continue;
                }
                
                this[key] = value;
            }
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            keyData.Clear();
            valueData.Clear();

            foreach (var item in this)
            {
                keyData.Add(Serialization.Utils.SerializeToJson(item.Key));
                valueData.Add(Serialization.Utils.SerializeToJson(item.Value));
            }
        }
    }
}