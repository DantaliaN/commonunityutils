using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class SerializedVersion : ISerializationCallbackReceiver, ISerializable, IComparable, IEquatable<SerializedVersion>, IComparable<SerializedVersion>
    {
        [SerializeField, HideInInspector]
        string valueData = string.Empty;

        [NotNull]
        public Version Value { get; set; } = new();

        public SerializedVersion() { }

        public SerializedVersion(Version version)
        {
            Value = version;
        }

        protected SerializedVersion(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                return;

            try
            {
                Value = Version.Parse(info.GetString(nameof(Value)));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public static implicit operator Version(SerializedVersion value)
        {
            return value.Value;
        }

        public static implicit operator SerializedVersion(Version value)
        {
            return new SerializedVersion(value);
        }

        public int CompareTo(object obj)
        {
            return Value.CompareTo(obj);
        }

        public int CompareTo(SerializedVersion other)
        {
            return Value.CompareTo(other.Value);
        }

        #region Equals

        public static bool operator ==([CanBeNull] SerializedVersion obj1, [CanBeNull] SerializedVersion obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] SerializedVersion obj1, [CanBeNull] SerializedVersion obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as SerializedVersion));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public bool Equals(SerializedVersion other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return Value.Equals(other.Value);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return Value.ToString();
        }

        #endregion

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue(nameof(Value), Value.ToString());
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            valueData = Value.ToString();
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Value = Version.TryParse(valueData, out var version) ? version : new Version();
        }
    }
}