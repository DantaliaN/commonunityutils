using System;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public struct MinMaxFloat
    {
        [SerializeField]
        float min;

        [SerializeField]
        float max;

        public float Min
        {
            get => min;
            set => min = value > max ? max : value;
        }

        public float Max
        {
            get => max;
            set => max = value < min ? min : value;
        }

        public MinMaxFloat(float min, float max)
        {
            this.min = min < max ? min : max;
            this.max = max > min ? max : min;
        }

        public static MinMaxFloat operator *(MinMaxFloat minMax, float value)
        {
            return new MinMaxFloat(minMax.min * value, minMax.max * value);
        }

        public static explicit operator MinMaxFloat(MinMax01 minMax)
        {
            return new MinMaxFloat(minMax.Min, minMax.Max);
        }

        public float Random() => UnityEngine.Random.Range(min, max);

        public float Clamp(float value) => Mathf.Clamp(value, min, max);

        public bool InRange(float value) => Utils.InRange(value, min, max);

        public bool InRangeExclusive(float value) => Utils.InRangeExclusive(value, min, max);
    }
}