﻿using System;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public struct MinMax01
    {
        [SerializeField]
        [Range(0f, 1f)]
        float min;

        [SerializeField]
        [Range(0f, 1f)]
        float max;

        public float Min
        {
            get => min;
            set => min = value < 0f ? 0f : value > 1f ? 1f : value > max ? max : value;
        }

        public float Max
        {
            get => max;
            set => max = value < 0f ? 0f : value > 1f ? 1f : value < min ? min : value;
        }

        public MinMax01(float min, float max)
        {
            this.min = min < 0f ? 0f : min > 1f ? 1f : min < max ? min : max;
            this.max = max < 0f ? 0f : max > 1f ? 1f : max > min ? max : min;
        }

        public static MinMax01 operator *(MinMax01 minMax, float value)
        {
            return new MinMax01(minMax.min * value, minMax.max * value);
        }

        public float Random() => UnityEngine.Random.Range(min, max);

        public float Clamp(float value) => Mathf.Clamp(value, min, max);

        public bool InRange(float value) => Utils.InRange(value, min, max);

        public bool InRangeExclusive(float value) => Utils.InRangeExclusive(value, min, max);
    }
}