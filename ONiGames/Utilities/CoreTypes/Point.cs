﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class Point : IEquatable<Point>
    {
        public readonly int x;
        public readonly int y;

        public Point(float x, float y) : this(Mathf.RoundToInt(x), Mathf.RoundToInt(y)) { }

        public Point(int x = 0, int y = 0)
        {
            this.x = x;
            this.y = y;
        }

        public static implicit operator Point(Vector2 value)
        {
            return new Point(value.x, value.y);
        }

        public static implicit operator Vector2(Point value)
        {
            return new Vector2(value.x, value.y);
        }

        #region Equals

        public static bool operator ==([CanBeNull] Point obj1, [CanBeNull] Point obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] Point obj1, [CanBeNull] Point obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as Point));
        }

        public override int GetHashCode()
        {
            return x ^ y;
        }

        public bool Equals(Point other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return x.Equals(other.x) && y.Equals(other.y);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return $"({x}, {y})";
        }

        #endregion
    }
}