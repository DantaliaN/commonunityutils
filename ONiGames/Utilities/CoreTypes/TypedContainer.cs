﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public abstract class ATypedContainer
    {
        // ReSharper disable once RedundantDefaultMemberInitializer
        [CanBeNull]
        [SerializeField]
        protected MonoBehaviour obj = default;

        public void Validate()
        {
            OnValidate();
        }

        protected abstract void OnValidate();
    }

    [Serializable]
    public class TypedContainer<T> : ATypedContainer where T : class
    {
        [CanBeNull]
        public T Value => obj as T;

        protected override void OnValidate()
        {
            if (Value == null)
                obj = null;
        }
    }
}