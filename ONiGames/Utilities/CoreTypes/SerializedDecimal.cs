using System;
using System.Globalization;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes
{
    [Serializable]
    public class SerializedDecimal : ISerializationCallbackReceiver, ISerializable, IComparable, IEquatable<SerializedDecimal>, IComparable<SerializedDecimal>
    {
        [SerializeField, HideInInspector]
        string valueData = string.Empty;

        public decimal Value { get; set; } = 0m;

        public SerializedDecimal() { }

        public SerializedDecimal(decimal version)
        {
            Value = version;
        }

        protected SerializedDecimal(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                return;

            try
            {
                Value = decimal.Parse(info.GetString(nameof(Value)), NumberStyles.Any, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public static implicit operator decimal(SerializedDecimal value)
        {
            return value.Value;
        }

        public static implicit operator SerializedDecimal(decimal value)
        {
            return new SerializedDecimal(value);
        }

        public int CompareTo(object obj)
        {
            return Value.CompareTo(obj);
        }

        public int CompareTo(SerializedDecimal other)
        {
            return Value.CompareTo(other.Value);
        }

        #region Equals

        public static bool operator ==([CanBeNull] SerializedDecimal obj1, [CanBeNull] SerializedDecimal obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] SerializedDecimal obj1, [CanBeNull] SerializedDecimal obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as SerializedDecimal));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public bool Equals(SerializedDecimal other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return Value.Equals(other.Value);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }

        #endregion

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue(nameof(Value), Value.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            valueData = Value.ToString(CultureInfo.InvariantCulture);
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            Value = decimal.TryParse(valueData, NumberStyles.Any, CultureInfo.InvariantCulture, out var version) ? version : 0m;
        }
    }
}