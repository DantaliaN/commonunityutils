using System;
using UnityEngine;

namespace ONiGames.Utilities.CoreTypes.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class MinMaxMinAttribute : PropertyAttribute
    {
        public readonly float min;

        public MinMaxMinAttribute(float min) => this.min = min;
    }
}