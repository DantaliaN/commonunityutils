using System;
using JetBrains.Annotations;

namespace ONiGames.Utilities
{
    public abstract class APreference<T, TRaw> where TRaw : IConvertible
    {
        class Cached
        {
            [CanBeNull]
            public T value;
        }

        [CanBeNull]
        protected virtual TRaw RawDefault => default;

        protected virtual string Name => GetType().FullName;

        [CanBeNull]
        Cached cached;

        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public TRaw GetRaw(TRaw def = default) => GetPrefs(Name, def);

        public void SetRaw([NotNull] TRaw value) => SetPrefs(Name, value);

        [NotNull]
        public T Get(bool forceRecache = false)
        {
            if (cached == null || cached.value == null || forceRecache)
                cached = new Cached { value = ConvertFromRaw(GetRaw(RawDefault)) };

            return cached.value;
        }

        public void Set([NotNull] T value)
        {
            cached ??= new Cached();

            cached.value = value;
        }

        [UsedImplicitly]
        public void Apply()
        {
            if (cached == null || cached.value == null)
                return;

            SetRaw(ConvertToRaw(cached.value));
        }

        [UsedImplicitly]
        public void Clear(bool isForce = false)
        {
            cached = null;

            if (isForce)
            {
                var def = RawDefault;
                if (def != null)
                    SetRaw(def);
            }
        }

        [NotNull]
        protected abstract T ConvertFromRaw([CanBeNull] TRaw value);

        protected abstract TRaw ConvertToRaw([NotNull] T value);

        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        protected virtual TRaw GetPrefs(string name, TRaw def = default) => Utils.GetPlayerPrefs(name, def);

        protected virtual void SetPrefs(string name, [NotNull] TRaw value) => Utils.SetPlayerPrefs(name, value);
    }
}