using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ONiGames.Utilities
{
    public class Pool
    {
        readonly GameObject poolPrefab;
        [CanBeNull]
        readonly Transform poolTransform;

        readonly List<GameObject> objects = new();

        public Pool(GameObject prefab, [CanBeNull] Transform transform = null)
        {
            poolPrefab = prefab;
            poolTransform = transform;
        }

        public void Push(int count)
        {
            var activeSelf = poolPrefab.gameObject.activeSelf;
            poolPrefab.gameObject.SetActive(false);

            for (var i = 0; i < count; i++)
                objects.Add(poolTransform != null ? Object.Instantiate(poolPrefab, poolTransform) : Object.Instantiate(poolPrefab));

            poolPrefab.gameObject.SetActive(activeSelf);
        }

        public GameObject PopOrInstantiate()
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(null);
            }
            else
            {
                instance = Object.Instantiate(poolPrefab);
            }

            return instance;
        }

        public GameObject PopOrInstantiate(Vector3 position, Quaternion rotation)
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(null);

                t.position = position;
                t.rotation = rotation;
            }
            else
            {
                instance = Object.Instantiate(poolPrefab, position, rotation);
            }

            return instance;
        }

        public GameObject PopOrInstantiate(Transform transform)
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(transform);
            }
            else
            {
                instance = Object.Instantiate(poolPrefab, transform);
            }

            return instance;
        }

        [CanBeNull]
        public GameObject PopOrDefault()
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(null);
            }

            return instance;
        }

        public void Clear()
        {
            foreach (var obj in objects)
                Object.Destroy(obj.gameObject);
        }
    }

    public class Pool<T> where T : Component
    {
        readonly T poolPrefab;
        [CanBeNull]
        readonly Transform poolTransform;

        readonly List<T> objects = new();

        public Pool(T prefab, [CanBeNull] Transform transform = null)
        {
            poolPrefab = prefab;
            poolTransform = transform;
        }

        public void Push(int count = 1, [CanBeNull] Action<T> map = null)
        {
            var activeSelf = poolPrefab.gameObject.activeSelf;
            poolPrefab.gameObject.SetActive(false);

            for (var i = 0; i < count; i++)
            {
                var instance = poolTransform != null ? Object.Instantiate(poolPrefab, poolTransform) : Object.Instantiate(poolPrefab);
                map?.Invoke(instance);
                objects.Add(instance);
            }

            poolPrefab.gameObject.SetActive(activeSelf);
        }

        public T PopOrInstantiate()
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(null);
            }
            else
            {
                instance = Object.Instantiate(poolPrefab);
            }

            return instance;
        }

        public T PopOrInstantiate(Vector3 position, Quaternion rotation)
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(null);

                t.position = position;
                t.rotation = rotation;
            }
            else
            {
                instance = Object.Instantiate(poolPrefab, position, rotation);
            }

            return instance;
        }

        public T PopOrInstantiate(Transform transform)
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(transform);
            }
            else
            {
                instance = Object.Instantiate(poolPrefab, transform);
            }

            return instance;
        }

        [CanBeNull]
        public T PopOrDefault()
        {
            var instance = objects.FirstOrDefault();
            if (instance != null)
            {
                objects.Remove(instance);

                instance.gameObject.SetActive(true);

                var t = instance.transform;
                t.SetParent(null);
            }

            return instance;
        }

        public void Clear()
        {
            foreach (var obj in objects)
                Object.Destroy(obj.gameObject);
        }
    }
}