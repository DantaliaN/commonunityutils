using System;
using JetBrains.Annotations;

namespace ONiGames.Utilities.Serialization.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class BinarySerializationDefaultValueAttribute : Attribute
    {
        [CanBeNull]
        public object Value { get; }

        public BinarySerializationDefaultValueAttribute(object value) => Value = value;
    }
}