using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using JetBrains.Annotations;
using ONiGames.Utilities.Serialization.Attributes;
using UnityEngine;

namespace ONiGames.Utilities.Serialization
{
    public static class Utils
    {
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T DeepCloneJson<T>(T obj, T def = default)
        {
            return TryDeepCloneJson(obj, out var value) ? value : def;
        }
        
        public static bool TryDeepCloneJson<T>(T obj, out T value)
        {
            value = DeserializeFromJson<T>(SerializeToJson(obj));
            return value != null;
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T DeepClone<T>(T obj, T def = default)
        {
            return TryDeepClone(obj, out var value) ? value : def;
        }

        public static bool TryDeepClone<T>(T obj, out T value)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();

                var surrogateSelector = new SurrogateSelector();

                surrogateSelector.AddSurrogate(typeof(T), new StreamingContext(StreamingContextStates.All), new BinarySerializationDefaultValueAttributeSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector2Int), new StreamingContext(StreamingContextStates.All), new Vector2IntSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector3Int), new StreamingContext(StreamingContextStates.All), new Vector3IntSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new Vector2SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), new Vector3SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector4), new StreamingContext(StreamingContextStates.All), new Vector4SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), new QuaternionSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new ColorSerializationSurrogate());

                formatter.SurrogateSelector = surrogateSelector;

                try
                {
                    formatter.Serialize(stream, obj);

                    stream.Position = 0L;

                    value = (T) formatter.Deserialize(stream);
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);

                    value = default;
                    return false;
                }
            }
        }

        public static string Serialize(object obj)
        {
            var serializer = new XmlSerializer(obj.GetType());
            using (var stringWriter = new StringWriter())
            {
                try
                {
                    serializer.Serialize(stringWriter, obj);
                    return stringWriter.ToString();
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                    return string.Empty;
                }
            }
        }

        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T Deserialize<T>(string xml, T def = default)
        {
            return TryDeserialize<T>(xml, out var result) ? result : def;
        }

        public static bool TryDeserialize<T>(string xml, out T result)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stringReader = new StringReader(xml))
            {
                try
                {
                    result = (T) serializer.Deserialize(stringReader);
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);

                    result = default;
                    return false;
                }
            }
        }

        public static byte[] SerializeToBytes(object obj)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();

                var surrogateSelector = new SurrogateSelector();

                surrogateSelector.AddSurrogate(obj.GetType(), new StreamingContext(StreamingContextStates.All), new BinarySerializationDefaultValueAttributeSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector2Int), new StreamingContext(StreamingContextStates.All), new Vector2IntSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector3Int), new StreamingContext(StreamingContextStates.All), new Vector3IntSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new Vector2SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), new Vector3SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector4), new StreamingContext(StreamingContextStates.All), new Vector4SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), new QuaternionSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new ColorSerializationSurrogate());

                formatter.SurrogateSelector = surrogateSelector;

                try
                {
                    formatter.Serialize(stream, obj);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }

                return stream.ToArray();
            }
        }

        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T Deserialize<T>(byte[] arrBytes, T def = default)
        {
            return TryDeserialize<T>(arrBytes, out var result) ? result : def;
        }

        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T Deserialize<T>(byte[] arrBytes, bool useStringAssembly, T def = default)
        {
            return TryDeserialize<T>(arrBytes, useStringAssembly, out var result) ? result : def;
        }

        public static bool TryDeserialize<T>(byte[] arrBytes, out T result)
        {
            return TryDeserialize(arrBytes, true, out result);
        }

        public static bool TryDeserialize<T>(byte[] arrBytes, bool useStringAssembly, out T result)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();

                var surrogateSelector = new SurrogateSelector();

                surrogateSelector.AddSurrogate(typeof(T), new StreamingContext(StreamingContextStates.All), new BinarySerializationDefaultValueAttributeSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector2Int), new StreamingContext(StreamingContextStates.All), new Vector2IntSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector3Int), new StreamingContext(StreamingContextStates.All), new Vector3IntSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new Vector2SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), new Vector3SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Vector4), new StreamingContext(StreamingContextStates.All), new Vector4SerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), new QuaternionSerializationSurrogate());
                surrogateSelector.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new ColorSerializationSurrogate());

                formatter.SurrogateSelector = surrogateSelector;

                stream.Write(arrBytes, 0, arrBytes.Length);
                stream.Seek(0, SeekOrigin.Begin);

                if (useStringAssembly)
                    formatter.Binder = new FromAllAssembliesDeserializationBinder();

                try
                {
                    result = (T) formatter.Deserialize(stream);
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);

                    result = default;
                    return false;
                }
            }
        }
        
        public static string SerializeToJson(object obj, bool prettyPrint = false)
        {
            try
            {
                return JsonUtility.ToJson(obj, prettyPrint);
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                return string.Empty;
            }
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public static T DeserializeFromJson<T>(string json, T def = default)
        {
            return TryDeserializeFromJson<T>(json, out var result) ? result : def;
        }
        
        public static bool TryDeserializeFromJson<T>(string json, out T result)
        {
            try
            {
                result = JsonUtility.FromJson<T>(json);
                return result != null;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                
                result = default;
                return false;
            }
        }
    }
    
    public class Vector2IntSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            try
            {
                var vector2Int = (Vector2Int) obj;
                info.AddValue("x", vector2Int.x);
                info.AddValue("y", vector2Int.y);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            try
            {
                var vector2Int = (Vector2Int) obj;
                vector2Int.x = (int) info.GetValue("x", typeof(int));
                vector2Int.y = (int) info.GetValue("y", typeof(int));
                obj = vector2Int;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return obj;
        }
    }

    public class Vector3IntSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            try
            {
                var vector3Int = (Vector3Int) obj;
                info.AddValue("x", vector3Int.x);
                info.AddValue("y", vector3Int.y);
                info.AddValue("z", vector3Int.z);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            try
            {
                var vector3Int = (Vector3Int) obj;
                vector3Int.x = (int) info.GetValue("x", typeof(int));
                vector3Int.y = (int) info.GetValue("y", typeof(int));
                vector3Int.z = (int) info.GetValue("z", typeof(int));
                obj = vector3Int;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return obj;
        }
    }

    public class Vector2SerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            try
            {
                var vector2 = (Vector2) obj;
                info.AddValue("x", vector2.x);
                info.AddValue("y", vector2.y);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            try
            {
                var vector2 = (Vector2) obj;
                vector2.x = (float) info.GetValue("x", typeof(float));
                vector2.y = (float) info.GetValue("y", typeof(float));
                obj = vector2;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return obj;
        }
    }

    public class Vector3SerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            try
            {
                var vector3 = (Vector3) obj;
                info.AddValue("x", vector3.x);
                info.AddValue("y", vector3.y);
                info.AddValue("z", vector3.z);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            try
            {
                var vector3 = (Vector3) obj;
                vector3.x = (float) info.GetValue("x", typeof(float));
                vector3.y = (float) info.GetValue("y", typeof(float));
                vector3.z = (float) info.GetValue("z", typeof(float));
                obj = vector3;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return obj;
        }
    }

    public class Vector4SerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            try
            {
                var vector4 = (Vector4) obj;
                info.AddValue("x", vector4.x);
                info.AddValue("y", vector4.y);
                info.AddValue("z", vector4.z);
                info.AddValue("w", vector4.w);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            try
            {
                var vector4 = (Vector4) obj;
                vector4.x = (float) info.GetValue("x", typeof(float));
                vector4.y = (float) info.GetValue("y", typeof(float));
                vector4.z = (float) info.GetValue("z", typeof(float));
                vector4.w = (float) info.GetValue("w", typeof(float));
                obj = vector4;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return obj;
        }
    }

    public class QuaternionSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            try
            {
                var quaternion = (Quaternion) obj;
                info.AddValue("x", quaternion.x);
                info.AddValue("y", quaternion.y);
                info.AddValue("z", quaternion.z);
                info.AddValue("w", quaternion.w);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            try
            {
                var quaternion = (Quaternion) obj;
                quaternion.x = (float) info.GetValue("x", typeof(float));
                quaternion.y = (float) info.GetValue("y", typeof(float));
                quaternion.z = (float) info.GetValue("z", typeof(float));
                quaternion.w = (float) info.GetValue("w", typeof(float));
                obj = quaternion;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return obj;
        }
    }

    public class ColorSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            try
            {
                var color = (Color) obj;
                info.AddValue("r", color.r);
                info.AddValue("g", color.g);
                info.AddValue("b", color.b);
                info.AddValue("a", color.a);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            try
            {
                var color = (Color) obj;
                color.r = (float) info.GetValue("r", typeof(float));
                color.g = (float) info.GetValue("g", typeof(float));
                color.b = (float) info.GetValue("b", typeof(float));
                color.a = (float) info.GetValue("a", typeof(float));
                obj = color;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
            }

            return obj;
        }
    }

    public class BinarySerializationDefaultValueAttributeSerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var fieldInfos = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var fieldInfo in fieldInfos)
            {
                try
                {
                    var fieldValue = fieldInfo.GetValue(obj);
                    if (fieldValue == null)
                    {
                        var attribute = fieldInfo.GetCustomAttribute<BinarySerializationDefaultValueAttribute>(true);
                        if (attribute != null)
                            fieldValue = attribute.Value;
                    }

                    info.AddValue(fieldInfo.Name, fieldValue);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }
            }
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            var fieldInfos = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var fieldInfo in fieldInfos)
            {
                var fieldValue = (object) null;

                try
                {
                    fieldValue = info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }

                try
                {
                    if (fieldValue == null)
                    {
                        var attribute = fieldInfo.GetCustomAttribute<BinarySerializationDefaultValueAttribute>(true);
                        if (attribute != null)
                            fieldValue = attribute.Value;
                    }

                    fieldInfo.SetValue(obj, fieldValue);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }
            }

            return obj;
        }
    }
    
    sealed class FromAllAssembliesDeserializationBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var type = Type.GetType($"{typeName}, {assembly.FullName}");
                if (type != null)
                    return type;
            }
            
            // For each assemblyName/typeName that you want to deserialize to
            // a different type, set typeToDeserialize to the desired type
            var exeAssembly = Assembly.GetExecutingAssembly().FullName;
            
            // The following line of code returns the type
            var typeToDeserialize = Type.GetType($"{typeName}, {exeAssembly}");
            
            return typeToDeserialize;
        }
    }
}
