﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(RequireFieldAttribute))]
    public class RequireFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.ObjectReference || property.objectReferenceValue != null)
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }

            var color = GUI.color;
            GUI.color = Color.yellow;

            label.tooltip = "Required field!";

            EditorGUI.PropertyField(position, property, label);

            GUI.color = color;
        }
    }
}

#endif