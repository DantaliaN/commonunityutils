﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(CanBeNullFieldAttribute))]
    public class CanBeNullFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var l = new GUIContent($"{label.text} [or None]")
            {
                tooltip = $"{label.tooltip} Field can be empty"
            };

            EditorGUI.PropertyField(position, property, l);
        }
    }
}

#endif