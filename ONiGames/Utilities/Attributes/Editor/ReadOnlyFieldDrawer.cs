﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(ReadOnlyFieldAttribute))]
    public class ReadOnlyFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var value = string.Empty;

            switch (property.propertyType)
            {
                case SerializedPropertyType.Integer:
                    value = $"{property.intValue}";
                    break;
                case SerializedPropertyType.Boolean:
                    value = $"{property.boolValue}";
                    break;
                case SerializedPropertyType.Float:
                    value = $"{property.floatValue}";
                    break;
                case SerializedPropertyType.String:
                    value = $"{property.stringValue}";
                    break;
                case SerializedPropertyType.Color:
                    value = $"{property.colorValue}";
                    break;
                case SerializedPropertyType.LayerMask:
                    value = $"{property.intValue}";
                    break;
                case SerializedPropertyType.Enum:
                    value = $"{property.enumDisplayNames[property.enumValueIndex]}";
                    break;
                case SerializedPropertyType.Vector2:
                    value = $"{property.vector2Value}";
                    break;
                case SerializedPropertyType.Vector3:
                    value = $"{property.vector3Value}";
                    break;
                case SerializedPropertyType.Vector4:
                    value = $"{property.vector4Value}";
                    break;
                case SerializedPropertyType.Rect:
                    value = $"{property.rectValue}";
                    break;
                case SerializedPropertyType.Quaternion:
                    value = $"{property.quaternionValue}";
                    break;
                case SerializedPropertyType.Vector2Int:
                    value = $"{property.vector2IntValue}";
                    break;
                case SerializedPropertyType.Vector3Int:
                    value = $"{property.vector3IntValue}";
                    break;
                case SerializedPropertyType.RectInt:
                    value = $"{property.rectIntValue}";
                    break;
                case SerializedPropertyType.BoundsInt:
                    value = $"{property.boundsIntValue}";
                    break;
                default:
                    Debug.LogWarning($"{property.propertyType} not implemented");
                    break;
            }

            EditorGUI.LabelField(position, label, new GUIContent(value), EditorStyles.label);
        }
    }
}

#endif