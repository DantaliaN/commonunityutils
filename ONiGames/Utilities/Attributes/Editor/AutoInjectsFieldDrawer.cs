#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(AutoInjectsFieldAttribute))]
    public class AutoInjectsFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            const string arrayIndicator = ".Array.data[";

            var arrayIndex = property.propertyPath.LastIndexOf(arrayIndicator, StringComparison.Ordinal);
            if (arrayIndex != -1)
            {
                var propertyName = property.propertyPath.Substring(0, arrayIndex);

                var arrayProperty = property.serializedObject.FindProperty(propertyName);
                if (arrayProperty != null)
                {
                    if (arrayProperty.arraySize == 1)
                    {
                        var elementType = fieldInfo.FieldType.GetElementType();
                        if (elementType != null)
                        {
                            if (elementType.IsSubclassOf(typeof(MonoBehaviour)))
                            {
                                if (arrayProperty.serializedObject.targetObject as MonoBehaviour is var mb && mb != null)
                                {
                                    arrayProperty.ClearArray();

                                    foreach (var component in Object.FindObjectsOfType(elementType, true))
                                    {
                                        arrayProperty.InsertArrayElementAtIndex(arrayProperty.arraySize);
                                        var arrayElement = arrayProperty.GetArrayElementAtIndex(arrayProperty.arraySize - 1);

                                        arrayElement.objectReferenceValue = component;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            EditorGUI.PropertyField(position, property, label);
        }
    }
}

#endif