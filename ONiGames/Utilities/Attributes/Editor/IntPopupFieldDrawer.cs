#if UNITY_EDITOR

using System.Linq;
using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(IntPopupFieldAttribute))]
    public class IntPopupFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var startIndex = 0;
            var items = new string[] { };
            var showIndex = false;
            if (fieldInfo != null && fieldInfo.GetCustomAttributes(typeof(IntPopupFieldAttribute), false).FirstOrDefault() is IntPopupFieldAttribute intPopupAttribute)
            {
                startIndex = intPopupAttribute.startIndex;
                items = intPopupAttribute.nameItems;
                showIndex = intPopupAttribute.showIndex;
            }

            var value = items.InBounds(property.intValue - startIndex) ? items[property.intValue - startIndex] : items.FirstOrDefault() ?? string.Empty;

            if (showIndex)
            {
                value = ShowIndex(value, property.intValue);
                items = items.Select(ShowIndex).ToArray();
            }
            
            value = GUIUtilsEditor.Popup(position, label, value, items);

            property.intValue = items.IndexOf(value) + startIndex;
        }

        static string ShowIndex(string value, int index) => $"[{index}] {value}";
    }
}

#endif