#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(PrefabFieldAttribute))]
    public class PrefabFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var l = new GUIContent($"{label.text} [Prefab]")
            {
                tooltip = $"{label.tooltip} Field must be a prefab"
            };

            var color = GUI.color;

            if (property.objectReferenceValue != null)
            {
                if (property.objectReferenceValue as GameObject is var go && go != null)
                {
                    if (go.scene != default)
                        GUI.color = Color.yellow;
                }
                else if (property.objectReferenceValue as Component is var c && c != null)
                {
                    if (c.gameObject.scene != default)
                        GUI.color = Color.yellow;
                }
            }

            EditorGUI.PropertyField(position, property, l);

            GUI.color = color;
        }
    }
}

#endif