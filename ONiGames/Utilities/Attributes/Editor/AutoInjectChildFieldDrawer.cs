#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(AutoInjectChildFieldAttribute))]
    public class AutoInjectChildFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.ObjectReference)
            {
                if (property.objectReferenceValue == null)
                {
                    if (fieldInfo.FieldType.IsSubclassOf(typeof(MonoBehaviour)))
                    {
                        if (property.serializedObject.targetObject as MonoBehaviour is var mb && mb != null)
                        {
                            property.objectReferenceValue = mb.gameObject.GetComponentInChildren(fieldInfo.FieldType, true);
                        }
                    }
                }
            }

            EditorGUI.PropertyField(position, property, label);
        }
    }
}

#endif