#if UNITY_EDITOR

using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(AutoInjectFieldAttribute))]
    public class AutoInjectFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.ObjectReference)
            {
                if (property.objectReferenceValue == null)
                {
                    if (fieldInfo.FieldType.IsSubclassOf(typeof(ScriptableObject)))
                    {
                        property.objectReferenceValue = UtilsEditor.FindAssetInInspector(fieldInfo.FieldType);
                    }
                    else if (fieldInfo.FieldType.IsSubclassOf(typeof(MonoBehaviour)))
                    {
                        if (UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage() == null)
                            property.objectReferenceValue = Object.FindObjectOfType(fieldInfo.FieldType, true);
                    }
                }
            }

            EditorGUI.PropertyField(position, property, label);
        }
    }
}

#endif