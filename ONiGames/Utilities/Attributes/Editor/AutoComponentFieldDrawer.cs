#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(AutoComponentFieldAttribute))]
    public class AutoComponentFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.ObjectReference)
            {
                if (property.objectReferenceValue == null)
                {
                    if (fieldInfo.FieldType.IsSubclassOf(typeof(Component)))
                    {
                        if (UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage() == null)
                        {
                            if (property.serializedObject.targetObject is MonoBehaviour monoBehaviour)
                            {
                                var component = monoBehaviour.gameObject.GetComponent(fieldInfo.FieldType);
                                if (component == null)
                                {
                                    if (attribute as AutoComponentFieldAttribute is { AsChild: true })
                                    {
                                        var go = new GameObject(fieldInfo.FieldType.Name);
                                        go.transform.SetParent(monoBehaviour.gameObject.transform);
                                        component = go.AddComponent(fieldInfo.FieldType);
                                    }
                                    else
                                    {
                                        component = monoBehaviour.gameObject.AddComponent(fieldInfo.FieldType);
                                    }
                                }

                                property.objectReferenceValue = component;
                            }
                        }
                    }
                }
            }

            EditorGUI.PropertyField(position, property, label);
        }
    }
}

#endif