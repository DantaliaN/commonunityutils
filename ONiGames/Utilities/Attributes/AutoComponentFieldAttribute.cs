using System;
using UnityEngine;

namespace ONiGames.Utilities.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoComponentFieldAttribute : PropertyAttribute
    {
        public bool AsChild { get; private set; } = false;
        
        public AutoComponentFieldAttribute(bool asChild = false) => AsChild = asChild;
    }
}