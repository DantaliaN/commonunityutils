using System;
using UnityEngine;

namespace ONiGames.Utilities.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class IntPopupFieldAttribute : PropertyAttribute
    {
        public readonly int startIndex;
        public readonly string[] nameItems;
        public readonly bool showIndex;

        public IntPopupFieldAttribute(string[] nameItems, int startIndex = 0, bool showIndex = false)
        {
            this.nameItems = nameItems;
            this.startIndex = startIndex;
            this.showIndex = showIndex;
        }

        public IntPopupFieldAttribute(string nameItems, int startIndex = 0, bool showIndex = false)
        {
            this.nameItems = nameItems.Split(',');
            this.startIndex = startIndex;
        }
    }
}