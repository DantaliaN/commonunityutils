using System;
using UnityEngine;

namespace ONiGames.Utilities.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoInjectsFieldAttribute : PropertyAttribute { }
}