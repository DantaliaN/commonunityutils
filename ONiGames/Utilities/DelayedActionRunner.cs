using System.Collections.Generic;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class DelayedActionRunner : MonoBehaviour
    {
        struct Item
        {
            public readonly DelayedAction action;
            public readonly bool useUnscaledTime;

            public Item(DelayedAction a, bool u)
            {
                action = a;
                useUnscaledTime = u;
            }
        }

        readonly List<Item> items = new();

        void Update()
        {
            foreach (var item in items)
                item.action.Update(item.useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);

            items.RemoveAll(o => o.action.IsFinished);
        }

        public void Run(DelayedAction action, bool useUnscaledTime = false)
        {
            items.Add(new Item(action, useUnscaledTime));
        }
    }
}