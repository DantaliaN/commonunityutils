using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace ONiGames.Utilities
{
    public class Asyncer : MonoBehaviour
    {
        readonly Queue<Task> tasks = new();

        bool isStarted = true;

        bool IsStarted
        {
            get
            {
                lock (this)
                {
                    return isStarted;
                }
            }
        }

        void Start()
        {
            isStarted = true;

            Task.Run(async () =>
            {
                while (IsStarted)
                {
                    if (tasks.Count > 0)
                        await tasks.Dequeue();

                    await Task.Delay(10);
                }
            });
        }

        void OnDestroy()
        {
            isStarted = false;

            tasks.Clear();
        }

        public void Wait(Task task, Action onComplete)
        {
            tasks.Enqueue(TaskRunner(task, onComplete));
        }

        public void Wait(Task task, Action onComplete, Action<string> onError)
        {
            tasks.Enqueue(TaskRunner(task, onComplete, onError));
        }

        public void Wait<T>(Task<T> task, Action<T> onComplete, Action<string> onError)
        {
            tasks.Enqueue(TaskRunner(task, onComplete, onError));
        }

        async Task TaskRunner(Task task, Action onComplete)
        {
            try
            {
                await task;
                onComplete();
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {ex}");

                onComplete();
            }
        }

        static async Task TaskRunner(Task task, Action onComplete, Action<string> onError)
        {
            try
            {
                await task;
                onComplete();
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        static async Task TaskRunner<T>(Task<T> task, Action<T> onComplete, Action<string> onError)
        {
            try
            {
                var result = await task;
                onComplete(result);
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }
    }
}