﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Editor
{
    public static class UtilsEditor
    {
        public static void SelectAssetInInspector<T>() where T : UnityEngine.Object
        {
            var assets = FindAssetsInInspector<T>().ToList();
            switch (assets.Count)
            {
                case 0:
                    return;
                case 1:
                    Selection.activeObject = assets[0];
                    return;
                default:
                    ObjectListWindowEditor.Show(assets);
                    break;
            }
        }
        
        public static IEnumerable<string> FindAssetsPath(string typeName)
        {
            var guids = AssetDatabase.FindAssets($"t:{typeName}");
            return guids.Select(AssetDatabase.GUIDToAssetPath);
        }

        public static IEnumerable<string> FindAssetsPath<T>() where T : UnityEngine.Object
        {
            var guids = AssetDatabase.FindAssets($"t:{typeof(T).FullName}");
            return guids.Select(AssetDatabase.GUIDToAssetPath);
        }

        public static IEnumerable<string> FindAssetsPath(Type type)
        {
            var guids = AssetDatabase.FindAssets($"t:{type.FullName}");
            return guids.Select(AssetDatabase.GUIDToAssetPath);
        }

        public static IEnumerable<T> FindAssetsInInspector<T>() where T : UnityEngine.Object
        {
            return FindAssetsPath<T>().Select(AssetDatabase.LoadAssetAtPath<T>);
        }

        public static IEnumerable<UnityEngine.Object> FindAssetsInInspector(Type type)
        {
            return FindAssetsPath(type).Select(o => AssetDatabase.LoadAssetAtPath(o, type));
        }

        [CanBeNull]
        public static T FindAssetInInspector<T>() where T : UnityEngine.Object
        {
            var guids = AssetDatabase.FindAssets($"t:{typeof(T).FullName}");
            return guids.Length > 0 ? AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guids[0])) : null;
        }

        [CanBeNull]
        public static UnityEngine.Object FindAssetInInspector(Type type)
        {
            var guids = AssetDatabase.FindAssets($"t:{type.FullName}");
            return guids.Length > 0 ? AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guids[0]), type) : null;
        }

        public static IEnumerable<GameObject> LoadAllPrefabs()
        {
            return AssetDatabase.FindAssets("t:prefab").Select(AssetDatabase.GUIDToAssetPath).Select(AssetDatabase.LoadAssetAtPath<GameObject>);
        }

        public static IEnumerable<T> PrefabsByType<T>() where T : Component
        {
            return LoadAllPrefabs().Select(o => o.GetComponent<T>()).Where(o => o != null);
        }

        public static IEnumerable<Component> PrefabsByType(Type type)
        {
            return LoadAllPrefabs().Select(o => o.GetComponent(type)).Where(o => o != null);
        }

        [CanBeNull]
        public static object GetTargetObjectOfProperty(SerializedProperty prop, bool inCurrent = false)
        {
            if (prop == null)
                return null;

            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');

            foreach (var element in inCurrent ? elements : elements.Take(elements.Length - 1))
            {
                if (element.Contains("["))
                {
                    var bracketIndex = element.IndexOf("[", StringComparison.Ordinal);
                    var elementName = element.Substring(0, bracketIndex);
                    var index = Convert.ToInt32(element.Substring(bracketIndex).Replace("[", "").Replace("]", ""));

                    obj = Utils.GetObjectValue(obj, elementName, index);
                }
                else
                {
                    obj = Utils.GetObjectValue(obj, element);
                }
            }

            return obj;
        }

        public static void SetTargetObjectOfProperty(SerializedProperty prop, object value)
        {
            var obj = GetTargetObjectOfProperty(prop);
            if (obj is null)
                return;

            var path = prop.propertyPath.Replace(".Array.data[", "[");
            var elements = path.Split('.');
            var element = elements.Last();

            if (element.Contains("["))
            {
                var bracketIndex = element.IndexOf("[", StringComparison.Ordinal);
                var elementName = element.Substring(0, bracketIndex);
                var index = Convert.ToInt32(element.Substring(bracketIndex).Replace("[", "").Replace("]", ""));

                Utils.SetObjectValue(obj, elementName, index, value);
            }
            else
            {
                Utils.SetObjectValue(obj, element, value);
            }
        }

        public static void SafeOnValidate(Action onValidate)
        {
            EditorApplication.delayCall += OnValidate;

            void OnValidate()
            {
                EditorApplication.delayCall -= OnValidate;

                onValidate();
            }
        }
    }
}

#endif