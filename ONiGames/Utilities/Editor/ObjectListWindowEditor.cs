#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Editor
{
    public class ObjectListWindowEditor : EditorWindow
    {
        readonly List<Object> assets = new();

        Vector2 scrollPos = Vector2.zero;
        
        void OnGUI()
        {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            
            foreach (var o in assets)
                EditorGUILayout.ObjectField(o, typeof(Object), false);
            
            EditorGUILayout.EndScrollView();
        }
        
        public static void Show<T>(IEnumerable<T> assets) where T : Object
        {
            var window = GetWindow<ObjectListWindowEditor>();

            window.titleContent = new GUIContent($"List of {typeof(T).Name}");

            window.assets.Refill(assets);

            window.ShowPopup();
        }
    }
}

#endif