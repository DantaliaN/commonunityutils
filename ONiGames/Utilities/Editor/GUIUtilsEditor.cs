﻿#if UNITY_EDITOR

using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Editor
{
    public static class GUIUtilsEditor
    {
        public static T Popup<T>(Rect position, GUIContent content, T selected, T[] options, GUIStyle guiStyle = null)
        {
            var color = GUI.color;

            if (options.Length == 0)
            {
                GUI.color = Color.red;

                var c = new GUIContent(content.text)
                {
                    text = $"{content.text} (Not Available)!",
                    tooltip = "Add items to options"
                };

                EditorGUI.TextField(position, c, $"{selected}");

                GUI.color = color;

                return selected;
            }

            var style = guiStyle ?? EditorStyles.popup;

            var index = Array.IndexOf(options, selected);

            GUI.color = index == -1 ? Color.red : color;

            if (index == -1)
                content.tooltip = $"Selected item {index} not found";

            index = EditorGUI.Popup(position, content, index, options.Select(i => new GUIContent(i.ToString())).ToArray(), style);

            GUI.color = color;

            return index == -1 ? selected : options[index];
        }

        public static T Popup<T>(GUIContent content, T selected, T[] options, GUIStyle guiStyle = null)
        {
            var color = GUI.color;

            if (options.Length == 0)
            {
                GUI.color = Color.red;

                var c = new GUIContent(content.text)
                {
                    text = $"{content.text} (Not Available)",
                    tooltip = "Add items to options"
                };

                EditorGUILayout.TextField(c, $"{selected}");

                GUI.color = color;

                return selected;
            }

            var style = guiStyle ?? EditorStyles.popup;

            var index = Array.IndexOf(options, selected);

            GUI.color = index == -1 ? Color.red : color;

            if (index == -1)
                content.tooltip = $"Selected item {index} not found";

            index = EditorGUILayout.Popup(content, index, options.Select(i => new GUIContent(i.ToString())).ToArray(), style);

            GUI.color = color;

            return index == -1 ? selected : options[index];
        }

        public static void MenuOfSubclasses<T>(Action<Type> select)
        {
            MenuOfSubclasses<T>(type => true, select);
        }

        public static void MenuOfSubclasses<T>(Func<Type, bool> predicate, Action<Type> select)
        {
            var menu = new GenericMenu();

            var items = AppDomain.CurrentDomain.GetAssemblies().SelectMany(o => o.GetTypes()).Where(o => o.IsSubclassOf(typeof(T))).ToArray();

            foreach (var item in items.Where(predicate))
            {
                menu.AddItem(new GUIContent(item.Name), false, () =>
                {
                    select.Invoke(item);
                });
            }

            menu.ShowAsContext();
        }

        public static void HorizontalLine()
        {
            var horizontalLine = new GUIStyle
            {
                normal = { background = EditorGUIUtility.whiteTexture },
                margin = new RectOffset(0, 0, 4, 4),
                fixedHeight = 1
            };

            var color = GUI.color;

            GUI.color = Color.black;
            GUILayout.Box(GUIContent.none, horizontalLine);

            GUI.color = color;
        }
    }
}

#endif