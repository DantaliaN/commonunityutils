#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Utilities.Editor
{
    public class InputDialog : EditorWindow
    {
        string description = string.Empty;

        string inputText = string.Empty;

        string okButton = "Ok";
        string cancelButton = "Cancel";

        bool initializedPosition = false;

        Action<string> pressOk;
        Action pressCancel;
        
        bool shouldClose = false;

        Vector2 maxScreenPos;

        void OnGUI()
        {
            var e = Event.current;
            if (e.type == EventType.KeyDown)
            {
                switch (e.keyCode)
                {
                    case KeyCode.Escape:
                        shouldClose = true;
                        e.Use();
                        break;
                    case KeyCode.Return:
                    case KeyCode.KeypadEnter:
                        shouldClose = true;
                        e.Use();
                        break;
                }
            }

            if (shouldClose)
            {
                Close();
                
                if (inputText.IsNullOrEmpty())
                    pressCancel?.Invoke();
                else
                    pressOk?.Invoke(inputText);

                return;
            }

            var rect = EditorGUILayout.BeginVertical();

            EditorGUILayout.Space(12);

            EditorGUILayout.LabelField(description);

            EditorGUILayout.Space(8);

            GUI.SetNextControlName("inText");

            inputText = EditorGUILayout.TextField("", inputText);

            GUI.FocusControl("inText");

            EditorGUILayout.Space(12);

            var r = EditorGUILayout.GetControlRect();
            r.width /= 2;

            if (GUI.Button(r, okButton))
            {
                shouldClose = true;
            }

            r.x += r.width;
            if (GUI.Button(r, cancelButton))
            {
                inputText = string.Empty;
                shouldClose = true;
            }

            EditorGUILayout.Space(8);
            EditorGUILayout.EndVertical();

            if (rect.width != 0 && minSize != rect.size)
                minSize = maxSize = rect.size;

            if (!initializedPosition && e.type == EventType.Layout)
            {
                initializedPosition = true;

                var mousePos = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);
                mousePos.x += 32;
                if (mousePos.x + position.width > maxScreenPos.x)
                    mousePos.x -= position.width + 64;
                if (mousePos.y + position.height > maxScreenPos.y)
                    mousePos.y = maxScreenPos.y - position.height;

                position = new Rect(mousePos.x, mousePos.y, position.width, position.height);

                Focus();
            }
        }

        public static void Show(string title, string description, string ok, string cancel, Action<string> onOk, Action onCancel)
        {
            var maxPos = GUIUtility.GUIToScreenPoint(new Vector2(Screen.width, Screen.height));

            var window = GetWindow<InputDialog>();

            window.maxScreenPos = maxPos;
            window.titleContent = new GUIContent(title);
            window.description = description;
            window.okButton = ok;
            window.cancelButton = cancel;

            window.pressOk = onOk;
            window.pressCancel = onCancel;

            window.ShowPopup();
        }
    }
}

#endif