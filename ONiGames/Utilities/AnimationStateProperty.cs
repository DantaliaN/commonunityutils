﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Utilities
{
    [Serializable]
    public class AnimationStateProperty
    {
        class Cached
        {
            public int hash;
        }

        [NotNull]
        public string name = string.Empty;

        [CanBeNull]
        Cached id;

        public int NameHash(bool forceRehash = false)
        {
            if (id == null || forceRehash)
                id = new Cached { hash = Animator.StringToHash(name) };
            return id.hash;
        }

        #region ToString

        public override string ToString()
        {
            return $"{name}";
        }

        #endregion
    }
}