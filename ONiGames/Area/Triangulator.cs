﻿using System.Collections.Generic;
using UnityEngine;

namespace ONiGames.Area
{
    public class Triangulator
    {
        readonly List<Vector2> points;

        public Triangulator(IEnumerable<Vector2> points)
        {
            this.points = new List<Vector2>(points);
        }

        public int[] Triangulate()
        {
            var indices = new List<int>();

            var n = points.Count;
            if (n < 3)
                return indices.ToArray();

            var vs = new int[n];
            if (Area() > 0)
            {
                for (var v = 0; v < n; v++)
                    vs[v] = v;
            }
            else
            {
                for (var v = 0; v < n; v++)
                    vs[v] = n - 1 - v;
            }

            var nv = n;
            var count = 2 * nv;
            for (var v = nv - 1; nv > 2;)
            {
                if (count-- <= 0)
                    return indices.ToArray();

                var u = v;
                if (nv <= u)
                    u = 0;
                v = u + 1;
                if (nv <= v)
                    v = 0;
                var w = v + 1;
                if (nv <= w)
                    w = 0;

                if (Snip(u, v, w, nv, vs))
                {
                    var a = vs[u];
                    var b = vs[v];
                    var c = vs[w];
                    indices.Add(a);
                    indices.Add(b);
                    indices.Add(c);
                    for (int s = v, t = v + 1; t < nv; s++, t++)
                        vs[s] = vs[t];
                    nv--;
                    count = 2 * nv;
                }
            }

            indices.Reverse();
            return indices.ToArray();
        }

        float Area()
        {
            var n = points.Count;
            var a = 0.0f;
            for (int p = n - 1, q = 0; q < n; p = q++)
            {
                var pval = points[p];
                var qval = points[q];
                a += pval.x * qval.y - qval.x * pval.y;
            }

            return a * 0.5f;
        }

        bool Snip(int u, int v, int w, int n, int[] vs)
        {
            var a = points[vs[u]];
            var b = points[vs[v]];
            var c = points[vs[w]];
            if (Mathf.Epsilon > (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x))
                return false;
            for (var p = 0; p < n; p++)
            {
                if (p == u || p == v || p == w)
                    continue;
                var ps = points[vs[p]];
                if (InsideTriangle(a, b, c, ps))
                    return false;
            }

            return true;
        }

        static bool InsideTriangle(Vector2 a, Vector2 b, Vector2 c, Vector2 p)
        {
            var ax = c.x - b.x;
            var ay = c.y - b.y;
            var bx = a.x - c.x;
            var by = a.y - c.y;
            var cx = b.x - a.x;
            var cy = b.y - a.y;
            var apx = p.x - a.x;
            var apy = p.y - a.y;
            var bpx = p.x - b.x;
            var bpy = p.y - b.y;
            var cpx = p.x - c.x;
            var cpy = p.y - c.y;

            var aCrossBp = ax * bpy - ay * bpx;
            var cCrossAp = cx * apy - cy * apx;
            var bCrossCp = bx * cpy - by * cpx;

            return aCrossBp >= 0.0f && bCrossCp >= 0.0f && cCrossAp >= 0.0f;
        }
    }
}