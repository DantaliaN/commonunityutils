﻿using System.Linq;
using UnityEngine;

namespace ONiGames.Area
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshCollider))]
    public class DynamicArea : MonoBehaviour
    {
        public float height = 1f;

        public bool reverseNormals = false;

        public bool showDebug = true;

        public void Create()
        {
            var points = new Vector3[transform.childCount];
            for (var i = 0; i < points.Length; i++)
            {
                points[i] = transform.GetChild(i).localPosition;
            }

            if (points.Length == 0)
                return;

            var mf = GetComponent<MeshFilter>();

            var sharedMesh = mf.sharedMesh;
            var mesh = sharedMesh ? sharedMesh : new Mesh();

            mf.sharedMesh = mesh;

            mesh.Clear();

            var bottom = new Triangulator(points.Select(i => new Vector2(i.x, i.z))).Triangulate();
            var top = bottom.Select(i => i + points.Length);

            mesh.SetVertices(points.Concat(points.Select(i => i + Vector3.up * height)).ToList());

            var side = new int[points.Length * 6];
            var indx = 0;
            for (var i = 0; i < points.Length - 1; i++)
            {
                side[indx++] = i;
                side[indx++] = i + 1;
                side[indx++] = i + points.Length;

                side[indx++] = i + points.Length + 1;
                side[indx++] = i + points.Length;
                side[indx++] = i + 1;
            }

            side[indx++] = points.Length - 1;
            side[indx++] = 0;
            side[indx++] = points.Length - 1 + points.Length;

            side[indx++] = points.Length;
            side[indx++] = points.Length - 1 + points.Length;
            side[indx] = 0;

            var sides = new int[0];
            sides = sides.Concat(bottom.Reverse()).ToArray();
            sides = sides.Concat(top).ToArray();
            sides = Enumerable.Concat(sides, side).ToArray();

            if (reverseNormals)
                sides = sides.Reverse().ToArray();

            mesh.SetTriangles(sides, 0);

            //mesh.SetNormals(points.Select(i => Vector3.left).Concat(points.Select(i => Vector3.left)).ToList());

            //mesh.SetUVs(0, points.Select(i => new Vector2(i.x, i.z)).ToList());

            mesh.MarkDynamic();
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            mesh.RecalculateTangents();

            GetComponent<MeshCollider>().sharedMesh = mesh;
        }

        void OnDrawGizmos()
        {
            if (!showDebug)
                return;

            var points = new Vector3[transform.childCount];
            for (var i = 0; i < points.Length; i++)
            {
                points[i] = transform.GetChild(i).position;
            }

            if (points.Length == 0)
                return;

            for (var i = 0; i < points.Length - 1; i++)
            {
                Gizmos.DrawLine(points[i], points[i + 1]);
            }

            Gizmos.DrawLine(points[0], points[points.Length - 1]);

            for (var i = 0; i < points.Length - 1; i++)
            {
                Gizmos.DrawLine(points[i] + Vector3.up * height, points[i + 1] + Vector3.up * height);
            }

            Gizmos.DrawLine(points[0] + Vector3.up * height, points[points.Length - 1] + Vector3.up * height);

            foreach (var point in points)
            {
                Gizmos.DrawLine(point, point + Vector3.up * height);
            }
        }

        void OnDrawGizmosSelected()
        {
            if (!showDebug)
                return;

            var points = new Vector3[transform.childCount];
            for (var i = 0; i < points.Length; i++)
            {
                points[i] = transform.GetChild(i).position;
            }

            if (points.Length == 0)
                return;

            foreach (var point in points)
            {
                Gizmos.DrawWireCube(point, Vector3.one * 0.1f);
            }
        }
    }
}