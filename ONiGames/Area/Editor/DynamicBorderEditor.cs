﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Area.Editor
{
    [CustomEditor(typeof(DynamicBorder))]
    public class DynamicBorderEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var script = (DynamicBorder) target;
            if (GUILayout.Button("Create Mesh"))
            {
                script.Create();
            }
        }
    }
}

#endif