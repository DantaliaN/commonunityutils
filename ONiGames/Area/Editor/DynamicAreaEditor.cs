﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.Area.Editor
{
    [CustomEditor(typeof(DynamicArea))]
    public class DynamicAreaEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var script = (DynamicArea) target;
            if (GUILayout.Button("Create Mesh"))
            {
                script.Create();
            }
        }
    }
}

#endif