using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using ONiGames.Asserts.Attributes;
using ONiGames.Utilities;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace ONiGames.Asserts
{
    public static class Assert
    {
        public static void SerializedFields(MonoBehaviour current)
        {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
            var fields = current.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var fieldInfo in fields)
            {
                if (fieldInfo.GetCustomAttribute<SerializeField>() == null)
                    continue;

                if (fieldInfo.FieldType == typeof(GameObject))
                {
                    var value = fieldInfo.GetValue(current) as GameObject;

                    var canBeNull = fieldInfo.GetCustomAttribute<CanBeNullAttribute>() != null;
                    var skipSceneCheck = fieldInfo.GetCustomAttribute<SkipSceneCheckAttribute>() != null;
                    var isPrefab = fieldInfo.GetCustomAttribute<PrefabAttribute>() != null;

                    if (!canBeNull && value == null)
                        throw new UnassignedReferenceException($"{current.gameObject.GetPath()}: {fieldInfo.Name} not set");

                    if (!skipSceneCheck && value != null)
                    {
                        if (isPrefab)
                        {
                            if (value.scene != default)
                                throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} must be a prefab");
                        }
                        else
                        {
                            if (value.scene == default)
                                throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} must be a scene object");
                        }
                    }
                }
                else if (fieldInfo.FieldType == typeof(GameObject[]))
                {
                    var values = fieldInfo.GetValue(current) as GameObject[] ?? new GameObject[0];

                    var canBeNull = fieldInfo.GetCustomAttribute<ItemCanBeNullAttribute>() != null;
                    var skipSceneCheck = fieldInfo.GetCustomAttribute<ItemSkipSceneCheckAttribute>() != null;
                    var isPrefab = fieldInfo.GetCustomAttribute<ItemPrefabAttribute>() != null;

                    for (var i = 0; i < values.Length; i++)
                    {
                        if (!canBeNull && values[i] == null)
                            throw new UnassignedReferenceException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] not set");

                        if (!skipSceneCheck && values[i] != null)
                        {
                            if (isPrefab)
                            {
                                if (values[i].scene != default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] must be a prefab");
                            }
                            else
                            {
                                if (values[i].scene == default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] must be a scene object");
                            }
                        }
                    }
                }
                else if (fieldInfo.FieldType == typeof(List<GameObject>))
                {
                    var values = fieldInfo.GetValue(current) as List<GameObject> ?? new List<GameObject>();

                    var canBeNull = fieldInfo.GetCustomAttribute<ItemCanBeNullAttribute>() != null;
                    var skipSceneCheck = fieldInfo.GetCustomAttribute<ItemSkipSceneCheckAttribute>() != null;
                    var isPrefab = fieldInfo.GetCustomAttribute<ItemPrefabAttribute>() != null;

                    for (var i = 0; i < values.Count; i++)
                    {
                        if (!canBeNull && values[i] == null)
                            throw new UnassignedReferenceException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] not set");

                        if (!skipSceneCheck && values[i] != null)
                        {
                            if (isPrefab)
                            {
                                if (values[i].scene != default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] must be a prefab");
                            }
                            else
                            {
                                if (values[i].scene == default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] must be a scene object");
                            }
                        }
                    }
                }
                else if (fieldInfo.FieldType.IsSubclassOf(typeof(Component)))
                {
                    var value = fieldInfo.GetValue(current) as Component;

                    var canBeNull = fieldInfo.GetCustomAttribute<CanBeNullAttribute>() != null;
                    var skipSceneCheck = fieldInfo.GetCustomAttribute<SkipSceneCheckAttribute>() != null;
                    var isPrefab = fieldInfo.GetCustomAttribute<PrefabAttribute>() != null;

                    if (!canBeNull && value == null)
                        throw new UnassignedReferenceException($"{current.gameObject.GetPath()}: {fieldInfo.Name} not set");

                    if (!skipSceneCheck && value != null)
                    {
                        if (isPrefab)
                        {
                            if (value.gameObject.scene != default)
                                throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} must be a prefab");
                        }
                        else
                        {
                            if (value.gameObject.scene == default)
                                throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} must be a scene object");
                        }
                    }
                }
                else if (fieldInfo.FieldType.IsSubclassOf(typeof(Object)))
                {
                    var value = fieldInfo.GetValue(current) as Object;

                    var canBeNull = fieldInfo.GetCustomAttribute<CanBeNullAttribute>() != null;

                    if (!canBeNull && value == null)
                        throw new UnassignedReferenceException($"{current.gameObject.GetPath()}: {fieldInfo.Name} not set");
                }
                else if (fieldInfo.FieldType.IsSubclassOf(typeof(Array)))
                {
                    var values = fieldInfo.GetValue(current) as Object[] ?? new Object[0];

                    var canBeNull = fieldInfo.GetCustomAttribute<ItemCanBeNullAttribute>() != null;
                    var skipSceneCheck = fieldInfo.GetCustomAttribute<ItemSkipSceneCheckAttribute>() != null;
                    var isPrefab = fieldInfo.GetCustomAttribute<ItemPrefabAttribute>() != null;

                    for (var i = 0; i < values.Length; i++)
                    {
                        if (!canBeNull && values[i] == null)
                            throw new UnassignedReferenceException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] not set");

                        if (!skipSceneCheck && values[i] as Component is var c && c != null)
                        {
                            if (isPrefab)
                            {
                                if (c.gameObject.scene != default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] must be a prefab");
                            }
                            else
                            {
                                if (c.gameObject.scene == default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} [{i}] must be a scene object");
                            }
                        }
                    }
                }
                else if (fieldInfo.FieldType.IsInterfaceOf<IList>())
                {
                    var values = ((IList) fieldInfo.GetValue(current)).OfType<Object>();

                    var canBeNull = fieldInfo.GetCustomAttribute<ItemCanBeNullAttribute>() != null;
                    var skipSceneCheck = fieldInfo.GetCustomAttribute<ItemSkipSceneCheckAttribute>() != null;
                    var isPrefab = fieldInfo.GetCustomAttribute<ItemPrefabAttribute>() != null;

                    foreach (var value in values)
                    {
                        if (!canBeNull && value == null)
                            throw new UnassignedReferenceException($"{current.gameObject.GetPath()}: {fieldInfo.Name} not set");

                        if (!skipSceneCheck && value as Component is var c && c != null)
                        {
                            if (isPrefab)
                            {
                                if (c.gameObject.scene != default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} must be a prefab");
                            }
                            else
                            {
                                if (c.gameObject.scene == default)
                                    throw new UnityException($"{current.gameObject.GetPath()}: {fieldInfo.Name} must be a scene object");
                            }
                        }
                    }
                }
            }
#endif
        }

        public static void IsNotNull(string name, params object[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == null)
                    throw new UnassignedReferenceException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} is null");
            }
        }

        public static void IsNull(string name, params object[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] != null)
                    throw new UnassignedReferenceException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} is not null");
            }
        }

        public static void IsSet(string name, params object[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == null)
                    throw new UnassignedReferenceException($"{name}: {GetParamName(GetMethodParams(2), i + 2)} not set");
            }
        }

        public static void IsSet(string name, Type type, params object[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == null || values[i].GetType() != type)
                    throw new UnassignedReferenceException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} [{type}] not set");
            }
        }

        public static void IsSet(string name, params Object[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == null)
                    throw new UnassignedReferenceException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} not set");
            }
        }

        public static void IsSet(string name, params Component[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == null)
                    throw new UnassignedReferenceException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} not set");
            }
        }

        public static void IsSet(string name, params GameObject[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == null)
                    throw new UnassignedReferenceException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} not set");
            }
        }

        public static void IsPrefab(string name, params GameObject[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i].scene != default)
                    throw new UnityException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} must be a prefab");
            }
        }

        public static void IsPrefab(string name, params Component[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i].gameObject.scene != default)
                    throw new UnityException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} must be a prefab");
            }
        }

        public static void IsSceneObject(string name, params GameObject[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i].scene == default)
                    throw new UnityException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} must be a scene object");
            }
        }

        public static void IsSceneObject(string name, params Component[] values)
        {
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i].gameObject.scene == default)
                    throw new UnityException($"{name}: {GetParamName(GetMethodParams(2), i + 1)} must be a scene object");
            }
        }

        public static string GetParamName(string str, int index)
        {
            var arr = str.Split(',');
            return arr.Length > index ? arr[index] : string.Empty;
        }

        public static string GetMethodParams(int level = 1)
        {
#if UNITY_EDITOR
            var stackFrame = new StackTrace(true).GetFrame(level);
            var fileName = stackFrame.GetFileName() ?? string.Empty;
            var lineNumber = stackFrame.GetFileLineNumber();

            if (string.IsNullOrEmpty(fileName))
                return string.Empty;

            try
            {
                string line;

                using (var file = new StreamReader(fileName))
                {
                    for (var i = 0; i < lineNumber - 1; i++)
                        file.ReadLine();

                    line = file.ReadLine() ?? string.Empty;
                }

                var startWith = line.IndexOf('(');
                var endWith = line.LastIndexOf(')');

                return line.Substring(startWith + 1, endWith - (startWith + 1));
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                return string.Empty;
            }
#else
            return string.Empty;
#endif
        }
    }
}