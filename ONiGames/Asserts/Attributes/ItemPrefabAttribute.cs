using System;

namespace ONiGames.Asserts.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ItemPrefabAttribute : Attribute { }
}