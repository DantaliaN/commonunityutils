﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ONiGames.CustomProjectSettings.Utilities
{
    //just fot namespace
    public class IdSettings
    {
        [Serializable]
        public class Id
        {
            public string name = "";
            
            public int uid = 0;
        }
    }

    public class IdSettings<T> : CustomSettings<T> where T : CustomSettingsBase
    {
        [SerializeField]
        List<IdSettings.Id> items = new();

        public IReadOnlyList<IdSettings.Id> Items => items;
    }
}