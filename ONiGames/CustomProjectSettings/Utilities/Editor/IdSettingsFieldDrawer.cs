﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.CustomProjectSettings.Utilities.Editor
{
    [CustomPropertyDrawer(typeof(IdSettings.Id))]
    public class IdSettingsFieldDrawer : PropertyDrawer
    {
        const string NAME_PROP_NAME = "name";
        const string UID_PROP_NAME = "uid";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var nameProp = property.FindPropertyRelative(NAME_PROP_NAME);
            if (nameProp == null || nameProp.propertyType != SerializedPropertyType.String)
                return;

            var uidProp = property.FindPropertyRelative(UID_PROP_NAME);
            if (uidProp == null || uidProp.propertyType != SerializedPropertyType.Integer)
                return;

            EditorGUIUtility.labelWidth = 50f;
            
            EditorGUI.PropertyField(position, nameProp);

            EditorGUI.BeginDisabledGroup(true);

            GUILayout.TextField(uidProp.intValue.ToString());

            EditorGUI.EndDisabledGroup();
        }
    }
}

#endif