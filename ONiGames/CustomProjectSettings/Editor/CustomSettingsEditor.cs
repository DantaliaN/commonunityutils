﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ONiGames.CustomProjectSettings.Editor
{
    [CustomEditor(typeof(CustomSettingsBase))]
    public class CustomSettingsEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var csb = target as CustomSettingsBase;
            if (csb == null)
                return;

            if (GUILayout.Button("Save"))
            {
                csb.Save();
                Debug.Log($"Settings of {csb.GetType().Name} saved!");
            }
        }
    }
}

#endif