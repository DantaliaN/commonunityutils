#if UNITY_EDITOR

using System.IO;
using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace ONiGames.CustomProjectSettings.Editor
{
    [UsedImplicitly]
    public class CustomSettingsBuilder : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        public int callbackOrder => 0;

        public void OnPreprocessBuild(BuildReport report)
        {
            //Force save the settings before build
            ProjectSave.OnWillSaveAssets(null);

            var rootFolder = CustomSettingsUtility.SettingsFolder;
            var targetFolder = Path.Combine(Application.dataPath, "Resources", "CustomSettings");

            //NO BUENO EXISTING FOLDER
            if (Directory.Exists(targetFolder))
                Directory.Delete(targetFolder, true);

            //Ensure the resources directory exists !!!
            if (!Directory.Exists(Path.Combine(Application.dataPath, "Resources")))
                Directory.CreateDirectory(Path.Combine(Application.dataPath, "Resources"));

            Directory.CreateDirectory(targetFolder);

            //Try to copy all files from target directory
            if (Directory.Exists(rootFolder))
                Copy(rootFolder, targetFolder);
        }

        public void OnPostprocessBuild(BuildReport report)
        {
            var folder = Path.Combine(Application.dataPath, "Resources", "CustomSettings");

            //just straight up delete that fam

            if (Directory.Exists(folder))
            {
                var folderMeta = Path.Combine(Application.dataPath, "Resources", "CustomSettings.meta");
                if (File.Exists(folderMeta))
                    File.Delete(folderMeta);

                Directory.Delete(folder, true);

                var resFolder = Path.Combine(Application.dataPath, "Resources");
                if (Directory.GetFiles(resFolder).Length == 0)
                {
                    var resFolderMeta = Path.Combine(Application.dataPath, "Resources.meta");
                    if (File.Exists(resFolderMeta))
                        File.Delete(resFolderMeta);

                    Directory.Delete(resFolder, true);
                }
            }
        }

        /// <summary>
        /// Copies all .json files in <paramref name="sourceDir"/> to <paramref name="targetDir"/>
        /// </summary>
        static void Copy(string sourceDir, string targetDir)
        {
            var files = Directory.GetFiles(sourceDir);
            foreach (var file in files)
            {
                var ext = Path.GetExtension(file);
                if (ext != ".json")
                    continue;

                var fileName = Path.GetFileName(file);
                File.Copy(file, Path.Combine(targetDir, fileName));
                AssetDatabase.ImportAsset(Path.Combine("Assets", "Resources", "CustomSettings", fileName), ImportAssetOptions.ForceUpdate);
            }
        }
    }
}

#endif