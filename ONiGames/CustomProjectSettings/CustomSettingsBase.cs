using System;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.CustomProjectSettings
{
    public abstract class CustomSettingsBase : ScriptableObject
    {
        public DateTime writeTime;
        
        public void Initialise()
        {
            OnInitialise();
            
            Save();
        }

        public void Save()
        {
            OnWillSave();
            
            CustomSettingsUtility.SaveInstance(this);
        }

        protected virtual void OnWillSave()
        {

        }

        protected virtual void OnInitialise()
        {

        }
    }

    public abstract class CustomSettings<T> : CustomSettingsBase where T : CustomSettingsBase
    {
        public static T Instance
        {
            get
            {
                if (instance == null)
                    CustomSettingsUtility.GetInstance(out instance);

                CustomSettingsUtility.UpdateInstanceIfNeeded(instance);
                
                return instance;
            }
        }
        
        [CanBeNull]
        static T instance;

        protected static void Select()
        {
#if UNITY_EDITOR
            UnityEditor.Selection.activeObject = Instance;
#endif
        }
    }
}