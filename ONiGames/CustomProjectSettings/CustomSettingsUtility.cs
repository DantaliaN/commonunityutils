using System;
using System.IO;
using System.Linq;
using UnityEngine;

namespace ONiGames.CustomProjectSettings
{
    public static class CustomSettingsUtility
    {
        public static string SettingsFolder => Directory.GetParent(Application.dataPath) + "/CustomProjectSettings"; //Application.streamingAssetsPath + "/CustomProjectSettings/";

        public static void UpdateInstanceIfNeeded<T>(T settings) where T : CustomSettingsBase
        {
#if UNITY_EDITOR
            var fileNameWithoutExtension = $"{typeof(T)}";
            
            var fileName = $"{fileNameWithoutExtension}.json";

            var folder = SettingsFolder;

            var path = Path.Combine(folder, fileName);

            if (File.Exists(path))
            {
                try
                {
                    //we can do some another checks for update data from disk
                    if (settings.writeTime != File.GetLastWriteTimeUtc(path))
                    {
                        JsonUtility.FromJsonOverwrite(File.ReadAllText(path), settings);

                        settings.writeTime = File.GetLastWriteTimeUtc(path);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning($"{nameof(CustomSettingsUtility)}: Failed to check file LastWriteTimeUtc {path} {e}");
                }
            }
#endif
        }

        public static void GetInstance<T>(out T settings) where T : CustomSettingsBase
        {
            var fileNameWithoutExtension = $"{typeof(T)}";

#if UNITY_EDITOR
            var fileName = $"{fileNameWithoutExtension}.json";
            
            var folder = SettingsFolder;

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            
            var path = Path.Combine(folder, fileName);

            if (File.Exists(path))
            {
                try
                {
                    settings = (T) ScriptableObject.CreateInstance(typeof(T));

                    JsonUtility.FromJsonOverwrite(File.ReadAllText(path), settings);

                    settings.writeTime = File.GetLastWriteTimeUtc(path);

                    if (!ProjectSave.HasReference(settings.Save))
                        ProjectSave.Save += settings.Save;
                    return;
                }
                catch (Exception e)
                {
                    Debug.LogWarning($"{nameof(CustomSettingsUtility)}: Failed to read type of {typeof(T)} from {path} : {e}");
                }
            }

            Debug.Log($"{nameof(CustomSettingsUtility)}: Creating new instance of {typeof(T)}");

            settings = (T) ScriptableObject.CreateInstance(typeof(T));
            settings.Initialise();

            if (!ProjectSave.HasReference(settings.Save))
                ProjectSave.Save += settings.Save;
#else
            try
            {
                settings = (T) ScriptableObject.CreateInstance(typeof(T));

                var txtAsset = Resources.Load<TextAsset>(Path.Combine("CustomSettings", fileNameWithoutExtension));

                JsonUtility.FromJsonOverwrite(txtAsset.text, settings);
            }
            catch
            {
                Debug.LogWarning($"{nameof(CustomSettingsUtility)}: Failed to read custom setting {fileNameWithoutExtension}. Creating default instance.");

                settings = (T) ScriptableObject.CreateInstance(typeof(T));
                settings.Initialise();
            }
#endif
        }

        public static void SaveInstance(CustomSettingsBase settings)
        {
#if UNITY_EDITOR
            var fileNameWithoutExtension = $"{settings.GetType()}";
            
            var fileName = $"{fileNameWithoutExtension}.json";
            
            var folder = SettingsFolder;

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            var path = Path.Combine(folder, fileName);

            try
            {
                var json = JsonUtility.ToJson(settings, true);

                if (File.Exists(path))
                {
                    var old = File.ReadAllText(path);
                    if (string.Equals(old, json))
                        return;
                }

                File.WriteAllText(path, json);

                settings.writeTime = File.GetLastWriteTimeUtc(path);
            }
            catch (Exception e)
            {
                Debug.LogError($"{nameof(CustomSettingsUtility)}: Failed to save type of {fileNameWithoutExtension} to {path} : {e}");
            }
#else
            Debug.LogWarning($"{nameof(CustomSettingsUtility)}: Cannot save project settings at runtime");
#endif
        }
    }

#if UNITY_EDITOR
    public class ProjectSave : UnityEditor.AssetModificationProcessor
    {
        public static event Action Save = delegate { };

        public static string[] OnWillSaveAssets(string[] paths)
        {
            Save.Invoke();
            return paths;
        }

        public static bool HasReference(Action action)
        {
            var list = Save.GetInvocationList();
            return list.Any(item => item.Target == action.Target && item.Method == action.Method);
        }
    }
#endif
}