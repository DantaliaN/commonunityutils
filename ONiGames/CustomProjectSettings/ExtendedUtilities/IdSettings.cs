﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.CustomProjectSettings.ExtendedUtilities
{
    //just fot namespace
    public class IdSettings
    {
        [Serializable]
        public class Id
        {
            [NotNull]
            public string name = string.Empty;

            [ReadOnly]
            public int uid = 0;
        }
    }

    public abstract class ASettings<TId, T> : CustomSettings<T> where TId : IdSettings.Id, new() where T : CustomSettingsBase
    {
        [ListDrawerSettings(CustomAddFunction = nameof(AddItem))]
        [SerializeField]
        //[Searchable]
        List<TId> items = new();

        public IReadOnlyList<TId> Items => items;

        public static int GenerateUid()
        {
#if UNITY_EDITOR
            return UnityEditor.GUID.Generate().GetHashCode();
#else
            return System.Guid.NewGuid().GetHashCode();
#endif
        }

        void AddItem()
        {
            items.Add(new TId { uid = GenerateUid() });
        }
    }

    public class IdSettings<T> : ASettings<IdSettings.Id, T> where T : CustomSettingsBase { }
}