﻿#if UNITY_EDITOR

using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.CustomProjectSettings.ExtendedUtilities.Editor
{
    [CustomEditor(typeof(CustomSettingsBase), true)]
    public class IdSettingsEditor : OdinEditor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GUILayout.Space(10f);

            var csb = target as CustomSettingsBase;
            if (csb == null)
                return;

            GUILayout.Space(10f);

            if (GUILayout.Button("Save"))
            {
                csb.Save();
                Debug.Log($"{name}: Settings of {csb.GetType().Name} saved!");
            }
        }
    }
}

#endif