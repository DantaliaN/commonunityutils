using System;
using System.Collections;
using JetBrains.Annotations;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities
{
    public static class Utils
    {
        public static void OpenGoogleSheet(string id, string gid)
        {
            const string editUrlPattern = "https://docs.google.com/spreadsheets/d/{0}/edit#gid={1}";
            
            Application.OpenURL(string.Format(editUrlPattern, id, gid));
        }
        
        public static string LoadGoogleSheet(string id, string gid)
        {
            const string urlPattern = "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&gid={1}";

            return ONiGames.Utilities.Utils.HttpGet(string.Format(urlPattern, id, gid), "text/csv");
        }

        public static IEnumerator PlayTimedValueUnscale(float start, float stop, float step, float stepDelay, Action<float> onUpdate, Action onComplete)
        {
            if (start >= stop || step <= 0f)
            {
                onComplete();
                yield break;
            }

            var value = start;

            var delay = 0f;

            while (value < stop)
            {
                delay += stepDelay;
                if (delay > Time.unscaledDeltaTime && Time.unscaledDeltaTime > 0f)
                {
                    var mod = delay % Time.unscaledDeltaTime;
                    var div = delay / Time.unscaledDeltaTime - mod;
                    delay -= Time.unscaledDeltaTime * div;
                    yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime * div);
                }

                value += step;

                onUpdate(value);
            }

            onComplete();
        }

        public static IEnumerator PlayTimedValueUnscale(float start, float stop, AnimationCurve curve, float time, Action<float> onUpdate, Action onComplete)
        {
            if (start >= stop || time <= 0f)
            {
                onComplete();
                yield break;
            }

            var delay = 0f;

            while (delay < time)
            {
                yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);

                delay += Time.unscaledDeltaTime;

                var value = start + (stop - start) * curve.Evaluate(delay / time);

                onUpdate(value);
            }

            onComplete();
        }

        public static IEnumerator PlayTimedValue(float start, float stop, float step, float stepDelay, Action<float> onUpdate, Action onComplete)
        {
            if (start >= stop || step <= 0f)
            {
                onComplete();
                yield break;
            }

            var value = start;

            var delay = 0f;

            while (value < stop)
            {
                delay += stepDelay;
                if (delay > Time.deltaTime && Time.deltaTime > 0f)
                {
                    var mod = delay % Time.deltaTime;
                    var div = delay / Time.deltaTime - mod;
                    delay -= Time.deltaTime * div;
                    yield return new WaitForSeconds(Time.deltaTime * div);
                }

                value += step;

                onUpdate(value);
            }

            onComplete();
        }

        public static IEnumerator PlayTimedValue(float start, float stop, AnimationCurve curve, float time, Action<float> onUpdate, Action onComplete)
        {
            if (start >= stop || time <= 0f)
            {
                onComplete();
                yield break;
            }

            var delay = 0f;

            while (delay < time)
            {
                yield return new WaitForSeconds(Time.deltaTime);

                delay += Time.deltaTime;

                var value = start + (stop - start) * curve.Evaluate(delay / time);

                onUpdate(value);
            }

            onComplete();
        }

        public static ValueDropdownList<string> GetAnimatorLayerItems([CanBeNull] Animator animator)
        {
            var values = new ValueDropdownList<string>();

#if UNITY_EDITOR
            if (animator == null)
                return values;

            var ac = animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
            if (ac == null)
            {
                Debug.LogWarning($"{animator.gameObject.GetPath()}: Animation Controller not found");
                return values;
            }

            foreach (var name in ac.GetLayerNames())
                values.Add(name);
#endif

            return values;
        }

        public static ValueDropdownList<AnimationStateProperty> GetAnimatorStateItems([CanBeNull] Animator animator)
        {
            var values = new ValueDropdownList<AnimationStateProperty>();

#if UNITY_EDITOR
            if (animator == null)
                return values;

            var ac = animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
            if (ac == null)
            {
                Debug.LogWarning($"{animator.gameObject.GetPath()}: Animation Controller not found");
                return values;
            }

            foreach (var name in ac.GetStateNames())
                values.Add(new AnimationStateProperty { name = name });
#endif

            return values;
        }

        public static ValueDropdownList<string> GetAnimatorClipItems([CanBeNull] Animator animator)
        {
            var values = new ValueDropdownList<string>();

#if UNITY_EDITOR
            if (animator == null)
                return values;

            var ac = animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
            if (ac == null)
            {
                Debug.LogWarning($"{animator.gameObject.GetPath()}: Animation Controller not found");
                return values;
            }

            foreach (var name in ac.GetClipNames())
                values.Add(name);
#endif

            return values;
        }

        public static ValueDropdownList<AnimationStateProperty> GetAnimatorParameterItems([CanBeNull] Animator animator)
        {
            var values = new ValueDropdownList<AnimationStateProperty>();

#if UNITY_EDITOR
            if (animator == null)
                return values;

            var ac = animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
            if (ac == null)
            {
                Debug.LogWarning($"{animator.gameObject.GetPath()}: Animation Controller not found");
                return values;
            }

            foreach (var name in ac.GetParameterNames())
                values.Add(new AnimationStateProperty { name = name });
#endif

            return values;
        }

        public static int GetBundleVersionCode()
        {
#if UNITY_ANDROID
            try
            {
                var contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
                var packageManager = context.Call<AndroidJavaObject>("getPackageManager");
                var packageName = context.Call<string>("getPackageName");
                var packageInfo = packageManager.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
                return packageInfo.Get<int>("versionCode");
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                return 0;
            }
#else
            return 0;
#endif
        }

        public static string GetBundleVersionName()
        {
#if UNITY_ANDROID
            try
            {
                var contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
                var packageManager = context.Call<AndroidJavaObject>("getPackageManager");
                var packageName = context.Call<string>("getPackageName");
                var packageInfo = packageManager.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
                return packageInfo.Get<string>("versionName");
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                return string.Empty;
            }
#else
            return string.Empty;
#endif
        }

        public static int Lerp(int a, int b, float t)
        {
            return Mathf.FloorToInt(a + (b - a) * t);
        }
    }
}