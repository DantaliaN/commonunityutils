#if UNITY_EDITOR

using JetBrains.Annotations;
using ONiGames.Utilities;
using Sirenix.OdinInspector.Editor;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.Editor
{
    [UsedImplicitly]
    public class ShaderPropertyFieldDrawer : OdinValueDrawer<ShaderProperty>
    {
        const string PROP_NAME = "name";

        [CanBeNull]
        InspectorProperty nameProperty;

        protected override void Initialize()
        {
            nameProperty = Property.Children[PROP_NAME];
        }

        protected override void DrawPropertyLayout(GUIContent label)
        {
            nameProperty?.Draw(label);
        }
    }
}

#endif