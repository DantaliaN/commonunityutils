#if UNITY_EDITOR

using UnityEditor;

namespace ONiGames.Adjectives.Utilities.Editor
{
    public static class UtilsEditor
    {
        public static void FixTextureCompression()
        {
            foreach (var path in ONiGames.Utilities.Editor.UtilsEditor.FindAssetsPath("Texture2D"))
            {
                if (!path.StartsWith("Assets/FlappyLand"))
                    continue;

                var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
                if (textureImporter == null)
                    continue;

                if (textureImporter.textureType != TextureImporterType.Default)
                    continue;

                if (!(textureImporter.GetDefaultPlatformTextureSettings()?.crunchedCompression ?? false))
                    continue;

                var platforms = new[] { "Android", "iPhone", "Standalone" };
                foreach (var platformName in platforms)
                {
                    var platformSettings = textureImporter.GetPlatformTextureSettings(platformName);
                    if (platformSettings == null)
                        continue;

                    if (!platformSettings.overridden)
                    {
                        platformSettings.overridden = true;
                        platformSettings.format = TextureImporterFormat.ETC2_RGBA8Crunched;
                        platformSettings.compressionQuality = 100;
                        platformSettings.maxTextureSize = 4096;

                        textureImporter.SetPlatformTextureSettings(platformSettings);

                        EditorUtility.SetDirty(textureImporter);

                        textureImporter.SaveAndReimport();
                    }
                }
            }
        }
    }
}

#endif