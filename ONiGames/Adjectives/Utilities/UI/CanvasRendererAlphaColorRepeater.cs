using ONiGames.Asserts;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(CanvasRenderer))]
    public class CanvasRendererAlphaColorRepeater : MonoBehaviour
    {
        [SerializeField]
        CanvasRenderer target;

        [SerializeField]
        bool invert = false;

        CanvasRenderer _canvasRenderer;
        CanvasRenderer CanvasRenderer => _canvasRenderer ??= GetComponent<CanvasRenderer>();

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Start()
        {
            LateUpdate();
        }

        void LateUpdate()
        {
            CanvasRenderer.SetAlpha(!invert ? target.GetAlpha() : 1f - target.GetAlpha());
        }
    }
}