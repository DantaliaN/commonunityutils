using System;
using System.Reflection;
using ONiGames.Asserts;
using ONiGames.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(Selectable))]
    public class SelectableTransitionRepeater : MonoBehaviour
    {
        [SerializeField]
        Selectable target;

        [NonSerialized]
        Selectable _selectable;
        Selectable Selectable => _selectable ??= GetComponent<Selectable>();

        /*SelectionState*/ int oldState = 0;

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Start()
        {
            LateUpdate();
        }

        void LateUpdate()
        {
            var currentSelectionState = 0;

            try
            {
                var propertyInfo = target.GetType().GetProperty("currentSelectionState", BindingFlags.NonPublic | BindingFlags.Instance);
                if (propertyInfo != null)
                    currentSelectionState = (int) propertyInfo.GetValue(target);
            }
            catch (Exception ex)
            {
                Debug.LogError($"{gameObject.GetPath()}: {ex}");
                return;
            }

            if (oldState == currentSelectionState)
                return;

            oldState = currentSelectionState;

            try
            {
                var methodInfo = Selectable.GetType().GetMethod("DoStateTransition", BindingFlags.NonPublic | BindingFlags.Instance);
                if (methodInfo != null)
                    methodInfo.Invoke(Selectable, new[] { (object) currentSelectionState, false });
            }
            catch (Exception ex)
            {
                Debug.LogError($"{gameObject.GetPath()}: {ex}");
            }
        }
    }
}