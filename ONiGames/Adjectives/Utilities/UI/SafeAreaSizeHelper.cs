using System;
using JetBrains.Annotations;
using ONiGames.Asserts;
using UnityEngine;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(LayoutElement))]
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaSizeHelper : MonoBehaviour
    {
        [SerializeField]
        Canvas canvas;

        [SerializeField]
        CanvasScaler canvasScaler;

        [SerializeField]
        [Min(0f)]
        float preferredHeight = 0f;

        [SerializeField]
        [Min(0f)]
        float preferredWidth = 0f;

        [SerializeField]
        bool moveUpper = true;

#pragma warning disable CS0414
        [UsedImplicitly]
        [SerializeField]
        bool useSaveZoneIOS = true;

        [UsedImplicitly]
        [SerializeField]
        bool useSaveZoneAndroid = true;
#pragma warning restore CS0414

        [SerializeField]
        Vector2 offset = Vector2.zero;

        [CanBeNull]
        [NonSerialized]
        LayoutElement _layoutElement;
        LayoutElement Element => _layoutElement ??= GetComponent<LayoutElement>();

        [CanBeNull]
        [NonSerialized]
        RectTransform _rect;
        RectTransform Rect => _rect ??= GetComponent<RectTransform>();

        Rect currentCanvasRect;
        Rect currentSafeArea;
        ScreenOrientation screenOrientation = ScreenOrientation.AutoRotation;

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Update()
        {
            var canvasRect = canvas.pixelRect;
            var saveArea = canvasRect;

#if UNITY_IOS
            if (useSaveZoneIOS)
                saveArea = Screen.safeArea;
#elif UNITY_ANDROID
            if (useSaveZoneAndroid)
                saveArea = Screen.safeArea;
#endif

            if (currentCanvasRect != canvasRect || saveArea != currentSafeArea || screenOrientation != Screen.orientation)
            {
                screenOrientation = Screen.orientation;
                currentSafeArea = saveArea;
                currentCanvasRect = canvasRect;

                ApplySafeArea();
            }

            void ApplySafeArea()
            {
                var anchorMin = currentSafeArea.position;
                var anchorMax = currentSafeArea.position + currentSafeArea.size + offset;

                var refResolution = canvasScaler.referenceResolution;

                if (Mathf.Approximately(currentCanvasRect.width, 0f) || Mathf.Approximately(currentCanvasRect.height, 0f))
                    return;

                anchorMin.x /= currentCanvasRect.width;
                anchorMin.y /= currentCanvasRect.height;

                anchorMax.x /= currentCanvasRect.width;
                anchorMax.y /= currentCanvasRect.height;

                if (moveUpper)
                {
                    var width = preferredWidth + (1f - anchorMax.x) * refResolution.x;
                    var height = preferredHeight + (1f - anchorMax.y) * refResolution.y;

                    if (Element.preferredHeight > -1)
                        Element.preferredHeight = height;
                    if (Element.preferredWidth > -1)
                        Element.preferredWidth = width;

                    Rect.sizeDelta = new Vector2(width, height);
                }
            }
        }
    }
}