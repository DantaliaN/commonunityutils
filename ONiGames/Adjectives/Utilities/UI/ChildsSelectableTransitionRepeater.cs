using System;
using System.Reflection;
using ONiGames.Asserts;
using ONiGames.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Utilities.UI
{
    public class ChildsSelectableTransitionRepeater : MonoBehaviour
    {
        [SerializeField]
        Selectable target;

        /*SelectionState*/ int oldState = 0;

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Start()
        {
            LateUpdate();
        }

        void LateUpdate()
        {
            var currentSelectionState = 0;
            
            try
            {
                var propertyInfo = target.GetType().GetProperty("currentSelectionState", BindingFlags.NonPublic | BindingFlags.Instance);
                if (propertyInfo != null)
                    currentSelectionState = (int) propertyInfo.GetValue(target);
            }
            catch (Exception ex)
            {
                Debug.LogError($"{gameObject.GetPath()}: {ex}");
                return;
            }
            
            if (oldState == currentSelectionState)
                return;
            
            oldState = currentSelectionState;
            
            foreach (var selectable in GetComponentsInChildren<Selectable>())
            {
                try
                {
                    var methodInfo = selectable.GetType().GetMethod("DoStateTransition", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (methodInfo != null)
                        methodInfo.Invoke(selectable, new[] { (object) currentSelectionState, false });
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                }
            }
        }
    }
}