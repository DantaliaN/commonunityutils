using ONiGames.Utilities;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI
{
    public class SimpleSinglet : MonoBehaviour
    {
        void Awake()
        {
            //make singlet any
            transform.SetParent(null);

            var cacheName = name;
            if (!ONiGames.Utilities.Utils.MakeSinglet(this))
                Debug.LogWarning($"{gameObject.GetPath()}: Found another Singleton {typeof(SimpleSinglet)} on the scene! {cacheName} was destroyed!");
        }
    }
}