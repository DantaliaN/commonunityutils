using System;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Asserts;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(Transform))]
    public class AspectScaler : MonoBehaviour
    {
        [Serializable]
        public class Item
        {
            public Vector2Int aspectRatio = Vector2Int.zero;

            public Vector3 scale = Vector3.one;
        }

        [SerializeField]
        Camera cam;

        [SerializeField]
        float approximate = 0.01f;

        [SerializeField]
        Item[] items = new Item[0];

        [CanBeNull]
        [NonSerialized]
        Transform _transform;
        Transform Transform => _transform ??= GetComponent<Transform>();

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Start()
        {
            RecalculateAspect();
        }

        public void RecalculateAspect()
        {
            var item = items.OrderBy(o => (float) o.aspectRatio.x / o.aspectRatio.y).FirstOrDefault(o => cam.aspect < (float) o.aspectRatio.x / o.aspectRatio.y + approximate);
            if (item != null)
                Transform.localScale = item.scale;
        }
    }
}