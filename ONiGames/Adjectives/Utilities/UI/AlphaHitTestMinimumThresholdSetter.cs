using JetBrains.Annotations;
using ONiGames.Asserts;
using UnityEngine;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(Image))]
    public class AlphaHitTestMinimumThresholdSetter : MonoBehaviour
    {
        [Range(0f, 1f)]
        [SerializeField]
        float alphaHitTestMinimumThreshold = 0.5f;
        
        [CanBeNull]
        Image _image;
        Image Image => _image ??= GetComponent<Image>();
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
        
        void Start()
        {
            Image.alphaHitTestMinimumThreshold = alphaHitTestMinimumThreshold;
        }
    }
}