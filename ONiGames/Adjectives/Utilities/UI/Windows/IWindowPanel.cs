namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    interface IWindowPanel
    {
        void Open();
        void Close();
    }
}