using System;
using JetBrains.Annotations;

namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class APopupAnimatedWindow : AAnimatedWindow
    {
        [CanBeNull]
        Action close = null;

        public void Show(Action onClose)
        {
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }
    
    public abstract class APopupAnimatedWindow<TModel> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action onClose)
        {
            Model = model;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup1AnimatedWindow : AAnimatedWindow
    {
        [CanBeNull]
        Action action = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action onAction, Action onClose)
        {
            action = onAction;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action()
        {
            MakeAction();

            Close(() =>
            {
                action?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup1AnimatedWindow<TModel> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action action = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action onAction, Action onClose)
        {
            Model = model;
            action = onAction;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action()
        {
            MakeAction();

            Close(() =>
            {
                action?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup1TAnimatedWindow<T> : AAnimatedWindow
    {
        [CanBeNull]
        Action<T> action = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action<T> onAction, Action onClose)
        {
            action = onAction;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action(T obj)
        {
            MakeAction(obj);

            Close(() =>
            {
                action?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction(T obj) { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup1TAnimatedWindow<TModel, T> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action<T> action = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action<T> onAction, Action onClose)
        {
            Model = model;
            action = onAction;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action(T obj)
        {
            MakeAction(obj);

            Close(() =>
            {
                action?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction(T obj) { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup2AnimatedWindow : AAnimatedWindow
    {
        [CanBeNull]
        Action action1 = null;

        [CanBeNull]
        Action action2 = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action onAction1, Action onAction2, Action onClose)
        {
            action1 = onAction1;
            action2 = onAction2;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1()
        {
            MakeAction1();

            Close(() =>
            {
                action1?.Invoke();
            });
        }

        protected void Action2()
        {
            MakeAction2();

            Close(() =>
            {
                action2?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1() { }
        protected virtual void MakeAction2() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup2AnimatedWindow<TModel> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action action1 = null;

        [CanBeNull]
        Action action2 = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action onAction1, Action onAction2, Action onClose)
        {
            Model = model;
            action1 = onAction1;
            action2 = onAction2;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1()
        {
            MakeAction1();

            Close(() =>
            {
                action1?.Invoke();
            });
        }

        protected void Action2()
        {
            MakeAction2();

            Close(() =>
            {
                action2?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1() { }
        protected virtual void MakeAction2() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup2TAnimatedWindow<T1, T2> : AAnimatedWindow
    {
        [CanBeNull]
        Action<T1> action1 = null;

        [CanBeNull]
        Action<T2> action2 = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action<T1> onAction1, Action<T2> onAction2, Action onClose)
        {
            action1 = onAction1;
            action2 = onAction2;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1(T1 obj)
        {
            MakeAction1(obj);

            Close(() =>
            {
                action1?.Invoke(obj);
            });
        }

        protected void Action2(T2 obj)
        {
            MakeAction2(obj);

            Close(() =>
            {
                action2?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1(T1 obj) { }
        protected virtual void MakeAction2(T2 obj) { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup2TAnimatedWindow<TModel, T1, T2> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action<T1> action1 = null;

        [CanBeNull]
        Action<T2> action2 = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action<T1> onAction1, Action<T2> onAction2, Action onClose)
        {
            Model = model;
            action1 = onAction1;
            action2 = onAction2;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1(T1 obj)
        {
            MakeAction1(obj);

            Close(() =>
            {
                action1?.Invoke(obj);
            });
        }

        protected void Action2(T2 obj)
        {
            MakeAction2(obj);

            Close(() =>
            {
                action2?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1(T1 obj) { }
        protected virtual void MakeAction2(T2 obj) { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup3AnimatedWindow : AAnimatedWindow
    {
        [CanBeNull]
        Action action1 = null;

        [CanBeNull]
        Action action2 = null;

        [CanBeNull]
        Action action3 = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action onAction1, Action onAction2, Action onAction3, Action onClose)
        {
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1()
        {
            MakeAction1();

            Close(() =>
            {
                action1?.Invoke();
            });
        }

        protected void Action2()
        {
            MakeAction2();

            Close(() =>
            {
                action2?.Invoke();
            });
        }

        protected void Action3()
        {
            MakeAction3();

            Close(() =>
            {
                action3?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1() { }
        protected virtual void MakeAction2() { }
        protected virtual void MakeAction3() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup3AnimatedWindow<TModel> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action action1 = null;

        [CanBeNull]
        Action action2 = null;

        [CanBeNull]
        Action action3 = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action onAction1, Action onAction2, Action onAction3, Action onClose)
        {
            Model = model;
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1()
        {
            MakeAction1();

            Close(() =>
            {
                action1?.Invoke();
            });
        }

        protected void Action2()
        {
            MakeAction2();

            Close(() =>
            {
                action2?.Invoke();
            });
        }

        protected void Action3()
        {
            MakeAction3();

            Close(() =>
            {
                action3?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1() { }
        protected virtual void MakeAction2() { }
        protected virtual void MakeAction3() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup3TAnimatedWindow<T1, T2, T3> : AAnimatedWindow
    {
        [CanBeNull]
        Action<T1> action1 = null;

        [CanBeNull]
        Action<T2> action2 = null;

        [CanBeNull]
        Action<T3> action3 = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action<T1> onAction1, Action<T2> onAction2, Action<T3> onAction3, Action onClose)
        {
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1(T1 obj)
        {
            MakeAction1(obj);

            Close(() =>
            {
                action1?.Invoke(obj);
            });
        }

        protected void Action2(T2 obj)
        {
            MakeAction2(obj);

            Close(() =>
            {
                action2?.Invoke(obj);
            });
        }

        protected void Action3(T3 obj)
        {
            MakeAction3(obj);

            Close(() =>
            {
                action3?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1(T1 obj) { }
        protected virtual void MakeAction2(T2 obj) { }
        protected virtual void MakeAction3(T3 obj) { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup3TAnimatedWindow<TModel, T1, T2, T3> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action<T1> action1 = null;

        [CanBeNull]
        Action<T2> action2 = null;

        [CanBeNull]
        Action<T3> action3 = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action<T1> onAction1, Action<T2> onAction2, Action<T3> onAction3, Action onClose)
        {
            Model = model;
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1(T1 obj)
        {
            MakeAction1(obj);

            Close(() =>
            {
                action1?.Invoke(obj);
            });
        }

        protected void Action2(T2 obj)
        {
            MakeAction2(obj);

            Close(() =>
            {
                action2?.Invoke(obj);
            });
        }

        protected void Action3(T3 obj)
        {
            MakeAction3(obj);

            Close(() =>
            {
                action3?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1(T1 obj) { }
        protected virtual void MakeAction2(T2 obj) { }
        protected virtual void MakeAction3(T3 obj) { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup4AnimatedWindow : AAnimatedWindow
    {
        [CanBeNull]
        Action action1 = null;

        [CanBeNull]
        Action action2 = null;

        [CanBeNull]
        Action action3 = null;

        [CanBeNull]
        Action action4 = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action onAction1, Action onAction2, Action onAction3, Action onAction4, Action onClose)
        {
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            action4 = onAction4;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1()
        {
            MakeAction1();

            Close(() =>
            {
                action1?.Invoke();
            });
        }

        protected void Action2()
        {
            MakeAction2();

            Close(() =>
            {
                action2?.Invoke();
            });
        }

        protected void Action3()
        {
            MakeAction3();

            Close(() =>
            {
                action3?.Invoke();
            });
        }

        protected void Action4()
        {
            MakeAction4();

            Close(() =>
            {
                action4?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1() { }
        protected virtual void MakeAction2() { }
        protected virtual void MakeAction3() { }
        protected virtual void MakeAction4() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup4AnimatedWindow<TModel> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action action1 = null;

        [CanBeNull]
        Action action2 = null;

        [CanBeNull]
        Action action3 = null;

        [CanBeNull]
        Action action4 = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action onAction1, Action onAction2, Action onAction3, Action onAction4, Action onClose)
        {
            Model = model;
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            action4 = onAction4;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1()
        {
            MakeAction1();

            Close(() =>
            {
                action1?.Invoke();
            });
        }

        protected void Action2()
        {
            MakeAction2();

            Close(() =>
            {
                action2?.Invoke();
            });
        }

        protected void Action3()
        {
            MakeAction3();

            Close(() =>
            {
                action3?.Invoke();
            });
        }

        protected void Action4()
        {
            MakeAction4();

            Close(() =>
            {
                action4?.Invoke();
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1() { }
        protected virtual void MakeAction2() { }
        protected virtual void MakeAction3() { }
        protected virtual void MakeAction4() { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup4TAnimatedWindow<T1, T2, T3, T4> : AAnimatedWindow
    {
        [CanBeNull]
        Action<T1> action1 = null;

        [CanBeNull]
        Action<T2> action2 = null;

        [CanBeNull]
        Action<T3> action3 = null;

        [CanBeNull]
        Action<T4> action4 = null;

        [CanBeNull]
        Action close = null;

        public void Show(Action<T1> onAction1, Action<T2> onAction2, Action<T3> onAction3, Action<T4> onAction4, Action onClose)
        {
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            action4 = onAction4;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1(T1 obj)
        {
            MakeAction1(obj);

            Close(() =>
            {
                action1?.Invoke(obj);
            });
        }

        protected void Action2(T2 obj)
        {
            MakeAction2(obj);

            Close(() =>
            {
                action2?.Invoke(obj);
            });
        }

        protected void Action3(T3 obj)
        {
            MakeAction3(obj);

            Close(() =>
            {
                action3?.Invoke(obj);
            });
        }

        protected void Action4(T4 obj)
        {
            MakeAction4(obj);

            Close(() =>
            {
                action4?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1(T1 obj) { }
        protected virtual void MakeAction2(T2 obj) { }
        protected virtual void MakeAction3(T3 obj) { }
        protected virtual void MakeAction4(T4 obj) { }
        protected virtual void MakeHide() { }
    }

    public abstract class APopup4TAnimatedWindow<TModel, T1, T2, T3, T4> : AAnimatedWindow<TModel>
    {
        [CanBeNull]
        Action<T1> action1 = null;

        [CanBeNull]
        Action<T2> action2 = null;

        [CanBeNull]
        Action<T3> action3 = null;

        [CanBeNull]
        Action<T4> action4 = null;

        [CanBeNull]
        Action close = null;

        public void Show(TModel model, Action<T1> onAction1, Action<T2> onAction2, Action<T3> onAction3, Action<T4> onAction4, Action onClose)
        {
            Model = model;
            action1 = onAction1;
            action2 = onAction2;
            action3 = onAction3;
            action4 = onAction4;
            close = onClose;

            Open(() => { });

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() =>
            {
                close?.Invoke();
            });
        }

        protected void Action1(T1 obj)
        {
            MakeAction1(obj);

            Close(() =>
            {
                action1?.Invoke(obj);
            });
        }

        protected void Action2(T2 obj)
        {
            MakeAction2(obj);

            Close(() =>
            {
                action2?.Invoke(obj);
            });
        }

        protected void Action3(T3 obj)
        {
            MakeAction3(obj);

            Close(() =>
            {
                action3?.Invoke(obj);
            });
        }

        protected void Action4(T4 obj)
        {
            MakeAction4(obj);

            Close(() =>
            {
                action4?.Invoke(obj);
            });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeAction1(T1 obj) { }
        protected virtual void MakeAction2(T2 obj) { }
        protected virtual void MakeAction3(T3 obj) { }
        protected virtual void MakeAction4(T4 obj) { }
        protected virtual void MakeHide() { }
    }
}