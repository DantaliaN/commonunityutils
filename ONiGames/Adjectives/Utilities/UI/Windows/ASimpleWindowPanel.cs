namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class ASimpleWindowPanel<TWindow> : AWindowPanel<TWindow> where TWindow : AWindow
    {
        public void Show(TWindow owner)
        {
            Owner = owner;

            if (Owner != null)
                Owner.AddPanel(this);
            
            MakeShow();
        }

        public void Hide()
        {
            MakeHide();

            if (Owner != null)
                Owner.RemovePanel(this);
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }

    public abstract class ASimpleWindowPanel<TModel, TWindow> : AWindowPanel<TModel, TWindow> where TWindow : AWindow
    {
        public void Show(TWindow owner, TModel model)
        {
            Owner = owner;
            Model = model;

            if (Owner != null)
                Owner.AddPanel(this);

            MakeShow();
        }

        public void Hide()
        {
            MakeHide();

            if (Owner != null)
                Owner.RemovePanel(this);
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }
}