namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class ASimpleWindow : AWindow
    {
        public void Show()
        {
            Open();

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close();
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }
    
    public abstract class ASimpleWindow<TModel> : AWindow<TModel>
    {
        public void Show(TModel model)
        {
            Model = model;
            
            Open();

            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close();
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }
}