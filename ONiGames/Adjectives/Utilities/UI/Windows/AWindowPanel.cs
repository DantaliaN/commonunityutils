using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class AWindowPanel<TWindow> : MonoBehaviour, IWindowPanel where TWindow : AWindow
    {
        [CanBeNull]
        public TWindow Owner { get; protected set; }

        public bool IsOpen { get; private set; } = false;

        protected virtual void OnValidate()
        {
            if (name.Contains("#"))
                return;

            name = $"{GetType().Name}";
        }

        protected virtual void Start()
        {
            gameObject.SetActive(IsOpen);
        }

        void IWindowPanel.Open()
        {
            if (IsOpen)
                return;

            IsOpen = true;

            gameObject.SetActive(true);

            MakeOpen();
        }

        void IWindowPanel.Close()
        {
            if (!IsOpen)
                return;

            IsOpen = false;
            
            MakeClose();
            
            gameObject.SetActive(false);
        }

        protected virtual void MakeOpen() { }
        protected virtual void MakeClose() { }
    }

    public abstract class AWindowPanel<TModel, TWindow> : AWindowPanel<TWindow> where TWindow : AWindow
    {
        [CanBeNull]
        public TModel Model { get; protected set; }
    }
}