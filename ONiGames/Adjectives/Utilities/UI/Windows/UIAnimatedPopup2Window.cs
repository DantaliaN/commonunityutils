namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public class UIPopup2AnimatedWindow : APopup2AnimatedWindow
    {
        public void UI_Action1()
        {
            Action1();
        }

        public void UI_Action2()
        {
            Action2();
        }

        public void UI_Close()
        {
            Hide();
        }
    }
}