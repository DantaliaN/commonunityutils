using System.Collections.Generic;
using JetBrains.Annotations;
using ONiGames.Utilities.Attributes;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class AWindow : MonoBehaviour
    {
        [FoldoutGroup("Window")]
        [CanBeNull]
        [CanBeNullField]
        [SerializeField]
        WindowGroup windowGroup;
        
        [FoldoutGroup("Window")]
        [AutoInjectField]
        [CanBeNull]
        [CanBeNullField]
        [SerializeField]
        BackSystemButtonController backSystemButton;
        
        [FoldoutGroup("Window")]
        [SerializeField]
        UnityEvent backButton = new();
        
        [FoldoutGroup("Window")]
        [ShowInInspector, ReadOnly]
        public bool IsOpen { get; private set; } = false;

        readonly List<IWindowPanel> panels = new();

        protected virtual void OnValidate()
        {
            if (name.Contains("#"))
                return;

            name = $"{GetType().Name}";
        }

        protected virtual void Start()
        {
            gameObject.SetActive(IsOpen);
        }

        protected virtual void OnEnable()
        {
            if (windowGroup != null)
                windowGroup.Add(this);
        }

        protected virtual void OnDisable()
        {
            if (windowGroup != null)
                windowGroup.Remove(this);
        }
        
        protected virtual void OnDestroy()
        {
            if (backSystemButton != null)
                backSystemButton.AddListener(backButton.Invoke);
        }
        
        public virtual void Hide() => Close();

        internal void AddPanel(IWindowPanel panel)
        {
            if (panels.Contains(panel))
                return;

            panels.Add(panel);

            if (IsOpen)
                panel.Open();
        }

        internal void RemovePanel(IWindowPanel panel)
        {
            if (!panels.Contains(panel))
                return;
            
            panels.Remove(panel);
            
            panel.Close();
        }

        protected void Open()
        {
            if (IsOpen)
                return;

            if (windowGroup != null)
                windowGroup.HideOpened();

            IsOpen = true;

            gameObject.SetActive(true);

            if (backSystemButton != null)
                backSystemButton.AddListener(backButton.Invoke);

            MakeOpen();

            foreach (var panel in panels)
                panel.Open();
        }

        protected void Close()
        {
            if (!IsOpen)
                return;

            IsOpen = false;

            foreach (var panel in panels)
                panel.Close();

            MakeClose();

            if (backSystemButton != null)
                backSystemButton.RemoveListener(backButton.Invoke);

            gameObject.SetActive(false);
        }

        protected virtual void MakeOpen() { }
        protected virtual void MakeClose() { }
    }

    public abstract class AWindow<TModel> : AWindow
    {
        [FoldoutGroup("Window")]
        [CanBeNull]
        [ShowInInspector, ReadOnly]
        public TModel Model { get; protected set; }
    }
}