using System;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    [RequireComponent(typeof(Animator))]
    public class AAnimatedWindow : AWindow
    {
        [FoldoutGroup("Animated Window")]
        [CanBeNull]
        [CanBeNullField]
        [SerializeField]
        RectTransform clicksBlockPanel;
        
        [FoldoutGroup("Animated Window")]
        [ValueDropdown(nameof(GetAnimatorStateItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        [SerializeField]
        AnimationStateProperty openProp = new() { name = "Open" };
        
        [FoldoutGroup("Animated Window")]
        [SerializeField]
        [Min(0f)]
        float openTime = 0.3f;
        
        [FoldoutGroup("Animated Window")]
        [ValueDropdown(nameof(GetAnimatorStateItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        [SerializeField]
        AnimationStateProperty closeProp = new() { name = "Close" };
        
        [FoldoutGroup("Animated Window")]
        [SerializeField]
        [Min(0f)]
        float closeTime = 0.5f;
        
        [FoldoutGroup("Animated Window")]
        public bool useUnscaledTime = true;
        
        [FoldoutGroup("Animated Window")]
        [ShowInInspector, ReadOnly]
        public bool InOpening => openWaiter is { IsFinished : false };
        
        [FoldoutGroup("Animated Window")]
        [ShowInInspector, ReadOnly]
        public bool InClosing => closeWaiter is { IsFinished : false };

        [CanBeNull]
        [NonSerialized]
        Animator _animator;
        Animator Animator => _animator ??= GetComponent<Animator>();

        [CanBeNull]
        DelayedAction openWaiter;

        [CanBeNull]
        DelayedAction closeWaiter;

        protected virtual void Update()
        {
            openWaiter?.Update(useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
            closeWaiter?.Update(useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
        }

        public override void Hide() => Close(() => { });

        protected void Open(Action onComplete)
        {
            if (IsOpen)
                return;
            
            if (InOpening)
                return;

            Open();

            if (clicksBlockPanel != null)
                clicksBlockPanel.gameObject.SetActive(true);

            Animator.Play(openProp.NameHash());

            openWaiter = new DelayedAction(openTime, () =>
            {
                if (clicksBlockPanel != null)
                    clicksBlockPanel.gameObject.SetActive(false);

                onComplete();
            });
        }

        protected void Close(Action onComplete)
        {
            if (!IsOpen)
                return;
            
            if (InClosing)
                return;

            if (clicksBlockPanel != null)
                clicksBlockPanel.gameObject.SetActive(true);

            Animator.Play(closeProp.NameHash());

            closeWaiter = new DelayedAction(closeTime, () =>
            {
                Close();

                onComplete();
            });
        }
        
        ValueDropdownList<AnimationStateProperty> GetAnimatorStateItems() => Utils.GetAnimatorStateItems(Animator);
    }
    
    [RequireComponent(typeof(Animator))]
    public class AAnimatedWindow<TModel> : AWindow<TModel>
    {
        [FoldoutGroup("Animated Window")]
        [CanBeNull]
        [CanBeNullField]
        [SerializeField]
        RectTransform clicksBlockPanel;
        
        [FoldoutGroup("Animated Window")]
        [ValueDropdown(nameof(GetAnimatorStateItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        [SerializeField]
        AnimationStateProperty openProp = new() { name = "Open" };
        
        [FoldoutGroup("Animated Window")]
        [SerializeField]
        [Min(0f)]
        float openTime = 0.3f;
        
        [FoldoutGroup("Animated Window")]
        [ValueDropdown(nameof(GetAnimatorStateItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        [SerializeField]
        AnimationStateProperty closeProp = new() { name = "Close" };
        
        [FoldoutGroup("Animated Window")]
        [SerializeField]
        [Min(0f)]
        float closeTime = 0.5f;
        
        [FoldoutGroup("Animated Window")]
        public bool useUnscaledTime = true;
        
        [FoldoutGroup("Animated Window")]
        [ShowInInspector, ReadOnly]
        public bool InOpening => openWaiter is { IsFinished : false };
        
        [FoldoutGroup("Animated Window")]
        [ShowInInspector, ReadOnly]
        public bool InClosing => closeWaiter is { IsFinished : false };

        [CanBeNull]
        [NonSerialized]
        Animator _animator;
        Animator Animator => _animator ??= GetComponent<Animator>();

        [CanBeNull]
        DelayedAction openWaiter;

        [CanBeNull]
        DelayedAction closeWaiter;

        protected virtual void Update()
        {
            openWaiter?.Update(useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
            closeWaiter?.Update(useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
        }

        public override void Hide() => Close(() => { });

        protected void Open(Action onComplete)
        {
            if (IsOpen)
                return;

            if (InOpening)
                return;

            Open();

            if (clicksBlockPanel != null)
                clicksBlockPanel.gameObject.SetActive(true);

            Animator.Play(openProp.NameHash());

            openWaiter = new DelayedAction(openTime, () =>
            {
                if (clicksBlockPanel != null)
                    clicksBlockPanel.gameObject.SetActive(false);

                onComplete();
            });
        }

        protected void Close(Action onComplete)
        {
            if (!IsOpen)
                return;
            
            if (InClosing)
                return;

            if (clicksBlockPanel != null)
                clicksBlockPanel.gameObject.SetActive(true);

            Animator.Play(closeProp.NameHash());

            closeWaiter = new DelayedAction(closeTime, () =>
            {
                Close();

                onComplete();
            });
        }
        
        ValueDropdownList<AnimationStateProperty> GetAnimatorStateItems() => Utils.GetAnimatorStateItems(Animator);
    }
}