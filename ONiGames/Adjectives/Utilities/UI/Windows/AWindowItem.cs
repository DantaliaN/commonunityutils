using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class AWindowItem<TWindow> : MonoBehaviour where TWindow : AWindow
    {
        [ShowInInspector, ReadOnly]
        [CanBeNull]
        public TWindow Owner { get; set; }
    }
    
    public abstract class AWindowItem<TModel, TWindow> : MonoBehaviour where TWindow : AWindow
    {
        [ShowInInspector, ReadOnly]
        [CanBeNull]
        public TWindow Owner { get; set; }
        
        [ShowInInspector, ReadOnly]
        [CanBeNull]
        public TModel Model { get; set; }
    }
}