using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class AWindowPanelItem<TWindowPanel, TWindow> : MonoBehaviour where TWindowPanel : AWindowPanel<TWindow> where TWindow : AWindow
    {
        [CanBeNull]
        public TWindowPanel Owner { get; set; }
    }
}