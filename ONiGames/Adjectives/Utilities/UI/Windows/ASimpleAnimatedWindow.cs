namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public abstract class ASimpleAnimatedWindow : AAnimatedWindow
    {
        public void Show()
        {
            Open(() => { });
            
            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() => { });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }
    
    public abstract class ASimpleAnimatedWindow<T> : AAnimatedWindow<T>
    {
        public void Show(T model)
        {
            Model = model;
            
            Open(() => { });
            
            MakeShow();
        }

        public sealed override void Hide()
        {
            MakeHide();

            Close(() => { });
        }

        protected virtual void MakeShow() { }
        protected virtual void MakeHide() { }
    }
}