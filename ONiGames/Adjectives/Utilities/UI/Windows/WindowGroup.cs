using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI.Windows
{
    public class WindowGroup : MonoBehaviour
    {
        readonly List<AWindow> windows = new();

        public void Add(AWindow window)
        {
            if (!windows.Contains(window))
                windows.Add(window);
        }

        public void Remove(AWindow window)
        {
            if (windows.Contains(window))
                windows.Remove(window);
        }

        public void HideOpened()
        {
            foreach (var window in windows.Where(o => o.IsOpen).ToList())
                window.Hide();
        }
    }
}