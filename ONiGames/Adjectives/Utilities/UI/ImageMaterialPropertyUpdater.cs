using JetBrains.Annotations;
using ONiGames.Asserts;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Utilities.UI
{
    [ExecuteAlways]
    [RequireComponent(typeof(Image))]
    public class ImageMaterialPropertyUpdater : MonoBehaviour
    {
        enum ValueType
        {
            Int = 0,
            Float,
            Color
        }
        
        [SerializeField]
        ShaderProperty prop = new();
        
        [SerializeField]
        ValueType valueType = ValueType.Float;
        
        [ShowIf(nameof(valueType), ValueType.Int)]
        [SerializeField]
        int intValue = 0;
        
        [ShowIf(nameof(valueType), ValueType.Float)]
        [SerializeField]
        float floatValue = 0;
        
        [ShowIf(nameof(valueType), ValueType.Color)]
        [SerializeField]
        Color colorValue = Color.clear;
        
        [CanBeNull]
        Image _image;
        Image Image => _image ??= GetComponent<Image>();
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
        
        void Update()
        {
            switch (valueType)
            {
                case ValueType.Int:
                    Image.material.SetInt(prop.NameHash(), intValue);
                    break;
                case ValueType.Float:
                    Image.material.SetFloat(prop.NameHash(), floatValue);
                    break;
                case ValueType.Color:
                    Image.material.SetColor(prop.NameHash(), colorValue);
                    break;
                default:
                    Debug.LogWarning($"{gameObject.GetPath()}: {valueType} not implemented");
                    break;
            }
        }
    }
}