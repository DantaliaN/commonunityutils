using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Utilities.UI
{
    public class RaycastTargetInChildsSetter : MonoBehaviour
    {
        [SerializeField]
        bool raycastTarget = false;
        
        void Start()
        {
            SetRaycastTarget();
        }
        
        void SetRaycastTarget()
        {
            foreach (var image in GetComponentsInChildren<Image>())
                image.raycastTarget = raycastTarget;
            foreach (var text in GetComponentsInChildren<TMP_Text>())
                text.raycastTarget = raycastTarget;
        }
    }
}
