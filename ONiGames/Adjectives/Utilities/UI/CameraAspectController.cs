﻿using System;
using System.Linq;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(Camera))]
    public class CameraAspectController : MonoBehaviour
    {
        [Serializable]
        public class AspectRatio
        {
            public Vector2Int aspectRatio = Vector2Int.zero;
            public float aspectCorrect = 0f;

            public float orthoSize = 0f;
        }

        [SerializeField]
        AspectRatio[] aspectRatios = new AspectRatio[0];

        [CanBeNull]
        Camera _camera;
        Camera Cam => _camera != null ? _camera : _camera = GetComponent<Camera>();

        void Start()
        {
            RecalculateAspect();
        }

        [Button]
        void RecalculateAspect()
        {
            var defaultAspect = aspectRatios.FirstOrDefault();
            if (defaultAspect != null)
                Cam.orthographicSize = defaultAspect.orthoSize;

            foreach (var aspect in aspectRatios)
            {
                if (Cam.aspect > (float) aspect.aspectRatio.x / aspect.aspectRatio.y + aspect.aspectCorrect)
                    Cam.orthographicSize = aspect.orthoSize;
            }
        }
    }
}