﻿using System;
using JetBrains.Annotations;
using ONiGames.Asserts;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaPanel : MonoBehaviour
    {
        [SerializeField]
        Canvas canvas;

        [SerializeField]
        bool moveUpper = true;

        [SerializeField]
        bool moveBottom = false;

#pragma warning disable CS0414
        [UsedImplicitly]
        [SerializeField]
        bool useSaveZoneIOS = true;

        [UsedImplicitly]
        [SerializeField]
        bool useSaveZoneAndroid = true;
#pragma warning restore CS0414

        [SerializeField]
        Vector2 offset = Vector2.zero;

        [CanBeNull]
        [NonSerialized]
        RectTransform _rectTransform;
        RectTransform RectTransform => _rectTransform ??= GetComponent<RectTransform>();

        Rect currentCanvasRect;
        Rect currentSafeArea;
        ScreenOrientation screenOrientation = ScreenOrientation.AutoRotation;

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Update()
        {
            var canvasRect = canvas.pixelRect;
            var saveArea = canvasRect;

#if UNITY_IOS
            if (useSaveZoneIOS)
                saveArea = Screen.safeArea;
#elif UNITY_ANDROID
            if (useSaveZoneAndroid)
                saveArea = Screen.safeArea;
#endif

            if (currentCanvasRect != canvasRect || saveArea != currentSafeArea || screenOrientation != Screen.orientation)
            {
                screenOrientation = Screen.orientation;
                currentSafeArea = saveArea;
                currentCanvasRect = canvasRect;

                ApplySafeArea();
            }
        }

        void ApplySafeArea()
        {
            var anchorMin = currentSafeArea.position;
            var anchorMax = currentSafeArea.position + currentSafeArea.size + offset;

            if (Mathf.Approximately(currentCanvasRect.width, 0f) || Mathf.Approximately(currentCanvasRect.height, 0f))
                return;

            anchorMin.x /= currentCanvasRect.width;
            anchorMin.y /= currentCanvasRect.height;

            anchorMax.x /= currentCanvasRect.width;
            anchorMax.y /= currentCanvasRect.height;

            if (moveUpper)
                RectTransform.anchorMax = anchorMax;
            if (moveBottom)
                RectTransform.anchorMin = anchorMin;
        }
    }
}