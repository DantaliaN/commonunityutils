using ONiGames.Asserts;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI
{
    [RequireComponent(typeof(CanvasRenderer))]
    public class CanvasRendererColorRepeater : MonoBehaviour
    {
        [SerializeField]
        CanvasRenderer target;

        CanvasRenderer _canvasRenderer;
        CanvasRenderer CanvasRenderer => _canvasRenderer ??= GetComponent<CanvasRenderer>();

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Start()
        {
            LateUpdate();
        }

        void LateUpdate()
        {
            CanvasRenderer.SetColor(target.GetColor());
        }
    }
}