﻿using System;
using System.Collections.Generic;
using System.Linq;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.UI
{
    public class BackSystemButtonController : MonoBehaviour
    {
        class Listener
        {
            public readonly Action action;
            public readonly bool isDisposable;

            public Listener(Action action, bool isDisposable)
            {
                this.action = action;
                this.isDisposable = isDisposable;
            }
        }
        
        [ShowIf(nameof(useOwnKey))]
        [SerializeField]
        KeyCode key = KeyCode.Escape;
        
        [SerializeField]
        bool allowCrossScene = false;
        
        public bool useOwnKey = true;

        readonly List<Listener> _listeners = new();
        static readonly List<Listener> _staticListeners = new();
        
        IList<Listener> Listeners => allowCrossScene ? _staticListeners : _listeners;
        
        void OnValidate()
        {
            if (name.Contains("#"))
                return;
            
            name = $"[{GetType().Name}]";
        }
        
        void Update()
        {
            if (!useOwnKey)
                return;

            if (Input.GetKeyDown(key))
                Proceed();
        }

        public void Proceed()
        {
            var listener = Listeners.LastOrDefault();
            if (listener == null)
                return;
            
            if (listener.isDisposable)
                Listeners.Remove(listener);

            listener.action.Invoke();
        }

        public void AddListener(Action action, bool isDisposable = false)
        {
            RemoveListener(action);

            Listeners.Add(new Listener(action, isDisposable));
        }

        public void RemoveListener(Action action)
        {
            Listeners.Remove(o => o.action == action);
        }
    }
}