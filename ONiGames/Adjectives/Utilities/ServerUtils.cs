using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using ONiGames.Utilities;
using UnityEngine;
using UnityEngine.Networking;

namespace ONiGames.Adjectives.Utilities
{
    public static class ServerUtils
    {
        //Based on https://www.owasp.org/index.php/Certificate_and_Public_Key_Pinning#.Net
        public class AcceptAllCertificatesSignedWithASpecificKeyPublicKey : CertificateHandler
        {
            //Encoded RSAPublicKey
            //const string PUB_KEY = "04C8655EC70CA3EBF7C36C63F3AF31DFDE5E129A4A457C8F7F92CB29C520F8642886B311A5C748D11F502CE66F063BAE0D83B04BDB4F24CF336FC9CD150A1E36F5";

            protected override bool ValidateCertificate(byte[] certificateData)
            {
                //var certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2(certificateData);
                //var pk = certificate.GetPublicKeyString();
                //return pk != null && pk.Equals(PUB_KEY);
                return true;
            }
        }

        const int RequestTimeout = 1;

        const int RetriesCountOnNetworkError = 5;

        const float RequestDelayTime = .1f;

        public class RequestError
        {
            public string Url { get; }
            public string Method { get; }
            public IReadOnlyDictionary<string, string> Headers { get; }
            public byte[] Body { get; }
            public long Code { get; }
            public string Message { get; }
            public int Status { get; }
            public string Text { get; }

            public RequestError(string url, string method, IReadOnlyDictionary<HttpRequestHeader, string> headers, long code, string message) : this(url, method, headers, new byte[0], code, message) { }
            public RequestError(string url, string method, IReadOnlyDictionary<HttpRequestHeader, string> headers, byte[] body, long code, string message) : this(url, method, headers.ToDictionary(o => o.Key.ToHeaderString(), o => o.Value), body, code, message, 0, string.Empty) { }

            public RequestError(string url, string text) : this(url, 0, text) { }
            public RequestError(string url, int status, string text) : this(url, string.Empty, new Dictionary<string, string>(), new byte[0], 200, string.Empty, status, text) { }

            public RequestError(string url, string method, IReadOnlyDictionary<HttpRequestHeader, string> headers, string message) : this(url, method, headers, new byte[0], message) { }
            public RequestError(string url, string method, IReadOnlyDictionary<HttpRequestHeader, string> headers, byte[] body, string message) : this(url, method, headers, body, 200, message, 0, string.Empty) { }

            public RequestError(string url, string method, IReadOnlyDictionary<HttpRequestHeader, string> headers, byte[] body, long code, string message, int status, string text) : this(url, method, headers.ToDictionary(o => o.Key.ToHeaderString(), o => o.Value), body, code, message, status, text) { }

            public RequestError(string url, string method, IReadOnlyDictionary<string, string> headers, byte[] body, long code, string message, int status, string text)
            {
                Url = url;
                Method = method;
                Headers = headers;
                Body = body;
                Code = code;
                Message = message;
                Status = status;
                Text = text;
            }

            public override string ToString()
            {
                return $"[{Method}] {Url} > {Code} : {Message} \n {Status} : {Text} \n Headers: {string.Join(", ", Headers.Select(o => $"{o.Key} - {o.Value}"))} \n Body: {Encoding(Body)}";
            }
        }

        #region REST API

        public static IEnumerator Get(string requestUri, Action<string> onComplete, Action<RequestError> onError)
        {
            return Get(requestUri, new Dictionary<HttpRequestHeader, string>(), onComplete, onError);
        }

        public static IEnumerator Get(string requestUri, Dictionary<HttpRequestHeader, string> headers, Action<string> onComplete, Action<RequestError> onError)
        {
            yield return Get(requestUri, headers, response =>
            {
                if (!string.IsNullOrEmpty(response.error))
                {
                    onError(new RequestError(response.url, response.method, headers, response.responseCode, response.error));
                }
                else
                {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Debug.Log($"{typeof(ServerUtils)}: [{response.method}] {response.url} > {response.responseCode} \n Headers: {string.Join(", ", headers.Select(o => $"{o.Key.ToHeaderString()} - {o.Value}"))} \n Response: {response.downloadHandler.text}");
#endif
                    onComplete(response.downloadHandler.text);
                }
            });
        }

        public static IEnumerator Get(string requestUri, Dictionary<HttpRequestHeader, string> headers, Action<UnityWebRequest> onComplete)
        {
            return Get(requestUri, new AcceptAllCertificatesSignedWithASpecificKeyPublicKey(), headers, onComplete);
        }

        public static IEnumerator Get(string requestUri, CertificateHandler certificate, Dictionary<HttpRequestHeader, string> headers, Action<UnityWebRequest> onComplete)
        {
            var i = 0;
            do
            {
                using (var request = UnityWebRequest.Get(requestUri))
                {
                    foreach (var pair in headers)
                        request.SetRequestHeader(pair.Key.ToHeaderString(), pair.Value);

                    request.timeout = RequestTimeout * (i + 1);

                    request.certificateHandler = certificate;

                    yield return new WaitForSeconds(RequestDelayTime * i);

                    yield return request.SendWebRequest();

                    if (request.result == UnityWebRequest.Result.Success || ++i >= RetriesCountOnNetworkError)
                    {
                        onComplete(request);
                        break;
                    }
                }
            } while (true);
        }

        public static IEnumerator Post(string requestUri, Action<string> onComplete, Action<RequestError> onError)
        {
            return Post(requestUri, new Dictionary<HttpRequestHeader, string>(), new byte[0], onComplete, onError);
        }

        public static IEnumerator Post(string requestUri, Dictionary<HttpRequestHeader, string> headers, Action<string> onComplete, Action<RequestError> onError)
        {
            return Post(requestUri, headers, new byte[0], onComplete, onError);
        }

        public static IEnumerator Post(string requestUri, byte[] body, Action<string> onComplete, Action<RequestError> onError)
        {
            return Post(requestUri, new Dictionary<HttpRequestHeader, string>(), body, onComplete, onError);
        }

        public static IEnumerator Post(string requestUri, Dictionary<HttpRequestHeader, string> headers, byte[] body, Action<string> onComplete, Action<RequestError> onError)
        {
            yield return Post(requestUri, headers, body, response =>
            {
                if (!string.IsNullOrEmpty(response.error))
                {
                    onError(new RequestError(response.url, response.method, headers, body, response.responseCode, response.error));
                }
                else
                {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Debug.Log($"{typeof(ServerUtils)}: [{response.method}] {response.url} > {response.responseCode} \n Headers: {string.Join(", ", headers.Select(o => $"{o.Key.ToHeaderString()} - {o.Value}"))} \n Body: {Encoding(body)} \n Response: {response.downloadHandler.text}");
#endif
                    onComplete(response.downloadHandler.text);
                }
            });
        }

        public static IEnumerator Post(string requestUri, Dictionary<HttpRequestHeader, string> headers, byte[] body, Action<UnityWebRequest> onComplete)
        {
            return Post(requestUri, new AcceptAllCertificatesSignedWithASpecificKeyPublicKey(), headers, body, onComplete);
        }

        public static IEnumerator Post(string requestUri, CertificateHandler certificate, Dictionary<HttpRequestHeader, string> headers, byte[] body, Action<UnityWebRequest> onComplete)
        {
            var i = 0;
            do
            {
                using (var request = new UnityWebRequest(requestUri, UnityWebRequest.kHttpVerbPOST))
                {
                    request.timeout = RequestTimeout * (i + 1);

                    request.uploadHandler = body.Length > 0 ? new UploadHandlerRaw(body) : null;
                    request.downloadHandler = new DownloadHandlerBuffer();

                    foreach (var pair in headers)
                        request.SetRequestHeader(pair.Key.ToHeaderString(), pair.Value);

                    request.certificateHandler = certificate;

                    yield return new WaitForSeconds(RequestDelayTime * i);

                    yield return request.SendWebRequest();

                    if (request.result == UnityWebRequest.Result.Success || ++i >= RetriesCountOnNetworkError)
                    {
                        onComplete(request);
                        break;
                    }
                }
            } while (true);
        }

        #endregion

        #region Utils

        public static byte[] Decoding(string data)
        {
            try
            {
                return System.Text.Encoding.UTF8.GetBytes(data);
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{typeof(ServerUtils)}: {ex}");
                return new byte[0];
            }
        }

        public static string Encoding(byte[] data)
        {
            try
            {
                return System.Text.Encoding.UTF8.GetString(data);
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{typeof(ServerUtils)}: {ex}");
                return string.Empty;
            }
        }

        #endregion
    }
}