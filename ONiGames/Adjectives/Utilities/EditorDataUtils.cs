using System.Collections.Generic;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities
{
    public static class EditorDataUtils
    {
        static readonly Dictionary<string, ScriptableObject> CachedAssets = new();

        public static bool TryGetData<T>(out T data) where T : ScriptableObject
        {
            var key = typeof(T).FullName ?? string.Empty;

#if UNITY_EDITOR
            if (!CachedAssets.ContainsKey(key))
            {
                var guids = UnityEditor.AssetDatabase.FindAssets($"t:{key}");
                if (guids.Length > 0)
                {
                    var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]));
                    CachedAssets.Add(key, asset);
                }
            }
#endif

            data = CachedAssets.TryGetValue(key, out var obj) && obj is T value ? value : null;
            return data != null;
        }
    }
}