using System;
using JetBrains.Annotations;
using ONiGames.Utilities.CoreTypes;
using ONiGames.Utilities.Serialization.Attributes;

namespace ONiGames.Adjectives.Utilities.Attributes
{
    [UsedImplicitly]
    [AttributeUsage(AttributeTargets.Field)]
    public class SerializedVersionBinarySerializationDefaultValueAttribute : BinarySerializationDefaultValueAttribute
    {
        public SerializedVersionBinarySerializationDefaultValueAttribute() : base(new SerializedVersion()) { }
    }
}