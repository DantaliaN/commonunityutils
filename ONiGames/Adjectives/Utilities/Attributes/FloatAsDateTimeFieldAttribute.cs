using System;

namespace ONiGames.Adjectives.Utilities.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class FloatAsDateTimeFieldAttribute : Attribute { }
}