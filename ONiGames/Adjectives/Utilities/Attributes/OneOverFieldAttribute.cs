using System;

namespace ONiGames.Adjectives.Utilities.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class OneOverFieldAttribute : Attribute
    {
    }
}