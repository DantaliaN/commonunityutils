#if UNITY_EDITOR

using JetBrains.Annotations;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.Attributes.Editor
{
    [UsedImplicitly]
    public class FloatAsDateTimeFieldDrawer : OdinAttributeDrawer<FloatAsDateTimeFieldAttribute, float>
    {
        const int DAY_IN_SEC = 86400;
        const int HOUR_IN_SEC = 3600;
        const int MINUTE_IN_SEC = 60;

        protected override void DrawPropertyLayout(GUIContent label)
        {
            var rect = EditorGUILayout.GetControlRect();

            if (label != null)
                rect = EditorGUI.PrefixLabel(rect, label);

            var value = ValueEntry.SmartValue;

            var sign = (int) Mathf.Sign(value);

            var days = Mathf.FloorToInt(Mathf.Abs(value) / DAY_IN_SEC);
            var hours = Mathf.FloorToInt((Mathf.Abs(value) - days * DAY_IN_SEC) / HOUR_IN_SEC);
            var minutes = Mathf.FloorToInt((Mathf.Abs(value) - days * DAY_IN_SEC - hours * HOUR_IN_SEC) / MINUTE_IN_SEC);
            var seconds = Mathf.FloorToInt(Mathf.Abs(value) - days * DAY_IN_SEC - hours * HOUR_IN_SEC - minutes * MINUTE_IN_SEC);

            GUIHelper.PushLabelWidth(50f);

            sign = SirenixEditorFields.TextField(new Rect(rect.x - 20f, rect.y, 20f, rect.height), "", sign > 0 ? "" : "-") == "-" ? -1 : 1;

            days = SirenixEditorFields.IntField(rect.Split(0, 4), "    Days:", days);
            hours = SirenixEditorFields.IntField(rect.Split(1, 4), "   Hours:", hours);
            minutes = SirenixEditorFields.IntField(rect.Split(2, 4), "      Min:", minutes);
            seconds = SirenixEditorFields.IntField(rect.Split(3, 4), "      Sec:", seconds);

            GUIHelper.PopLabelWidth();

            if (ValueEntry.IsEditable)
                ValueEntry.SmartValue = sign * (days * DAY_IN_SEC + hours * HOUR_IN_SEC + minutes * MINUTE_IN_SEC + seconds);
        }
    }
}

#endif