#if UNITY_EDITOR

using JetBrains.Annotations;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.Attributes.Editor
{
    [UsedImplicitly]
    public class PlusOneFieldDrawer : OdinAttributeDrawer<PlusOneFieldAttribute, int>
    {
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var codeValue = ValueEntry.SmartValue;

            var editorValue = codeValue + 1;

            editorValue = SirenixEditorFields.IntField(label, editorValue);
            
            if (ValueEntry.IsEditable)
                ValueEntry.SmartValue = editorValue - 1;
        }
    }
}

#endif