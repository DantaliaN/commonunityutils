#if UNITY_EDITOR

using JetBrains.Annotations;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.Attributes.Editor
{
    [UsedImplicitly]
    public class OneOverFieldDrawer : OdinAttributeDrawer<OneOverFieldAttribute, float>
    {
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var codeValue = ValueEntry.SmartValue;

            var editorValue = ONiGames.Utilities.Utils.SafeDivide(1f, codeValue);

            editorValue = SirenixEditorFields.FloatField(label, editorValue);
            
            if (ValueEntry.IsEditable)
                ValueEntry.SmartValue = ONiGames.Utilities.Utils.SafeDivide(1f, editorValue);
        }
    }
}

#endif