﻿#if UNITY_EDITOR

using System;
using JetBrains.Annotations;
using ONiGames.Utilities.CoreTypes;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.Attributes.Editor
{
    [UsedImplicitly]
    public class SerializedDateTimeAsDateTimeFieldDrawer : OdinAttributeDrawer<SerializedDateTimeAsDateTimeFieldAttribute, SerializedDateTime>
    {
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var rect = EditorGUILayout.GetControlRect();

            if (label != null)
                rect = EditorGUI.PrefixLabel(rect, label);
            
            var day = Mathf.FloorToInt(ValueEntry.SmartValue.Value.Day);
            var month = Mathf.FloorToInt(ValueEntry.SmartValue.Value.Month);
            var year = Mathf.FloorToInt(ValueEntry.SmartValue.Value.Year);
            var hour = Mathf.FloorToInt(ValueEntry.SmartValue.Value.Hour);
            var minute = Mathf.FloorToInt(ValueEntry.SmartValue.Value.Minute);
            var second = Mathf.FloorToInt(ValueEntry.SmartValue.Value.Second);

            GUIHelper.PushLabelWidth(50f);
            
            day = SirenixEditorFields.IntField(rect.Split(0, 6), "Day", day);
            month = SirenixEditorFields.IntField(rect.Split(1, 6), "Month", month);
            year = SirenixEditorFields.IntField(rect.Split(2, 6), "Year", year);
            hour = SirenixEditorFields.IntField(rect.Split(3, 6), "Hour", hour);
            minute = SirenixEditorFields.IntField(rect.Split(4, 6), "Minute", minute);
            second = SirenixEditorFields.IntField(rect.Split(5, 6), "Second", second);

            GUIHelper.PopLabelWidth();
            
            if (ValueEntry.IsEditable)
                ValueEntry.SmartValue = new SerializedDateTime(new DateTime(year, month, day, hour, minute, second));
        }
    }
}

#endif