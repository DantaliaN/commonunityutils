#if UNITY_EDITOR

using JetBrains.Annotations;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.Attributes.Editor
{
    [UsedImplicitly]
    public class PercentVector2FieldDrawer : OdinAttributeDrawer<PercentFieldAttribute, Vector2>
    {
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var codeValue = ValueEntry.SmartValue;

            var editorValue = SirenixEditorFields.TextField(label, $"{codeValue.x * 100f:F1} - {codeValue.y * 100f:F1}%");

            var substrings = editorValue.Trim('-', '%', ' ');

            if (substrings.Length < 2)
                return;

            if (ValueEntry.IsEditable)
                ValueEntry.SmartValue = new Vector2(float.TryParse(substrings[0].ToString(), out var x) ? x : 0f, float.TryParse(substrings[1].ToString(), out var y) ? y : 0f);
        }
    }
}

#endif