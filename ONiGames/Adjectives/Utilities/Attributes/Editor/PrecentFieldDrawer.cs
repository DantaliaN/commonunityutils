#if UNITY_EDITOR

using JetBrains.Annotations;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities.Attributes.Editor
{
    [UsedImplicitly]
    public class PercentFieldDrawer : OdinAttributeDrawer<PercentFieldAttribute, float>
    {
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var codeValue = ValueEntry.SmartValue;

            var editorValue = SirenixEditorFields.TextField(label, $"{codeValue * 100f:F1}%");

            if (ValueEntry.IsEditable)
                ValueEntry.SmartValue = float.TryParse(editorValue.Trim('%'), out var value) ? value / 100f : 0f;
        }
    }
}

#endif