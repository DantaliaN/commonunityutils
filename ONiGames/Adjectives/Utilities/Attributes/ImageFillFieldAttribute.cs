using System;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Utilities.Attributes
{
    [IncludeMyAttributes]
#if UNITY_EDITOR
    [InfoBox("@ImageFillFieldAttribute.IncorrectImageTypeMessage($root, $property)", InfoMessageType.Warning, "@!ImageFillFieldAttribute.IsCorrectImageType($root, $property)")]
#endif
    [AttributeUsage(AttributeTargets.Field)]
    public class ImageFillFieldAttribute : Attribute
    {
#if UNITY_EDITOR
        [UsedImplicitly]
        public static string IncorrectImageTypeMessage(object root, Sirenix.OdinInspector.Editor.InspectorProperty property)
        {
            var attribute = property.GetAttribute<ImageFillFieldAttribute>();
            if (attribute == null)
                return string.Empty;
            
            var image = property.ValueEntry.WeakSmartValue as Image;
            if (image == null)
                return string.Empty;
            
            return "Image type must be 'Filled'";
        }
        
        [UsedImplicitly]
        public static bool IsCorrectImageType(object root, Sirenix.OdinInspector.Editor.InspectorProperty property)
        {
            var attribute = property.GetAttribute<ImageFillFieldAttribute>();
            if (attribute == null)
                return false;
            
            var image = property.ValueEntry.WeakSmartValue as Image;
            if (image == null)
                return false;
            
            return image.type == Image.Type.Filled;
        }
#endif
    }
}