using ONiGames.Utilities;
using UnityEngine;

namespace ONiGames.Adjectives.Utilities
{
    public static class GizmosUtils
    {
        public static void GizmosDrawMesh(GameObject go, Transform t, bool drawBones = false)
        {
            GizmosDrawMesh(go, t.position, t.rotation, t.lossyScale, drawBones);
        }

        public static void GizmosDrawMesh(GameObject go, Vector3 position, Quaternion rotation, Vector3 scale, bool drawBones = false)
        {
            foreach (var skinnedMeshRenderer in go.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                var st = skinnedMeshRenderer.transform;
                Gizmos.DrawWireMesh(skinnedMeshRenderer.sharedMesh, ONiGames.Utilities.Utils.TransformPoint(st.position, position, rotation, scale), rotation * st.rotation, Vector3.Scale(scale, st.lossyScale));
            }

            if (drawBones)
            {
                var style = new GUIStyle
                {
                    alignment = TextAnchor.LowerCenter,
                    fontSize = 10,
                    normal = new GUIStyleState { textColor = Color.cyan }
                };

                foreach (var ot in go.transform.All())
                {
                    var tPos = ot.position;
                    var tRot = ot.rotation;

#if UNITY_EDITOR
                    UnityEditor.Handles.Label(ONiGames.Utilities.Utils.TransformPoint(tPos, position, rotation, scale), $"{ot.name}", style);
#endif

                    GizmosDrawArrow(ONiGames.Utilities.Utils.TransformPoint(tPos, position, rotation, scale), rotation * tRot, Color.cyan, 0.15f, 0.05f);
                }
            }
        }

        public static void GizmosDrawArrow(Vector3 position, Quaternion rotation, Color color, float arrowLength = 1f, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f, float sphereRadius = 0.003f)
        {
            var direction = rotation * Vector3.forward * arrowLength;

            var right = rotation * Quaternion.Euler(arrowHeadAngle, 0f, 0f) * Vector3.back * arrowHeadLength;
            var left = rotation * Quaternion.Euler(-arrowHeadAngle, 0f, 0f) * Vector3.back * arrowHeadLength;
            var up = rotation * Quaternion.Euler(0f, arrowHeadAngle, 0f) * Vector3.back * arrowHeadLength;
            var down = rotation * Quaternion.Euler(0f, -arrowHeadAngle, 0f) * Vector3.back * arrowHeadLength;
            var end = position + direction;

            var colorPrew = Gizmos.color;
            Gizmos.color = color;

            Gizmos.DrawSphere(position, sphereRadius);

            Gizmos.DrawRay(position, direction);
            Gizmos.DrawRay(end, right);
            Gizmos.DrawRay(end, left);
            Gizmos.DrawRay(end, up);
            Gizmos.DrawRay(end, down);

            Gizmos.color = colorPrew;
        }
    }
}