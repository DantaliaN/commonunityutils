using System;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace ONiGames.Adjectives.TypedUtils
{
    [Serializable]
    public class TypedItem<T> : IEquatable<TypedItem<T>> where T : TypedId, new()
    {
        [ValueDropdown(nameof(GetItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        [NotNull]
        public T id = new();

        #region Equals

        public static bool operator ==([CanBeNull] TypedItem<T> obj1, [CanBeNull] TypedItem<T> obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] TypedItem<T> obj1, [CanBeNull] TypedItem<T> obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as TypedItem<T>));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public bool Equals(TypedItem<T> other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return id.Equals(other.id);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return $"{GetType()}: {id}";
        }

        #endregion

        protected virtual ValueDropdownList<T> GetItems() => new();
    }
}