using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Adjectives.TypedUtils
{
    [Serializable]
    public class TypedId : IEquatable<TypedId>
    {
        [HideInInspector]
        public int uid = 0;

        public virtual string Name => string.Empty;

        #region Equals

        public static bool operator ==([CanBeNull] TypedId obj1, [CanBeNull] TypedId obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] TypedId obj1, [CanBeNull] TypedId obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as TypedId));
        }

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return uid;
        }

        public bool Equals(TypedId other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return uid.Equals(other.uid);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return $"{GetType()}: {uid} {Name}";
        }

        #endregion
    }
}