#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using ONiGames.CustomProjectSettings.ExtendedUtilities;
using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Adjectives.TypedUtils.Editor
{
    public abstract class TypedIdFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var fieldProp = property.FindPropertyRelative(FieldName);
            if (fieldProp == null || fieldProp.propertyType != SerializedPropertyType.Integer)
                return;
            
            var item = Items.FirstOrDefault(o => o.uid == fieldProp.intValue);
            
            var items = Items.Select(o => o.name).ToArray();
            
            if (IsNeedSort)
                items = items.OrderBy(o => o).ToArray();
            
            var name = GUIUtilsEditor.Popup(position, label, item?.name, items);
            
            item = Items.FirstOrDefault(o => o.name == name);
            if (item != null)
                fieldProp.intValue = item.uid;
        }
        
        protected virtual bool IsNeedSort => false;
        
        protected virtual string FieldName => "uid";
        
        protected abstract IReadOnlyList<IdSettings.Id> Items { get; }
    }
}

#endif