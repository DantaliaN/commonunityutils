#if UNITY_EDITOR

using JetBrains.Annotations;
using Sirenix.OdinInspector.Editor;
using UnityEngine;

namespace ONiGames.Adjectives.TypedUtils.Editor
{
    public abstract class TypedItemFieldDrawer<T> : OdinValueDrawer<T>
    {
        const string ID_PROP_NAME = "id";

        [CanBeNull]
        InspectorProperty idProperty;

        protected override void Initialize()
        {
            idProperty = Property.Children[ID_PROP_NAME];
        }

        protected override void DrawPropertyLayout(GUIContent label)
        {
            idProperty?.Draw(label);
        }
    }
}

#endif