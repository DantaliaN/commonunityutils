#if UNITY_EDITOR

using JetBrains.Annotations;
using Sirenix.OdinInspector.Editor;
using UnityEngine;

namespace ONiGames.Adjectives.TypedUtils.Editor
{
    public abstract class TypedGuidFieldDrawer<T> : OdinValueDrawer<T>
    {
        const string GUID_PROP_NAME = "guid";

        [CanBeNull]
        InspectorProperty guidProperty;

        protected override void Initialize()
        {
            guidProperty = Property.Children[GUID_PROP_NAME];
        }

        protected override void DrawPropertyLayout(GUIContent label)
        {
            guidProperty?.Draw(label);
        }
    }
}

#endif