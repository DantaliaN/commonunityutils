using System;
using JetBrains.Annotations;
using ONiGames.Utilities.CoreTypes;

namespace ONiGames.Adjectives.TypedUtils
{
    [Serializable]
    public class TypedGuid : IEquatable<TypedGuid>
    {
        [NotNull]
        public SerializedGuid guid;

        public TypedGuid()
        {
            guid = new SerializedGuid();
        }

        #region Equals

        public static bool operator ==([CanBeNull] TypedGuid obj1, [CanBeNull] TypedGuid obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;
            return obj1.Equals(obj2);
        }

        public static bool operator !=([CanBeNull] TypedGuid obj1, [CanBeNull] TypedGuid obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType() && (ReferenceEquals(this, obj) || Equals(obj as TypedGuid));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return guid.GetHashCode();
        }

        public bool Equals(TypedGuid other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return guid.Equals(other.guid);
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return $"{GetType()}: {guid}";
        }

        #endregion
    }
}