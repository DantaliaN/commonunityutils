//#define GPG_APP_REVIEW_ENABLE

using System;
using JetBrains.Annotations;
using ONiGames.Asserts;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class AppReviewController : MonoBehaviour
    {
        const float OPEN_URL_DELAY = 0.5f;
        
        [AutoComponentField]
        [SerializeField]
        Yielder yielder;
        
        [CanBeNull]
        DelayedAction openUrlDelayedAction = null;

#if UNITY_ANDROID && GPG_APP_REVIEW_ENABLE
        [CanBeNull]
        Google.Play.Review.ReviewManager reviewManager = null;
#endif
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Start()
        {
#if UNITY_ANDROID && GPG_APP_REVIEW_ENABLE
            reviewManager = new Google.Play.Review.ReviewManager();
#endif
        }

        void Update()
        {
            openUrlDelayedAction?.Update(Time.deltaTime);
        }

        public void SendUserReview(string email, string userId, string message = "")
        {
            var subject = "Review".EscapeURL();
            var body = $"{userId}\r\n{message}".EscapeURL();

            openUrlDelayedAction = new DelayedAction(OPEN_URL_DELAY, () =>
            {
                Application.OpenURL($"mailto:{email}?subject={subject}&body={body}");
            });
        }

        public void OpenMarket()
        {
            openUrlDelayedAction = new DelayedAction(OPEN_URL_DELAY, () =>
            {
#if UNITY_ANDROID
                Application.OpenURL($"market://details?id={Application.identifier}");
#elif UNITY_IOS
                Application.OpenURL($"itms-apps://itunes.apple.com/app/id{Application.identifier}");
#endif
            });
        }

        public void Review(Action<bool> onComplete)
        {
#if UNITY_ANDROID && GPG_APP_REVIEW_ENABLE
            if (reviewManager == null)
            {
                onComplete(false);
                return;
            }

            yielder.Wait(reviewManager.RequestReviewFlow(), requestReviewOperation =>
            {
                if (requestReviewOperation.Error != Google.Play.Review.ReviewErrorCode.NoError)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {requestReviewOperation.Error}");
                    
                    onComplete(false);
                    return;
                }

                var reviewInfo = requestReviewOperation.GetResult();
                if (reviewInfo == null)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: result is null");
                    
                    onComplete(false);
                    return;
                }

                yielder.Wait(reviewManager.LaunchReviewFlow(reviewInfo), launchReviewOperation =>
                {
                    if (launchReviewOperation.Error != Google.Play.Review.ReviewErrorCode.NoError)
                    {
                        Debug.LogWarning($"{gameObject.GetPath()}: {launchReviewOperation.Error}");
                        
                        onComplete(false);
                        return;
                    }
                    
                    onComplete(true);
                });
            });
            
#elif UNITY_IOS
            var canReview = UnityEngine.iOS.Device.RequestStoreReview();
            if (!canReview)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: Can't open review");
                
                onComplete(false);
                return;
            }
            
            onComplete(true);
#else
            onComplete(false);
#endif
        }
    }
}