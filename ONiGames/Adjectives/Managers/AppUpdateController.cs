//#define GPG_APP_UPDATE_ENABLE

using System;
using ONiGames.Asserts;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class AppUpdateController : MonoBehaviour
    {
        public enum UpdateAvailability
        {
            Unknown = 0,
            UpdateNotAvailable = 1,
            UpdateAvailable = 2,
            DeveloperTriggeredUpdateInProgress = 3
        }
        
        [AutoComponentField]
        [SerializeField]
        Yielder yielder;

#if UNITY_ANDROID
        [SerializeField]
        bool isFlexible = false;
#endif

#if UNITY_ANDROID && GPG_APP_UPDATE_ENABLE
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        [JetBrains.Annotations.CanBeNull]
        Google.Play.AppUpdate.AppUpdateManager appUpdateManager = null;
#endif
        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void Start()
        {
#if !UNITY_EDITOR
#if UNITY_ANDROID && GPG_APP_UPDATE_ENABLE
            appUpdateManager = new Google.Play.AppUpdate.AppUpdateManager();
#endif
#endif
        }

        public void CheckForUpdate(Action<UpdateAvailability> onComplete)
        {
#if UNITY_ANDROID && GPG_APP_UPDATE_ENABLE
            if (appUpdateManager == null)
            {
                onComplete(UpdateAvailability.Unknown);
                return;
            }
            
            yielder.Wait(appUpdateManager.GetAppUpdateInfo(), appUpdateInfoOperation =>
            {
                if (appUpdateInfoOperation.Error != Google.Play.AppUpdate.AppUpdateErrorCode.NoError)
                {
                    Debug.LogError($"{gameObject.GetPath()}: {appUpdateInfoOperation.Error}");
                    return;
                }
                
                var appUpdateInfo = appUpdateInfoOperation.GetResult();
                if (appUpdateInfo == null)
                {
                    Debug.LogError($"{gameObject.GetPath()}: result is null");
                    return;
                }

                var updateAvailability = appUpdateInfo.UpdateAvailability switch
                {
                    Google.Play.AppUpdate.UpdateAvailability.Unknown => UpdateAvailability.Unknown,
                    Google.Play.AppUpdate.UpdateAvailability.UpdateNotAvailable => UpdateAvailability.UpdateNotAvailable,
                    Google.Play.AppUpdate.UpdateAvailability.UpdateAvailable => UpdateAvailability.UpdateAvailable,
                    Google.Play.AppUpdate.UpdateAvailability.DeveloperTriggeredUpdateInProgress => UpdateAvailability.DeveloperTriggeredUpdateInProgress,
                    _ => UpdateAvailability.Unknown
                };

                onComplete(updateAvailability);
            });
            
#elif UNITY_IOS
            onComplete(UpdateAvailability.Unknown);
#else
            onComplete(UpdateAvailability.Unknown);
#endif
        }

        public void StartUpdate(Action<bool> onComplete)
        {
#if UNITY_ANDROID && GPG_APP_UPDATE_ENABLE
            if (appUpdateManager == null)
            {
                onComplete(false);
                return;
            }
            
            var appUpdateOptions = isFlexible ? Google.Play.AppUpdate.AppUpdateOptions.FlexibleAppUpdateOptions() : Google.Play.AppUpdate.AppUpdateOptions.ImmediateAppUpdateOptions();

            yielder.Wait(appUpdateManager.GetAppUpdateInfo(), appUpdateInfoOperation =>
            {
                if (appUpdateInfoOperation.Error != Google.Play.AppUpdate.AppUpdateErrorCode.NoError)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {appUpdateInfoOperation.Error}");
                    
                    onComplete(false);
                    return;
                }

                var appUpdateInfo = appUpdateInfoOperation.GetResult();
                if (appUpdateInfo == null)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: result is null");
                    
                    onComplete(false);
                    return;
                }
                
                yielder.Wait(appUpdateManager.StartUpdate(appUpdateInfo, appUpdateOptions), startUpdateRequest =>
                {
                    if (startUpdateRequest.Error != Google.Play.AppUpdate.AppUpdateErrorCode.NoError)
                    {
                        Debug.LogWarning($"{gameObject.GetPath()}: {startUpdateRequest.Error}");
                        
                        onComplete(false);
                        return;
                    }

                    yielder.Wait(appUpdateManager.CompleteUpdate(), completeUpdateOperation =>
                    {
                        if (completeUpdateOperation.Error != Google.Play.AppUpdate.AppUpdateErrorCode.NoError)
                        {
                            Debug.LogWarning($"{gameObject.GetPath()}: {completeUpdateOperation.Error}");
                            
                            onComplete(false);
                            return;
                        }

                        onComplete(true);
                    });
                });
            });

#elif UNITY_IOS
            onComplete(false);
#else
            onComplete(false);
#endif
        }
    }
}