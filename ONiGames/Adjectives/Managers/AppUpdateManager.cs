using System;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class AppUpdateManager : ASingletonBehaviour<AppUpdateManager>
    {
        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        AppUpdateController controller;

        public void CheckForUpdate(Action<AppUpdateController.UpdateAvailability> onComplete)
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");

                onComplete(AppUpdateController.UpdateAvailability.Unknown);
                return;
            }

            controller.CheckForUpdate(onComplete);
        }

        public void StartUpdate(Action<bool> onComplete)
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");

                onComplete(false);
                return;
            }

            controller.StartUpdate(onComplete);
        }
    }
}