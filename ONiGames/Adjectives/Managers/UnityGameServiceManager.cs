using System;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class UnityGameServiceManager : ASingletonBehaviour<UnityGameServiceManager>
    {
        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        UnityGameServiceController controller;

        public string Test
        {
            get
            {
                if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Warning))
                    return string.Empty;
                
                return controller.test;
            }
        }

        public void Initialize(string environmentName, string userId, Action onComplete)
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Warning))
            {
                onComplete();
                return;
            }
            
            controller.Initialize(environmentName, userId, onComplete);
        }
        
        public void FetchRemoteConfig(Action onComplete)
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Warning))
            {
                onComplete();
                return;
            }
            
            controller.FetchRemoteConfig(onComplete);
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public T GetRemoteConfigValue<T>(string key, T def = default) where T : IConvertible
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Warning))
                return def;
            
            return controller.GetRemoteConfigValue(key, def);
        }
    }
}