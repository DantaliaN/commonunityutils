﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;
using UnityEngine.Purchasing;

namespace ONiGames.Adjectives.Managers
{
    public class PurchaseManager : ASingletonBehaviour<PurchaseManager>
    {
        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        PurchaseController purchaseController;

        public List<Product> AutoRestoreProduct { get; } = new();

        public bool IsInitialized
        {
            get
            {
                if (purchaseController == null)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                    return false;
                }

                return purchaseController.IsInitialized;
            }
        }

        public void Initialize(IEnumerable<PurchaseController.IPurchaseItem> items, Action onComplete, Action onError)
        {
            Initialize(items, onComplete, onError, product => AutoRestoreProduct.Add(product));
        }

        public void Initialize(IEnumerable<PurchaseController.IPurchaseItem> items, Action onComplete, Action onError, Action<Product> onPurchaseAutoRestore)
        {
            if (purchaseController == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                onError();
                return;
            }

            purchaseController.Initialize(items, onComplete, onError, onPurchaseAutoRestore);
        }

        [CanBeNull]
        public PurchaseController.IPurchaseItem FindItem(string id)
        {
            if (purchaseController == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                return null;
            }

            return purchaseController.FindItem(id);
        }

        [CanBeNull]
        public Product FindProduct(PurchaseController.IPurchaseItem item)
        {
            if (purchaseController == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                return null;
            }

            return purchaseController.FindProduct(item);
        }
        
        [CanBeNull]
        public PurchaseController.ProductItem FindProductItem(PurchaseController.IPurchaseItem item)
        {
            if (purchaseController == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                return null;
            }
            
            return purchaseController.FindProductItem(item);
        }

        public void BuyItem(PurchaseController.IPurchaseItem item, Action<Product> onComplete, PurchaseController.PurchaseError onError)
        {
            if (purchaseController == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                
				onError(PurchaseFailureReason.Unknown, null);
                return;
            }

            purchaseController.BuyItem(item, onComplete, onError);
        }

        public void ConfirmPurchase(Product product)
        {
            if (purchaseController == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                return;
            }

            purchaseController.ConfirmPurchase(product);
        }

        public void ConfirmPurchase(PurchaseController.IPurchaseItem item)
        {
            if (purchaseController == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                return;
            }

            purchaseController.ConfirmPurchase(item);
        }

        public void RestorePurchases()
        {
            if (purchaseController == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(purchaseController)} not set");
                return;
            }

            purchaseController.RestorePurchases();
        }
    }
}