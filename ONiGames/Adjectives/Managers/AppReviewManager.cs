using System;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class AppReviewManager : ASingletonBehaviour<AppReviewManager>
    {
        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        AppReviewController controller;

        public void SendUserReview(string email, string userId, string message = "")
        {
            if (controller == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return;
            }

            controller.SendUserReview(email, userId, message);
        }

        public void OpenMarket()
        {
            if (controller == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return;
            }

            controller.OpenMarket();
        }

        public void Review(Action<bool> onComplete)
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");

                onComplete(false);
                return;
            }

            controller.Review(onComplete);
        }
    }
}