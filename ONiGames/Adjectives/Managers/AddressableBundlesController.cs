﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using ONiGames.Utilities;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;

namespace ONiGames.Adjectives.Managers
{
    public class AddressableBundlesController : MonoBehaviour
    {
        class AssetHandle
        {
            [NotNull]
            public string name = string.Empty;

            public AsyncOperationHandle handle;
        }

        readonly Queue<Task> tasks = new();

        readonly List<AssetHandle> assetHandles = new();

        bool isStarted = false;

        bool IsStarted
        {
            get
            {
                lock (this)
                {
                    return isStarted;
                }
            }
        }

        void Start()
        {
            isStarted = true;

            Task.Run(async () =>
            {
                while (IsStarted)
                {
                    if (tasks.Count > 0)
                        await tasks.Dequeue();

                    await Task.Delay(10);
                }
            });
        }

        void OnDestroy()
        {
            StopAllCoroutines();

            isStarted = false;

            tasks.Clear();

            for (var i = assetHandles.Count - 1; i >= 0; i--)
            {
                if (!assetHandles[i].handle.IsValid())
                    continue;

                try
                {
                    Addressables.Release(assetHandles[i].handle);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                }
            }

            assetHandles.Clear();
        }

        public static void ClearCache()
        {
#if !UNITY_WEBGL && !UNITY_EDITOR
            Caching.ClearCache();
#endif
        }

        public void Initialize(Action onComplete, Action<string> onError)
        {
            tasks.Enqueue(InitializeAsync(onComplete, onError));
        }

        public void ResourceLocations(IEnumerable<string> labels, Action<IList<IResourceLocation>> onComplete, Action<string> onError)
        {
            tasks.Enqueue(ResourceLocationsAsync(labels, onComplete, onError));
        }

        public void ResourceLocation(string label, Action<IResourceLocation> onComplete, Action<string> onError)
        {
            ResourceLocations(new[] { label }, locations =>
            {
                if (locations.Count != 1)
                {
                    onError($"{label} wrong label");
                    return;
                }

                onComplete(locations.FirstOrDefault());
            }, onError);
        }

        public void AssetsSize(IList<IResourceLocation> locations, Action<long> onComplete, Action<string> onError)
        {
            tasks.Enqueue(AssetsSizeAsync(locations, onComplete, onError));
        }

        public void AssetSize(IResourceLocation location, Action<long> onComplete, Action<string> onError)
        {
            AssetsSize(new[] { location }, onComplete, onError);
        }

        public void AssetsSize(IEnumerable<string> assetNames, Action<long> onComplete, Action<string> onError)
        {
            tasks.Enqueue(AssetsSizeAsync(assetNames, onComplete, onError));
        }

        public void AssetsSize(IEnumerable<AssetReference> assets, Action<long> onComplete, Action<string> onError)
        {
            AssetsSize(assets.Where(o => !string.IsNullOrEmpty(o.AssetGUID)).Select(o => o.AssetGUID), onComplete, onError);
        }

        public void AssetSize(string assetName, Action<long> onComplete, Action<string> onError)
        {
            AssetsSize(new[] { assetName }, onComplete, onError);
        }

        public void AssetSize(AssetReference asset, Action<long> onComplete, Action<string> onError)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            AssetSize(asset.AssetGUID, onComplete, onError);
        }

        public void DownloadAssets(IList<IResourceLocation> locations, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            tasks.Enqueue(DownloadAssetsAsync(locations, onProgress, onComplete, onError));
        }

        public void DownloadAsset(IResourceLocation location, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            DownloadAssets(new[] { location }, onProgress, onComplete, onError);
        }

        public void DownloadAssets(IReadOnlyCollection<string> assetNames, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            tasks.Enqueue(DownloadAssetsAsync(assetNames, onProgress, onComplete, onError));
        }

        public void DownloadAsset(string assetName, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            DownloadAssets(new[] { assetName }, onProgress, onComplete, onError);
        }

        public void DownloadAsset(AssetReference asset, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            DownloadAsset(asset.AssetGUID, onProgress, onComplete, onError);
        }

        public void DownloadAssets(IEnumerable<AssetReference> assets, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            DownloadAssets(assets.Where(o => !string.IsNullOrEmpty(o.AssetGUID)).Select(o => o.AssetGUID).ToArray(), onProgress, onComplete, onError);
        }

        public void LoadScene(string assetName, LoadSceneMode loadMode, Action<Scene> onComplete, Action<string> onError)
        {
            tasks.Enqueue(LoadSceneAsync(assetName, loadMode, result =>
            {
                onComplete(result.Scene);
            }, onError));
        }

        public void LoadScene(AssetReference asset, LoadSceneMode loadMode, Action<Scene> onComplete, Action<string> onError)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            LoadScene(asset.AssetGUID, loadMode, onComplete, onError);
        }

        public async Task<Scene> LoadSceneAsync(string assetName, LoadSceneMode loadMode)
        {
            try
            {
                var handle = Addressables.LoadSceneAsync(assetName, loadMode);

                await handle.Task;

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    return new Scene();
                }

                assetHandles.Add(new AssetHandle { name = assetName, handle = handle });

                return handle.Result.Scene;
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                return new Scene();
            }
        }

        public async Task<Scene> LoadSceneAsync(AssetReference asset, LoadSceneMode loadMode)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                Debug.LogWarning($"{gameObject.GetPath()}: Asset {asset} not set");
                return new Scene();
            }

            return await LoadSceneAsync(asset.AssetGUID, loadMode);
        }

        public void UnloadScene(string assetName, Action<Scene> onComplete, Action<string> onError)
        {
            tasks.Enqueue(UnloadSceneAsync(assetName, result =>
            {
                onComplete(result.Scene);
            }, onError));
        }

        public void UnloadScene(AssetReference asset, Action<Scene> onComplete, Action<string> onError)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            UnloadScene(asset.AssetGUID, onComplete, onError);
        }

        public async Task UnloadSceneAsync(string assetName)
        {
            for (var i = assetHandles.Count - 1; i >= 0; i--)
            {
                var assetHandle = assetHandles[i];

                if (assetHandle.name != assetName)
                    continue;

                assetHandles.RemoveAt(i);

                if (!assetHandle.handle.IsValid())
                    continue;

                try
                {
                    var handle = Addressables.UnloadSceneAsync(assetHandle.handle);

                    await handle.Task;

                    if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                        Debug.LogWarning($"{gameObject.GetPath()}: {assetName}: {(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");

                    return;
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                }
            }
        }

        public async Task UnloadSceneAsync(AssetReference asset)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                Debug.LogWarning($"{gameObject.GetPath()}: Asset {asset} not set");
                return;
            }

            await UnloadSceneAsync(asset.AssetGUID);
        }

        public void LoadAssets<T>(IEnumerable<IResourceLocation> locations, Action<ICollection<T>> onComplete, Action<string> onError)
        {
            tasks.Enqueue(LoadAssetsAsync<T>(locations, prefabs =>
            {
                StartCoroutine(WaitForEndOfFrame(() =>
                {
                    onComplete(prefabs);
                }));
            }, onError));
        }

        public void LoadAsset<T>(IResourceLocation location, Action<T> onComplete, Action<string> onError)
        {
            LoadAssets<T>(new[] { location }, prefabs =>
            {
                if (prefabs.Count != 1)
                {
                    onError($"{location} wrong asset");
                    return;
                }

                onComplete(prefabs.FirstOrDefault());
            }, onError);
        }

        public void LoadAssets<T>(IEnumerable<string> assetNames, Action<ICollection<T>> onComplete, Action<string> onError)
        {
            tasks.Enqueue(LoadAssetsAsync<T>(assetNames, prefabs =>
            {
                StartCoroutine(WaitForEndOfFrame(() =>
                {
                    onComplete(prefabs);
                }));
            }, onError));
        }

        public void LoadAssets<T>(IEnumerable<AssetReference> assets, Action<ICollection<T>> onComplete, Action<string> onError)
        {
            LoadAssets(assets.Where(o => !string.IsNullOrEmpty(o.AssetGUID)).Select(o => o.AssetGUID), onComplete, onError);
        }

        public void LoadAsset<T>(string assetName, Action<T> onComplete, Action<string> onError)
        {
            LoadAssets<T>(new[] { assetName }, prefabs =>
            {
                if (prefabs.Count != 1)
                {
                    onError($"{assetName} wrong asset");
                    return;
                }

                onComplete(prefabs.FirstOrDefault());
            }, onError);
        }

        public void LoadAsset<T>(AssetReference asset, Action<T> onComplete, Action<string> onError)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            LoadAsset(asset.AssetGUID, onComplete, onError);
        }

        public void LoadGameObjectAssets<T>(IEnumerable<string> assetNames, Action<ICollection<T>> onComplete, Action<string> onError) where T : Component
        {
            LoadAssets<GameObject>(assetNames, prefabs =>
            {
                var components = new List<T>();

                foreach (var prefab in prefabs)
                {
                    var component = prefab.GetComponent<T>();
                    if (component == null)
                    {
                        onError($"Component of {typeof(T)} not found on {prefab.GetPath()}");
                        return;
                    }

                    components.Add(component);
                }

                onComplete(components);
            }, onError);
        }

        public void LoadGameObjectDeepAssets<T>(IEnumerable<string> assetNames, Action<ICollection<T>> onComplete, Action<string> onError) where T : Component
        {
            LoadAssets<GameObject>(assetNames, prefabs =>
            {
                var components = new List<T>();

                foreach (var prefab in prefabs)
                {
                    var component = prefab.GetComponentInChildren<T>();
                    if (component == null)
                    {
                        onError($"Component of {typeof(T)} not found on {prefab.GetPath()}");
                        return;
                    }

                    components.Add(component);
                }

                onComplete(components);
            }, onError);
        }

        public void LoadGameObjectAssets<T>(IEnumerable<AssetReference> assets, Action<ICollection<T>> onComplete, Action<string> onError) where T : Component
        {
            LoadGameObjectAssets(assets.Where(o => !string.IsNullOrEmpty(o.AssetGUID)).Select(o => o.AssetGUID), onComplete, onError);
        }

        public void LoadGameObjectAsset<T>(string assetName, Action<T> onComplete, Action<string> onError) where T : Component
        {
            LoadGameObjectAssets<T>(new[] { assetName }, prefabs =>
            {
                if (prefabs.Count != 1)
                {
                    onError($"{assetName} wrong asset");
                    return;
                }

                onComplete(prefabs.FirstOrDefault());
            }, onError);
        }

        public void LoadGameObjectDeepAsset<T>(string assetName, Action<T> onComplete, Action<string> onError) where T : Component
        {
            LoadGameObjectDeepAssets<T>(new[] { assetName }, prefabs =>
            {
                if (prefabs.Count != 1)
                {
                    onError($"{assetName} wrong asset");
                    return;
                }

                onComplete(prefabs.FirstOrDefault());
            }, onError);
        }

        public void LoadGameObjectAsset<T>(AssetReference asset, Action<T> onComplete, Action<string> onError) where T : Component
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            LoadGameObjectAsset(asset.AssetGUID, onComplete, onError);
        }

        public void LoadGameObjectDeepAsset<T>(AssetReference asset, Action<T> onComplete, Action<string> onError) where T : Component
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            LoadGameObjectDeepAsset(asset.AssetGUID, onComplete, onError);
        }

        public void LoadSpriteAsset(string assetName, string spriteName, Action<Sprite> onComplete, Action<string> onError)
        {
            LoadAssets<SpriteAtlas>(new[] { assetName }, prefabs =>
            {
                if (prefabs.Count != 1)
                {
                    onError($"{assetName} wrong asset");
                    return;
                }

                var sprite = prefabs.FirstOrDefault()?.GetSprite(spriteName);
                if (sprite == null)
                {
                    onError($"{assetName} {spriteName} not set");
                    return;
                }

                onComplete(sprite);
            }, onError);
        }

        public void LoadSpriteAsset(AssetReference asset, Action<Sprite> onComplete, Action<string> onError)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }

            if (string.IsNullOrEmpty(asset.SubObjectName))
            {
                onError($"Asset {asset} SubObject not set");
                return;
            }

            LoadSpriteAsset(asset.AssetGUID, asset.SubObjectName, onComplete, onError);
        }

        public void ReleaseAsset(string assetName)
        {
            for (var i = assetHandles.Count - 1; i >= 0; i--)
            {
                var assetHandle = assetHandles[i];

                if (assetHandle.name != assetName)
                    continue;

                assetHandles.RemoveAt(i);

                if (!assetHandle.handle.IsValid())
                    continue;

                try
                {
                    Addressables.Release(assetHandle.handle);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex.ToString());
                }
            }
        }

        public void ReleaseAsset(AssetReference asset)
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
                return;

            ReleaseAsset(asset.AssetGUID);
        }

        static async Task InitializeAsync(Action onComplete, Action<string> onError)
        {
            try
            {
                var handle = Addressables.InitializeAsync();

                await handle.Task;

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                    onError($"{(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                else
                    onComplete();
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        static async Task ResourceLocationsAsync(IEnumerable<string> labels, Action<IList<IResourceLocation>> onComplete, Action<string> onError)
        {
            try
            {
                var handle = Addressables.LoadResourceLocationsAsync(labels, Addressables.MergeMode.Union);

                await handle.Task;

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                {
                    onError($"{(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    return;
                }

                onComplete(handle.Result.DistinctBy(o => o.PrimaryKey).ToList());
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        // ReSharper disable once ParameterTypeCanBeEnumerable.Local - b/c we need exactly IList<IResourceLocation>
        static async Task AssetsSizeAsync(IList<IResourceLocation> locations, Action<long> onComplete, Action<string> onError)
        {
            try
            {
                var handle = Addressables.GetDownloadSizeAsync(locations);

                await handle.Task;

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                {
                    onError($"{(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    return;
                }

                onComplete(handle.Result);
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        static async Task AssetsSizeAsync(IEnumerable<string> assetNames, Action<long> onComplete, Action<string> onError)
        {
            try
            {
                var handle = Addressables.GetDownloadSizeAsync(assetNames);

                await handle.Task;

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                {
                    onError($"{(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    return;
                }

                onComplete(handle.Result);
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        static async Task DownloadAssetsAsync(IList<IResourceLocation> locations, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            try
            {
                var handle = Addressables.DownloadDependenciesAsync(locations);

                while (!handle.IsDone)
                {
                    onProgress(handle.GetDownloadStatus());

                    await Task.Delay(10);
                }

                onProgress(handle.GetDownloadStatus());

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                {
                    onError($"{(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    return;
                }
				
				try
				{
					Addressables.Release(handle);
				}
				catch (Exception ex)
				{
					onError(ex.ToString());
					return;
				}
				
				onComplete();
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        static async Task DownloadAssetsAsync(IEnumerable<string> assetNames, Action<DownloadStatus> onProgress, Action onComplete, Action<string> onError)
        {
            try
            {
                var handle = Addressables.DownloadDependenciesAsync(assetNames, Addressables.MergeMode.Union);

                while (!handle.IsDone)
                {
                    onProgress(handle.GetDownloadStatus());

                    await Task.Delay(10);
                }

                onProgress(handle.GetDownloadStatus());

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                {
                    onError($"{(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    return;
                }

				try
				{
					Addressables.Release(handle);
				}
				catch (Exception ex)
				{
					onError(ex.ToString());
					return;
				}

                onComplete();
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        async Task LoadAssetsAsync<T>(IEnumerable<IResourceLocation> locations, Action<ICollection<T>> onComplete, Action<string> onError)
        {
            try
            {
                var prefabs = new List<T>();

                foreach (var location in locations)
                {
                    var handle = Addressables.LoadAssetAsync<T>(location);

                    await handle.Task;

                    if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                    {
                        onError($"{location}: {(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                        return;
                    }

                    assetHandles.Add(new AssetHandle { name = location.PrimaryKey, handle = handle });

                    prefabs.Add(handle.Result);
                }

                onComplete(prefabs);
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        async Task LoadAssetsAsync<T>(IEnumerable<string> assetNames, Action<ICollection<T>> onComplete, Action<string> onError)
        {
            try
            {
                var prefabs = new List<T>();

                foreach (var assetName in assetNames.Distinct())
                {
                    var handle = Addressables.LoadAssetAsync<T>(assetName);

                    await handle.Task;

                    if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                    {
                        onError($"{assetName}: {(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                        return;
                    }

                    assetHandles.Add(new AssetHandle { name = assetName, handle = handle });

                    prefabs.Add(handle.Result);
                }

                onComplete(prefabs);
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        async Task LoadSceneAsync(string assetName, LoadSceneMode loadMode, Action<SceneInstance> onComplete, Action<string> onError)
        {
            try
            {
                var handle = Addressables.LoadSceneAsync(assetName, loadMode);

                await handle.Task;

                if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                {
                    onError($"{assetName}: {(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    return;
                }

                assetHandles.Add(new AssetHandle { name = assetName, handle = handle });

                onComplete(handle.Result);
            }
            catch (Exception ex)
            {
                onError(ex.ToString());
            }
        }

        async Task UnloadSceneAsync(string assetName, Action<SceneInstance> onComplete, Action<string> onError)
        {
            for (var i = assetHandles.Count - 1; i >= 0; i--)
            {
                var assetHandle = assetHandles[i];

                if (assetHandle.name != assetName)
                    continue;

                assetHandles.RemoveAt(i);

                if (!assetHandle.handle.IsValid())
                    continue;

                try
                {
                    var handle = Addressables.UnloadSceneAsync(assetHandle.handle);

                    await handle.Task;

                    if (handle.Status != AsyncOperationStatus.Succeeded || !handle.IsValid())
                        onError($"{assetName}: {(handle.OperationException != null ? handle.OperationException.ToString() : handle.Status.ToString())}");
                    else
                        onComplete(handle.Result);

                    return;
                }
                catch (Exception ex)
                {
                    onError(ex.ToString());
                }
            }
        }

        static IEnumerator WaitForEndOfFrame(Action onComplete)
        {
            yield return new WaitForEndOfFrame();
            onComplete();
        }
    }
}