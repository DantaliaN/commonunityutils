using System;
using System.Collections.Generic;
using System.Globalization;
using JetBrains.Annotations;
using ONiGames.Asserts;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
using Unity.Services.RemoteConfig;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class UnityGameServiceController : MonoBehaviour
    {
        struct UserAttributes { }
        
        struct AppAttributes { }
        
        struct filterAttributes { }
        
        public const string DateFormat = "yyyy-MM-dd HH:mm:ss.fffffff";
        
        [AutoComponentField]
        [SerializeField]
        MainThreadDispatcher mainThreadDispatcher;
        
        [AutoComponentField]
        [SerializeField]
        Asyncer asyncer;
        
        [DynamicField]
        public string test = string.Empty;
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
        
        public void Initialize(string environmentName, string userId, Action onComplete)
        {
            try
            {
                if (UnityServices.State != ServicesInitializationState.Uninitialized)
                {
                    onComplete();
                    return;
                }
                
                UnityServices.ExternalUserId = userId;
                
                var options = new InitializationOptions();
                options.SetEnvironmentName(environmentName);
                
                asyncer.Wait(UnityServices.InitializeAsync(options), () =>
                {
                    asyncer.Wait(AuthenticationService.Instance.SignInAnonymouslyAsync(), () =>
                    {
                        mainThreadDispatcher.Process(onComplete);
                    });
                });
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                
                onComplete();
            }
        }
        
        public void FetchRemoteConfig(Action onComplete)
        {
            asyncer.Wait(RemoteConfigService.Instance.FetchConfigsAsync(new UserAttributes(), new AppAttributes(), new filterAttributes()), obj =>
            {
                switch (obj.origin)
                {
                    case ConfigOrigin.Default:
                    case ConfigOrigin.Cached:
                    case ConfigOrigin.Remote:
                        test = RemoteConfigService.Instance.appConfig.GetString("Test", string.Empty);
                        break;
                    default:
                        Debug.LogWarning($"{gameObject.GetPath()}: {obj.origin} not implemented!");
                        break;
                }
                
                mainThreadDispatcher.Process(onComplete);
            }, error =>
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {error}");
                
                mainThreadDispatcher.Process(onComplete);
            });
        }
        
        [ContractAnnotation("def:notNull=>notNull")]
        [CanBeNull]
        public T GetRemoteConfigValue<T>(string key, T def = default) where T : IConvertible
        {
            if (RemoteConfigService.Instance.appConfig == null)
            {
                return def;
            }
            
            try
            {
                var @switch = new Dictionary<Type, Func<T>>
                {
                    { typeof(int), () => Utils.ConvertValue<int, T>(RemoteConfigService.Instance.appConfig.GetInt(key, Utils.ConvertValue<T, int>(def))) },
                    { typeof(float), () => Utils.ConvertValue<float, T>(RemoteConfigService.Instance.appConfig.GetFloat(key, Utils.ConvertValue<T, float>(def))) },
                    { typeof(string), () => Utils.ConvertValue<string, T>(RemoteConfigService.Instance.appConfig.GetString(key, Utils.ConvertValue<T, string>(def))) },
                    { typeof(DateTime), () => Utils.ConvertValue<DateTime, T>(DateTime.TryParse(RemoteConfigService.Instance.appConfig.GetString(key, Utils.ConvertValue<T, DateTime>(def).ToString(DateFormat)), out var dateTime) ? dateTime : DateTime.MinValue) },
                    { typeof(bool), () => Utils.ConvertValue<bool, T>(RemoteConfigService.Instance.appConfig.GetInt(key, Utils.ConvertValue<T, bool>(def) ? 1 : 0) == 1) },
                    { typeof(Enum), () => Utils.GetEnumMember<T>(RemoteConfigService.Instance.appConfig.GetString(key, Utils.GetEnumValue((Enum) (object) def))) },
                    { typeof(double), () => Utils.ConvertValue<double, T>(Convert.ToDouble(RemoteConfigService.Instance.appConfig.GetString(key, Convert.ToString(Utils.ConvertValue<T, double>(def), CultureInfo.InvariantCulture)), CultureInfo.InvariantCulture)) },
                    { typeof(decimal), () => Utils.ConvertValue<decimal, T>(Convert.ToDecimal(RemoteConfigService.Instance.appConfig.GetString(key, Convert.ToString(Utils.ConvertValue<T, decimal>(def), CultureInfo.InvariantCulture)), CultureInfo.InvariantCulture)) },
                };
                var type = typeof(T).IsEnum ? typeof(Enum) : typeof(T);
                return @switch[type].Invoke();
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                return def;
            }
        }
    }
}