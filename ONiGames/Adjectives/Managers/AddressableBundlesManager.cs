using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace ONiGames.Adjectives.Managers
{
    public class AddressableBundlesManager : ASingletonBehaviour<AddressableBundlesManager>
    {
        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        AddressableBundlesController addressableBundles;

        protected override bool UseDontDestroyOnLoad => false;

        public void Transient<T>(string assetName, Action<T> onComplete, Action<string> onError) where T : Component
        {
            if (addressableBundles == null)
            {
                onError($"{typeof(AddressableBundlesController)} not set");
                return;
            }
            
            addressableBundles.LoadGameObjectAsset<T>(assetName, prefab =>
            {
                var instance = Instantiate(prefab, gameObject.transform);

                try
                {
                    onComplete(instance);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                }

                Destroy(instance.gameObject);
            }, onError);
        }

        public void Transient<T>(AssetReference asset, Action<T> onComplete, Action<string> onError) where T : Component
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                onError($"Asset {asset} not set");
                return;
            }
            
            Transient(asset.AssetGUID, onComplete, onError);
        }

        public void Transient<T>(string assetName, Action<T> onComplete) where T : Component
        {
            Transient(assetName, onComplete, error =>
            {
                Debug.LogError($"{gameObject.GetPath()}: {error}");
            });
        }

        public void Transient<T>(AssetReference asset, Action<T> onComplete) where T : Component
        {
            if (string.IsNullOrEmpty(asset.AssetGUID))
            {
                Debug.LogError($"{gameObject.GetPath()}: Asset {asset} not set");
                return;
            }
            
            Transient(asset, onComplete, error =>
            {
                Debug.LogError($"{gameObject.GetPath()}: {error}");
            });
        }

        public void Transient<T>(IEnumerable<string> assetNames, Action<IEnumerable<T>> onComplete) where T : Component
        {
            if (addressableBundles == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {typeof(AddressableBundlesController)} not set");

                onComplete(new List<T>());
                return;
            }

            var bundles = addressableBundles;

            bundles.LoadGameObjectAssets<T>(assetNames, prefabs =>
            {
                var objs = prefabs.Select(o => Instantiate(o, gameObject.transform)).ToList();

                try
                {
                    onComplete(objs);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                }

                foreach (var obj in objs)
                    Destroy(obj.gameObject);
            }, error =>
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {error}");

                onComplete(new List<T>());
            });
        }

        public void Transient<T>(IEnumerable<AssetReference> assets, Action<IEnumerable<T>> onComplete) where T : Component
        {
            var validAssets = new List<string>();

            foreach (var asset in assets)
            {
                if (string.IsNullOrEmpty(asset.AssetGUID))
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: Asset {asset} not set");
                    continue;
                }

                validAssets.Add(asset.AssetGUID);
            }
            
            Transient(validAssets, onComplete);
        }
        
        public void Transient<TKey, T>(IReadOnlyCollection<KeyValuePair<TKey, string>> pairs, Action<IEnumerable<KeyValuePair<TKey, T>>> onComplete) where T : Component
        {
            if (addressableBundles == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {typeof(AddressableBundlesController)} not set");

                onComplete(new List<KeyValuePair<TKey, T>>());
                return;
            }

            var bundles = addressableBundles;
            
            var prefabPairs = new Queue<KeyValuePair<TKey, T>>();

            var count = new RefValue<int>(pairs.Count);
            
            foreach (var pair in pairs)
            {
                bundles.LoadGameObjectAsset<T>(pair.Value, prefab =>
                {
                    prefabPairs.Enqueue(new KeyValuePair<TKey, T>(pair.Key, prefab));

                    count.value--;

                    if (count.value <= 0)
                        TransientAll();
                }, error =>
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {error}");
                    
                    count.value--;
                    
                    if (count.value <= 0)
                        TransientAll();
                });
            }

            void TransientAll()
            {
                var objPairs = new List<KeyValuePair<TKey, T>>();

                while (prefabPairs.Count > 0)
                {
                    var pair = prefabPairs.Dequeue();
                    
                    var instance = Instantiate(pair.Value, gameObject.transform);
                    
                    objPairs.Add(new KeyValuePair<TKey, T>(pair.Key, instance));
                }

                try
                {
                    onComplete(objPairs);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                }

                foreach (var pair in objPairs)
                    Destroy(pair.Value.gameObject);
            }
        }

        public void Transient<TKey, T>(IEnumerable<KeyValuePair<TKey, AssetReference>> pairs, Action<IEnumerable<KeyValuePair<TKey, T>>> onComplete) where T : Component
        {
            var validAssets = new List<KeyValuePair<TKey, string>>();

            foreach (var pair in pairs)
            {
                if (string.IsNullOrEmpty(pair.Value.AssetGUID))
                {
                    Debug.LogError($"{gameObject.GetPath()}: Asset {pair.Value} not set");
                    continue;
                }

                validAssets.Add(new KeyValuePair<TKey, string>(pair.Key, pair.Value.AssetGUID));
            }
            
            Transient(validAssets, onComplete);
        }
        
        public void LoadGameObjectAsset<T>(AssetReference asset, Action<T> onComplete, Action<string> onError) where T : Component
        {
            if (addressableBundles == null)
            {
                onError($"{typeof(AddressableBundlesController)} not set");
                return;
            }
            
            addressableBundles.LoadGameObjectAsset(asset, onComplete, onError);
        }
        
        public void ReleaseAsset(AssetReference asset)
        {
            if (addressableBundles == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {typeof(AddressableBundlesController)} not set");
                return;
            }
            
            addressableBundles.ReleaseAsset(asset);
        }
    }
}