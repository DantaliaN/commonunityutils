//#define GPG_PLAY_GAMES_ENABLE

using System;
using ONiGames.Asserts;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

#pragma warning disable CS0618 // Type or member is obsolete

namespace ONiGames.Adjectives.Managers
{
    public class SocialController : MonoBehaviour
    {
        [AutoComponentField]
        [SerializeField]
        MainThreadDispatcher mainThreadDispatcher;
        
#if UNITY_ANDROID
        [SerializeField]
        string saveFileName = "GPGSaves";
#endif
        
        void Awake()
        {
            Assert.SerializedFields(this);
        }
        
        public void Authenticate(Action<string> onComplete)
        {
#if UNITY_ANDROID && GPG_PLAY_GAMES_ENABLE
            if (PlayAs.IsDevelopmentBuild)
                GooglePlayGames.PlayGamesPlatform.DebugLogEnabled = true;

            GooglePlayGames.PlayGamesPlatform.Activate();
#endif
            Social.localUser.Authenticate((success, message) =>
            {
                if (!success)
                    Debug.LogWarning($"{gameObject.GetPath()}: Authenticate failed: {message}");
                
                var userId = success ? Social.localUser.id : SystemInfo.deviceUniqueIdentifier;
                
                mainThreadDispatcher.Process(() => onComplete(userId));
            });
        }
        
        public void LoadAndSave(Func<byte[], byte[]> onMigration, Action<byte[]> onComplete, Action<string> onError)
        {
#if UNITY_ANDROID && GPG_PLAY_GAMES_ENABLE
            if (GooglePlayGames.PlayGamesPlatform.Instance.SavedGame == null)
            {
                onError("Can't load GPG");
                return;
            }

            GooglePlayGames.PlayGamesPlatform.Instance.SavedGame.OpenWithAutomaticConflictResolution(saveFileName, GooglePlayGames.BasicApi.DataSource.ReadCacheOrNetwork, GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy.UseLongestPlaytime, (status, metadata) =>
            {
                if (status != GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus.Success || metadata == null)
                {
                    mainThreadDispatcher.Process(() => onError($"Can't open saves: {status}"));
                    return;
                }

                GooglePlayGames.PlayGamesPlatform.Instance.SavedGame.ReadBinaryData(metadata, (requestStatus, bytes) =>
                {
                    if (requestStatus != GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus.Success)
                    {
                        mainThreadDispatcher.Process(() => onError($"Can't read saves: {requestStatus}"));
                        return;
                    }
                    
                    mainThreadDispatcher.Process(() =>
                    {
                        var newBytes = onMigration(bytes);
                        
                        var saveOptions = new GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate.Builder()
                            .WithUpdatedPlayedTime(TimeSpan.FromTicks(TimeManager.Instance.Utc().Ticks)) //just use last
                            .Build();
                        
                        GooglePlayGames.PlayGamesPlatform.Instance.SavedGame.CommitUpdate(metadata, saveOptions, newBytes, (gameRequestStatus, gameMetadata) =>
                        {
                            if (gameRequestStatus != GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus.Success || gameMetadata == null)
                            {
                                mainThreadDispatcher.Process(() => onError($"Can't upload saves: {gameRequestStatus}"));
                                return;
                            }
                            
                            mainThreadDispatcher.Process(() => onComplete(newBytes));
                        }); 
                    });
                });
            });
#else
            onError("Unsupported platform");
#endif
        }
        
        public void ReportScore(int bestScores, string leaderboardId, Action onComplete)
        {
            Social.ReportScore(bestScores, leaderboardId, status =>
            {
                if (!status)
                    Debug.LogWarning($"{gameObject.GetPath()}: Can't send leaderboard scores");
                
                onComplete();
            });
        }
        
        public void ShowLeaderboard()
        {
            Social.ShowLeaderboardUI();
        }
        
        public void ShowAchievements()
        {
            Social.ShowAchievementsUI();
        }
    }
}