﻿//#define IN_APP_PURCHASE_ENABLE

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Utilities;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;

namespace ONiGames.Adjectives.Managers
{
    public class PurchaseController : MonoBehaviour, IDetailedStoreListener
    {
        public interface IPurchaseItem
        {
            string Id { get; }
            ProductType Type { get; }
            string AppleStoreId { get; }
            string GoogleStoreId { get; }
        }
        
        public class ProductItem
        {
            readonly Product product;
            
            public string LocalizedPriceString => product.metadata?.localizedPriceString;
            
            internal ProductItem(Product product) => this.product = product;
        }

        class InitializeProcess
        {
            public DisposableAction2 Process { get; }
            public Action<Product> PurchasesAutoRestore { get; }

            public InitializeProcess(Action onComplete, Action onError, Action<Product> onPurchasesAutoRestore)
            {
                Process = new DisposableAction2(onComplete, onError);
                PurchasesAutoRestore = onPurchasesAutoRestore;
            }
        }

        class PurchaseProcess
        {
            public Action<Product> Complete { get; }
            public PurchaseError Error { get; }

            public PurchaseProcess(Action<Product> onComplete, PurchaseError onError)
            {
                Complete = onComplete;
                Error = onError;
            }
        }

        public delegate void PurchaseError(PurchaseFailureReason reason, [CanBeNull] Product product);

        public bool IsInitialized => storeController != null && storeExtensionProvider != null;

        readonly Dictionary<string, PurchaseProcess> purchaseProcesses = new();

        readonly List<IPurchaseItem> purchaseItems = new();

        [CanBeNull]
        InitializeProcess initializeProcess;

        [CanBeNull]
        IStoreController storeController;

        [CanBeNull]
        IExtensionProvider storeExtensionProvider;

        public void Initialize(IEnumerable<IPurchaseItem> items, Action onComplete, Action onError, Action<Product> onPurchasesAutoRestore)
        {
            if (IsInitialized)
            {
                onComplete();
                return;
            }

            if (initializeProcess != null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: Already in initialize process");

                onError();
                return;
            }

#if !UNITY_EDITOR
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: NetworkReachability.NotReachable");

                onError();
                return;
            }
#endif

            initializeProcess = new InitializeProcess(onComplete, onError, onPurchasesAutoRestore);

            purchaseItems.Clear();
            purchaseItems.AddRange(items);

            /*
            //TODO local receipt validation
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            var appleConfig = builder.Configure<IAppleConfiguration>();
            var receiptData = System.Convert.FromBase64String(appleConfig.appReceipt);
            var receipt = new AppleValidator(AppleTangle.Data()).Validate(receiptData);

            Debug.Log(receipt.bundleID);
            Debug.Log(receipt.receiptCreationDate);
            foreach (AppleInAppPurchaseReceipt productReceipt in receipt.inAppPurchaseReceipts)
            {
                Debug.Log(productReceipt.transactionIdentifier);
                Debug.Log(productReceipt.productIdentifier);
            }
#endif
            */

            try
            {
                var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

                foreach (var item in purchaseItems)
                {
                    builder.AddProduct(item.Id, item.Type, new IDs
                    {
                        { item.AppleStoreId, AppleAppStore.Name },
                        { item.GoogleStoreId, GooglePlay.Name }
                    });
                }

                UnityPurchasing.Initialize(this, builder);
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {ex}");

                onError();
            }
        }

        [CanBeNull]
        public IPurchaseItem FindItem(string id)
        {
            return purchaseItems.FirstOrDefault(o => o.Id == id);
        }

        [CanBeNull]
        public Product FindProduct(IPurchaseItem item)
        {
            return storeController?.products.WithID(item.Id);
        }
        
        [CanBeNull]
        public ProductItem FindProductItem(IPurchaseItem item)
        {
            var product = FindProduct(item);
            return product != null ? new ProductItem(product) : null;
        }

        public void BuyItem(IPurchaseItem item, Action<Product> onComplete, PurchaseError onError)
        {
            var product = FindProduct(item);
            if (product == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: Can't find product {item.Id}");

                onError(PurchaseFailureReason.ProductUnavailable, null);
                return;
            }

            if (purchaseProcesses.ContainsKey(product.definition.id))
            {
                onError(PurchaseFailureReason.DuplicateTransaction, product);
                return;
            }

            purchaseProcesses.Add(product.definition.id, new PurchaseProcess(onComplete, onError));

#if UNITY_EDITOR
            var purchase = purchaseProcesses[product.definition.id];
            purchaseProcesses.Remove(product.definition.id);

            purchase.Complete(product);
#else
            storeController?.InitiatePurchase(product);
#endif
        }

        public void ConfirmPurchase(IPurchaseItem item)
        {
            var product = FindProduct(item);
            if (product == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: Can't find product {item.Id}");
                return;
            }

            ConfirmPurchase(product);
        }

        public void ConfirmPurchase(Product product)
        {
#if UNITY_EDITOR
            Debug.Log($"{gameObject.GetPath()}: Product {product.definition.id} confirmed");
#else
            storeController?.ConfirmPendingPurchase(product);
#endif
        }

        public void RestorePurchases()
        {
            if (storeExtensionProvider == null)
                return;

            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                var apple = storeExtensionProvider.GetExtension<IAppleExtensions>();
                apple.RestoreTransactions((result, message) =>
                {
                    Debug.Log($"{gameObject.GetPath()}: RestorePurchases continuing: {result} - {message}. If no further messages, no purchases available to restore.");
                });
            }
            else
            {
                Debug.LogWarning($"{gameObject.GetPath()}: RestorePurchases FAIL. Not supported on this platform. Current = {Application.platform}");
            }
        }

        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            storeController = controller;
            storeExtensionProvider = extensions;

            initializeProcess?.Process.Invoke1();
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
        {
            initializeProcess?.Process.Invoke2();
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error, string message)
        {
            initializeProcess?.Process.Invoke2();
        }

        PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs e)
        {
            var product = e.purchasedProduct;

            var validPurchase = true;

#if UNITY_ANDROID //|| UNITY_IOS || UNITY_STANDALONE_OSX
            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);

            try
            {
                validator.Validate(product.receipt);
            }
            catch (IAPSecurityException ex)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: Invalid receipt, not unlocking content {ex}");

                validPurchase = false;
            }
#endif

            if (validPurchase)
            {
                // it may be auto-restore for android 
                // or restore of unfinished transaction for iOS, which has no purchase process started
                if (!purchaseProcesses.ContainsKey(product.definition.id))
                {
                    initializeProcess?.PurchasesAutoRestore.Invoke(product);
                    return PurchaseProcessingResult.Pending;
                }

                var purchase = purchaseProcesses[product.definition.id];
                purchaseProcesses.Remove(product.definition.id);

                purchase.Complete(product);

                return PurchaseProcessingResult.Pending;
            }
            else
            {
                if (!purchaseProcesses.ContainsKey(product.definition.id))
                    return PurchaseProcessingResult.Complete;

                var purchase = purchaseProcesses[product.definition.id];
                purchaseProcesses.Remove(product.definition.id);

                purchase.Error(PurchaseFailureReason.SignatureInvalid, product);

                return PurchaseProcessingResult.Complete;
            }
        }

        void IStoreListener.OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            if (!purchaseProcesses.ContainsKey(product.definition.id))
                return;

            var purchase = purchaseProcesses[product.definition.id];
            purchaseProcesses.Remove(product.definition.id);

            purchase.Error(failureReason, product);
        }

        void IDetailedStoreListener.OnPurchaseFailed(Product product, PurchaseFailureDescription failureDescription)
        {
            if (!purchaseProcesses.ContainsKey(product.definition.id))
                return;

            var purchase = purchaseProcesses[product.definition.id];
            purchaseProcesses.Remove(product.definition.id);

            purchase.Error(failureDescription.reason, product);   
        }
    }
}

#if !IN_APP_PURCHASE_ENABLE

#region Purchase struct Mock

namespace UnityEngine.Purchasing
{
    public class Product
    {
        [CanBeNull]
        public Metadata metadata;
        
        [NotNull]
        public Definition definition = new();
        
        [NotNull]
        public Receipt receipt = new();
    }
    
    public class Receipt
    {
        
    }
    
    public class Metadata
    {
        [NotNull]
        public string localizedPriceString = string.Empty;
    }
    
    public class Definition
    {
        [NotNull]
        public string id = string.Empty;
    }
    
    public enum PurchaseFailureReason
    {
        Unknown,
        ProductUnavailable,
        DuplicateTransaction,
        SignatureInvalid
    }
    
    public enum ProductType
    {
        
    }
    
    public enum PurchaseProcessingResult
    {
        Pending,
        Complete
    }
    
    public class IAPSecurityException : Exception { }
    
    public class PurchaseFailureDescription
    {
        public PurchaseFailureReason reason = PurchaseFailureReason.Unknown;
    }
    
    public interface IStoreController
    {
        Products products { get; }
        
        public void InitiatePurchase(Product product);
        public void ConfirmPendingPurchase(Product product);
    }
    
    public class Products
    {
        [CanBeNull]
        public Product WithID(string id) => new();
    }
    
    public interface IDetailedStoreListener : IStoreListener
    {
        void OnPurchaseFailed(Product product, PurchaseFailureDescription failureDescription);
    }
    
    public interface IStoreListener
    {
        void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason);
        PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e);
        void OnInitialized(IStoreController controller, IExtensionProvider extensions);
        void OnInitializeFailed(InitializationFailureReason error);
        void OnInitializeFailed(InitializationFailureReason error, string message);
    }
    
    public interface InitializationFailureReason
    {
        
    }
    
    public class StandardPurchasingModule
    {
        public static StandardPurchasingModule Instance() => new();
    }
    
    public class ConfigurationBuilder
    {
        public static Builder Instance(StandardPurchasingModule instance) => new();
    }
    
    public class Builder
    {
        public void AddProduct(string name, ProductType type, IDs ids)
        {
            
        }
    }
    
    public class UnityPurchasing
    {
        public static void Initialize(IDetailedStoreListener listener, Builder builder)
        {
            
        }
    }
    
    public class PurchaseEventArgs
    {
        public Product purchasedProduct = new();
    }
    
    public class IDs : IEnumerable<KeyValuePair<string, string>>
    {
        // ReSharper disable once CollectionNeverUpdated.Local
        readonly Dictionary<string, string> dic = new();
        
        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return dic.GetEnumerator();
        }
        
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return dic.GetEnumerator();
        }
        
        public void Add(string id, params string[] stores) { }
    }
    
    public class AppleAppStore
    {
        public static string Name = string.Empty;
    }
    
    public class GooglePlay
    {
        public static string Name = string.Empty;
    }
}

namespace UnityEngine.Purchasing.Extension
{
    public interface IExtensionProvider
    {
        IAppleExtensions GetExtension<T>();
    }
    
    public interface IAppleExtensions
    {
        public void RestoreTransactions(Action<string, string> onComplete) { }
    }
}

namespace UnityEngine.Purchasing.Security
{
    public class GooglePlayTangle
    {
        public static string Data() => string.Empty;
    }
    
    public class AppleTangle
    {
        public static string Data() => string.Empty;
    }
    
    public class CrossPlatformValidator
    {
        // ReSharper disable UnusedParameter.Local
        public CrossPlatformValidator(string googleData, string appleData, string appId) { }
        
        public void Validate(Receipt receipt) { }
    }
}

#endregion

#endif 