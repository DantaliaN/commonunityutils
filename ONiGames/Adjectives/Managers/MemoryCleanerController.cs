using System;
using ONiGames.Utilities;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class MemoryCleanerController : MonoBehaviour
    {
        [SerializeField]
        bool inDevelopmentOnly = true;
        
        void OnEnable()
        {
            Application.lowMemory += ApplicationOnLowMemory;
        }

        void OnDisable()
        {
            Application.lowMemory -= ApplicationOnLowMemory;
        }

        void ApplicationOnLowMemory()
        {
            if (!Debug.isDebugBuild && inDevelopmentOnly)
                return;
            
            Debug.LogWarning($"{gameObject.GetPath()}: Low memory: {GC.GetTotalMemory(false) / 1024 / 1024}Mb of {SystemInfo.systemMemorySize}Mb. Graphic memory: {SystemInfo.graphicsMemorySize}Mb");

            Resources.UnloadUnusedAssets();
            GC.Collect();
        }
    }
}