using System;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class SocialManager : ASingletonBehaviour<SocialManager>
    {
        [AutoComponentField]
        [SerializeField]
        SocialController controller;
        
        public void Authenticate(Action<string> onComplete)
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Warning))
            {
                onComplete(string.Empty);
                return;
            }
            
            controller.Authenticate(onComplete);
        }
        
        public void LoadAndSave(Func<byte[], byte[]> onMigration, Action<byte[]> onComplete, Action<string> onError)
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Warning))
            {
                onError("Controller not set");
                return;
            }
            
            controller.LoadAndSave(onMigration, onComplete, onError);
        }
        
        public void ReportScore(int bestScores, string leaderboardId, Action onComplete)
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Warning))
            {
                onComplete();
                return;
            }
            
            controller.ReportScore(bestScores, leaderboardId, onComplete);
        }
        
        public void ShowLeaderboard()
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Error))
                return;
            
            controller.ShowLeaderboard();
        }
        
        public void ShowAchievements()
        {
            if (Insist.IsNull(controller, $"{gameObject.GetPath()}: {nameof(controller)} not set", LogType.Error))
                return;
            
            controller.ShowAchievements();
        }
    }
}
