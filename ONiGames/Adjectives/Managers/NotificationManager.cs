﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class NotificationManager : ASingletonBehaviour<NotificationManager>
    {
        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        NotificationController controller;

        public NotificationController.AuthorizationStatus Status
        {
            get
            {
                if (controller == null)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");
                    return NotificationController.AuthorizationStatus.NotDetermined;
                }

                return controller.Status;
            }
        }

        public void RequestAuthorization(Action<bool> onComplete)
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");

                onComplete(false);
                return;
            }

            controller.RequestAuthorization(onComplete);
        }

        public void Initialize(IEnumerable<NotificationController.ChannelInfo> channelInfos)
        {
            if (controller == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return;
            }

            controller.Initialize(channelInfos);
        }

        public int ScheduleNotification(string channelId, string title, string body, string smallIcon, string largeIcon, DateTime deliveryTime)
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return -1;
            }

            return controller.ScheduleNotification(channelId, title, body, smallIcon, largeIcon, deliveryTime);
        }

        public void CancelNotification(int notificationId, bool cancelDisplayed = true)
        {
            if (controller == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return;
            }

            controller.CancelNotification(notificationId, cancelDisplayed);
        }

        public void CancelAllNotifications(bool cancelDisplayed = true)
        {
            if (controller == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return;
            }

            controller.CancelAllNotifications(cancelDisplayed);
        }
    }
}