﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Asserts;
// ReSharper disable once RedundantUsingDirective
using ONiGames.Utilities;
using UnityEngine;
using NotNullAttribute = JetBrains.Annotations.NotNullAttribute;

namespace ONiGames.Adjectives.Managers
{
    public class NotificationController : MonoBehaviour
    {
        public enum AuthorizationStatus
        {
            NotDetermined,
            Denied,
            Authorized,
        }

        public enum NotificationIconType
        {
            Small = 0,
            Large = 1
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public static class NotificationIconHelper
        {
            [Serializable]
            class SettingsData
            {
                [Serializable]
                public class NotificationSettingsManager
                {
                    [Serializable]
                    public class DrawableResourceData
                    {
                        public string Id = string.Empty;
                        public NotificationIconType Type = NotificationIconType.Small;
                    }

                    [NotNull]
                    public List<DrawableResourceData> DrawableResources = new();
                }

                [NotNull]
                public NotificationSettingsManager MonoBehaviour = new();
            }

            const string SettingsPath = "ProjectSettings/NotificationsSettings.asset";

            static SettingsData Settings
            {
                get
                {
                    var settings = new SettingsData();

#if UNITY_EDITOR
                    if (File.Exists(SettingsPath))
                    {
                        var settingsJson = File.ReadAllText(SettingsPath);
                        if (!string.IsNullOrEmpty(settingsJson))
                            UnityEditor.EditorJsonUtility.FromJsonOverwrite(settingsJson, settings);
                    }
#endif

                    return settings;
                }
            }

            [UsedImplicitly]
            public static IEnumerable<string> NotificationIconList(NotificationIconType type)
            {
                return Settings.MonoBehaviour.DrawableResources.Where(o => o.Type == type).Select(o => o.Id);
            }
        }

        public class ChannelInfo
        {
            public readonly string id;
            public readonly string name;
            public readonly string description;

            public bool canBypassDnd = true;
            public bool canShowBadge = true;
            public bool enableLights = true;
            public bool enableVibration = true;

            [CanBeNull]
            public long[] vibrationPattern = null;

            public ChannelInfo(string id, string name, string description)
            {
                this.id = id;
                this.name = name;
                this.description = description;
            }
        }

        public AuthorizationStatus Status
        {
            get
            {
#if UNITY_EDITOR
                return AuthorizationStatus.NotDetermined;
#else
#if UNITY_ANDROID
                return AuthorizationStatus.Authorized;
#elif UNITY_IOS
                return Unity.Notifications.iOS.iOSNotificationCenter.GetNotificationSettings().AuthorizationStatus switch
                {
                    Unity.Notifications.iOS.AuthorizationStatus.NotDetermined => AuthorizationStatus.NotDetermined,
                    Unity.Notifications.iOS.AuthorizationStatus.Denied => AuthorizationStatus.Denied,
                    Unity.Notifications.iOS.AuthorizationStatus.Authorized => AuthorizationStatus.Authorized,
                    _ => AuthorizationStatus.NotDetermined
                };
#else
                return AuthorizationStatus.NotDetermined;
#endif
#endif
            }
        }

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        void OnApplicationFocus(bool hasFocus)
        {
#if !UNITY_EDITOR
#if UNITY_IOS
            if (hasFocus)
                Unity.Notifications.iOS.iOSNotificationCenter.ApplicationBadge = 0;
#endif
#endif
        }

        void OnDestroy()
        {
            StopAllCoroutines();
        }

        public void RequestAuthorization(Action<bool> onComplete)
        {
            StartCoroutine(RequestPushAuthorization(onComplete));

            IEnumerator RequestPushAuthorization(Action<bool> onGranted)
            {
#if UNITY_EDITOR
                yield return new WaitForEndOfFrame();

                onGranted(false);
#else
#if UNITY_ANDROID
                var request = new Unity.Notifications.Android.PermissionRequest();
                while (request.Status == Unity.Notifications.Android.PermissionStatus.RequestPending)
                    yield return new WaitForEndOfFrame();

                onGranted(request.Status == Unity.Notifications.Android.PermissionStatus.Allowed);
#elif UNITY_IOS
                var status = Unity.Notifications.iOS.iOSNotificationCenter.GetNotificationSettings().AuthorizationStatus;
                if (status != Unity.Notifications.iOS.AuthorizationStatus.NotDetermined)
                {
                    onComplete(status == Unity.Notifications.iOS.AuthorizationStatus.Authorized);
                    yield break;
                }

                using (var request = new Unity.Notifications.iOS.AuthorizationRequest(Unity.Notifications.iOS.AuthorizationOption.Alert | Unity.Notifications.iOS.AuthorizationOption.Badge | Unity.Notifications.iOS.AuthorizationOption.Sound, true))
                {
                    while (!request.IsFinished)
                        yield return new WaitForEndOfFrame();

                    onGranted(request.Granted);
                }
#else
                yield return new WaitForEndOfFrame();
                
                onGranted(false);
#endif
#endif
            }
        }

        public void Initialize(IEnumerable<ChannelInfo> channelInfos)
        {
#if !UNITY_EDITOR
#if UNITY_ANDROID
            try
            {
                Unity.Notifications.Android.AndroidNotificationCenter.Initialize();
                
                foreach (var channel in channelInfos)
                {
                    var androidChannel = new Unity.Notifications.Android.AndroidNotificationChannel(channel.id, channel.name, channel.description, Unity.Notifications.Android.Importance.High)
                    {
                        CanBypassDnd = channel.canBypassDnd,
                        CanShowBadge = channel.canShowBadge,
                        EnableLights = channel.enableLights,
                        EnableVibration = channel.enableVibration,
                        VibrationPattern = channel.vibrationPattern,
                        LockScreenVisibility = Unity.Notifications.Android.LockScreenVisibility.Private
                    };
                    
                    Unity.Notifications.Android.AndroidNotificationCenter.RegisterNotificationChannel(androidChannel);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"{gameObject.GetPath()}: {ex}");
            }
#endif
#endif
        }

        public int ScheduleNotification(string channelId, string title, string body, string smallIcon, string largeIcon, DateTime deliveryTime)
        {
#if UNITY_EDITOR
            return -1;
#else
#if UNITY_ANDROID
            try
            {
                var notification = new Unity.Notifications.Android.AndroidNotification
                {
                    Title = title,
                    Text = body,
                    SmallIcon = smallIcon,
                    LargeIcon = largeIcon,
                    FireTime = deliveryTime,
                };
                
                var id = Unity.Notifications.Android.AndroidNotificationCenter.SendNotification(notification, channelId);
                
                return id;
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                
                return -1;
            }
#elif UNITY_IOS
            try
            {
                var trigger = new Unity.Notifications.iOS.iOSNotificationCalendarTrigger
                {
                    Year = deliveryTime.Year,
                    Month = deliveryTime.Month,
                    Day = deliveryTime.Day,
                    Hour = deliveryTime.Hour,
                    Minute = deliveryTime.Minute,
                    Second = deliveryTime.Second
                };
                
                var id = Math.Abs(DateTime.Now.ToString("yyMMddHHmmssffffff").GetHashCode());
                
                var notification = new Unity.Notifications.iOS.iOSNotification(id.ToString())
                {
                    Title = title,
                    Body = body,
                    Subtitle = string.Empty,
                    Trigger = trigger,
                    ShowInForeground = true
                };
                
                Unity.Notifications.iOS.iOSNotificationCenter.ScheduleNotification(notification);
                
                return id;
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {ex}");
                
                return -1;   
            }
#else
            return -1;
#endif
#endif
        }

        public void CancelNotification(int notificationId, bool cancelDisplayed = true)
        {
#if !UNITY_EDITOR
#if UNITY_ANDROID
            Unity.Notifications.Android.AndroidNotificationCenter.CancelScheduledNotification(notificationId);
            if (cancelDisplayed)
                Unity.Notifications.Android.AndroidNotificationCenter.CancelDisplayedNotification(notificationId);
#elif UNITY_IOS
            Unity.Notifications.iOS.iOSNotificationCenter.RemoveScheduledNotification(notificationId.ToString());
            if (cancelDisplayed)
                Unity.Notifications.iOS.iOSNotificationCenter.RemoveDeliveredNotification(notificationId.ToString());
#endif
#endif
        }

        public void CancelAllNotifications(bool cancelDisplayed = true)
        {
#if !UNITY_EDITOR
#if UNITY_ANDROID
            Unity.Notifications.Android.AndroidNotificationCenter.CancelAllScheduledNotifications();
            if (cancelDisplayed)
                Unity.Notifications.Android.AndroidNotificationCenter.CancelAllDisplayedNotifications();
#elif UNITY_IOS
            Unity.Notifications.iOS.iOSNotificationCenter.RemoveAllScheduledNotifications();
            if (cancelDisplayed)
                Unity.Notifications.iOS.iOSNotificationCenter.RemoveAllDeliveredNotifications();
#endif
#endif
        }
    }
}