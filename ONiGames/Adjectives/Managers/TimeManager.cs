﻿using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using ONiGames.Utilities;
using ONiGames.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class TimeManager : ASingletonBehaviour<TimeManager>
    {
        [Serializable]
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        [SuppressMessage("ReSharper", "NotAccessedField.Local")]
        class TimeData
        {
            public string abbreviation = string.Empty;
            public string client_ip = string.Empty;
            public string datetime = string.Empty;
            public string day_of_week = string.Empty;
            public string day_of_year = string.Empty;
            public string dst = string.Empty;
            public string dst_from = string.Empty;
            public string dst_offset = string.Empty;
            public string dst_until = string.Empty;
            public string raw_offset = string.Empty;
            public string timezone = string.Empty;
            public string unixtime = string.Empty;
            public string utc_datetime = string.Empty;
            public string utc_offset = string.Empty;
            public string week_number = string.Empty;
        }

        const string URL_TIME = "http://worldtimeapi.org/api/ip"; //todo replace with google

        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        TimeController controller;

        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        Asyncer asyncer;

        [CanBeNull]
        [AutoComponentField]
        [SerializeField]
        MainThreadDispatcher mainThreadDispatcher;

        public void DelayedApplyTimeUtc()
        {
            ReceiveUtcTime(SetTimeUtc);
        }

        public static DateTime ReceiveUtcTime()
        {
            var response = Utils.HttpGet(URL_TIME, "application/json");

            var data = ONiGames.Utilities.Serialization.Utils.DeserializeFromJson(response, new TimeData());

            return DateTimeOffset.TryParse(data.utc_datetime, out var result) ? result.UtcDateTime : DateTime.UtcNow;
        }

        public void ReceiveUtcTime(Action<DateTime> onComplete)
        {
            if (asyncer == null || mainThreadDispatcher == null)
            {
                onComplete(DateTime.UtcNow);
                return;
            }

            asyncer.Wait(Utils.HttpGetAsync(URL_TIME, "application/json"), response =>
            {
                mainThreadDispatcher.Process(() =>
                {
                    var data = ONiGames.Utilities.Serialization.Utils.DeserializeFromJson(response, new TimeData());

                    onComplete(DateTimeOffset.TryParse(data.utc_datetime, out var result) ? result.UtcDateTime : DateTime.UtcNow);
                });
            }, error =>
            {
                mainThreadDispatcher.Process(() =>
                {
                    Debug.LogError($"{gameObject.GetPath()}: {error}");

                    onComplete(DateTime.UtcNow);
                });
            });
        }

        public void SetTimeUtc(DateTime dateTime)
        {
            if (controller == null)
            {
                Debug.LogError($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return;
            }

            controller.SetTimeUtc(dateTime);
        }

        public DateTime CorrectedTime()
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return DateTime.Now;
            }

            return controller.CorrectedTime();
        }

        public DateTime UtcToCorrectedTime(DateTime dateTime)
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return DateTime.Now;
            }

            return controller.UtcToCorrectedTime(dateTime);
        }

        public DateTime Utc()
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return DateTime.UtcNow;
            }

            return controller.Utc();
        }

        public DateTime LocalTime()
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return DateTime.Now;
            }

            return controller.LocalTime();
        }

        public DateTime CorrectedTimeToLocalTime(DateTime dateTime)
        {
            if (controller == null)
            {
                Debug.LogWarning($"{gameObject.GetPath()}: {nameof(controller)} not set");
                return dateTime;
            }

            return controller.CorrectedTimeToLocalTime(dateTime);
        }
    }
}