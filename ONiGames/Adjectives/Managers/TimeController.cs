﻿using System;
using ONiGames.Adjectives.Utilities.Attributes;
using UnityEngine;

namespace ONiGames.Adjectives.Managers
{
    public class TimeController : MonoBehaviour
    {
        [FloatAsDateTimeField]
        [SerializeField]
        float timeZoneOffset = -3f * 60f * 60f;

        DateTime timeUtc = DateTime.UtcNow;

        float offset = 0f;

        void Start()
        {
            SetTimeUtc(DateTime.UtcNow);
        }

        public void SetTimeUtc(DateTime dateTime)
        {
            timeUtc = dateTime;
            
            offset = Time.realtimeSinceStartup;
        }

        public DateTime CorrectedTime()
        {
            return LocalTime().AddSeconds(timeZoneOffset);
        }

        public DateTime CorrectedTimeToLocalTime(DateTime dateTime)
        {
            return dateTime.AddSeconds(-timeZoneOffset);
        }
        
        public DateTime UtcToCorrectedTime(DateTime dateTime)
        {
            return dateTime.ToLocalTime().AddSeconds(timeZoneOffset);
        }
        
        public DateTime Utc()
        {
            return timeUtc.AddSeconds(Time.realtimeSinceStartup - offset);
        }

        public DateTime LocalTime()
        {
            return timeUtc.ToLocalTime().AddSeconds(Time.realtimeSinceStartup - offset); 
        }
    }
}