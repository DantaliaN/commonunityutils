using System;
using JetBrains.Annotations;
using ONiGames.Utilities;
using UnityEngine;

namespace ONiGames.Adjectives.Localization
{
    public class LocalizationManager : Singleton<LocalizationManager>
    {
        public class LocalizationDataSettings
        {
            public virtual string ResourcesPath => "Resources";

            public virtual string Path => "Localization/Data/LocalizationData";

            [CanBeNull]
            public static LocalizationData LoadData(LocalizationDataSettings settings)
            {
                var datas = Resources.LoadAll<LocalizationData>(settings.Path);

                if (datas.Length > 1)
                    Debug.LogWarning($"{nameof(LocalizationManager)}: Find more than 1 {nameof(LocalizationData)} settings!");

                if (datas.Length > 0)
                    return datas[0];

                Debug.LogWarning($"{nameof(LocalizationManager)}: Cannot find {nameof(LocalizationData)} settings, creating default");

                const string assetsFolder = "Assets";
                const string assetExtension = ".asset";

                var assetPath = System.IO.Path.Combine(assetsFolder, settings.ResourcesPath, settings.Path + assetExtension);

                //in OnValidate we need to wait a bit before Resources.Load initialize
                if (System.IO.File.Exists(assetPath))
                    return null;

                var data = ScriptableObject.CreateInstance<LocalizationData>();

#if UNITY_EDITOR
                var directoryPath = System.IO.Path.GetDirectoryName(assetPath) ?? string.Empty;
                if (!System.IO.Directory.Exists(directoryPath))
                    System.IO.Directory.CreateDirectory(directoryPath);

                UnityEditor.AssetDatabase.CreateAsset(data, assetPath);
#endif

                return data;
            }
        }

        public event Action<SystemLanguage> ChangeLanguage = delegate { };

        public SystemLanguage Language
        {
            get => currentLanguage;
            set
            {
                currentLanguage = value;
                ChangeLanguage(currentLanguage);
            }
        }

        SystemLanguage currentLanguage = SystemLanguage.English;

        [NotNull]
        LocalizationDataSettings settings = new();

        [CanBeNull]
        Localizator _localizator;

        [CanBeNull]
        public Localizator Localizator
        {
            get
            {
                if (_localizator != null)
                    return _localizator;

                var data = LocalizationDataSettings.LoadData(settings);
                if (data != null)
                    _localizator = new Localizator(data);

                return _localizator;
            }
        }

        public void ChangeSettings(LocalizationDataSettings newSettings)
        {
            settings = newSettings;

            var data = LocalizationDataSettings.LoadData(settings);
            if (data != null)
                _localizator = new Localizator(data);
        }

        public string Localize(string key)
        {
            return Localize(key, Language);
        }

        public string Localize(string key, SystemLanguage language)
        {
            if (Localizator == null)
                return string.Empty;

            return Localizator.Localize(key, language);
        }

        public bool TryLocalize(string key, SystemLanguage language, out string text)
        {
            if (Localizator == null)
            {
                text = string.Empty;
                return false;
            }

            return Localizator.TryLocalize(key, language, out text);
        }

        public string Localize(string key, params object[] args)
        {
            return Localize(key, Language, args);
        }

        public string Localize(string key, SystemLanguage language, params object[] args)
        {
            if (Localizator == null)
                return string.Empty;

            return Localizator.Localize(key, language, args);
        }
    }
}