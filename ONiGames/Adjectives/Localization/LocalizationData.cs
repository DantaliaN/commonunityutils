using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using JetBrains.Annotations;
using ONiGames.FgCSVReader;
using ONiGames.Utilities;
using ONiGames.Utilities.CoreTypes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Localization
{
    [CreateAssetMenu(menuName = "ONiGames/Adjectives/Localization/LocalizationData")]
    public class LocalizationData : ScriptableObject
    {
        [Serializable]
        public class LanguageBlock
        {
            [Serializable]
            public class TextsDictionary : SerializedDictionary<SystemLanguage, string>
            {
                public TextsDictionary() { }
                public TextsDictionary(SerializationInfo info, StreamingContext context) : base(info, context) { }
            }

            [SerializeField]
            TextsDictionary texts = new();

            public TextsDictionary Texts => texts;
        }

        [Serializable]
        public class LanguageBlocksDictionary : SerializedDictionary<string, LanguageBlock>
        {
            public LanguageBlocksDictionary() { }
            public LanguageBlocksDictionary(SerializationInfo info, StreamingContext context) : base(info, context) { }
        }

        [Serializable]
        public class Table
        {
            [Serializable]
            public class Gid
            {
                public string name = string.Empty;
                public string gid = string.Empty;
            }

            public string id = string.Empty;
            public Gid[] gids = { new() { name = "Base", gid = "0" } };
        }

        static readonly string[] LanguageNames = { };
        static readonly SystemLanguage[] SystemLanguageNames = { };

        const string UrlPattern = "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&gid={1}";
        const string EditUrlPattern = "https://docs.google.com/spreadsheets/d/{0}/edit#gid={1}";

        [BoxGroup("Google Table")]
        [SerializeField]
        Table[] tables = new Table[0];

        [BoxGroup("Resources")]
        [SerializeField]
        string[] paths = new string[0];

        [ReadOnly]
        [SerializeField]
        LanguageBlocksDictionary languageBlocks = new();

        public IReadOnlyCollection<string> Keys => languageBlocks.Keys;

        public void LoadFromGoogle(IEnumerable<Table> ts)
        {
            var text = new StringBuilder();

            foreach (var table in ts)
            {
                foreach (var gid in table.gids)
                {
                    text.Append(Utils.HttpGet(string.Format(UrlPattern, table.id, gid.gid), "text/csv"));
                    text.Append(Environment.NewLine);
                }
            }

            Rebuild(text.ToString());
        }

        public void Parse(IEnumerable<string> ps)
        {
            var text = new StringBuilder();

            foreach (var path in ps)
            {
                var asset = Resources.Load<TextAsset>(path);
                if (asset == null)
                {
                    Debug.LogWarning($"{name}: Asset {path} not found");
                    continue;
                }

                text.Append(asset.text);
                text.Append(Environment.NewLine);
            }

            Rebuild(text.ToString());
        }

        public bool TryGetText(string key, SystemLanguage language, out string text)
        {
            if (!languageBlocks.TryGetValue(key, out var block))
            {
                text = string.Empty;
                return false;
            }

            return block.Texts.TryGetValue(language, out text);
        }

        void Rebuild(string text)
        {
            languageBlocks.Clear();

            var languages = new List<SystemLanguage>();

            CSVReader.LoadFromString(text, (index, line) =>
            {
                if (line.Count < 1)
                    return;

                var id = line[0];

                if (languageBlocks.ContainsKey(id))
                {
                    Debug.LogError($"{name}: Key {id} not add {string.Join(",", line)}");
                    return;
                }

                if (index == 0)
                {
                    for (var i = 1; i < line.Count; i++)
                    {
                        try
                        {
                            var nameIndex = LanguageNames.IndexOf(line[i]);
                            if (nameIndex > -1 && SystemLanguageNames.Length > nameIndex)
                                languages.Add(SystemLanguageNames[nameIndex]);
                            else
                                languages.Add((SystemLanguage) Enum.Parse(typeof(SystemLanguage), line[i]));
                        }
                        catch
                        {
                            Debug.LogWarning($"{name}: {line[i]} not found in {typeof(SystemLanguage)}");
                        }
                    }
                }

                var block = new LanguageBlock();

                for (var i = 0; i < line.Count - 1 && i < languages.Count; i++)
                {
                    block.Texts.Add(languages[i], line[i + 1]);
                }

                languageBlocks.Add(id, block);
            });
        }

        [BoxGroup("Google Table")]
        [UsedImplicitly]
        [Button]
        void LoadFromGoogle()
        {
            LoadFromGoogle(tables);
        }

        [BoxGroup("Resources")]
        [UsedImplicitly]
        [Button]
        void Parse()
        {
            Parse(paths);
        }

        [UsedImplicitly]
        [BoxGroup("Google Table")]
        [Button("Edit localization table", ButtonSizes.Large), GUIColor(0.4f, 0.8f, 0.5f)]
        void OpenForEdit()
        {
            if (tables.Length == 0)
            {
                Debug.LogError($"{name}: Localization tables in LocalizationData file are null. You need to add new Table to that file.");
                return;
            }

            if (tables[0].gids.Length == 0)
            {
                Debug.LogError($"{name}: GIDs in Tables from LocalizationData file are null. You need to set GIDs in table.");
                return;
            }

            Application.OpenURL(string.Format(EditUrlPattern, tables[0].id, tables[0].gids[0]));
        }
    }
}