using System;
using System.Collections.Generic;
using UnityEngine;

namespace ONiGames.Adjectives.Localization
{
    public class Localizator
    {
        readonly LocalizationData localizationData;

        public IEnumerable<string> Keys => localizationData.Keys;

        public Localizator(LocalizationData localizationData)
        {
            this.localizationData = localizationData;
        }

        public string Localize(string key, SystemLanguage language)
        {
            if (!localizationData.TryGetText(key, language, out var text))
                Debug.LogWarning($"{nameof(Localizator)}: Localization for {key} {language} not found");

            return text;
        }

        public bool TryLocalize(string key, SystemLanguage language, out string text)
        {
            return localizationData.TryGetText(key, language, out text);
        }

        public string Localize(string key, SystemLanguage language, params object[] args)
        {
            var text = Localize(key, language);

            try
            {
                return string.Format(text, args);
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{nameof(Localizator)}: {ex}");
                return text;
            }
        }
    }
}