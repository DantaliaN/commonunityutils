﻿using System;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace ONiGames.Adjectives.Localization
{
    public class ItemLocalizer : MonoBehaviour
    {
        [Serializable]
        public class Item
        {
            public SystemLanguage language = SystemLanguage.Russian;

            [CanBeNull]
            public GameObject item;
        }

        [SerializeField]
        Item[] items = new Item[0];

        void Start()
        {
            Refresh(LocalizationManager.Instance.Language);
        }

        void OnEnable()
        {
            LocalizationManager.Instance.ChangeLanguage += OnChangeLanguage;
        }

        void OnDisable()
        {
            LocalizationManager.Instance.ChangeLanguage -= OnChangeLanguage;
        }

        public void Refresh(SystemLanguage language)
        {
            foreach (var cover in items)
            {
                if (cover.item != null)
                    cover.item.gameObject.SetActive(false);
            }

            var c = items.FirstOrDefault(o => o.language == language);
            if (c != null && c.item != null)
                c.item.gameObject.SetActive(true);
        }

        void OnChangeLanguage(SystemLanguage language)
        {
            Refresh(language);
        }
    }
}