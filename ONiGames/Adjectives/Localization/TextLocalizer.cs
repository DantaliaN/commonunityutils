﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using ONiGames.Adjectives.Localization.Attributes;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace ONiGames.Adjectives.Localization
{
    [RequireComponent(typeof(Text))]
    public class TextLocalizer : MonoBehaviour
    {
        [LocalizationListPopupField]
        [SerializeField]
        string localizationKey = string.Empty;

        public int ParametersCount => LocalizationManager.Instance.TryLocalize(localizationKey, LocalizationManager.Instance.Language, out var text) ? text.ParametersCount() : 0;
        
        [CanBeNull]
        Text _text;
        Text Text => _text ??= GetComponent<Text>();

        [CanBeNull]
        ContentSizeFitter _csFitter;

        [CanBeNull]
        ContentSizeFitter CsFitter => _csFitter ??= GetComponent<ContentSizeFitter>();

        readonly List<object> textInsertions = new();

        [CanBeNull]
        public Localizator Localizator { private get; set; }

        void OnValidate()
        {
            name = $"Text: {localizationKey}";

#if !(UNITY_EDITOR && UNITY_IOS) //b/c something happens with resourse.load on ios here           
            Localize(LocalizationManager.Instance.Language);
#endif
        }

        void Start()
        {
            Localize(LocalizationManager.Instance.Language);
            SetDimensions();
        }

        void OnEnable()
        {
            Localize(LocalizationManager.Instance.Language);
            SetDimensions();

            LocalizationManager.Instance.ChangeLanguage += OnChangeLanguage;
        }

        void OnDisable()
        {
            LocalizationManager.Instance.ChangeLanguage -= OnChangeLanguage;
        }

        public void Localize(params object[] parameters)
        {
            textInsertions.Clear();
            textInsertions.AddRange(parameters);

            Localize(LocalizationManager.Instance.Language);
            SetDimensions();
        }

        public void ChangeKey(string newKey)
        {
            localizationKey = newKey;

            Localize(LocalizationManager.Instance.Language);
            SetDimensions();
        }

        public void ChangeKey(string newKey, params object[] parameters)
        {
            localizationKey = newKey;

            textInsertions.Clear();
            textInsertions.AddRange(parameters);

            Localize(LocalizationManager.Instance.Language);
            SetDimensions();
        }

        void OnChangeLanguage(SystemLanguage language)
        {
            Localize(language);
            SetDimensions();
        }

        void Localize(SystemLanguage language)
        {
            var localizator = Localizator ?? LocalizationManager.Instance.Localizator;
            if (localizator == null)
                return;
            
            if (!localizator.TryLocalize(localizationKey, language, out var text))
                Debug.LogWarning($"{gameObject.GetPath()}: Localization for {localizationKey} {language} not found");

            if (textInsertions.Count > 0)
            {
                try
                {
                    text = string.Format(text, textInsertions.ToArray());
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{gameObject.GetPath()}: Wrong localization params {text} {string.Join(",", textInsertions)} {ex}");
                }
            }

            Text.text = text;
        }

        void SetDimensions()
        {
            if (CsFitter == null || !gameObject.activeInHierarchy)
                return;

            CsFitter.SetLayoutHorizontal();
            CsFitter.SetLayoutVertical();
        }

        [Button]
        void PresetText()
        {
            Localize(LocalizationManager.Instance.Language);
            SetDimensions();
        }
    }
}