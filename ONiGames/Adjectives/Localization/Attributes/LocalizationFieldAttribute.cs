using System;
using Sirenix.OdinInspector;

namespace ONiGames.Adjectives.Localization.Attributes
{
    [IncludeMyAttributes]
    [SuffixLabel("@LocalizationAttributeHelper.Localize($value)")]
    [PropertyTooltip("@LocalizationAttributeHelper.Localize($value)")]
    [AttributeUsage(AttributeTargets.Field)]
    public class LocalizationFieldAttribute : Attribute { }
}