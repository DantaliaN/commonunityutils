using System;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace ONiGames.Adjectives.Localization.Attributes
{
    [IncludeMyAttributes]
#if UNITY_EDITOR
    [InfoBox("@TMProLocalizerParametersCountFieldAttribute.IncorrectParametersCountMessage($root, $property)", InfoMessageType.Warning, "@!TMProLocalizerParametersCountFieldAttribute.IsCorrectParametersCount($root, $property)")]
#endif
    [AttributeUsage(AttributeTargets.Field)]
    public class TMProLocalizerParametersCountFieldAttribute : Attribute
    {
        public int ParametersCount { get; }

        public TMProLocalizerParametersCountFieldAttribute(int parametersCount) => ParametersCount = parametersCount;
        
#if UNITY_EDITOR
        [UsedImplicitly]
        public static string IncorrectParametersCountMessage(object root, Sirenix.OdinInspector.Editor.InspectorProperty property)
        {
            var attribute = property.GetAttribute<TMProLocalizerParametersCountFieldAttribute>();
            if (attribute == null)
                return string.Empty;

            var text = property.ValueEntry.WeakSmartValue as TMProLocalizer;
            if (text == null)
                return string.Empty;

            return $"Incorrect parameter count: {text.ParametersCount} of {attribute.ParametersCount}";
        }

        [UsedImplicitly]
        public static bool IsCorrectParametersCount(object root, Sirenix.OdinInspector.Editor.InspectorProperty property)
        {
            var attribute = property.GetAttribute<TMProLocalizerParametersCountFieldAttribute>();
            if (attribute == null)
                return false;

            var text = property.ValueEntry.WeakSmartValue as TMProLocalizer;
            if (text == null)
                return false;

            return attribute.ParametersCount < 0 || text.ParametersCount == attribute.ParametersCount;
        }
#endif
    }
}