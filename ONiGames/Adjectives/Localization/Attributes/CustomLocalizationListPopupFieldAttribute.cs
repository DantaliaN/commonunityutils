using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Localization.Attributes
{
    [IncludeMyAttributes]
#if UNITY_EDITOR
    [ValueDropdown("@CustomLocalizationListPopupFieldAttribute.LocalizationList($root, $property)", DropdownWidth = 500)]
    [SuffixLabel("@CustomLocalizationListPopupFieldAttribute.Localize($root, $property, $value)")]
    [PropertyTooltip("@CustomLocalizationListPopupFieldAttribute.Localize($root, $property, $value)")]
#endif
    [AttributeUsage(AttributeTargets.Field)]
    public class CustomLocalizationListPopupFieldAttribute : Attribute
    {
        public string LocalizationDataFieldName { get; }

        public CustomLocalizationListPopupFieldAttribute(string localizationDataFieldName) => LocalizationDataFieldName = localizationDataFieldName;

#if UNITY_EDITOR
        [UsedImplicitly]
        public static IEnumerable<string> LocalizationList(object root, Sirenix.OdinInspector.Editor.InspectorProperty property)
        {
            if (!TryGetLocalizationData(root, property, out var data))
                return new string[0];

            return data.Keys;
        }

        [UsedImplicitly]
        public static string Localize(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string key) => Localize(root, property, key, SystemLanguage.Russian);

        [UsedImplicitly]
        public static string Localize(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string[] keys) => string.Empty;

        [UsedImplicitly]
        public static string Localize(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string key, SystemLanguage language)
        {
            if (!TryGetLocalizationData(root, property, out var data))
                return string.Empty;

            var localizator = new Localizator(data);

            return localizator.Localize(key, language);
        }

        static bool TryGetLocalizationData(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, out LocalizationData data)
        {
            var attribute = property.GetAttribute<CustomLocalizationListPopupFieldAttribute>();
            if (attribute == null)
            {
                data = null;
                return false;
            }

            var type = root.GetType();
            var fieldInfos = type.GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic);
            var fieldInfo = fieldInfos.FirstOrDefault(o => o.Name == attribute.LocalizationDataFieldName);
            if (fieldInfo == null)
            {
                data = null;
                return false;
            }

            data = fieldInfo.GetValue(root) as LocalizationData;
            return data != null;
        }
#endif
    }
}