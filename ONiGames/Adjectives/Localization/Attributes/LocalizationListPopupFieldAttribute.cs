using System;
using Sirenix.OdinInspector;

namespace ONiGames.Adjectives.Localization.Attributes
{
    [IncludeMyAttributes]
    [ValueDropdown("@LocalizationAttributeHelper.LocalizationList()", DropdownWidth = 500)]
    [SuffixLabel("@LocalizationAttributeHelper.Localize($value, 50)")]
    [PropertyTooltip("@LocalizationAttributeHelper.Localize($value)")]
    [AttributeUsage(AttributeTargets.Field)]
    public class LocalizationListPopupFieldAttribute : Attribute { }
}