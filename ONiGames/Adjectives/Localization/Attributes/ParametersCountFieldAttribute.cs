using System;
using JetBrains.Annotations;
using ONiGames.Utilities;
using Sirenix.OdinInspector;

namespace ONiGames.Adjectives.Localization.Attributes
{
    [IncludeMyAttributes]
#if UNITY_EDITOR
    [InfoBox("@ParametersCountFieldAttribute.IncorrectParametersCountMessage($root, $property, $value)", InfoMessageType.Warning, "@!ParametersCountFieldAttribute.IsCorrectParametersCount($root, $property, $value)")]
#endif
    [AttributeUsage(AttributeTargets.Field)]
    public class ParametersCountFieldAttribute : Attribute
    {
        public int ParametersCount { get; }

        public ParametersCountFieldAttribute(int parametersCount) => ParametersCount = parametersCount;
        
#if UNITY_EDITOR
        [UsedImplicitly]
        public static string IncorrectParametersCountMessage(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string key)
        {
            var attribute = property.GetAttribute<ParametersCountFieldAttribute>();
            if (attribute == null)
                return string.Empty;

            return $"Incorrect parameter count: {LocalizationAttributeHelper.Localize(key).ParametersCount()} of {attribute.ParametersCount}";
        }

        [UsedImplicitly]
        public static bool IsCorrectParametersCount(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string key)
        {
            var attribute = property.GetAttribute<ParametersCountFieldAttribute>();
            if (attribute == null)
                return false;

            return attribute.ParametersCount < 0 || LocalizationAttributeHelper.Localize(key).ParametersCount() == attribute.ParametersCount;
        }
#endif
    }
}