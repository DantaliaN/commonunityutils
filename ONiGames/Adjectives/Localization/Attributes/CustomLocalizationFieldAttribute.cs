using System;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ONiGames.Adjectives.Localization.Attributes
{
    [IncludeMyAttributes]
#if UNITY_EDITOR
    [SuffixLabel("@CustomLocalizationFieldAttribute.Localize($root, $property, $value, 50)")]
    [PropertyTooltip("@CustomLocalizationFieldAttribute.Localize($root, $property, $value)")]
#endif
    [AttributeUsage(AttributeTargets.Field)]
    public class CustomLocalizationFieldAttribute : Attribute
    {
        public string LocalizationDataFieldName { get; }

        public CustomLocalizationFieldAttribute(string localizationDataFieldName) => LocalizationDataFieldName = localizationDataFieldName;

#if UNITY_EDITOR
        [UsedImplicitly]
        public static string Localize(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string key) => Localize(root, property, key, SystemLanguage.Russian);

        [UsedImplicitly]
        public static string Localize(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string key, int width)
        {
            var str = Localize(root, property, key, SystemLanguage.Russian);
            var result = new System.Text.StringBuilder();
            for (var i = 0; i < str.Length; i += width)
            {
                if (i != 0)
                    result.Append("\n");
                result.Append(str.Clamp(i, width));
            }

            return result.ToString();
        }

        [UsedImplicitly]
        public static string Localize(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, string key, SystemLanguage language)
        {
            if (!TryGetLocalizationData(root, property, out var data))
                return string.Empty;

            var localizator = new Localizator(data);

            return localizator.Localize(key, language);
        }

        static bool TryGetLocalizationData(object root, Sirenix.OdinInspector.Editor.InspectorProperty property, out LocalizationData data)
        {
            var attribute = property.GetAttribute<CustomLocalizationFieldAttribute>();
            if (attribute == null)
            {
                data = null;
                return false;
            }

            var type = root.GetType();
            var fieldInfos = type.GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic);
            var fieldInfo = fieldInfos.FirstOrDefault(o => o.Name == attribute.LocalizationDataFieldName);
            if (fieldInfo == null)
            {
                data = null;
                return false;
            }

            data = fieldInfo.GetValue(root) as LocalizationData;
            return data != null;
        }
#endif
    }
}