using System.Collections.Generic;
using JetBrains.Annotations;
using ONiGames.Utilities;
using UnityEngine;

namespace ONiGames.Adjectives.Localization
{
    [UsedImplicitly]
    public static class LocalizationAttributeHelper
    {
        [UsedImplicitly]
        public static IEnumerable<string> LocalizationList() => LocalizationManager.Instance.Localizator?.Keys ?? new List<string>();

        [UsedImplicitly]
        public static string Localize(string key) => Localize(key, SystemLanguage.Russian);

        [UsedImplicitly]
        public static string Localize(string key, int width)
        {
            var str = Localize(key, SystemLanguage.Russian);
            var result = new System.Text.StringBuilder();
            for (var i = 0; i < str.Length; i += width)
            {
                if (i != 0)
                    result.Append("\n");
                result.Append(str.Clamp(i, width));
            }
            return result.ToString();
        }

        [UsedImplicitly]
        public static string Localize(string[] keys) => string.Empty;

        [UsedImplicitly]
        public static string Localize(string[] keys, int width) => string.Empty;

        [UsedImplicitly]
        public static string Localize(string key, SystemLanguage language) => LocalizationManager.Instance.Localize(key, language);
    }
}