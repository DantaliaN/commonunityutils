﻿using JetBrains.Annotations;

namespace ONiGames.Adjectives.Editor
{
    [UsedImplicitly]
    public class AddressableHelper
    {
#if UNITY_IOS
        [UsedImplicitly]
        public static string scheme = "https";
#elif UNITY_ANDROID
        [UsedImplicitly]
        public static string scheme = "http";
#else
        [UsedImplicitly]
        public static string scheme = "https";
#endif
        
        [UsedImplicitly]
        public static string branch = "local";
        
        [UsedImplicitly]
        public static string version = "local";
    }
}