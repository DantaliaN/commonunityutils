#if UNITY_EDITOR && UNITY_ANDROID

using System;
using System.IO;
using System.Linq;
using ONiGames.Tools.Editor;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace ONiGames.Adjectives.Editor
{
    public class AndroidSymbolsShrinkerPostProcessor : IPostprocessBuildWithReport
    {
        int IOrderedCallback.callbackOrder { get; } = 0;

        void IPostprocessBuildWithReport.OnPostprocessBuild(BuildReport report)
        {
            if (EditorUserBuildSettings.androidCreateSymbols == AndroidCreateSymbols.Disabled)
                return;

            var outputFilePath = report.GetFiles().FirstOrDefault(file => file.path.Substring(Math.Max(0, file.path.Length - 4)) == (EditorUserBuildSettings.buildAppBundle ? ".aab" : ".apk")).path;

            if (string.IsNullOrWhiteSpace(outputFilePath))
			{
                Debug.LogError($"Output file not found for {outputFilePath}!");
				return;
			}
            
            var outputFileNameWithoutExtension = Path.GetFileNameWithoutExtension(outputFilePath);
			var symbolFilePath = Path.Combine(Path.GetDirectoryName(outputFilePath) ?? string.Empty, $"{outputFileNameWithoutExtension}-{PlayerSettings.bundleVersion}-v{PlayerSettings.Android.bundleVersionCode}-{PlayerSettings.GetScriptingBackend(NamedBuildTarget.Android).ToString()}.symbols.zip");
			
            if (!File.Exists(symbolFilePath))
            {
                Debug.LogError($"Symbol file for {outputFilePath} not found with given path {symbolFilePath}!");
                return;
            }

            AndroidSymbolsShrinker.ShrinkSymbols(symbolFilePath);

            //File.Delete(symbolFilePath);
        }
    }
}

#endif