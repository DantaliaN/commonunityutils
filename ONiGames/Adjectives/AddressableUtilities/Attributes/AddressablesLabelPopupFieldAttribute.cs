using System;
using UnityEngine;

namespace ONiGames.Adjectives.AddressableUtilities.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AddressablesLabelPopupFieldAttribute : PropertyAttribute
    {
    }
}