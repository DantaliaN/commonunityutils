#if UNITY_EDITOR

using ONiGames.Utilities.Editor;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEngine;

namespace ONiGames.Adjectives.AddressableUtilities.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(AddressablesLabelPopupFieldAttribute))]
    public class AddressablesLabelPopupFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var labels = AddressableAssetSettingsDefaultObject.Settings.GetLabels().ToArray();
            property.stringValue = GUIUtilsEditor.Popup(position, label, property.stringValue, labels);
        }
    }
}

#endif