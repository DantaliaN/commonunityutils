using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using ONiGames.Adjectives.Managers;
using ONiGames.Adjectives.Utilities;
using ONiGames.Asserts;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace ONiGames.Adjectives.AddressableUtilities
{
    [RequireComponent(typeof(AddressableBundlesController))]
    public class AssetReferenceLoader<T> : MonoBehaviour where T : Component
    {
        [Serializable]
        public class BoneFollower
        {
            [SerializeField]
            [ValueDropdown(nameof(BoneNamesHelper), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
            string boneName = string.Empty;
            
            [ItemCanBeNull]
            [SerializeField]
            Transform[] targets = new Transform[0];
            
            public string BoneName => boneName;
            
            [ItemCanBeNull]
            public IEnumerable<Transform> Targets => targets;
            
            internal IEnumerable<string> BoneNamesHelper { private get; set; }
        }
        
        [ValidateInput(nameof(IsValidAssetReference), ContinuousValidationCheck = true)]
        [SerializeField]
        public AssetReference assetReference = new();

        [SerializeField]
        BoneFollower[] boneFollowers = new BoneFollower[0];
        
#if UNITY_EDITOR
        [InlineEditor(DrawHeader = false, DrawPreview = true, Expanded = true, PreviewHeight = 250f)]
        [ShowInInspector]
        GameObject AssetObjectPreview => assetReference.editorAsset as GameObject;
        
        [InlineEditor(DrawHeader = false, Expanded = true)]
        [ShowInInspector]
        GameObject AssetObjectButtons => assetReference.editorAsset as GameObject;
#endif
        
        public AssetReference AssetReference => assetReference;

        [CanBeNull]
        [NonSerialized]
        AddressableBundlesController _addressableBundles;
        AddressableBundlesController AddressableBundles => _addressableBundles ??= GetComponent<AddressableBundlesController>();

        void OnValidate()
        {
#if UNITY_EDITOR
            ONiGames.Utilities.Editor.UtilsEditor.SafeOnValidate(() =>
            {
                if (assetReference.editorAsset != null && assetReference.editorAsset is GameObject go)
                {
                    foreach (var boneFollower in boneFollowers)
                        boneFollower.BoneNamesHelper = go.transform.All().Select(o => o.name);
                } 
            });
#endif
        }

#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            if (Application.isPlaying)
                return;

            if (assetReference.editorAsset == null || !(assetReference.editorAsset is GameObject go))
                return;

            var t = transform;

            GizmosUtils.GizmosDrawMesh(go, t, true);
                
            var style = new GUIStyle
            {
                alignment = TextAnchor.MiddleCenter,
                normal = new GUIStyleState { textColor = Color.green }
            };

            var childs = go.transform.All().ToList();
            foreach (var boneFollower in boneFollowers)
            {
                var bone = childs.FirstOrDefault(o => o.name == boneFollower.BoneName);
                if (bone == null)
                    continue;

                foreach (var target in boneFollower.Targets)
                {
                    if (target == null)
                        continue;
                    
                    var tPos = t.TransformPoint(bone.position);
                    var tRot = t.rotation * bone.rotation;

                    target.position = tPos;
                    target.rotation = tRot;
                    
                    GizmosUtils.GizmosDrawArrow(tPos, tRot, Color.green);
                    UnityEditor.Handles.Label(tPos, $"{bone.name} - {target.name}", style);
                }
            }
        }
#endif

        void Awake()
        {
            Assert.SerializedFields(this);
        }

        public void Instantiate(Action<T> onComplete, Action<string> onError)
        {
            AddressableBundles.LoadGameObjectAsset<T>(assetReference, prefab =>
            {
                var instance = Instantiate(prefab, transform);

                var childs = instance.transform.All().ToList();
                foreach (var boneFollower in boneFollowers)
                {
                    var bone = childs.FirstOrDefault(o => o.name == boneFollower.BoneName);
                    if (bone == null)
                        continue;

                    foreach (var target in boneFollower.Targets)
                    {
                        if (target == null)
                            continue;

                        target.SetParent(bone);
                    }
                }

                onComplete(instance);
            }, onError);
        }
        
        bool IsValidAssetReference(AssetReference asset, ref string message) => AssetReferenceValidateHelper.IsValidAssetReference<T>(asset, ref message);
    }
}