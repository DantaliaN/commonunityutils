using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.U2D;

namespace ONiGames.Adjectives.AddressableUtilities
{
    public static class AssetReferenceValidateHelper
    {
        public static bool IsValidAssetReference<T>(AssetReference asset, ref string message)
        {
            if (asset == null)
                return true;
#if UNITY_EDITOR
            if (asset.editorAsset == null)
                return true;

            if (!(asset.editorAsset is GameObject go) || go.GetComponent<T>() == null)
            {
                message = $"Value must be {typeof(T)}";
                return false;
            }
#endif
            return true;
        }
        
        public static bool IsValidSceneReference(AssetReference asset, ref string message)
        {
            if (asset == null)
                return true;
#if UNITY_EDITOR
            if (asset.editorAsset == null)
                return true;

            if (!(asset.editorAsset is UnityEditor.SceneAsset))
                return false;
#endif
            return true;
        }

        public static bool IsValidAssetReferenceSpriteAtlas(AssetReference asset, ref string message)
        {
            if (asset == null)
                return true;
#if UNITY_EDITOR
            if (asset.editorAsset == null)
                return true;
            
            if (!(asset.editorAsset is SpriteAtlas sa) || sa.GetSprite(asset.SubObjectName) == null)
            {
                message = $"Value must be {typeof(SpriteAtlas)} and sprite is set";
                return false;
            }
#endif
            return true;
        }
    }
}