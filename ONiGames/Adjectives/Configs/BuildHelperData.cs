﻿#if UNITY_EDITOR

using System.Collections.Generic;
using ONiGames.Utilities;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Adjectives.Configs
{
    [CreateAssetMenu(menuName = "ONiGames/Adjectives/BuildHelperData")]
    public class BuildHelperData : ScriptableObject
    {
        const string SALT = "78c870c19cd9e0f416675550cfe3c8c2";

        [SerializeField]
        bool isDevelopmentBuild = false;

        [SerializeField]
        bool applyFixTextureCompression = true;

        [BoxGroup("Version")]
        [SerializeField]
        string version = string.Empty;

        [BoxGroup("Version")]
        [SerializeField]
        [Min(1)]
        int buildNumber = 1;

        [BoxGroup("Version")]
        [SerializeField]
        bool addBuildNumberToVersion = false;

        [BoxGroup("Version")]
        [ShowInInspector]
        public string Version => System.Version.TryParse(addBuildNumberToVersion ? $"{version}.{buildNumber}" : $"{version}", out var v) ? v.ToString() : string.Empty;

        [BoxGroup("Addressable Settings")]
        [SerializeField]
        bool buildAddressable = true;

        [BoxGroup("Addressable Settings")]
        [SerializeField]
        bool cleanAddressableBuild = true;

        [BoxGroup("Addressable Settings")]
        [SerializeField]
        [ValueDropdown(nameof(AddressableProfileItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        string addressableProfileId = string.Empty;

        [BoxGroup("Addressable Settings")]
        [SerializeField]
        [ValueDropdown(nameof(AddressableBuilderItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        int addressableBuilderIndex = 0;

        [BoxGroup("Addressable Settings")]
        [SerializeField]
        bool excludeGroupsInProductionOnly = true;

        [BoxGroup("Addressable Settings")]
        [SerializeField]
        [ValueDropdown(nameof(AddressableGroupItems), NumberOfItemsBeforeEnablingSearch = 5, SortDropdownItems = true)]
        string[] excludeGroups = new string[0];

        [BoxGroup("FTP Settings")]
        [SerializeField]
        bool sendAddressableToFtp = true;

        [BoxGroup("FTP Settings")]
        [SerializeField]
        bool useAsyncSending = true;

        [BoxGroup("FTP Settings")]
        [SerializeField]
        string url = string.Empty;

        [BoxGroup("FTP Settings")]
        [SerializeField]
        string userName = string.Empty;

        [BoxGroup("FTP Settings")]
        [SerializeField]
        string password = string.Empty;

        [BoxGroup("FTP Settings")]
        [ShowInInspector]
        public string Url => Utils.DecryptAes(url, SALT, string.Empty);

        [BoxGroup("FTP Settings")]
        [ShowInInspector]
        public string UserName => Utils.DecryptAes(userName, SALT, string.Empty);

        [BoxGroup("FTP Settings")]
        [ShowInInspector]
        public string Password => Utils.DecryptAes(password, SALT, string.Empty);

        [BoxGroup("Build Settings")]
        [SerializeField]
        bool buildGame = true;

        [BoxGroup("Build Settings")]
        [SerializeField]
        bool isAppBundle = true;

        [BoxGroup("Build Settings")]
        [SerializeField]
        bool splitApplicationBinary = false;

        [BoxGroup("Build Settings")]
        [SerializeField]
        bool createSymbolsZip = false;

        [BoxGroup("Build Settings")]
        [SerializeField]
        bool enableConsoleInProduction = false;

        [BoxGroup("Build Settings")]
        [SerializeField]
        bool useDefaultBuildOptions = false;

        [BoxGroup("Build Settings")]
        [SerializeField]
        ScriptingImplementation mode = ScriptingImplementation.IL2CPP;

        [BoxGroup("Build Settings")]
        [SerializeField]
        string keystorePassword = string.Empty;

        [BoxGroup("Build Settings")]
        [SerializeField]
        string keyaliasPassword = string.Empty;
        
        [BoxGroup("Build Settings")]
        [ShowInInspector]
        public string KeystorePassword => Utils.DecryptAes(keystorePassword, SALT, string.Empty);
        
        [BoxGroup("Build Settings")]
        [ShowInInspector]
        public string KeyaliasPassword => Utils.DecryptAes(keyaliasPassword, SALT, string.Empty);
        
        public bool IsDevelopmentBuild => isDevelopmentBuild;
        
        public bool ApplyFixTextureCompression => applyFixTextureCompression;
        
        public int BuildNumber => buildNumber;

        public bool AddBuildNumberToVersion => addBuildNumberToVersion;

        public bool BuildAddressable => buildAddressable;

        public bool CleanAddressableBuild => cleanAddressableBuild;

        public string AddressableProfileId => addressableProfileId;

        public int AddressableBuilderIndex => addressableBuilderIndex;

        public bool ExcludeGroupsInProductionOnly => excludeGroupsInProductionOnly;

        public IReadOnlyList<string> ExcludeGroups => excludeGroups;

        public bool SendAddressableToFtp => sendAddressableToFtp;

        public bool UseAsyncSending => useAsyncSending;

        public bool BuildGame => buildGame;

        public bool IsAppBundle => isAppBundle;

        public bool SplitApplicationBinary => splitApplicationBinary;

        public bool CreateSymbolsZip => createSymbolsZip;

        public bool EnableConsoleInProduction => enableConsoleInProduction;

        public bool UseDefaultBuildOptions => useDefaultBuildOptions;

        public ScriptingImplementation Mode => mode;
        
        [BoxGroup("FTP Settings")]
        [Button]
        void SaltFtp()
        {
            if (EditorUtility.DisplayDialog("Salt FTP", "Are you sure?", "Yes", "No"))
            {
                url = Utils.EncryptAes(url, SALT);
                userName = Utils.EncryptAes(userName, SALT);
                password = Utils.EncryptAes(password, SALT);
            }
        }

        [BoxGroup("Build Settings")]
        [Button]
        void SaltPasswords()
        {
            if (EditorUtility.DisplayDialog("Salt Passwords", "Are you sure?", "Yes", "No"))
            {
                keystorePassword = Utils.EncryptAes(keystorePassword, SALT);
                keyaliasPassword = Utils.EncryptAes(keyaliasPassword, SALT);
            }
        }

        [MenuItem("ONiGames/Adjectives/Editor/BuildHelperData", false)]
        static void ED_SelectConfig()
        {
            Selection.activeObject = Utilities.Editor.UtilsEditor.FindAssetInInspector<BuildHelperData>();
        }
        
        protected virtual IEnumerable<ValueDropdownItem<int>> AddressableBuilderItems() => new List<ValueDropdownItem<int>>();
        
        protected virtual IEnumerable<ValueDropdownItem<string>> AddressableProfileItems() => new List<ValueDropdownItem<string>>();
        
        protected virtual IEnumerable<ValueDropdownItem<string>> AddressableGroupItems() => new List<ValueDropdownItem<string>>();
    }
}

#endif