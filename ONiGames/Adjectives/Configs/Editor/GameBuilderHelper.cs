#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ONiGames.Adjectives.Editor;
using ONiGames.Utilities;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;
using UnityEditor.Build;
using UnityEditor.Build.Pipeline.Utilities;
using UnityEngine;

namespace ONiGames.Adjectives.Configs.Editor
{
    public static class GameBuilderHelper
    {
        public static void Build(BuildHelperData buildHelperData) 
        {
            EditorUserBuildSettings.development = buildHelperData.IsDevelopmentBuild;

            PlayerSettings.bundleVersion = buildHelperData.Version;
            PlayerSettings.Android.bundleVersionCode = buildHelperData.BuildNumber;
            PlayerSettings.iOS.buildNumber = $"{buildHelperData.BuildNumber}";

#if UNITY_IOS
            if (EditorUserBuildSettings.development)
            {
                var timestamp = System.Convert.ToInt32(System.TimeSpan.FromTicks(System.DateTime.UtcNow.Ticks - new System.DateTime(2022, 1, 1).Ticks).TotalSeconds);
                PlayerSettings.iOS.buildNumber = $"{PlayerSettings.iOS.buildNumber}.{timestamp}.{3}";
            }
#endif

            AddressableHelper.version = $"{PlayerSettings.bundleVersion}.{buildHelperData.BuildNumber}";

            if (buildHelperData.ApplyFixTextureCompression)
            {
                Utilities.Editor.UtilsEditor.FixTextureCompression();
            }

            if (buildHelperData.BuildAddressable)
            {
                var settings = AddressableAssetSettingsDefaultObject.Settings;
                if (settings == null)
                {
                    Debug.LogError($"{nameof(GameBuilderHelper)}: Can't build addressable");
                    return;
                }

                settings.activeProfileId = buildHelperData.AddressableProfileId;
                settings.ActivePlayerDataBuilderIndex = buildHelperData.AddressableBuilderIndex;

                if (buildHelperData.CleanAddressableBuild)
                {
                    AddressableAssetSettings.CleanPlayerContent();
                    BuildCache.PurgeCache(false);

                    var buildPath = settings.profileSettings.EvaluateString(settings.activeProfileId, settings.profileSettings.GetValueByName(settings.activeProfileId, AddressableAssetSettings.kRemoteBuildPath));

                    if (Directory.Exists(buildPath))
                        Directory.Delete(buildPath, true);
                }

                var buildGroupState = new List<Tuple<string, bool>>();

                foreach (var group in settings.groups)
                {
                    if (!buildHelperData.ExcludeGroups.Contains(group.Name))
                        continue;

                    var bundledAssetGroupSchema = group.Schemas.OfType<BundledAssetGroupSchema>().FirstOrDefault();
                    if (bundledAssetGroupSchema != null)
                    {
                        buildGroupState.Add(new Tuple<string, bool>(group.Name, bundledAssetGroupSchema.IncludeInBuild));

                        bundledAssetGroupSchema.IncludeInBuild = buildHelperData.IsDevelopmentBuild || !buildHelperData.ExcludeGroupsInProductionOnly;
                    }
                }

                AddressableAssetSettings.BuildPlayerContent(out var result);
                
                foreach (var group in settings.groups)
                {
                    var groupState = buildGroupState.FirstOrDefault(o => o.Item1 == group.Name);
                    if (groupState == null)
                        continue;

                    var bundledAssetGroupSchema = group.Schemas.OfType<BundledAssetGroupSchema>().FirstOrDefault();
                    if (bundledAssetGroupSchema != null)
                        bundledAssetGroupSchema.IncludeInBuild = groupState.Item2;
                }
                
                if (string.IsNullOrEmpty(result.Error))
                {
                    Debug.Log($"{nameof(GameBuilderHelper)}: Addressable build success:{result.OutputPath}");
                }
                else
                {
                    Debug.LogError($"{nameof(GameBuilderHelper)}: Addressable Build Error:{result.Error}");
                    return;
                }
            }

            if (buildHelperData.SendAddressableToFtp)
            {
                var settings = AddressableAssetSettingsDefaultObject.Settings;
                if (settings == null)
                {
                    Debug.LogError($"{nameof(GameBuilderHelper)}: Can't send addressable to cdn");
                    return;
                }

                settings.activeProfileId = buildHelperData.AddressableProfileId;

                var buildPath = settings.profileSettings.EvaluateString(settings.activeProfileId, settings.profileSettings.GetValueByName(settings.activeProfileId, AddressableAssetSettings.kRemoteBuildPath));

                var files = Directory.GetFiles(buildPath, "*.*", SearchOption.AllDirectories);

                if (buildHelperData.UseAsyncSending)
                {
                    Task.Run(() =>
                    {
                        Debug.Log($"{nameof(GameBuilderHelper)}: Start sending files to FTP. Wait for the complete message!");

                        for (var i = 0; i < files.Length; i++)
                        {
                            Debug.Log($"{nameof(GameBuilderHelper)}: Sending {i} of {files.Length}: {files[i]}");

                            var fileContents = File.ReadAllBytes(files[i]);

                            Utils.FtpSendFile(buildHelperData.Url, buildHelperData.UserName, buildHelperData.Password, files[i], fileContents, true, false);
                        }

                        Debug.Log($"{nameof(GameBuilderHelper)}: Sending files is complete!");
                    });
                }
                else
                {
                    for (var i = 0; i < files.Length; i++)
                    {
                        EditorUtility.DisplayProgressBar($"Processing Sending Addressable", files[i], (float) i / files.Length);

                        var fileContents = File.ReadAllBytes(files[i]);

                        Utils.FtpSendFile(buildHelperData.Url, buildHelperData.UserName, buildHelperData.Password, files[i], fileContents, true, false);
                    }

                    EditorUtility.ClearProgressBar();
                }
            }

            if (buildHelperData.BuildGame)
            {
                var consoleEnabled = SRDebugger.Settings.Instance.IsEnabled;
                var consoleTrigger = SRDebugger.Settings.Instance.EnableTrigger;

                SRDebugger.Settings.Instance.IsEnabled = buildHelperData.IsDevelopmentBuild || buildHelperData.EnableConsoleInProduction;
                SRDebugger.Settings.Instance.EnableTrigger = SRDebugger.Settings.Instance.IsEnabled ? SRDebugger.Settings.TriggerEnableModes.Enabled : SRDebugger.Settings.TriggerEnableModes.Off;

                EditorUtility.SetDirty(SRDebugger.Settings.Instance);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                EditorUserBuildSettings.buildAppBundle = buildHelperData.IsAppBundle;

#if UNITY_2021_3_OR_NEWER
                EditorUserBuildSettings.androidCreateSymbols = !buildHelperData.CreateSymbolsZip ? AndroidCreateSymbols.Disabled : buildHelperData.IsDevelopmentBuild ? AndroidCreateSymbols.Debugging : AndroidCreateSymbols.Public;
#else
                EditorUserBuildSettings.androidCreateSymbolsZip = buildHelperData.CreateSymbolsZip;
#endif

                PlayerSettings.Android.splitApplicationBinary = buildHelperData.SplitApplicationBinary;

                PlayerSettings.Android.keystorePass = buildHelperData.KeystorePassword;
                PlayerSettings.Android.keyaliasPass = buildHelperData.KeyaliasPassword;

#if UNITY_ANDROID
                PlayerSettings.SetScriptingBackend(NamedBuildTarget.Android, buildHelperData.Mode);
#elif UNITY_IOS
                PlayerSettings.SetScriptingBackend(NamedBuildTarget.iOS, buildHelperData.Mode);
#elif UNITY_WEBGL
                PlayerSettings.SetScriptingBackend(NamedBuildTarget.WebGL, buildHelperData.Mode);
#else
                PlayerSettings.SetScriptingBackend(NamedBuildTarget.Standalone, buildHelperData.Mode);
#endif

                var buildOptions = new BuildPlayerOptions();

                if (buildHelperData.UseDefaultBuildOptions)
                {
                    try
                    {
                        buildOptions = BuildPlayerWindow.DefaultBuildMethods.GetBuildPlayerOptions(buildOptions);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogWarning($"{nameof(GetType)}: {ex}");
                    }
                }
                else
                {
                    var buildPath = Path.Combine(Application.dataPath, "..", "Builds");

#if UNITY_ANDROID
                    buildPath = Path.Combine(buildPath, "Android");
#elif UNITY_IOS
                    buildPath = Path.Combine(buildPath, "IOS");
#elif UNITY_WEBGL
                    buildPath = Path.Combine(buildPath, "WebGL");
#else
                    buildPath = Path.Combine(buildPath, "Standalone");
#endif

                    var buildName = $"{Application.productName}".ReplaceRegex(@"[^0-9a-zA-Z]+", "_");
                    
                    buildName =$"{buildName}_{buildHelperData.Version}.{buildHelperData.BuildNumber}";

                    if (buildHelperData.IsDevelopmentBuild)
                        buildName = $"{buildName}_dev";

                    if (SRDebugger.Settings.Instance.IsEnabled)
                        buildName = $"{buildName}_c";

#if UNITY_ANDROID
                    buildName = buildHelperData.IsAppBundle ? $"{buildName}.aab" : $"{buildName}.apk";
#elif UNITY_IOS
#elif UNITY_WEBGL

#elif UNITY_STANDALONE_OSX
                    buildPath = Path.Combine(buildPath, buildName);
#elif UNITY_STANDALONE_LINUX
                    buildPath = Path.Combine(buildPath, buildName);
#elif UNITY_STANDALONE_WIN
                    buildPath = Path.Combine(buildPath, buildName);
                    
                    buildName = $"{buildName}.exe";
#else
#endif
                    
                    buildOptions.locationPathName = Path.Combine(buildPath, buildName);

                    buildOptions.scenes = EditorBuildSettings.scenes.Where(i => i.enabled).Select(o => o.path).ToArray();

                    buildOptions.options = buildHelperData.IsDevelopmentBuild ? BuildOptions.CompressWithLz4HC | BuildOptions.Development : BuildOptions.CompressWithLz4HC;

#if UNITY_ANDROID
                    buildOptions.target = BuildTarget.Android;
#elif UNITY_IOS
                    buildOptions.target = BuildTarget.iOS;
#elif UNITY_STANDALONE_OSX
                    buildOptions.target = BuildTarget.StandaloneOSX;
#elif UNITY_STANDALONE_LINUX
                    buildOptions.target = BuildTarget.StandaloneLinux64;
#elif UNITY_STANDALONE_WIN
                    buildOptions.target = BuildTarget.StandaloneWindows64;
#elif UNITY_WEBGL
                    buildOptions.target = BuildTarget.WebGL;
#else
                    buildOptions.target = BuildTarget.StandaloneWindows;
#endif

#if UNITY_ANDROID
                    buildOptions.targetGroup = BuildTargetGroup.Android;
#elif UNITY_IOS
                    buildOptions.targetGroup = BuildTargetGroup.iOS;
#elif UNITY_WEBGL
                    buildOptions.targetGroup = BuildTargetGroup.WebGL;
#else
                    buildOptions.targetGroup = BuildTargetGroup.Standalone;
#endif
                }

                try
                {
                    var report = BuildPipeline.BuildPlayer(buildOptions);

                    Debug.Log($"{nameof(GameBuilderHelper)}: Build time: {TimeSpan.FromSeconds(report.summary.totalTime.TotalSeconds)}");
                    Debug.Log($"{nameof(GameBuilderHelper)}: Build result: {report.summary.result}");

                    EditorUtility.RevealInFinder(report.summary.outputPath);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning($"{nameof(GetType)}: {ex}");
                }

                SRDebugger.Settings.Instance.IsEnabled = consoleEnabled;
                SRDebugger.Settings.Instance.EnableTrigger = consoleTrigger;

                EditorUtility.SetDirty(SRDebugger.Settings.Instance);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
    }
}

#endif