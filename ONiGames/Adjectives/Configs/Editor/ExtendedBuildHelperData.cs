#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Build;
using UnityEngine;

namespace ONiGames.Adjectives.Configs.Editor
{
    [CreateAssetMenu(menuName = "ONiGames/Adjectives/Editor/ExtendedBuildHelperData")]
    public class ExtendedBuildHelperData : BuildHelperData
    {
        protected override IEnumerable<ValueDropdownItem<int>> AddressableBuilderItems()
        {
            var items = new List<ValueDropdownItem<int>>();
            
            var settings = AddressableAssetSettingsDefaultObject.Settings;
            if (settings != null)
            {
                for (var i = 0; i < settings.DataBuilders.Count; i++)
                {
                    var m = settings.GetDataBuilder(i);
                    if (m.CanBuildData<AddressablesPlayerBuildResult>())
                    {
                        items.Add(new ValueDropdownItem<int>(m.Name, i));
                    }
                }
            }
            
            return items;
        }
        
        protected override IEnumerable<ValueDropdownItem<string>> AddressableProfileItems()
        {
            var items = new List<ValueDropdownItem<string>>();
            
            var settings = AddressableAssetSettingsDefaultObject.Settings;
            if (settings != null)
            {
                items.AddRange(settings.profileSettings.GetAllProfileNames().Select(o => new ValueDropdownItem<string>(o, settings.profileSettings.GetProfileId(o))));
            }
            
            return items;
        }
        
        protected override IEnumerable<ValueDropdownItem<string>> AddressableGroupItems()
        {
            var items = new List<ValueDropdownItem<string>>();
            
            var settings = AddressableAssetSettingsDefaultObject.Settings;
            if (settings != null)
            {
                items.AddRange(settings.groups.Select(o => new ValueDropdownItem<string>(o.Name, o.Name)));
            }
            
            return items;
        }
    }
}

#endif
