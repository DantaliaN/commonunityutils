#if UNITY_EDITOR

using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace ONiGames.Adjectives.Configs.Editor
{
    [CustomEditor(typeof(BuildHelperData), true)]
    public class BuildHelperDataEditor : OdinEditor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            
            GUILayout.Space(10f);
            
            var buildHelperData = target as BuildHelperData;
            if (buildHelperData == null)
                return;
            
            GUILayout.Space(10f);
            
            if (GUILayout.Button("Build"))
            {
                GameBuilderHelper.Build(buildHelperData);
            }
        }
    }
}

#endif