#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ONiGames.ReferenceViewer.Editor
{
    [InitializeOnLoad]
    public static class ReferenceStore
    {
        static IEnumerator mBuildingEnumerator;
        static GUIStyle mLabelStyle;
        static GUIStyle mNoRefsLabelStyle;
        static Texture2D mWhiteTex;
        static Texture2D mRedTex;

        static readonly Vector2 LargeOffset = new(4, 18);
        static readonly Vector2 SmallOffset = new(4, 0);

        static readonly List<string> IgnoredExtensions = new() { ".cs", ".js", ".xsd", ".xml", ".dll" };

        public static Dictionary<string, List<string>> DependsOn { get; } = new();
        public static Dictionary<string, List<string>> ReferencedBy { get; } = new();

        public static bool IsBuildInProgress => mBuildingEnumerator != null;

        public static int BuildListProgress { get; private set; }
        public static int BuildListTotal { get; private set; }

        static ReferenceStore()
        {
            mBuildingEnumerator = null;

            EnsureStyles();

            EditorApplication.update += Update;
            EditorApplication.projectWindowItemOnGUI += OnProjectItemGUI;
            //EditorApplication.projectWindowChanged += InvalidateReferenceData;
        }

        public static void EnsureReferenceData()
        {
            if (ReferencedBy.Count == 0 || DependsOn.Count == 0)
                BeginBuildReferencesMap();
        }

        public static void InvalidateReferenceData()
        {
            ReferencedBy.Clear();
            DependsOn.Clear();
        }

        static void Update()
        {
            if (mBuildingEnumerator == null)
                return;
            
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            while (sw.Elapsed.TotalMilliseconds < 1000.0f / 50.0f)
            {
                var moved = mBuildingEnumerator.MoveNext();
                if (!moved)
                {
                    mBuildingEnumerator = null;
                    // force repaint to ensure correct counters
                    EditorApplication.RepaintProjectWindow();
                    break;
                }
            }

            sw.Stop();
        }

        static void OnProjectItemGUI(string guid, Rect selectionRect)
        {
            //EnsureReferenceData();
            EnsureStyles();

            var assetPath = AssetDatabase.GUIDToAssetPath(guid);

            if (IsFolder(assetPath))
                return;

            if (!IgnoredExtensions.Contains(System.IO.Path.GetExtension(assetPath)))
            {
                if (IsBuildInProgress)
                {
                    //DrawRefCountBox("?", selectionRect, mLabelStyle);
                }
                else if (ReferencedBy.Count > 0)
                {
                    var refCount = 0;

                    if (ReferencedBy.ContainsKey(assetPath))
                        refCount = ReferencedBy[assetPath].Count;

                    var style = refCount != 0 ? mLabelStyle : mNoRefsLabelStyle;
                    DrawRefCountBox($"{refCount}", selectionRect, style);
                }
            }
        }

        static void DrawRefCountBox(string text, Rect canvasRect, GUIStyle style)
        {
            var content = new GUIContent(text);
            var desiredSize = style.CalcSize(content);

            var offset = canvasRect.height > EditorGUIUtility.singleLineHeight ? LargeOffset : SmallOffset;

            var destRect = new Rect(
                canvasRect.x + canvasRect.width - desiredSize.x - offset.x,
                canvasRect.y + canvasRect.height - desiredSize.y - offset.y,
                desiredSize.x,
                desiredSize.y);

            GUI.Box(destRect, content, style);
        }

        static bool IsFolder(string assetPath)
        {
            if (string.IsNullOrEmpty(assetPath))
                return true;

            return (int)(System.IO.File.GetAttributes(assetPath) & System.IO.FileAttributes.Directory) != 0;
        }

        static void BeginBuildReferencesMap()
        {
            if (!IsBuildInProgress)
            {
                DependsOn.Clear();
                ReferencedBy.Clear();

                mBuildingEnumerator = BuildingFunc(AssetDatabase.GetAllAssetPaths());
            }
        }

        static IEnumerator BuildingFunc(IReadOnlyList<string> allAssets)
        {
            BuildListTotal = allAssets.Count;
            for (var i = 0; i < allAssets.Count; ++i)
            {
                var deps = AssetDatabase.GetDependencies(allAssets[i], false);

                DependsOn[allAssets[i]] = deps.ToList();
                BuildListProgress = i;

                yield return null;
            }

            // this stage ends very fast, no need to display it at all
            foreach (var asset in DependsOn)
            {
                foreach (var value in asset.Value)
                {
                    if (!ReferencedBy.TryGetValue(value, out var refByList))
                    {
                        ReferencedBy[value] = refByList = new List<string>();
                    }

                    if (!refByList.Contains(asset.Key))
                        refByList.Add(asset.Key);

                    yield return null;
                }
            }
        }

        static void EnsureStyles()
        {
            if (mLabelStyle == null)
            {
                mLabelStyle = new GUIStyle
                {
                    fontSize = 10,
                    border = new RectOffset(2, 2, 2, 2),
                    padding = new RectOffset(5, 5, 3, 2)
                };

                mNoRefsLabelStyle = new GUIStyle(mLabelStyle);
            }

            if (mWhiteTex == null)
            {
                mWhiteTex = new Texture2D(4, 4, TextureFormat.RGB24, false);

                mWhiteTex.SetPixels32(new[]
                {
                    new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255), new Color32(220, 220, 220, 255), new Color32(220, 220, 220, 255),
                    new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255), new Color32(220, 220, 220, 255), new Color32(220, 220, 220, 255),
                    new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255)
                });

                mWhiteTex.Apply();

                mLabelStyle.normal.background = mWhiteTex;
            }

            if (mRedTex == null)
            {
                mRedTex = new Texture2D(4, 4, TextureFormat.RGB24, false);

                mRedTex.SetPixels32(new[]
                {
                    new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255), new Color32(240, 142, 142, 255), new Color32(240, 142, 142, 255),
                    new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255), new Color32(240, 142, 142, 255), new Color32(240, 142, 142, 255),
                    new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255), new Color32(0, 0, 0, 255),
                    new Color32(0, 0, 0, 255)
                });

                mRedTex.Apply();

                mNoRefsLabelStyle.normal.background = mRedTex;
            }
        }
    }
}

#endif