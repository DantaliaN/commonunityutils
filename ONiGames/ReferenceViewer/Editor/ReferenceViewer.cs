﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ONiGames.ReferenceViewer.Editor
{
    public class ReferenceViewer : EditorWindow
    {
        enum SortingType
        {
            Name,
            Type,
            Control,
            Max
        };

        enum SortingDirection
        {
            Ascending,
            Descending
        };

        Object mSelectedObject;
        bool mWasBuilding;
        SortingType mSortingType;
        SortingDirection mSortingDirection;
        GUIStyle mSortingButtonStyle;
        GUIStyle mRefCounterLabelStyle;

        Vector2 mLeftScrollPosition;
        Vector2 mRightScrollPosition;

        string[][] mReferenceBySorting;
        string[][] mDependsOnSorting;

        public Object SelectedObject
        {
            get => mSelectedObject;
            set
            {
                if (mSelectedObject != value)
                {
                    mSelectedObject = value;
                    RebuildSortingArray(ref mReferenceBySorting, ReferenceStore.ReferencedBy);
                    RebuildSortingArray(ref mDependsOnSorting, ReferenceStore.DependsOn);
                }
            }
        }

        [MenuItem("ONiGames/Reference Viewer/Open")]
        static void InitFromMainMenu()
        {
            ReferenceStore.InvalidateReferenceData();
            ReferenceStore.EnsureReferenceData();

            var window = GetWindow<ReferenceViewer>();
            window.Show();
        }

        [MenuItem("ONiGames/Reference Viewer/View Selected", priority = 39)]
        static void InitFromAssetBrowser()
        {
            ReferenceStore.EnsureReferenceData();

            var window = GetWindow<ReferenceViewer>();
            window.SelectedObject = Selection.activeObject;
            window.Show();
        }

        ReferenceViewer()
        {
            titleContent = new GUIContent("Reference Viewer", "Used to display dependencies between assets");
            mWasBuilding = false;
        }

        void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            {
				if (Selection.activeObject != null && Selection.activeObject != SelectedObject)
					SelectedObject = Selection.activeObject;
				
                SelectedObject = EditorGUILayout.ObjectField("Selected Object:", SelectedObject, typeof(Object), false);

                DrawSortingBlock();

                if (SelectedObject != null)
                {
                    // when unity reloads something these are cleared
                    if (mReferenceBySorting == null)
                        RebuildSortingArray(ref mReferenceBySorting, ReferenceStore.ReferencedBy);

                    if (mDependsOnSorting == null)
                        RebuildSortingArray(ref mDependsOnSorting, ReferenceStore.DependsOn);

                    GUILayout.Space(10);

                    if (!ReferenceStore.IsBuildInProgress)
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.BeginVertical();
                            {
                                GUILayout.Label("Used by Assets:");
                                GUILayout.Space(10);

                                if (mReferenceBySorting != null)
                                {
                                    mLeftScrollPosition = EditorGUILayout.BeginScrollView(mLeftScrollPosition);
                                    {
                                        DrawSortingArray(mReferenceBySorting);
                                    }
                                    EditorGUILayout.EndScrollView();
                                }
                            }
                            EditorGUILayout.EndVertical();

                            EditorGUILayout.BeginVertical();
                            {
                                GUILayout.Label("Depends on Assets:");
                                GUILayout.Space(10);

                                if (mDependsOnSorting != null)
                                {
                                    mRightScrollPosition = EditorGUILayout.BeginScrollView(mRightScrollPosition);
                                    {
                                        DrawSortingArray(mDependsOnSorting);
                                    }
                                    EditorGUILayout.EndScrollView();
                                }
                            }
                            EditorGUILayout.EndVertical();
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
            EditorGUILayout.EndVertical();
        }

        void Update()
        {
            if (ReferenceStore.IsBuildInProgress)
            {
                EditorUtility.DisplayProgressBar("Reference Viewer",
                    $"Building Lookup Tables... ({ReferenceStore.BuildListProgress}/{ReferenceStore.BuildListTotal})",
                    (float) ReferenceStore.BuildListProgress / ReferenceStore.BuildListTotal);

                mWasBuilding = true;
            }
            else if (mWasBuilding)
            {
                EditorUtility.ClearProgressBar();
                mWasBuilding = false;
            }
        }

        void OnDestroy()
        {
            if (ReferenceStore.IsBuildInProgress)
                EditorUtility.ClearProgressBar();
        }

        void DrawSortingBlock()
        {
            if (mSortingButtonStyle == null)
            {
                mSortingButtonStyle = new GUIStyle(GUI.skin.button);
                mSortingButtonStyle.alignment = TextAnchor.MiddleLeft;
            }

            EditorGUILayout.BeginHorizontal();
            {
                var nameButtonText = (mSortingType == SortingType.Name)
                    ? mSortingDirection == SortingDirection.Ascending ? "▼" : "▲"
                    : " ";
                var typeButtonText = (mSortingType == SortingType.Type)
                    ? mSortingDirection == SortingDirection.Ascending ? "▼" : "▲"
                    : " ";
                var ctrlButtonText = (mSortingType == SortingType.Control)
                    ? mSortingDirection == SortingDirection.Ascending ? "▼" : "▲"
                    : " ";

                if (GUILayout.Button($"Name {nameButtonText}", mSortingButtonStyle, GUILayout.Width(60)))
                {
                    ChangeSorting(SortingType.Name);
                }

                if (GUILayout.Button($"Type {typeButtonText}", mSortingButtonStyle, GUILayout.Width(60)))
                {
                    ChangeSorting(SortingType.Type);
                }

                if (GUILayout.Button($"Control {ctrlButtonText}", mSortingButtonStyle, GUILayout.Width(70)))
                {
                    ChangeSorting(SortingType.Control);
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        void DrawSortingArray(string[][] sortingArray)
        {
            var array = sortingArray[(int) mSortingType];

            if (mSortingDirection == SortingDirection.Ascending)
            {
                for (int i = 0; i < array.Length; ++i)
                    DrawReferenceBlock(array[i]);
            }
            else
            {
                for (int i = array.Length - 1; i >= 0; --i)
                    DrawReferenceBlock(array[i]);
            }
        }

        void DrawReferenceBlock(string assetPath)
        {
            if (mRefCounterLabelStyle == null)
            {
                mRefCounterLabelStyle = new GUIStyle(GUI.skin.label);
                mRefCounterLabelStyle.alignment = TextAnchor.MiddleRight;
            }

            var asset = AssetDatabase.LoadAssetAtPath<Object>(assetPath);

            EditorGUILayout.BeginHorizontal();
            {
                var refCount = 0;

                if (ReferenceStore.ReferencedBy.ContainsKey(assetPath))
                    refCount = ReferenceStore.ReferencedBy[assetPath].Count;

                GUILayout.Label($"{refCount}", mRefCounterLabelStyle, GUILayout.Width(25));

                EditorGUILayout.ObjectField(asset, typeof(Object), false, GUILayout.Width(250));

                if (GUILayout.Button("View", GUILayout.Width(40)))
                {
                    SelectedObject = asset;
                }

                var controlName = GetControlFromPath(assetPath);
                GUILayout.Label(controlName, GUILayout.Width(160));
            }
            EditorGUILayout.EndHorizontal();
        }

        void ChangeSorting(SortingType targetType)
        {
            if (mSortingType == targetType)
            {
                mSortingDirection = mSortingDirection == SortingDirection.Ascending
                    ? SortingDirection.Descending
                    : SortingDirection.Ascending;
            }
            else
            {
                mSortingType = targetType;
                mSortingDirection = SortingDirection.Ascending;
            }
        }

        string GetControlFromPath(string assetPath)
        {
            var cidx = assetPath.IndexOf("Control");

            if (cidx != -1)
            {
                var sidx = assetPath.LastIndexOfAny(new[] { '\\', '/' }, cidx);

                return assetPath.Substring(sidx + 1, cidx - sidx - 1);
            }

            return string.Empty;
        }

        void RebuildSortingArray(ref string[][] targetArray, Dictionary<string, List<string>> sourceDictionary)
        {
            var assetPath = AssetDatabase.GetAssetPath(SelectedObject);

            List<string> refList = null;
            if (sourceDictionary.TryGetValue(assetPath, out refList) && refList.Count > 0)
            {
                targetArray = new string[(int) SortingType.Max][];

                targetArray[(int) SortingType.Name] = refList.OrderBy(s => System.IO.Path.GetFileName(s)).ToArray();
                targetArray[(int) SortingType.Type] = refList
                    .OrderBy(s => AssetDatabase.LoadAssetAtPath<Object>(s).GetType().Name).ToArray();
                targetArray[(int) SortingType.Control] = refList.OrderBy(s => GetControlFromPath(s)).ToArray();
            }
            else
            {
                targetArray = null;
            }
        }
    }
}

#endif